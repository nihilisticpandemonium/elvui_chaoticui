local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI); --Inport: Engine, Locales, ProfileDB, GlobalDB
local addon = ...;
local CI = CUI:GetModule('Installer');
local CCFG = CUI:NewModule('ChaoticUIConfig');
local EP = LibStub("LibElvUIPlugin-1.0")

function CCFG:GenerateModuleList()
	local list = "";
	for _, m in pairs(CUI:GetRegisteredModules()) do
		list = list .. m .. "\n";
	end
	return list;
end

local feature_name_map = {
	["elvui"] = "ElvUI",
	["elvui_sle"] = "ElvUI Shadow and Light",
	["addonskins"] = "AddOnSkins",
	["xct+"] = "XCT+",
	["xct+beta"] = "xCT+ Beta",
	["details"] = "Details",
	["!kalielstracker"] = "Kaliel's Tracker",
	["inflight"] = "InFlight",
	['castbaroverlay'] = 'ElvUI CastBarOverlay',
	['everysecondcounts'] = 'ElvUI EverySecondCounts',
	['locplus'] = 'ElvUI Location Plus',
	['visualauratimers'] = 'ElvUI VisualAuraTimers',
	['combatstate'] = 'ElvUI ActionbarCombatState',
	['extraactionbars'] = 'ElvUI ExtraActionbars',
	['dtcolors'] = 'ElvUI Datatext Colors',
	['paragonrep'] = 'ParagonReputation',
	['ozcooldowns'] = 'OzCooldowns',
	['ifilger'] = 'iFilger',
	['projectazilroka'] = 'ProjectAzilroka',
	['enhancedpetbattleui'] = 'EnhancedPetBattleUI',
}

function CCFG:GenerateFeatureList()
	local list = "";
	local features = {};
	for i = 1, #E.private.chaoticui.features do
		if (feature_name_map[E.private.chaoticui.features[i]]) then
			features[feature_name_map[E.private.chaoticui.features[i]]] = true;
		end
	end

	for feature, _ in CUI_PairsByKeys(features) do
		list = list .. feature .. "\n";
	end
	return list;
end

function CCFG:GenerateOptions()
	local name = "|cfff02020ChaoticUI|r"..(": |cff99ff33%s|r"):format(CUI.version).." (|cfffe7b2cElvUI|r"..format(": |cff99ff33%s|r",E.version);
	if (IsAddOnLoaded("ElvUI_SLE")) then
		name = name .. ", |cff9482c9Shadow & Light|r"..format(": |cff99ff33%s|r",ElvUI_SLE[1].version);
	end
	name = name .. ")";
	E.Options.args.ElvUI_Header.name = name;
	local ACD = LibStub("AceConfigDialog-3.0-ElvUI")

	local function CreateButton(number, text, ...)
		local path = {}
		local num = select("#", ...)
		for i = 1, num do
			local name = select(i, ...)
			tinsert(path, #(path)+1, name)
		end
		local config = {
			order = number,
			type = 'execute',
			name = text,
			func = function() ACD:SelectGroup("ElvUI", "ChaoticUI", unpack(path)) end,
		}
		return config
	end
	--Main options group
	E.Options.args.ChaoticUI = {
		type = "group",
		order = 77,
		type = "group",
		name = [[|TInterface\ICONS\inv_cloth_challengewarlock_d_01helm:12:12:0:0:64:64:4:60:4:60|t|cfff02020ChaoticUI|r]],
		args = {
			header = {
				order = 1,
				type = "header",
				name = "|cfff02020ChaoticUI|r"..format(": |cff99ff33%s|r", CUI.version),
			},
			logo = {
				type = 'description',
				name = [=[|cfff02020ChaoticUI|r is an extension for ElvUI. It adds:
- a lot of new and exclusive features.
- more customization options for existing ones.]=],
				order = 2,
				image = function() return 'Interface\\AddOns\\ElvUI_ChaoticUI\\media\\textures\\chaoticui_logo', 200, 100 end,
			},
			sep1 = {
				type = "description",
				name = " ",
				order = 3,
			},
			Install = {
				order = 4,
				type = 'execute',
				name = "Install",
				desc = "Run the installation process.",
				func = function() CI:Install(); E:ToggleConfig() end,
			},
			InstallerFont = {
				order = 5,
				type = "select", dialogControl = 'LSM30_Font',
				name = L["Installer Font"],
				values = AceGUIWidgetLSMlists.font,
				get = function(info) return E.db.chaoticui.installer.font end,
				set = function(info, value) E.db.chaoticui.installer.font = value end,
			},
			InstallerTexture = {
				order = 6,
				type = "select", dialogControl = 'LSM30_Statusbar',
				name = L["Installer Texture"],
				values = AceGUIWidgetLSMlists.statusbar,
				get = function(info) return E.db.chaoticui.installer.texture end,
				set = function(info, value) E.db.chaoticui.installer.texture = value end,
			},
			sep2 = {
				type = "description",
				order = 7,
				name = " ",
			},
			buttons = {
				order = 8,
				type = "group",
				guiInline = true,
				name = "Module Configuration Shortcuts",
				args = {},
			},
			modules = {
				order = 9,
				type = "group",
				name = "Modules",
				desc = "Module Configuration",
				childGroups = "select",
				args = {},
			},
			features = {
				order = 7000,
				type = "group",
				name = "Features Installed",
				args = {
					header = {
						order = 1,
						type = "header",
						name = "Features Installed",
					},
					description = {
						order = 2,
						type = "description",
						name = CCFG:GenerateFeatureList(),
					},
				}
			},
		}
	};
	local mods = {};

	for _, m in pairs(CUI:GetRegisteredModules()) do
		local mod = CUI:GetModule(m);
		if (mod ~= CCFG and mod.GenerateOptions) then
			mods[mod:GetName()] = mod;
		end
	end
	local order = 1;
	for _, mod in CUI_PairsByKeys(mods) do
		local options = mod:GenerateOptions();
		if (options) then
			local name = mod:GetName();
			E.Options.args.ChaoticUI.args.modules.args[name] = options;
			E.Options.args.ChaoticUI.args.modules.args[name].order = order;
			E.Options.args.ChaoticUI.args.buttons.args[name.."Button"] = CreateButton(order, E.Options.args.ChaoticUI.args.modules.args[name].name, "modules", name);
			order = order + 1;
		end
	end
end

function CCFG:Initialize()
	EP:RegisterPlugin("ElvUI_ChaoticUI", CCFG.GenerateOptions);
end

CUI:RegisterModule(CCFG:GetName());