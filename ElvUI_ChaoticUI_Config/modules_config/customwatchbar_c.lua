local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local CWB = CUI:GetModule('CustomWatchBar')

function CWB:GenerateOptions()
	E.Options.args.databars.args.experience.args.textFormat = nil
	E.Options.args.databars.args.experience.args.tag = {
		type = 'input',
		width = 'full',
		name = L['Text Format'],
		desc = L['TEXT_FORMAT_DESC'],
		order = 4,
		get = function(info) return E.db.databars.experience.tag end,
		set = function(info,value) E.db.databars.experience.tag = value; CWB:UpdateTag('xp') end,
	}

	E.Options.args.databars.args.reputation.args.textFormat = nil
	E.Options.args.databars.args.reputation.args.tag = {
		type = 'input',
		width = 'full',
		name = L['Text Format'],
		desc = L['TEXT_FORMAT_DESC'],
		order = 4,
		get = function(info) return E.db.databars.reputation.tag end,
		set = function(info,value) E.db.databars.reputation.tag = value; CWB:UpdateTag('rep') end,
	}

	E.Options.args.databars.args.azerite.args.textFormat = nil
	E.Options.args.databars.args.azerite.args.tag = {
		type = 'input',
		width = 'full',
		name = L['Text Format'],
		desc = L['TEXT_FORMAT_DESC'],
		order = 4,
		get = function(info) return E.db.databars.azerite.tag end,
		set = function(info,value) E.db.databars.azerite.tag = value; CWB:UpdateTag('azerite') end,
	}

	E.Options.args.databars.args.honor.args.textFormat = nil
	E.Options.args.databars.args.honor.args.tag = {
		type = 'input',
		width = 'full',
		name = L['Text Format'],
		desc = L['TEXT_FORMAT_DESC'],
		order = 4,
		get = function(info) return E.db.databars.honor.tag end,
		set = function(info,value) E.db.databars.honor.tag = value; CWB:UpdateTag('honor') end,
	}
end
