local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local BS = CUI:GetModule('ButtonStyle')
local LSM = LibStub("LibSharedMedia-3.0")


function BS:GenerateOptions()
	local options = {
		type = "group",
		name = L["ButtonStyle"],
		get = function(info) return E.db.chaoticui.buttonStyle[info[#info]] end,
		set = function(info,value) E.db.chaoticui.buttonStyle[info[#info]] = value; BS:UpdateButtons(); end,
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["ChaoticUI ButtonStyle by Whiro"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["ChaoticUI ButtonStyle provides a style setting for ElvUI buttons similar to Masque or ButtonFacade\n"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the button style."],
					},
					texture = {
						type = "select", dialogControl = 'LSM30_Statusbar',
						order = 4,
						name = L["Texture"],
						desc = L["The texture to use."],
						values = AceGUIWidgetLSMlists.statusbar,
					},
					alpha = {
						order = 12,
						type = 'range',
						name = L['Alpha'],
						isPercent = true,
						min = 0, max = 1, step = 0.01,
					},
				},
			},
		}
	}

	options.args.general.args.invertedShadows = {
		name = 'InvertedShadows',
		type = 'toggle',
		order = 13,
	}

	return options;
end
