local CUI, E, L, V, P ,G = unpack(ElvUI_ChaoticUI)
local CUB = CUI:GetModule('UtilityBars')

function CUB:GenerateOptions()
    local options = {
        type = "group",
        name = "Utility Bars",
        desc = "Various Utility Bars provided by ChaoticUI",
        childGroups = "select",
        args = {
            hideincombat = {
			    type = "toggle",
			    order = 1,
		        name = L["Hide in Combat"],
			    desc = L["Hide the utility bars in combat"],
			    get = function(info) return E.db.chaoticui.utilitybars[info[#info]] end,
			    set = function(info,value) E.db.chaoticui.utilitybars[info[#info]] = value; end,
            },
        },
    }

    local bars = {};

    for _, mod in pairs(CUB["RegisteredBars"]) do

        if (mod.GenerateUtilityBarOptions) then
            bars[mod:GetName()] = mod;
        end
    end

    local order = 1;
    for _, mod in CUI_PairsByKeys(bars) do
        local _options = mod:GenerateUtilityBarOptions();
        if (_options) then
            local name = mod:GetName();
            options.args[name] = _options;
            options.args[name].order = order;
            order = order + 1;
        end
    end

    return options
end