if not ElvUI then return end
local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local RCD = CUI:GetModule('RaidCDs')
local LSM = LibStub("LibSharedMedia-3.0")

function RCD:GenerateOptions()
	local options = {
		type = "group",
		name = L["Raid CDs"],
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["Raid CDs"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["ChaoticUI Raid CDs provides a list of raid cds separated by category.\n"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the raid cds."],
						get = function(info) return self.db[info[#info]] end,
						set = function(info,value) self.db[info[#info]] = value; RCD:UpdateEnableState(); end,
					},
					resetsettings = {
						type = "execute",
						order = 2,
						name = L["Reset Settings"],
						desc = L["Reset the settings of this addon to their defaults."],
						func = function() E:CopyTable(E.db.chaoticui.raidcds,P.chaoticui.raidcds); RCD:UpdateEnableState(); RCD:UpdateMedia(); RCD:GROUP_ROSTER_UPDATE() end
					},
				},
			},
			partyXpOptions = {
				order = 4,
				type = "group",
				name = L["Raid CD Options"],
				guiInline = true,
				get = function(info) return self.db[info[#info]] end,
				set = function(info, value) self.db[info[#info]] = value; end, 
				args = {
					font = {
						type = "select", dialogControl = 'LSM30_Font',
						order = 1,
						name = L["Default Font"],
						desc = L["The font that the text on the cd bars will use."],
						values = AceGUIWidgetLSMlists.font, 
						get = function(info) return self.db[ info[#info] ] end, 
						set = function(info, value) self.db[ info[#info] ] = value; RCD:UpdateMedia() end,
					},
					fontSize = {
						type = "range",
						order = 2,
						name = L["Font Size"],
						desc = L["Set the size of the Text Font"],
						min = 10, max = 18, step = 1, 
						get = function(info) return self.db[ info[#info] ] end, 
						set = function(info, value) self.db[ info[#info] ] = value; RCD:UpdateMedia() end,
					},
					texture = {
						type = "select", dialogControl = 'LSM30_Statusbar',
						order = 3,
						name = L["Primary Texture"],
						desc = L["The texture that will be used for the experience bars."],
						values = AceGUIWidgetLSMlists.statusbar,
						get = function(info) return self.db[ info[#info] ] end,
						set = function(info, value) self.db[ info[#info] ] = value; RCD:UpdateMedia() end,
					},
					width = {
						type = "range",
						order = 4,
						name = L["Width"],
						desc = L["Width of the raid cd bar"],
						min = 5, max = 800, step = 1,
						get = function(info) return self.db[ info[#info] ] end,
						set = function(info, value) self.db[ info[#info] ] = value; self:UpdateCDs() end,
					},
					height = {
						type = "range",
						order = 5,
						name = L["Height"],
						desc = L["Height of the raid cd bar"],
						min = 5, max = 800, step = 1, 
						get = function(info) return self.db[ info[#info] ] end,
						set = function(info, value) self.db[ info[#info] ] = value; self:UpdateCDs() end,
					},
				},
			},
			visibilityStates = {
				order = 5,
				type = "group",
				name = L["Visibility"],
				guiInline = true,
				get = function(info) return self.db[info[#info]] end,
				set = function(info, value) self.db[info[#info]] = value; self:GROUP_ROSTER_UPDATE() end,
				args = {
					solo = {
						type = "toggle",
						order = 1,
						name = L["Solo"],
						desc = L["Show when solo"],
					},
					inParty = {
						type = "toggle",
						order = 2,
						name = L["In Party"],
						desc = L["Show when only in a party"],
					},
					inRaid = {
						type = "toggle",
						order = 3,
						name = L["In Raid"],
						desc = L["Show when in a raid group"],
					},
					onlyInCombat = {
						type = "toggle",
						order = 4,
						name = L["Only in Combat"],
						desc = L["Show only when in combat"],
						set = function(info, value) self.db[info[#info]] = value; self:CheckCombatState() end,
					},
				},
			},
		}
	}

	return options;
end