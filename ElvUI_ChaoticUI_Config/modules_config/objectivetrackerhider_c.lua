local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local OTH = CUI:GetModule('ObjectiveTrackerHider')

function OTH:GenerateOptions()
	local options = {
		type = "group",
		name = L["ObjectiveTrackerHider"],
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["ChaoticUI objectivetrackerhider by Whiro"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["ChaoticUI objectivetrackerhider hides or collapses the objective tracker based on your configuration.\n"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,

				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the objective tracker hider."],
						get = function(info) return E.db.chaoticui.objectivetrackerhider[info [#info] ] end,
						set = function(info,value) E.db.chaoticui.objectivetrackerhider[info [#info] ] = value; OTH:Enable(); end,
					},
				},
			},
			objectivetrackerhiderOptions = {
				order = 4,
				type = "group",
				name = L["objective tracker Hider Options"],
				guiInline = true,
				get = function(info) return E.db.chaoticui.objectivetrackerhider[info [#info] ] end,
				set = function(info, value) E.db.chaoticui.objectivetrackerhider[info [#info] ] = value; end,
				args = {
					hidePvP = {
						type = "toggle",
						order = 5,
						name = L["Hide during PvP"],
						desc = L["Hide the objective tracker during PvP (i.e. Battlegrounds)"],
					},
					hideArena = {
						type = "toggle",
						order = 6,
						name = L["Hide in arena"],
						desc = L["Hide the objective tracker when in the arena"],
					},
					hideParty = {
						type = "toggle",
						order = 7,
						name = L["Hide in dungeon"],
						desc = L["Hide the objective tracker when in a dungeon"],
					},
					hideRaid = {
						type = "toggle",
						order = 8,
						name = L["Hide in raid"],
						desc = L["Hide the objective tracker when in a dungeon"],
					},
					collapsePvP = {
						type = "toggle",
						order = 1,
						name = L["Collapse during PvP"],
						desc = L["Collapse the objective tracker during PvP (i.e. Battlegrounds)"],
					},
					collapseArena = {
						type = "toggle",
						order = 2,
						name = L["Collapse in arena"],
						desc = L["Collapse the objective tracker when in the arena"],
					},
					collapseParty = {
						type = "toggle",
						order = 3,
						name = L["Collapse in dungeon"],
						desc = L["Collapse the objective tracker when in a dungeon"],
					},
					collapseRaid = {
						type = "toggle",
						order = 4,
						name = L["Collapse in raid"],
						desc = L["Collapse the objective tracker during a raid"],
					},
				}
			},
		},
	}

	return options;
end