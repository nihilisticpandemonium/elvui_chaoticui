local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local CC = CUI:GetModule('ChaoticChat')
local LSM = LibStub("LibSharedMedia-3.0")

function CC:UpdateAll()
	local width = self.db.windows.width;
	local height = self.db.windows.height;
	local fontsize = self.db.windows.fontsize;
	local bcolor = self.db.general.backdropcolor
	local rcolor = self.db.general.bordercolor

	self.dock.backdrop:SetBackdropColor(bcolor.r, bcolor.g, bcolor.b, self.db.general.alpha)
	self.dock.backdrop:SetBackdropBorderColor(rcolor.r, rcolor.g, rcolor.b, self.db.general.alpha)

	for i, v in pairs(self.chats) do
		v:Size(width, height + 46);
		v.backdrop:SetBackdropColor(bcolor.r, bcolor.g, bcolor.b, self.db.general.alpha)
		v.backdrop:SetBackdropBorderColor(rcolor.r, rcolor.g, rcolor.b, self.db.general.alpha)
		v.Top:Size(width, 23);
		v.Top.backdrop:SetBackdropColor(bcolor.r, bcolor.g, bcolor.b, self.db.general.alpha)
		v.Top.backdrop:SetBackdropBorderColor(rcolor.r, rcolor.g, rcolor.b, self.db.general.alpha)
		v.Bottom:Size(width, 23);
		v.Bottom.backdrop:SetBackdropColor(bcolor.r, bcolor.g, bcolor.b, self.db.general.alpha)
		v.Bottom.backdrop:SetBackdropBorderColor(rcolor.r, rcolor.g, rcolor.b, self.db.general.alpha)
		v.EditBox:SetWidth(v:GetWidth())
		v.Text:SetFont(LSM:Fetch("font", self.db.windows.font), fontsize)
		if self.db.windows.showtitle then
			v.Name:SetAlpha(1)
		else
			v.Name:SetAlpha(0)
		end
	end
end

CC.MinimizeAll = true

function CC:MinimizeAllChats()
	local ACR = LibStub("AceConfigRegistry-3.0")
	for i, v in ipairs(self.chats) do
		if self.MinimizeAll then
			if not v.minimized then
				self:SetMinimized(v)
			end
		else
			self:SetMaximized(v)
		end
	end
	if self.MinimizeAll then
		self.MinimizeAll = false
		E.Options.args.ChaoticUI.args.modules.args.ChaoticChat.args.general.args.minimizeall.name = L["Maximize"]
	else
		self.MinimizeAll = true
		E.Options.args.ChaoticUI.args.modules.args.ChaoticChat.args.general.args.minimizeall.name = L["Minimize"]
	end
	ACR:NotifyChange("ElvUI")
end

function CC:GenerateOptions()
	local choices = {
		['12Hour'] = "12 Hour",
		['24Hour'] = "24 Hour",
	}
	local options = {
		type = "group",
		name = L["ChaoticChat"],
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["ChaoticChat by Whiro/Hydrazine (tukui.org)"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["ChaoticChat makes your chat experience awesome"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,
				get = function(info) return E.db.chaoticui.chaoticchat.general[ info [#info]] end,
				set = function(info,value) E.db.chaoticui.chaoticchat.general[ info [#info]] = value; CC:UpdateAll(); end, 
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable ChaoticChat."],
						set = function(info,value) E.db.chaoticui.chaoticchat.general[info[#info]] = value; E:StaticPopup_Show('CONFIG_RL'); end,
					},
					resetsettings = {
						type = "execute",
						order = 2,
						name = L["Reset Settings"],
						desc = L["Reset the settings of this addon to their defaults."],
						func = function() local old = E.db.chaoticui.chaoticchat.general.enabled; E:CopyTable(E.db.chaoticui.chaoticchat,P.chaoticui.ChaoticChat); if (old ~= E.db.chaoticui.chaoticchat.general.enabled) then E:StaticPopup_Show('CONFIG_RL') else CC:UpdateAll() end ; end
					},
					 minimizeall = {
						type = "execute",
						name = L["Minimize"],
						order = 3,
						func = function() CC:MinimizeAllChats() end,
					},
					bordercolor = {
						type = "color",
						order = 4,
						name = L["Border Color"],
						get = function(info)
							local t = E.db.chaoticui.chaoticchat.general.bordercolor
							return t.r, t.g, t.b, t.a
							end,
						set = function(info, r, g, b)
							local t = E.db.chaoticui.chaoticchat.general.bordercolor
							t.r, t.g, t.b = r, g, b
							CC:UpdateAll()
						end,
					},
					Backdropcolor = {
						type = "color",
						order = 5,
						name = L["Backdrop Color"];
						hasAlpha = true,
						get = function(info)
							local t = E.db.chaoticui.chaoticchat.general.backdropcolor
							return t.r, t.g, t.b, t.a
							end,
						set = function(info, r, g, b)
							local t = E.db.chaoticui.chaoticchat.general.backdropcolor
							t.r, t.g, t.b = r, g, b
							CC:UpdateAll()
						end,
					},
					alpha = {
						order = 6,
						name = L['Alpha'],
						type = 'range',
						min = 0, max = 1, step = .1,
					}
				},
			},
			windows = {
				order = 4,
				type = "group",
				name = L["Options"],
				guiInline = true,
				get = function(info) return E.db.chaoticui.chaoticchat.windows[ info [#info]] end,
				set = function(info,value) E.db.chaoticui.chaoticchat.windows[ info [#info]] = value; CC:UpdateAll(); end, 
				args = {
					autofade = {
						type = "toggle",
						order = 4,
						name = L["AutoFade"],
					},
					autohide = {
						type = "toggle",
						order = 5,
						name = L["AutoHide"],
					},
					showtitle = {
						type = "toggle",
						order = 6,
						name = L["ShowTitle"],
					},
					timestamp = {
						type = "toggle",
						name = L["Timestamps"],
						order = 8,
					},
					localtime = {
						order = 9,
						name = L["Local Time"],
						type = 'toggle',
					},
					width = {
						order = 10,
						name = L['Width'],
						type = 'range',
						min = 100, max = 500, step = 1,
					},
					height = {
						order = 11,
						name = L['Height'],
						type = 'range',
						min = 50, max = 120, step = 1,
					},
					fontsize = {
						order = 12,
						name = L["FontSize"],
						type = 'range',
						min = 9, max = 16, step = 1,
					},
					font = {
						type = "select", dialogControl = 'LSM30_Font',
						order = 13,
						name = L["Font"],
						values = AceGUIWidgetLSMlists.font,
					},
					timeformat = {
						order = 14,
						name = L["TimeFormat"],
						type = 'select',
						values = choices,
					},
				},
			},
  		}
	}

	return options;
end
