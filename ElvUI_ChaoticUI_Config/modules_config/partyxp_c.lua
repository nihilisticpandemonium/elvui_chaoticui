local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local PXP = CUI:GetModule('PartyXP')
local LSM = LibStub("LibSharedMedia-3.0")

function PXP:GenerateOptions()
	local options = {
		type = "group",
		name = L["PartyXP"],
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["ChaoticUI PartyXP by Whiro"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["ChaoticUI PartyXP provides a configurable set of party experience bars for use with ElvUI.\n"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the party experience bars."],
						get = function(info) return E.db.chaoticui.pxp[info[#info]] end,
						set = function(info,value) E.db.chaoticui.pxp[info[#info]] = value; PXP:Enable(); end,
					},
					resetsettings = {
						type = "execute",
						order = 2,
						name = L["Reset Settings"],
						desc = L["Reset the settings of this addon to their defaults."],
						func = function() E:CopyTable(E.db.chaoticui.pxp,P.chaoticui.pxp); PXP:Enable(); PXP:Update(); PXP:UpdateMedia(); PXP:UpdateColorSetting() end
					},
				},
			},
			partyXpOptions = {
				order = 4,
				type = "group",
				name = L["PartyXP Options"],
				guiInline = true,
				get = function(info) return E.db.chaoticui.pxp[info[#info]] end,
				set = function(info, value) E.db.chaoticui.pxp[info[#info]] = value; end, 
				args = {
					font = {
						type = "select", dialogControl = 'LSM30_Font',
						order = 1,
						name = L["Default Font"],
						desc = L["The font that the text on the experience bars will use."],
						values = AceGUIWidgetLSMlists.font, 
						get = function(info) return E.db.chaoticui.pxp[ info[#info] ] end, 
						set = function(info, value) E.db.chaoticui.pxp[ info[#info] ] = value; PXP:UpdateMedia() end,
					},
					fontsize = {
						type = "range",
						order = 2,
						name = L["Font Size"],
						desc = L["Set the Width of the Text Font"],
						min = 10, max = 18, step = 1, 
						get = function(info) return E.db.chaoticui.pxp[ info[#info] ] end, 
						set = function(info, value) E.db.chaoticui.pxp[ info[#info] ] = value; PXP:UpdateMedia() end,
					},
					detailedText = {
						type = "toggle",
						order = 3,
						name = L["Detailed Text"],
						desc = L["Use detailed text in the experience bars"],
						get = function(info) return E.db.chaoticui.pxp[info[#info]] end,
						set = function(info, value) E.db.chaoticui.pxp[info[#info]] = value; for i = 1,4 do PXP:UpdateBar(i) end end,
					},
					texture = {
						type = "select", dialogControl = 'LSM30_Statusbar',
						order = 4,
						name = L["Primary Texture"],
						desc = L["The texture that will be used for the experience bars."],
						values = AceGUIWidgetLSMlists.statusbar,
						get = function(info) return E.db.chaoticui.pxp[ info[#info] ] end,
						set = function(info, value) E.db.chaoticui.pxp[ info[#info] ] = value; PXP:UpdateMedia() end,
					},
					classColor = {
						type = "toggle",
						order = 5,
						name = L["Class Colors"],
						desc = L["Use class colors for the experience bars"],
						get = function(info) return E.db.chaoticui.pxp[info[#info]] end,
						set = function(info, value) E.db.chaoticui.pxp[info[#info]] = value; PXP:UpdateColorSetting(); end, 
					},
					width = {
						type = "range",
						order = 7,
						name = L["Width"],
						desc = L["Vertical offset from parent frame"],
						min = 5, max = 800, step = 1,
						get = function(info) return E.db.chaoticui.pxp[ info[#info] ] end,
						set = function(info, value) E.db.chaoticui.pxp[ info[#info] ] = value; PXP:UpdateMedia() for i = 1,4 do PXP:UpdateBar(i) end end,
					},
					height = {
						type = "range",
						order = 8,
						name = L["Height"],
						desc = L["Set the Width of the Text Font"],
						min = 5, max = 800, step = 1, 
						get = function(info) return E.db.chaoticui.pxp[ info[#info] ] end,
						set = function(info, value) E.db.chaoticui.pxp[ info[#info] ] = value; PXP:UpdateMedia() for i = 1,4 do PXP:UpdateBar(i) end end,
					},
				},
			},
			partyXpVariables = {
				order = 5,
				type = "group",
				name = L["Variables and Movers"],
				guiInline = true,
				get = function(info) return E.db.chaoticui.pxp[info[#info]] end,
				set = function(info, value) E.db.chaoticui.pxp[info[#info]] = value; PXP:UpdateMedia() end,
				args = {
					offset = {
						type = "range",
						order = 1,
						name = L["Offset"],
						desc = L["Vertical offset from parent frame"],
						min = -20, max = 20, step = 1,
					},
				},
			},
		}
	}

	return options;
end