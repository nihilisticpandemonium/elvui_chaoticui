local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI);
local TL = CUI:GetModule('TalentLoadouts');

local selectedTalentLoadout;

function deepcopy(object)
	local lookup_table = {}
	local function _copy(object)
		if type(object) ~= "table" then
			return object
		elseif lookup_table[object] then
			return lookup_table[object]
		end
		local new_table = {}
		lookup_table[object] = new_table
		for index, value in pairs(object) do
			new_table[_copy(index)] = _copy(value)
		end
			return setmetatable(new_table, getmetatable(object))
	end
	return _copy(object)
end
local formatStr = [[|T%s:12:12:0:0:64:64:4:60:4:60|t %s]]
local function GetTalentString(tier, column)
    local _, name, texture = GetTalentInfo(tier, column, 1);
    return formatStr:format(texture, name);
end

local cached_pvp_talents = {};

local function GetSlotNumPvPTalents(slot)
    if (not cached_pvp_talents[slot]) then
        local slotInfo = C_SpecializationInfo.GetPvpTalentSlotInfo(slot);
        local availableTalentIDs = slotInfo.availableTalentIDs;
   
        table.sort(availableTalentIDs, function(a, b)
            local unlockedA = select(7,GetPvpTalentInfoByID(a));
            local unlockedB = select(7,GetPvpTalentInfoByID(b));
        
            if (unlockedA ~= unlockedB) then
                return unlockedA;
            end
        
            if (not unlockedA) then
                local reqLevelA = C_SpecializationInfo.GetPvpTalentUnlockLevel(a);
                local reqLevelB = C_SpecializationInfo.GetPvpTalentUnlockLevel(b);
        
                if (reqLevelA ~= reqLevelB) then
                return reqLevelA < reqLevelB;
                end
            end
        
            return a < b;
        end);
        cached_pvp_talents[slot] = availableTalentIDs;
    end
    return #cached_pvp_talents[slot];
end

local function GetPvpTalentString(slot, index)
    local _, name, texture = GetPvpTalentInfoByID(cached_pvp_talents[slot][index]);
    return formatStr:format(texture, name);
end

local function GenerateValues(tier)
    local values = {};

    for i = 1, 3 do
        values[i] = GetTalentString(tier, i);
    end

    return values;
end

local function GeneratePvPValues(slot)
    local values = {};

    for i = 1, GetSlotNumPvPTalents(slot) do
        values[i] = GetPvpTalentString(slot, i);
    end

    return values;
end

local function UpdateTalentLoadout()
	if not selectedTalentLoadout then
		E.Options.args.ChaoticUI.args.modules.args.TalentLoadouts.args.talentLoadouts.args.talentLoadout = nil
		return
	end
    
    wipe(cached_pvp_talents);

	local options = {
		type = 'group',
		name = selectedTalentLoadout,
		guiInline = true,
		order = 4,
		args = {
        }
	}

    local maxTier = 7;
    local maxPvpSlot = 4;

    for i = 1, maxTier do
        options.args["tier"..i] = {
            type = 'select',
            order = i,
            style = 'dropdown',
            name = 'Tier '..i,
            get = function(info) return E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout][i] end,
		    set = function(info, value) E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout][i] = value; UpdateTalentLoadout() end,		
		
            values = function() return GenerateValues(i) end,
        }
    end

    options.args.pvp = {
        type = "group",
        name = "PvP Talents",
        enabled = C_PvP.IsWarModeDesired();
        order = 8;
        guiInline = true;
        args = {
        }
    };

    if (not E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout].pvp) then
        E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout].pvp = { 1, 1, 2, 3 };
    end

    for i = 1, maxPvpSlot do
        options.args.pvp.args["slot"..i] = {
            type = 'select',
            order = i,
            style = 'dropdown',
            name = i == 1 and "Trinket" or "PvP Talent Slot "..(i-1),
            get = function(info) return E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout].pvp[i] end,
            set = function(info, value) E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout].pvp[i] = value, UpdateTalentLoadout() end,
        
            values = function() return GeneratePvPValues(i) end,
        }
    end

    E.Options.args.ChaoticUI.args.modules.args.TalentLoadouts.args.talentLoadout = options;
end

function TL:GenerateOptions()
	local options = {};
	options = {
		type = "group",
		name = 'Talent Loadouts',
		args = {
			clearLoadout = {
				order = 1,
				type = 'execute',
				name = L['Clear Talent Loadouts'],
				desc = L['Deletes all saved talent loadouts'],
				func = function()
					E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())] = {};
					UpdateTalentLoadout();
				end
			},
            addLoadout = {
                type = "input",
                order = 2,
                name = L["Add Loadout"],
                desc = L["Input a loadout name"],
                get = function(info) return "" end,
                set = function(info, value) 
                    if not E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][value] then
                        E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][value] = {
                            [1] = 1,
                            [2] = 1,
                            [3] = 1,
                            [4] = 1,
                            [5] = 1,
                            [6] = 1,
                            [7] = 1,
                            pvp = {
                                [1] = 1,
                                [2] = 1,
                                [3] = 1,
                                [4] = 1,
                            }
                        };
                        selectedTalentLoadout = value;
                        UpdateTalentLoadout();
                    end
                end,	
            },
            loadoutList = {
                order = 3,
                type = 'select',
                name = L['Talent Loadout List'],
                get = function(info) return selectedTalentLoadout end,
                set = function(info, value) selectedTalentLoadout = value; UpdateTalentLoadout() end,							
                values = function()
                    loadouts = {}
                    for name in pairs(E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())]) do
                        loadouts[name] = name;
                    end
                    return loadouts;
                end,
            },
            removeLoadout = {
                order = 5,
                type = 'execute',
                name = L['Remove Loadout'],
                disabled = function() return not selectedTalentLoadout end,
                func = function()
                    if E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout] then
                        E.db.chaoticui.talentloadouts[GetSpecializationInfo(GetSpecialization())][selectedTalentLoadout] = nil
                        selectedTalentLoadout = nil
                        UpdateTalentLoadout();
                    end
                end
            },
        }
	}

	return options;
end