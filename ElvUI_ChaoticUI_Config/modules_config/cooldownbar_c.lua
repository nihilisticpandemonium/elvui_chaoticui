local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local CB = CUI:GetModule('CooldownBar');


function CB:GenerateOptions()
	local options = {
		type = "group",
		name = L["CooldownBar"],
		get = function(info) return E.db.chaoticui.cooldownBar[info[#info]] end,
		set = function(info,value) E.db.chaoticui.cooldownBar[info[#info]] = value; CB:UpdateSettings(); end,
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["ChaoticUI CooldownBar by Whiro"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["ChaoticUI CooldownBar provides a logarithmic cooldown display similar to SexyCooldown2\n"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the cooldown bar."],
					},
					
					alpha = {
						order = 12,
						type = 'range',
						name = L['Alpha'],
						isPercent = true,
						min = 0, max = 1, step = 0.01,
					},

					autohide = {
						order = 2,
						type = 'toggle',
						name = L["Autohide"],
						desc = L["Hide the cooldown bar when the mouse is not over it, you are not in combat, and there is nothing tracked on cooldown"],
					},
					switchTime = {
						order = 3,
						type = 'range',
						name = L['Switch Time'],
						min = 1, max = 5, step = 1,
					},
					resetblacklist = {
						type = "execute",
						order = 2,
						name = L["Reset Blacklist"],
						desc = L["Reset the blacklist."],
						func = function() wipe(E.db.chaoticui.cooldownBar.blacklist.spells); wipe(E.db.chaoticui.cooldownBar.blacklist.items); CB:UpdateCache(); CB:RefreshItemList(); end,
					},
				},
			},
			cooldownFlash = {
				order = 4,
				type = "group",
				name = "Cooldown Flash",
				guiInline = true,
				get = function(info) return E.db.chaoticui.cooldownBar.cooldownFlash[info[#info]] end,
				set = function(info, value) E.db.chaoticui.cooldownBar.cooldownFlash[info[#info]] = value; CB:UpdateFlashFrame(); end,
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the cooldown flash"],
					},
					resetsettings = {
						type = "execute",
						order = 2,
						name = L["Reset Settings"],
						desc = L["Reset the settings of this addon to their defaults."],
						func = function() E:CopyTable(E.db.chaoticui.cooldownBar.cooldownFlash, P.chaoticui.cooldownBar.cooldownFlash); CB:UpdateFlashFrame(); end,
					},
					size = {
						type = "range",
						order = 5,
						name = L["Size"],
						desc = L["Frame Size"],
						min = 12, max = 100, step = 1,	
					},
					length = {
						type = "range",
						order = 6,
						name = L["Length"],
						desc = L["Time the frame stays visible"],
						min = .1, max = 2.0, step = .1,
					},
					alpha = {
						type = "range",
						order = 7,
						name = L["Alpha"],
						desc = L["Alpha of the frame when visible"],
						min = 0.2, max = 1, step = .1,
					},
					delay = {
						type = "range",
						order = 8,
						name = L["Delay"],
						desc = L["Delay between updates"],
						min = .05, max = .5, step = .05,
					},
				},
			},
		}
	}

	return options;
end