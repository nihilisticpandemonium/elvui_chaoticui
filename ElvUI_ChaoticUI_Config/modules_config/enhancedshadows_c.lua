local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI);
local ES = CUI:GetModule('EnhancedShadows');

function ES:GenerateOptions()
	local options = {
		type = "group",
		name = "Enhanced Shadows",
		get = function(info) return E.db.chaoticui.enhancedshadows[info[#info]] end,
		set = function(info, value) E.db.chaoticui.enhancedshadows[info[#info]] = value ES:UpdateShadows() end,
		args = {
			enabled = {
				type = "toggle",
				order = 1,
				name = L["Enable"],
				desc = L["Enable the enhanced shadows."],
			},
			shadowcolor = {
				type = "color",
				order = 1,
				name = "Shadow Color",
				hasAlpha = false,
				get = function(info)
					local t = E.db.chaoticui.enhancedshadows[info[#info]]
					return t.r, t.g, t.b, t.a
				end,
				set = function(info, r, g, b)
					E.db.chaoticui.enhancedshadows[info[#info]] = {}
					local t = E.db.chaoticui.enhancedshadows[info[#info]]
					t.r, t.g, t.b = r, g, b
					ES:UpdateShadows()
				end,
			},
			classcolor = {
				type = 'toggle',
				order = 2,
				name = 'Class Color',
			},
			size = {
				order = 2,
				type = 'range',
				name = L["Size"],
				min = 2, max = 10, step = 1,
			},
		},
	}

	return options;
end
