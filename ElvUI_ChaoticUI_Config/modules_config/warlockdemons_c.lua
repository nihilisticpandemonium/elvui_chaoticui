if (not IsAddOnLoaded("zPets")) then return end

local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI);
local WD = CUI:GetModule('WarlockDemons');

function WD:GenerateOptions()
	if E.myclass ~= "WARLOCK" then return nil end
	local options = {
		type = "group",
		name = L["Warlock Demons"],
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["Demon Count"],
	
			},
			description = {
				order = 2,
				type = "description",
				name = L["Timer bars and counts for demonology demons"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,
				get = function(info) return E.db.chaoticui.warlockdemons[info[#info]] end,
				set = function(info, value) E.db.chaoticui.warlockdemons[info[#info]] = value; WD:UpdateAll(); end,
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the demon count"],
					},
					resetsettings = {
						type = "execute",
						order = 2,
						name = L["Reset Settings"],
						desc = L["Reset the settings of this addon to their defaults."],
						func = function() E:CopyTable(E.db.chaoticui.warlockdemons, P.chaoticui.warlockdemons); WD:UpdateAll(); end,
					},
                    width = {
                        type = "range",
                        order = 3,
                        name = L["Width"],
                        desc = L["Width of the bars"],
                        min = 10, max = 500, step = 1,
                    },
                    height = {
                        type = "range",
                        order = 4,
                        name = L["Height"],
                        desc = L["Height of the bars"],
                        min = 5, max = 100, step = 1,
                    },
					spacing = {
						type = "range",
						order = 5,
						name = L["Spacing"],
						desc = L["Spacing between bars"],
						min = 0, max = 100, step = 1,
					},
                    texture = {
                        type = 'select',
                        dialogControl = 'LSM30_Statusbar',
                        order = 6,
                        name = L['Statusbar Texture'],
                        values = AceGUIWidgetLSMlists.statusbar,
                    },
					font = {
						type = "select", dialogControl = 'LSM30_Font',
						order = 7,
						name = L["Font"],
						values = AceGUIWidgetLSMlists.font,
					},
                    fontSize = {
						order = 8,
						name = L["Font Size"],
						type = 'range',
						min = 9, max = 16, step = 1,
					},
                    grow = {
                       order = 9,
						type = "select",
						name = "Growth Direction",
						values = {
							["DOWN"] = "DOWN",
							["UP"] = "UP",
						},
                    },
                    color = {
                        type = 'color',
                        order = 11,
                        name = L['Static Statusbar Color'],
                        desc = L['Choose which color you want your statusbars to use.'],
                        hasAlpha = false,
                        get = function(info)
                            local t = E.db.chaoticui.warlockdemons[ info[#info] ]
                            return t.r, t.g, t.b, t.a
                        end,
                        set = function(info, r, g, b)
                            E.db.chaoticui.warlockdemons[ info[#info] ] = {}
                            local t = E.db.chaoticui.warlockdemons[ info[#info] ]
                            t.r, t.g, t.b = r, g, b
                        end,
                    },
					alpha = {
						type = 'range',
						order = 12,
						name = L["Alpha"],
						desc = L["Alpha of bars"],
						min = 0, max = 1, step = 0.1,
					},
				},
			},
		},
	};

    local demons = {
        type = 'group',
        guiInline = true,
        name = "Demons",
        args = {};
    }

    for k, v in pairs(self.demons) do
        demons.args[k] = {
            name = k,
            type = 'toggle',
            desc = 'Enable '..k..' tracking.',
            order = v.optionOrder,
            get = function(info) return E.db.chaoticui.warlockdemons.demons[k].enabled end,
            set = function(info, value) E.db.chaoticui.warlockdemons.demons[k].enabled = value end,
        }
    end

    options.args.demons = demons;
	return options;
end