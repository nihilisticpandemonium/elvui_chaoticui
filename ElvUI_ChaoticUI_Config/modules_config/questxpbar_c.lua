local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local QXP = CUI:GetModule('QuestXPBar')
local LSM = LibStub("LibSharedMedia-3.0")

function QXP:GenerateOptions()
    local options = {
		type = "group",
		name = L["Quest XP Bar"],
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["Quest XP Bar by Whiro"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["Quest XP Bar inspired by flourish"],
			},
			general = {
				order = 3,
				type = "group",
				name = L["General"],
				guiInline = true,
				args = {
					enabled = {
						type = "toggle",
						order = 1,
						name = L["Enable"],
						desc = L["Enable the Quest XP Bar."],
                        get = function(info) return E.db.chaoticui.questXPBar[ info [#info]] end,
                        set = function(info,value) E.db.chaoticui.questXPBar[ info [#info]] = value; QXP:UpdateEnabledState(); end, 
				    },
					resetsettings = {
						type = "execute",
						order = 2,
						name = L["Reset Settings"],
						desc = L["Reset the settings of this addon to their defaults."],
						func = function() E:CopyTable(E.db.chaoticui.questXPBar,P.chaoticui.questXPBar); QXP:UpdateEnabledState(); end,
					},
                },
            },
        },
    };
    return options;
end