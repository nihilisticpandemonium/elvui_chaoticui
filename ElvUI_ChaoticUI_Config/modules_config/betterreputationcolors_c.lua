local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI)
local BRC = CUI:GetModule('BetterReputationColors');

function BRC:GenerateOptions()
    local options = {
		type = "group",
		name = L["BetterReputationColors"],
		args = {
			header = {
				order = 1,
				type = "header",
				name = L["BetterReputationColors"],
			},
			description = {
				order = 2,
				type = "description",
				name = L["Allows you to change the faction colors"],
			},
			general = {
				order = 3,
				type = "group",
				name = "General",
				guiInline = true,
				args = {},
			},
		},
	};

    local db = E.db.chaoticui.betterreputationcolors;
    local order = 1;

    for i = 1, 8 do
        options.args.general.args[tostring(i)] = {
            type = "color",
			order = order,
			name = UnitSex("player") == 3 and _G["FACTION_STANDING_LABEL"..i.."_FEMALE"] or _G["FACTION_STANDING_LABEL"..i],
			hasAlpha = false,
			get = function(info)
				local t = db[i]
				return t.r, t.g, t.b
			end,
			set = function(info, r, g, b)
				db[i] = {}
				local t = db[i]
				t.r, t.g, t.b = r, g, b
                BRC:UpdateFactionColors();
			end,
        };
    end

	return options;
end