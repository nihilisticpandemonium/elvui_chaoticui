local CUI, E, L, V, P, G = unpack(ElvUI_ChaoticUI); --Inport: Engine, Locales, PrivateDB, ProfileDB, GlobalDB
local ISD = CUI:GetModule('ImprovedSystemDataText');
local PDT = CUI:GetModule("ProfessionsDataText");
local TDT = CUI:GetModule('TitlesDT');
local DT = E:GetModule('DataTexts');

function ISD:GenerateOptions()
	-- inject our config into elvui's config window
	local options = {
		type = "group",
		name = L["Improved System Datatext"],
		get = function(info) return E.db.chaoticui.sysdt[info[#info]] end,
		set = function(info, value) E.db.chaoticui.sysdt[info[#info]] = value; DT:LoadDataTexts() end,
		args = {
			maxAddons = {
				type = "range",
				order = 1,
				name = L["Max Addons"],
				desc = L["Maximum number of addons to show in the tooltip."],
				min = 1, max = 50, step = 1,
			},
			announceFreed = {
				type = "toggle",
				order = 2,
				name = L["Announce Freed"],
				desc = L["Announce how much memory was freed by the garbage collection."],
			},
			showFPS = {
				type = "toggle",
				order = 3,
				name = L["Show FPS"],
				desc = L["Show FPS on the datatext."],
			},
			showMemory = {
				type = "toggle",
				order = 4,
				name = L["Show Memory"],
				desc = L["Show total addon memory on the datatext."]
			},
			showMS = {
				type = "toggle",
				order = 5,
				name = L["Show Latency"],
				desc = L["Show latency on the datatext."],
			},
			latency = {
				type = "select",
				order = 6,
				name = L["Latency Type"],
				desc = L["Display world or home latency on the datatext.  Home latency refers to your realm server.  World latency refers to the current world server."],
				disabled = function() return not E.db.chaoticui.sysdt.showMS end,
				values = {
					["home"] = L["Home"],
					["world"] = L["World"],
				}
			},
		}
	}
	
	return options;
end

function PDT:GenerateOptions()
	local options = {
		type = "group",
		name = L["Professions Datatext"],
		get = function(info) return E.db.chaoticui.profdt[info[#info]] end,
		set = function(info, value) E.db.chaoticui.profdt[info[#info]] = value; DT:LoadDataTexts() end,
		args = {
			prof = {
				type = "select",
				order = 1,
				name = L["Professions"],
				desc = L["Select which profession to display."],
				
				values = function()
					local prof1, prof2, archy, fishing, cooking, firstAid = GetProfessions()
					local profValues = {}
					if prof1 ~= nil then profValues['prof1'] = GetProfessionName(prof1) end
					if prof2 ~= nil then profValues['prof2'] = GetProfessionName(prof2) end
					if archy ~= nil then profValues['archy'] = GetProfessionName(archy) end
					if fishing ~= nil then profValues['fishing'] = GetProfessionName(fishing) end
					if cooking ~= nil then profValues['cooking'] = GetProfessionName(cooking) end
					if firstAid ~= nil then profValues['firstaid'] = GetProfessionName(firstAid) end
					sort(profValues)
					return profValues
				end,
			},
			hint = {
				type = "toggle",
				order = 2,
				name = L["Show Hint"],
				desc = L["Show the hint in the tooltip."],
			},
		},
	}

	return options;
end

function TDT:GenerateOptions()	
	local options = {
		type = "group",
		name = L["Titles Datatext"],
		get = function(info) return E.db.chaoticui.titlesdt[info[#info]] end,
		set = function(info, value) E.db.chaoticui.titlesdt[info[#info]] = value; DT:LoadDataTexts() end,
		args = {
			useName = {
				type	= "toggle",
				order	= 4,
				name	= L["Use Character Name"],
				desc	= L["Use your character's class color and name in the tooltip."],
			},
		},
	}

	return options;
end