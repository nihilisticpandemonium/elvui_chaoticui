## Interface: 70250
## Title: |cff1784d1ElvUI |r|cfff02020ChaoticUI|r |cff1784d1Config|r
## Author: Whiro
## Version: 3.01
## Notes: Configuration for ElvUI_ChaoticUI
## RequiredDeps: ElvUI_ChaoticUI, ElvUI_Config
## LoadOnDemand: 1
## LoadWith: ElvUI_Config

modules_config\load_modules_config.xml
core_config\load_core_config.xml
