local MAJOR_VERSION = "LibChaoticUITags-1.0"
local MINOR_VERSION = 1

if not LibStub then error(MAJOR_VERSION .. " requires LibStub") end

local ChaoticLib, oldLib = LibStub:NewLibrary(MAJOR_VERSION, MINOR_VERSION)
if not ChaoticLib then
	return
end

ChaoticLib.registeredTags = {};
ChaoticLib.registeredEvents = {};
ChaoticLib.registeredTagStrings = {};

function ChaoticLib:RegisterTag(tag, evaluationFunc, events)
    if not events then events = "" end

    local _events = { strsplit(" ", events) };

    ChaoticLib.registeredTags[tag] = { evaluationFunc = evaluationFunc, events = _events };
    for i, event in ipairs(_events) do
        if (event ~= "") then
         if (not ChaoticLib.registeredEvents[event]) then
              ChaoticLib.driverFrame:RegisterEvent(event);
              ChaoticLib.registeredEvents[event] = 0;
         end
         ChaoticLib.registeredEvents[event] = ChaoticLib.registeredEvents[event] + 1;
        end
    end
end

function ChaoticLib:UnregisterTag(tag)
    if (not ChaoticLib.registeredTags[tag]) then
        return;
    end
    local events = ChaoticLib.registeredTags[tag].events;
    for i, event in ipairs(events) do
        ChaoticLib.registeredEvents[event] = ChaoticLib.registeredEvents[event] - 1;
        if (ChaoticLib.registeredEvents[event] == 0) then
            ChaoticLib.driverFrame:UnregisterEvent(event);
            ChaoticLib.registeredEvents[event] = nil;
        end
    end
    ChaoticLib.registeredTags[tag] = nil;
end

function ChaoticLib:RegisterFontString(key, fs)
    if (not ChaoticLib.registeredTagStrings[key]) then
        ChaoticLib.registeredTagStrings[key] = {};
    end
    ChaoticLib.registeredTagStrings[key].fs = fs;
    ChaoticLib.registeredTagStrings[key].backingText = "";
    local frame = fs:GetParent();
    frame:HookScript("OnEnter", function(self)
        if (not ChaoticLib.registeredTagStrings[key]) then
            return;
        end
        CTags_OnEnter(fs)
    end);
    frame:HookScript("OnLeave", function(self)
        if (not ChaoticLib.registeredTagStrings[key]) then
            return;
        end
        CTags_OnLeave(fs)
    end);
    -- We own this fs, so only we should change the text
    fs.blzSetText = fs.SetText;
    fs.SetText = function() end;
end

function ChaoticLib:UnregisterFontString(key)
    if (not ChaoticLib.registeredTagStrings[key]) then
        return;
    end
    local fs = ChaoticLib.registeredTagStrings[key].fs;
    fs.SetText = fs.blzSetText;
    fs.blzSetText = nil;
    ChaoticLib.registeredTagStrings[key] = nil;
end

function ChaoticLib:Tag(key, tagStr)
    if (not ChaoticLib.registeredTagStrings[key]) then
        return;
    end
    ChaoticLib.registeredTagStrings[key].backingText = tagStr;
    ChaoticLib:UpdateTagStrings();
end

ChaoticLib.driverFrame = CreateFrame("Frame");
ChaoticLib.driverFrame:SetScript("OnEvent", function(self, event, ...)
    ChaoticLib:UpdateTagStrings();
end)

local CurrentFS, CurrentKey;
ChaoticLib.tagDirector = {};
setmetatable(ChaoticLib.tagDirector, {
    __index = function(table, key)
        if (ChaoticLib.registeredTags[key]) then
            local res = ChaoticLib.registeredTags[key].evaluationFunc(CurrentFS);
            if (res) then
                ChaoticLib.registeredTags[key].cachedResult = res;
                return res;
            end
            return ChaoticLib.registeredTags[key].cachedResult;
        end
        return nil;
    end,
});

local mouseoverSeen = {};
ChaoticLib:RegisterTag('mouseover', function(fs) local key = CurrentKey; mouseoverSeen[key] = true; return ''; end);

function ChaoticLib:UpdateTagStrings(event)
    for k, v in pairs(ChaoticLib.registeredTagStrings) do
        CurrentKey = k;
        CurrentFS = v.fs;
        mouseoverSeen[k] = false;
        v.fs:blzSetText(v.backingText:gsub('%[([^%]]+)%]', ChaoticLib.tagDirector));
        if (mouseoverSeen[k]) then
            if (not v.fs:IsMouseOver()) then
                CTags_OnLeave(v.fs);
            else
                CTags_OnEnter(v.fs);
            end
        end
    end
end

function CTags_OnEnter(self)
    if (mouseoverSeen[self]) then
        E:UIFrameFadeIn(self, 0.2, 0, 1);
    end
end

function CTags_OnLeave(self)
    if (mouseoverSeen[self]) then
        E:UIFrameFadeOut(self, 0.2, 1, 0);
    end
end
