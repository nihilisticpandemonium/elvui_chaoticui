--
-- Created by IntelliJ IDEA.
-- User: mtindal
-- Date: 9/4/2017
-- Time: 1:56 PM
-- To change this template use File | Settings | File Templates.
--

local CUI, E, L, V, P, G = unpack(select(2, ...));

local CM = CUI:NewModule('Migration');

local moversToCheck = {
	"UI_SpecSwitchBar",
	"UI_PartyXPHolder",
	"UI_EquipmentSetsMover",
	"UI_ToyBarMover",
	"UI_PortalBarMover",
	"UI_ProfessionBarMover",
	"UF_PlayerVerticalFrame AuraBar Mover",
	"UF_TargetVerticalFrame AuraBar Mover",
	"UF_PlayerVerticalUnitFrame Castbar Mover",
	"UF_TargetVerticalUnitFrame Castbar Mover",
};

function CM:CheckMigrations()
	if (not E.db.chaoticui.migrated and E.db.nenaui) then
		E.db.chaoticui = E:CopyTable(E.db.nenaui);
		E.db.chaoticui.chaoticchat = E:CopyTable(E.db.nenaui.nenachat);
		E.db.nenaui.nenachat = nil;

		for i, mover in ipairs(moversToCheck) do
			if (E.db.movers["Nena"..mover]) then
				CUI:SaveMoverPosition("Chaotic"..mover, unpack(E.db.movers["Nena"..mover]));
				E.db.movers["Nena"..mover] = nil;
			end
		end
		
		E.db.chaoticui.migrated = true;

		E.db.nenaui = nil;
	end
end