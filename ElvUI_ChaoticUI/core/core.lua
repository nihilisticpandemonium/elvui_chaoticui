local addon, ns = ...
local E, L, V, P, G = unpack(ElvUI);
local AB = E:GetModule('ActionBars');
local CH = E:GetModule('Chat');
local SLE = ElvUI_SLE[1];
local lib = LibStub("LibElv-GameMenu-1.0")
local _G = _G
local HideUIPanel = HideUIPanel
local _SendAddonMessage = C_ChatInfo.SendAddonMessage;

ns.oUF = ElvUF;

local CUI = LibStub('AceAddon-3.0'):NewAddon(addon, 'AceEvent-3.0', 'AceHook-3.0');

ns[1] = CUI
ns[2] = E
ns[3] = L
ns[4] = V
ns[5] = P
ns[6] = G
_G[addon] = ns;

CUI.version = GetAddOnMetadata("ElvUI_ChaoticUI", "Version");

local blizzPath = [[|TInterface\ICONS\]]
local chaoticui = blizzPath..[[%s:12:12:0:0:64:64:4:60:4:60|t]]

local pairs, tinsert, strsplit, gsub, IsAddOnLoaded, LoadAddOn, C_PetJournal, LE_PET_JOURNAL_FILTER_COLLECTED, LE_PET_JOURNAL_FILTER_NOT_COLLECTED, InCombatLockdown, UnitAffectingCombat, ipairs, unpack, MainMenuBar_GetNumArtifactTraitsPurchasableFromXP = pairs, tinsert, strsplit, gsub, IsAddOnLoaded, LoadAddOn, C_PetJournal, LE_PET_JOURNAL_FILTER_COLLECTED, LE_PET_JOURNAL_FILTER_NOT_COLLECTED, InCombatLockdown, UnitAffectingCombat, ipairs, unpack

BINDING_HEADER_CHAOTICUI = "|cfff02020ChaoticUI|r"

local f=CreateFrame("Frame")
f:RegisterEvent("PLAYER_LOGIN")
f:SetScript("OnEvent", function()
	CUI:Initialize()
end)

-- GLOBALS: ElvDB, LibStub

CUI.ChatIconsByClass = {
	["WARLOCK"] = format(chaoticui,"inv_cloth_challengewarlock_d_01helm"),
	["MONK"] = format(chaoticui,"inv_helm_leather_challengemonk_d_01"),
	["DEATHKNIGHT"] = format(chaoticui,"inv_helm_plate_challengedeathknight_d_01"),
	["PALADIN"] = format(chaoticui,"inv_helmet_plate_challengepaladin_d_01"),
	["MAGE"] = format(chaoticui,"inv_helm_cloth_challengemage_d_01"),
	["SHAMAN"] = format(chaoticui,"inv_helmet_mail_challengeshaman_d_01"),
	["ROGUE"] = format(chaoticui,"inv_helmet_leather_challengerogue_d_01"),
	["PRIEST"] = format(chaoticui,"inv_helmet_cloth_challengepriest_d_01"),
	["HUNTER"] = format(chaoticui,"inv_helmet_mail_challengehunter_d_01"),
	["WARRIOR"] = format(chaoticui,"inv_helm_plate_challengewarrior_d_01"),
	["DRUID"] = format(chaoticui,"inv_helmet_challengedruid_d_01"),
	["DEMONHUNTER"] = format(chaoticui,"inv_helm_leather_classsetdemonhunter_d_01");
}

CUI.TexturePath = "Interface\\AddOns\\ElvUI_ChaoticUI\\media\\textures\\"
CUI.ClassFormat = "%s_crest.tga"
CUI.FactionFormat = "%s_fancy.tga"

function CUI:GetClassTexture()
	return CUI.TexturePath..string.format(CUI.ClassFormat, string.lower(E.myclass));
end

function CUI:GetFactionTexture()
	return CUI.TexturePath..string.format(CUI.FactionFormat, string.lower(E.myfaction));
end

CUI.SpecialChatIcons = {
	["WyrmrestAccord"] = {
		["Cyrizzak"] = "WARLOCK",
		["Sagome"] = "MONK",
		["Cialey"] = "DEATHKNIGHT",
		["Moneila"] = "PALADIN",
		["Shalerie"] = "MAGE",
		["Chalini"] = "SHAMAN",
		["Hioxy"] = "ROGUE",
		["Daraleine"] = "PRIEST",
		["Varysa"] = "HUNTER",
		["Caylasena"] = "WARRIOR",
		["Onaguda"] = "DRUID",
		["Sunaralei"] = "DEMONHUNTER",
	},
	["MoonGuard"] = {
		["Iyavice"] = "WARLOCK",
		["Thiradia"] = "DEMONHUNTER",
	},
}

function CUI_PairsByKeys(startChar, f)
	local a, i = {}, 0
	for n in pairs(startChar) do tinsert(a, n) end
	sort(a, f)
	local iter = function()
		i = i + 1
		if a[i] == nil then
			return nil
		else
			return a[i], startChar[a[i]]
		end
	end
	return iter
end

function CUI:InvertTable(t)
	local u = { }
	for k, v in pairs(t) do u[v] = k end
	return u
end

CUI["RegisteredModules"] = {}
function CUI:RegisterModule(name)
	if self.initialized then
		local module = self:GetModule(name);
		if (module and module.Initialize) then
			module:Initialize();
			if (ElvUIDev) then
				ElvUIDev:RegisterPluginModule("ElvUI_ChaoticUI", name, module);
			end
		end
	else
		self["RegisteredModules"][#self["RegisteredModules"] + 1] = name
	end
end

function CUI:GetRegisteredModules()
	return self["RegisteredModules"];
end

local pcall = pcall
function CUI:InitializeModules()
	for _, moduleName in pairs(CUI["RegisteredModules"]) do
		local module = self:GetModule(moduleName)
		if module.Initialize then
			module:Initialize();
			if (ElvUIDev) then
				ElvUIDev:RegisterPluginModule("ElvUI_ChaoticUI", moduleName, module);
			end
		end
	end
end

local function GetChatIcon(sender)
	local senderName, senderRealm
	if sender then
		senderName, senderRealm = strsplit('-', sender)
	else
		senderName = E.myname
	end
	senderRealm = senderRealm or E.myrealm
	senderRealm = gsub(senderRealm, ' ', '')

	if CUI.SpecialChatIcons[senderRealm] and CUI.SpecialChatIcons[senderRealm][senderName] then
		return CUI.ChatIconsByClass[CUI.SpecialChatIcons[senderRealm][senderName]];
	end

	return nil
end

local ACD;
function CUI:ClickGameMenu()
	if InCombatLockdown() then return end;
	ACD = ACD or LibStub("AceConfigDialog-3.0-ElvUI", true);
	if (not ACD) then
		if (not IsAddOnLoaded("ElvUI_Config")) then
			LoadAddOn("ElvUI_Config");
		end
		ACD = LibStub("AceConfigDialog-3.0-ElvUI", true);
	end
	E:ToggleConfig(); 
	ACD:SelectGroup("ElvUI", "ChaoticUI"); 
	HideUIPanel(_G["GameMenuFrame"]);
end

local SLEBuildGameMenu = SLE.BuildGameMenu;
SLE.BuildGameMenu = function(self) end;

function CUI:BuildGameMenu()
	local button = {
		["name"] = "GameMenu_ChaoticUIConfig",
		["text"] = "|cfff02020ChaoticUI|r",
		["func"] = function() CUI:ClickGameMenu() end,
	}
	lib:AddMenuButton(button)

	lib:UpdateHolder()

	SLEBuildGameMenu(SLE);
end

function CUI:FixPetJournal()
	C_PetJournal.SetFilterChecked(LE_PET_JOURNAL_FILTER_COLLECTED, true);
	C_PetJournal.SetFilterChecked(LE_PET_JOURNAL_FILTER_NOT_COLLECTED, true);
	C_PetJournal.SetAllPetTypesChecked(true);
	C_PetJournal.SetAllPetSourcesChecked(true);
end

function CUI:RegenWait(func, ...)
	if (not InCombatLockdown() and not UnitAffectingCombat("player") and not UnitAffectingCombat("pet")) then
		func(...);
		return;
	end

	if (not CUI.waitFuncs) then
		CUI.waitFuncs = {};
	end

	local newArgs = { ... };
	local found = false;
	for _, info in ipairs(CUI.waitFuncs) do
		if (info.func == func) then
			local argsEqual = true;
			if (#newArgs ~= #info.args) then
				argsEqual = false;
			else
				for i, arg in ipairs(info.args) do
					if (newArgs[i] ~= arg) then
						argsEqual = false;
						break;
					end
				end
			end
			if (argsEqual) then
				found = true;
				break;
			end
		end
	end

	if (not found) then
		tinsert(CUI.waitFuncs, { func = func, args = {...} });
	end
	self:RegisterEvent("PLAYER_REGEN_ENABLED");
end

function CUI:PLAYER_REGEN_ENABLED()
	if (not CUI.waitFuncs or #CUI.waitFuncs == 0) then
		self:UnregisterEvent("PLAYER_REGEN_ENABLED");
		return;
	end

	for _, funcInfo in ipairs(CUI.waitFuncs) do
		funcInfo.func(unpack(funcInfo.args));
	end

	wipe(CUI.waitFuncs);
	
	self:UnregisterEvent("PLAYER_REGEN_ENABLED");
end

function CUI:GlossifyChatPanels()
	local function GlossifyPanel(panel)
		local tex = panel:CreateTexture(nil, "OVERLAY");
		tex:SetInside();
		tex:SetTemplate();
		tex:SetAlpha(0.4);
	end
end

function CUI:UpdateRegisteredDBs()
	if (not CUI["RegisteredDBs"]) then
		return;
	end

	local dbs = CUI["RegisteredDBs"];

	for tbl, path in pairs(dbs) do
		self:UpdateRegisteredDB(tbl, path);
	end
end

function CUI:UpdateRegisteredDB(tbl, path)
	local path_parts = {strsplit(".", path)};
	local _db = E.db.chaoticui;
	for _, path_part in ipairs(path_parts) do
		_db = _db[path_part];
	end
	tbl.db = _db;
end

function CUI:RegisterDB(tbl, path)
	if (not CUI["RegisteredDBs"]) then
		CUI["RegisteredDBs"] = {};
	end
	self:UpdateRegisteredDB(tbl, path);
	CUI["RegisteredDBs"][tbl] = path;
end

function CUI:SetupProfileCallbacks()
	E.data.RegisterCallback(self, "OnProfileChanged", "UpdateRegisteredDBs");
	E.data.RegisterCallback(self, "OnProfileCopied", "UpdateRegisteredDBs");
	E.data.RegisterCallback(self, "OnProfileReset", "UpdateRegisteredDBs");
end

function CUI:Initialize()
	self.initialized = true;

	self:BuildGameMenu();
	self:FixPetJournal();

	self.questXPHooks = {};
	self.questXPArray = {};
	self.questCompleteSeen = {};

	CH:AddPluginIcons(GetChatIcon);
	self:InitializeModules();

	if (ElvUIDev) then
		ElvUIDev:RegisterPlugin(addon);
	end
	
	self:SetupProfileCallbacks();
end

function CUI:SaveMoverPosition(mover, anchor, parent, point, x, y)
	if not _G[mover] then return end
	local frame = _G[mover]

	frame:ClearAllPoints()
	frame:SetPoint(anchor, parent, point, x, y)
	E:SaveMoverPosition(mover)
end

function CUI:CheckQuestXPDirty(questID)
	local dirty = false;

	if self.questXPArray[questID] then
		if UnitLevel("player") ~= self.questXPArray[questID].level then
			dirty = true;
		end
		if C_PvP.IsWarModeDesired() ~= self.questXPArray[questID].warModeSetting then
			dirty = true;
		end
	end

	return dirty;
end

function CUI:CalculateQuestRewardXP()
	local xp = GetQuestLogRewardXP();
	if (C_PvP.IsWarModeDesired()) then
		xp = xp + (xp * 0.1);
	end
	return xp;
end

function CUI:GenerateQuestXPArray()
	local u = 1
	local lastSelected = GetQuestLogSelection()
	local mapID = E.MapInfo.mapID;
	if (not self.questXPArray.mapID or self.questXPArray.mapID ~= mapID) then
		wipe(self.questXPArray);
	end

	while GetQuestLogTitle(u) do
		local questID = select(8, GetQuestLogTitle(u));
		local questLogMapID = GetQuestUiMapID(questID);
		if (questLogMapID == mapID and (not self.questXPArray[questID] or self:CheckQuestXPDirty(questID))) then
			SelectQuestLogEntry(u)
			local questLogTitleText, level, suggestedGroup, isHeader, isCollapsed, isComplete, isDaily, questID = GetQuestLogTitle(u)
			if (not isHeader) then
				self.questXPArray[questID] = { xp = self:CalculateQuestRewardXP(), level = UnitLevel("player") or 0, warModeSetting = C_PvP.IsWarModeDesired() };
			end
		end
		u = u + 1
	end

	self.questXPArray.mapID = mapID;

	SelectQuestLogEntry(lastSelected)
end

local calculateWaiting = false;
function CUI:CalculateQuestLogXP()
	if (UnitAffectingCombat("player") or UnitAffectingCombat("pet")) then
		if (not calculateWaiting) then
			calculateWaiting = true;
			CUI:RegenWait(self.CalculateQuestLogXP, self);
		end
		return self.currQXP or 0;
	end

	calculateWaiting = false;
	local i = 1
	local currentQuestXPTotal = 0
	local mapID = E.MapInfo.mapID;
	self:GenerateQuestXPArray();
	while GetQuestLogTitle(i) do
		local questLogTitleText, level, suggestedGroup, isHeader, isCollapsed, isComplete, isDaily, questID = GetQuestLogTitle(i)
		local questMapID = GetQuestUiMapID(questID);
		if (not isHeader and questMapID == mapID) then
			local objectives = GetNumQuestLeaderBoards(i)
			if ((objectives == 0 or isComplete) and self.questXPArray[questID]) then
				currentQuestXPTotal = currentQuestXPTotal + self.questXPArray[questID].xp
			end
		end
		i = i + 1
	end
	self.currQXP = currentQuestXPTotal;
	return currentQuestXPTotal;
end

if (IsAddOnLoaded("ElvUI_SLE")) then
	hooksecurefunc(ElvUI_SLE[1], "Initialize", function() CUI:GetModule('HyperDT'):Initialize() end);
end

-- Woo glossy templates errywhere

local function addapi(object)
	local mt = getmetatable(object).__index
	local oldTemplate = object.SetTemplate
	object.SetTemplate = function(f, t, glossTex, ignoreUpdates, forcePixelMode, isUnitFrameElement)
		oldTemplate(f, t, true, ignoreUpdates, forcePixelMode, isUnitFrameElement);
	end
end

local handled = {["Frame"] = true}
local object = CreateFrame("Frame")
addapi(object)
addapi(object:CreateTexture())
addapi(object:CreateFontString())

object = EnumerateFrames()
while object do
	if not object:IsForbidden() and not handled[object:GetObjectType()] then
		addapi(object)
		handled[object:GetObjectType()] = true
	end

	object = EnumerateFrames(object)
end

--Hacky fix for issue on 7.1 PTR where scroll frames no longer seem to inherit the methods from the "Frame" widget
local scrollFrame = CreateFrame("ScrollFrame")
addapi(scrollFrame)
