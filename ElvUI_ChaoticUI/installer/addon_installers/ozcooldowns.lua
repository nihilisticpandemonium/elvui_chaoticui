local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:OzCooldownsSetup()
	self:AddProfile(OzCooldownsDB)

	CUI:SaveMoverPosition("OzCooldownsMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 283);
	local classColor = E.myclass == 'PRIEST' and E.PriestColors or RAID_CLASS_COLORS[E.myclass];
	self:SetProfile(OzCooldownsDB, {
		["StackFont"] = E.db.chaoticui.installer.font,
		["StatusBarTexture"] = 'Simpy Dry Swirl',
		['CooldownText'] = {
			['Font'] = 'KGSmallTownSouthernGirl',
		},
		["StatusBarTextureColor"] = {
			r = classColor.r,
			g = classColor.g,
			b = classColor.b,
		}
	});
end

CI:RegisterAddOnInstaller("OzCooldowns", "OzCooldownsSetup");