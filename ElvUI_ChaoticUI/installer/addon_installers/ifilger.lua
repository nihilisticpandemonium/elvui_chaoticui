local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:iFilgerSetup()
	self:AddProfile(iFilgerDB);
	CUI:SaveMoverPosition("iFilger Enhancements", "TOP", ElvUIParent, "TOP", 307, -217);
	CUI:SaveMoverPosition("iFilger Procs", "BOTTOM", ElvUIParent, "BOTTOM", 0, 436);
	CUI:SaveMoverPosition("iFilger Cooldowns", "BOTTOM", ElvUIParent, "BOTTOM", 0, 340);
	CUI:SaveMoverPosition("iFilger Buffs", "TOP", ElvUIParent, "TOP", 306, -68);
	CUI:SaveMoverPosition("Ifilger Debuffs", "TOPRIGHT", ElvUIParent, "TOPRIGHT", -489, -479);

	local function Disable()
		return { ["Enable"] = false };
	end

	self:SetProfile(iFilgerDB, {
		["Enhancements"] = Disable(),
		["Procs"] = {
			["IconSIze"] = 32,
			["Direction"] = "LEFT",
		},
		["Cooldowns"] = Disable(),
		["Buffs"] = Disable(),
		["Debuffs"] = Disable(),
		["FocusBuffs"] = Disable(),
		["PvPTargetBuffs"] = Disable(),
		["TargetDebuffs"] = Disable();
		["StackCountFont"] = E.db.chaoticui.installer.font,
	});
end

CI:RegisterAddOnInstaller("iFilger", "iFilgerSetup");