local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:InFlightSetup()
	InFlightDB.perchar = true;
	local db = {};

	db.border = "ElvUI Blank";
	db.font = E.db.chaoticui.installer.font;
	db.texture = E.db.chaoticui.installer.texture;
	local classColor = E.myclass == 'PRIEST' and E.PriestColors or RAID_CLASS_COLORS[E.myclass];
	db.barcolor = { r = classColor.r, g = classColor.g, b = classColor.b, a = 1.0 };
	db.bordercolor = { r = classColor.r, g = classColor.g, b = classColor.b, a = 1.0 };
	db.unknowncolor = { r = classColor.r, g = classColor.g, b = classColor.b, a = 1.0 };
	db.backcolor = { r = 0, g = 0, b = 0, a = 1 };
	db.totext = " => "
	InFlightCharDB = db;
end

CI:RegisterAddOnInstaller("InFlight", "InFlightSetup");