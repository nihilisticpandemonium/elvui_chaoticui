local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:XctPlusSetup()
	local keys = { "general", "power", "healing", "outgoing", "critical", "procs", "loot", "class", "damage" };

	local extraInfo = {
		["healing"] = {
			["Height"] = 132,
			["Width"] = 300,
			["X"] = -390,
			["Y"] = -310,
		},
		["outgoing"] = {
			["X"] = 455,
			["Y"] = -255,
		},
		["critical"] = {
			["Y"] = 76,
			["X"] = 459,
		},
		["damage"] = {
			["Width"] = 300,
			["Height"] = 148,
			["X"] = -391,
			["Y"] = -448,
		},
	};

	for _, key in ipairs(keys) do
		xCT_Plus.db.profile.frames[key].font = E.db.chaoticui.installer.font;
		if (extraInfo[key]) then
			for k, v in pairs(extraInfo[key]) do
				xCT_Plus.db.profile.frames[key][k] = v;
			end
		end
	end
end

CI:RegisterAddOnInstaller("xCT+", "XctPlusSetup");