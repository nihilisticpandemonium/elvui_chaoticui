local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:EnhancedPetBattleUISetup()
	self:AddProfile(EnhancedPetBattleUIDB);

	self:SetProfile(EnhancedPetBattleUIDB, {
		["HideBlizzard"] = true,
	});
end

CI:RegisterAddOnInstaller("EnhancedPetBattleUI", "EnhancedPetBattleUISetup");