local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:AddOnSkinsSetup()
	self:AddProfile(AddOnSkinsDB);

	self:SetProfile(AddOnSkinsDB, {
		["EmbedOoC"] = false,
		["EmbedLeft"] = "Details",
		["EmbedLeftWidth"] = 240,
		["DBMSkinHalf"] = true,
		["Blizzard_AbilityButton"] = true,
		["ParchmentRemover"] = true,
		["EmbedSystemDual"] = true,
		["EmbedRight"] = "Details",
		["EmbedMain"] = "",
		["EmbedIsHidden"] = true,
		["BarrelsOEasy"] = true,
		["SkinDebug"] = IsAddOnLoaded("AddOnSkins_PopupDisabler") and true or false,
	});
end

CI:RegisterAddOnInstaller("AddOnSkins", "AddOnSkinsSetup");