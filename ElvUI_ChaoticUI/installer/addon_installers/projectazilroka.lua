local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:ProjectAzilrokaSetup()
	self:AddProfile(DragonOverlayDB); 

	self:SetProfile(DragonOverlayDB, {
		['FlipDragon'] = true,
		["rare"] = "HeavenlyJade",
		["elite"] = "HeavenlyOnyx",
		["rareelite"] = "HeavenlyCrimson",
		["worldboss"] = "HeavenlyGolden",
		["ClassIcon"] = true,
	});

	self:AddProfile(EnhancedFriendsListDB);

	self:SetProfile(EnhancedFriendsListDB, {
		["Hero"] = "Launcher",
		["App"] = "Launcher",
		["Pro"] = "Launcher",
		["Neutral"] = "Launcher",
		["DST2"] = "Launcher",
		["BSAp"] = "BlizzardChat",
		["Alliance"] = "Flat",
		["D3"] = "Launcher",
		["Horde"] = "Flat",
		["S1"] = "Launcher",
		["WTCG"] = "Launcher",
		["S2"] = "Launcher",
		["NameFont"] = E.db.chaoticui.installer.font,
		["InfoFont"] = E.db.chaoticui.installer.font,
		["StatusIconPack"] = "Square",
	})
	self:AddProfile(ProjectAzilrokaDB);

	self:SetProfile(ProjectAzilrokaDB, {
		["ES"] = false,
		["DO"] = true,
		["SMB"] = false,
		["LC"] = false,
		["EFL"] = true,
		["MF"] = false,
	})
end

CI:RegisterAddOnInstaller("ProjectAzilroka", "ProjectAzilrokaSetup");