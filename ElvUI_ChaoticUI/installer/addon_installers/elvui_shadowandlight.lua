local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:SLESetup()
	E.db["sle"] = {
		["databars"] = {
			["azerite"] = {
				["chatfilter"] = {
					["enable"] = true,
					["style"] = "STYLE2",
				},
			},
			["exp"] = {
				["chatfilter"] = {
					["enable"] = true,
					["style"] = "STYLE2",
				},
			},
			["rep"] = {
				["autotrack"] = true,
				["chatfilter"] = {
					["style"] = "STYLE2",
					["enable"] = true,
					["styleDec"] = "STYLE2",
				},
			},
			["honor"] = {
				["chatfilter"] = {
					["awardStyle"] = "STYLE4",
					["enable"] = true,
					["style"] = "STYLE3",
				},
			},
		},
		["tooltipicon"] = true,
		["media"] = {
			["fonts"] = {
				["gossip"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["zone"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["subzone"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["questFontSuperHuge"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["objectiveHeader"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["mail"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["objective"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["editbox"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
				["pvp"] = {
					["font"] = E.db.chaoticui.installer.font,
				},
			},
		},
		["blizzard"] = {
			["rumouseover"] = true,
		},
		["characterframeoptions"] = {
			["equipmentgradient"] = true,
		},
		["minimap"] = {
			["instance"] = {
				["font"] = E.db.chaoticui.installer.font,
			},
			["mapicons"] = {
				["iconsize"] = 24,
				["iconmouseover"] = true,
			},
		},
		["legacy"] = {
			["garrison"] = {
				["autoOrder"] = {
					["enable"] = true,
				},
				["toolbar"] = {
					["enable"] = true,
				},
			},
			["farm"] = {
				["enable"] = true,
				["quest"] = true,
				["autotarget"] = true,
			},
		},
		["Glamour"] = {
			["Character"] = {
				["ItemLevel"] = {
					["outline"] = "OUTLINE",
				},
				["Stats"] = {
					["IlvlFull"] = true,
					["List"] = {
						["HEALTH"] = true,
						["ALTERNATEMANA"] = true,
						["SPELLPOWER"] = true,
						["ATTACK_DAMAGE"] = true,
						["MOVESPEED"] = true,
						["ATTACK_ATTACKSPEED"] = true,
						["FOCUS_REGEN"] = true,
						["RUNE_REGEN"] = true,
						["POWER"] = true,
						["ENERGY_REGEN"] = true,
						["ATTACK_AP"] = true,
					},
				},
			},
		},
		["loot"] = {
			["enable"] = true,
			["autoroll"] = {
				["autode"] = true,
				["bylevel"] = true,
				["autoconfirm"] = true,
				["autogreed"] = true,
			},
		},
		["lfr"] = {
			["legion"] = {
				["trial"] = true,
				["nightmare"] = true,
				["tomb"] = true,
				["palace"] = true,
			},
		},
		["chat"] = {
			["tab"] = {
				["select"] = true,
				["style"] = "SQUARE",
			},
			["textureAlpha"] = {
				["enable"] = true,
				["alpha"] = 0.25,
			},
		},
		["screensaver"] = {
			["subtitle"] = {
				["font"] = E.db.chaoticui.installer.font,
			},
			["date"] = {
				["font"] = E.db.chaoticui.installer.font,
			},
			["player"] = {
				["font"] = E.db.chaoticui.installer.font,
			},
			["title"] = {
				["font"] = E.db.chaoticui.installer.font,
			},
			["tips"] = {
				["font"] = E.db.chaoticui.installer.font,
			},
		},
		["lfrshow"] = {
			["enabled"] = true,
			["nightmare"] = true,
			["tomb"] = true,
			["palace"] = true,
			["trial"] = true,
		},
		["dt"] = {
			["friends"] = {
				["expandBNBroadcast"] = true,
			},
		},
		["tooltip"] = {
			["RaidProg"] = {
				["enable"] = true,
			},
			["alwaysCompareItems"] = true,
			["showFaction"] = true,
		},
		["orderhall"] = {
			["autoOrder"] = {
				["enable"] = true,
				["autoEquip"] = true,
			},
		},
		["datatexts"] = {
			["panel5"] = {
				["enabled"] = true,
			},
			["panel3"] = {
				["enabled"] = true,
			},
			["chathandle"] = false,
			["panel6"] = {
				["enabled"] = true,
			},
			["panel2"] = {
				["enabled"] = true,
			},
			["panel8"] = {
				["enabled"] = true,
			},
			["panel4"] = {
				["enabled"] = true,
			},
			["dashboard"] = {
				["enable"] = true,
			},
			["panel7"] = {
				["enabled"] = true,
			},
			["panel1"] = {
				["enabled"] = true,
			},
		},
		["raidmarkers"] = {
			["enable"] = false,
		},
		["Armory"] = {
			["Character"] = {
				["Azerite"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Stats"] = {
					["IlvlFull"] = true,
					["List"] = {
						["HEALTH"] = true,
						["ALTERNATEMANA"] = true,
						["SPELLPOWER"] = true,
						["ATTACK_DAMAGE"] = true,
						["MOVESPEED"] = true,
						["ATTACK_ATTACKSPEED"] = true,
						["FOCUS_REGEN"] = true,
						["RUNE_REGEN"] = true,
						["ENERGY_REGEN"] = true,
						["POWER"] = true,
						["ATTACK_AP"] = true,
					},
					["ItemLevel"] = {
						["font"] = E.db.chaoticui.installer.font,
					},
				},
				["Enchant"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Level"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Durability"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
			},
			["Inspect"] = {
				["guildMembers"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["pvpText"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["pvpRating"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Level"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["pvpType"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["pvpRecord"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Spec"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Enchant"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Name"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["guildName"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["Title"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
				["infoTabs"] = {
					["Font"] = E.db.chaoticui.installer.font,
				},
			},
		},
	}
	local ic = "99.00";
	E.private["sle"] = E.private["sle"] or {};
	E.private["sle"]["inspectframeoptions"] = {
		["enable"] = true,
	}
	E.private["sle"]["characterframeoptions"] = {
		["enable"] = true,
	}
	E.private["sle"]["install_complete"] = ic
	E.private["sle"]["minimap"] = {
		["mapicons"] = {
			["enable"] = true,
			["barenable"] = true,
		},
	}
	E.private["sle"]["module"] = {
		["blizzmove"] = true,
		["screensaver"] = true,
	}
	E.private["sle"]["skins"]["objectiveTracker"] = {
		["texture"] = E.db.chaoticui.installer.texture,
		["class"] = true,
	}
	E.private["sle"]["skins"]["questguru"] = {
		["enable"] = true,
		["removeParchment"] = true,
	}
	E.private["sle"]["skins"]["merchant"]["enable"] = true
	E.private["sle"]["skins"]["merchant"]["style"] = "List"
	E.global["sle"]["advanced"]["gameMenu"]["reload"] = true;
end

CI:RegisterAddOnInstaller("ElvUI_SLE", "SLESetup");