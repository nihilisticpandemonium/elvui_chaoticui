local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:VATSetup()
	E.db["VAT"] = {
		["threshold"] = {
			["tempenchants"] = true,
			["buffs"] = true,
		},
		["noduration"] = true,
		["showText"] = true,
		['statusbarTexture'] = 'Simpy Dry Swirl',
		['backdropTexture'] = 'Simpy Dry Swirl',
	}
end

CI:RegisterAddOnInstaller("ElvUI_VisualAuraTimers", "VATSetup");