local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:KalielSetup()
	self:AddProfile(KalielsTrackerDB)

	local modulesOrder;

	local KTVersion = GetAddOnMetadata("!KalielsTracker", "Version")
	if (IsAddOnLoaded("WorldQuestTracker") and string.find(KTVersion, "WQT")) then
		modulesOrder = {
			nil, -- [1]
			"WORLD_QUEST_TRACKER_MODULE", -- [2]
			"BONUS_OBJECTIVE_TRACKER_MODULE", -- [3]
			"WORLDQUESTTRACKER_TRACKER_MODULE", -- [4]
			"AUTO_QUEST_POPUP_TRACKER_MODULE", -- [5]
			"QUEST_TRACKER_MODULE", -- [6]
			"QUECHO_TRACKER_MODULE", -- [7]
			"PETTRACKER_TRACKER_MODULE", -- [8]
			"ACHIEVEMENT_TRACKER_MODULE", -- [9]
		}
	else
		modulesOrder = {
			nil, -- [1]
			"WORLD_QUEST_TRACKER_MODULE", -- [2]
			"BONUS_OBJECTIVE_TRACKER_MODULE", --[3]
			"AUTO_QUEST_POPUP_TRACKER_MODULE", --[4]
			"QUEST_TRACKER_MODULE", --[5]
			nil, --[6]
			"PETTRACKER_TRACKER_MODULE", --[7]
			"ACHIEVEMENT_TRACKER_MODULE" -- [8]
		}
	end
	self:SetProfile(KalielsTrackerDB, {
		["classBorder"] = true,
		["borderThickness"] = 2,
		["xOffset"] = -90,
		["yOffset"] = -340,
		["addonPetTracker"] = true,
		["bgrColor"] = {
			["a"] = 1,
			["r"] = 0.254901960784314,
			["g"] = 0.254901960784314,
			["b"] = 0.254901960784314,
		},
		["bgrInset"] = 0,
		["bgr"] = "Glamour3",
		["hideEmptyTracker"] = true,
		["font"] = E.db.chaoticui.installer.font,
		['progressBar'] = E.db.chaoticui.installer.texture,
		["version"] = "2.1.5",
		["hdrBgr"] = 4,
		["qiBgrBorder"] = true,
		["border"] = "1 Pixel",
		["hdrBgrColorShare"] = true,
		["colorDifficulty"] = true,
		["addonWorldQuestTracker"] = true,
		["addonQuecho"] = true,
		["hdrTxtColorShare"] = true,
		["textWordWrap"] = true,
		["helpTutorial"] = 10,
		["sink20OutputSink"] = "xCT_Plus",
		["collapseInInstance"] = true,
		["hdrBtnColorShare"] = true,
		["sink20ScrollArea"] = "General",
		["modulesOrder"] = modulesOrder,
	});
end

CI:RegisterAddOnInstaller("!KalielsTracker", "KalielSetup");