local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:GetModule("Installer");

function CI:ParagonRepSetup()
	ParagonReputationDB.r = 186/255;
	ParagonReputationDB.g = 183/255;
	ParagonReputationDB.b = 107/255;
end

CI:RegisterAddOnInstaller("ParagonReputation", "ParagonRepSetup");