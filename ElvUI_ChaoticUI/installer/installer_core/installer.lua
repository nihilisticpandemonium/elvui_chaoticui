local CUI, E, L, V, P, G = unpack(select(2, ...));

local CI = CUI:NewModule("Installer");

local LSM = LibStub("LibSharedMedia-3.0");

local _G = _G;

local RAID_CLASS_COLORS, pairs, IsAddOnLoaded, ipairs, type, select, unpack, SetCVar, format, ReloadUI, print, tinsert, tContains, table_deepcopy = RAID_CLASS_COLORS, pairs, IsAddOnLoaded, ipairs, type, select, unpack, SetCVar, format, ReloadUI, print, tinsert, tContains, table_deepcopy

-- luacheck: globals ElvUIParent xCT_Plus xCTPlus LibStub ElvUI_Bar1 DetailsOptionsWindow PluginInstallFrame KalielsTrackerDB RAID_CLASS_COLORS ipairs IsAddOnLoaded pairs type select unpack SetCVar format ReloadUI print tinsert tContains table_deepcopy

CI.DataTextsByRole = {
    ["tank"] = "Armor",
    ["dpsCaster"] = "Spell/Heal Power",
    ["dpsMelee"] = "Attack Power",
    ["healer"] = "Spell/Heal Power",
}

function CI:BaseDBSetup()
    local classColor = E.myclass == "PRIEST" and E.PriestColors or RAID_CLASS_COLORS[E.myclass];
    E.db["nameplates"] = {
		["healthBar"] = {
			["text"] = {
				["enable"] = true,
				["format"] = "CURRENT_PERCENT",
			},
		},
		["units"] = {
			["PLAYER"] = {
				["enable"] = false,
			},
		},
		["preciseTimer"] = true,
		["auraAnchor"] = 0,
		["threat"] = {
			["badScale"] = 1.2,
		},
		["glowColor"] = {
			["r"] = classColor.r,
			["g"] = classColor.g,
			["b"] = classColor.b,
		},
		["fontSize"] = 10,
		["statusbar"] = E.db.chaoticui.installer.texture,
		["font"] = E.db.chaoticui.installer.font,
		["healthFont"] = E.db.chaoticui.installer.font,
		["stackFont"] = E.db.chaoticui.installer.font,
		["durationFont"] = E.db.chaoticui.installer.font,
		["clampToScreen"] = true,
		["displayStyle"] = "BLIZZARD",
		["loadDistance"] = 100,
	}
	E.db["general"] = {
		["interruptAnnounce"] = "SAY",
		["autoAcceptInvite"] = true,
		["autoRepair"] = "GUILD",
		["bottomPanel"] = false,
		["backdropfadecolor"] = {
			["r"] = 0.054,
			["g"] = 0.054,
			["b"] = 0.054,
		},
		["valuecolor"] = {
			["a"] = 1,
			["r"] = classColor.r,
			["g"] = classColor.g,
			["b"] = classColor.b,
		},
		["vendorGrays"] = true,
		["font"] = E.db.chaoticui.installer.font,
		["minimap"] = {
			["icons"] = {
				["classHall"] = {
					["position"] = "TOPRIGHT",
				},
			},
			["locationFont"] = E.db.chaoticui.installer.font,
		},
    }
    E.db["databars"] = {
		["experience"] = {
			["enable"] = true,
			["orientation"] = "HORIZONTAL",
			["height"] = 10,
			["width"] = 410,
			["textSize"] = 9,
			["font"] = E.db.chaoticui.installer.font,
			["tag"] = "[name] Lvl [xp:level] XP: [xp:current]/[xp:max] ([xp:percent]) [xp:rested] Rested ([xp:quest] Quest) [xp:levelup?]",
		},
		["reputation"] = {
			["enable"] = true,
			["orientation"] = "HORIZONTAL",
			["height"] = 10,
			["width"] = 410,
			["textSize"] = 9,
			["font"] = E.db.chaoticui.installer.font,
			["tag"] = "[rep:name]: [rep:standing] ([rep:current-max-percent])"
		},
		["azerite"] = {
			["enable"] = true,
			["orientation"] = "HORIZONTAL",
			["height"] = 10,
			["width"] = 410,
			["textSize"] = 9,
			["font"] = E.db.chaoticui.installer.font,
			["tag"] = "[azerite:name] Level: [azerite:level] (XP: [azerite:current] / [azerite:max], [azerite:percent])"
		},
		["honor"] = {
			["enable"] = true,
			["orientation"] = "HORIZONTAL",
			["height"] = 10,
			["width"] = 410,
			["textSize"] = 9,
			["font"] = E.db.chaoticui.installer.font,
			["tag"] = "[name] Honor Level [honor:level] XP: [honor:current]/[honor:max] ([honor:percent])",
			["maxleveltag"] = "[name] [honor:maxlevel]"
		},
	}

	E.db["hideTutorial"] = 1
	E.db["auras"] = {
		["timeYOffset"] = -4,
		["font"] = E.db.chaoticui.installer.font,
		["buffs"] = { ["growthDirection"] = "RIGHT_DOWN" },
		["debuffs"] = { ["growthDirection"] = "RIGHT_DOWN" }
    }
    
    E.db["bagsOffsetFixed"] = true
	E.db["bags"] = {
		["countFontSize"] = 12,
		["itemLevelFont"] = E.db.chaoticui.installer.font,
		["itemLevelFontSize"] = 12,
		["clearSearchOnClose"] = true,
		["useTooltipScanCIng"] = true,
		["countFont"] = E.db.chaoticui.installer.font,
	}

	E.db["gridSize"] = 128
	E.db["currentTutorial"] = 1

	E.db["unitframe"] = {
		["vertstatusbar"] = E.db.chaoticui.installer.texture,
		["colors"] = {
			["castClassColor"] = true,
			["auraBarBuff"] = {
				["r"] = classColor.r,
				["g"] = classColor.g,
				["b"] = classColor.b,
			},
			["health"] = {
				["r"] = 0.1,
				["g"] = 0.1,
				["b"] = 0.1,
			},
		},
		["hud"] = {
			["copied"] = true,
			["hideOOC"] = true,
		},
		["statusbar"] = E.db.chaoticui.installer.texture,
		["font"] = E.db.chaoticui.installer.font,
		["units"] = {
			["pet"] = {
				["enable"] = false,
			},
			["targettarget"] = {
				["enable"] = false,
			},
			["player"] = {
				["enable"] = false,
				["castbar"] = {
					["height"] = 28,
					["width"] = 401.666666666667,
				},
			},
			["focus"] = {
				["enable"] = false,
				["castbar"] = {
					["height"] = 0,
					["width"] = 0,
				},
			},
			["target"] = {
				["debuffs"] = {
					["enable"] = false,
				},
				["smartAuraDisplay"] = "SHOW_DEBUFFS_ON_FRIENDLIES",
				["enable"] = false,
				["buffs"] = {
					["playerOnly"] = {
						["friendly"] = true,
					},
				},
				["castbar"] = {
					["height"] = 0,
					["width"] = 0,
				},
				["aurabar"] = {
					["attachTo"] = "BUFFS",
				},
			},
			["arena"] = {
				["castbar"] = {
					["height"] = 6.66664361953735,
					["width"] = 192.500137329102,
				},
			},
			["boss"] = {
				["growthDirection"] = "RIGHT",
				["castbar"] = {
					["width"] = 214,
					["height"] = 5,
				},
			},
		},
	}
	E.db["datatexts"] = {
		["font"] = E.db.chaoticui.installer.font,
		["actionbar3"] = true,
		["actionbar1"] = true,
		["actionbar5"] = true,
		["panels"] = {
			["SLE_DataPanel_4"] = {
				["right"] = "S&L Item Level",
				["left"] = "Garrison",
				["middle"] = "Orderhall",
			},
			["LeftCoordDtPanel"] = "S&L Friends",
			["RightCoordDtPanel"] = "S&L Guild",
			["SLE_DataPanel_1"] = {
				["left"] = "ChaoticUI Account Item Level",
				["middle"] = "Locked Out",
				["right"] = "Rarity",
			},
			["SLE_DataPanel_6"] = {
				["right"] = "Combat/Arena Time",
				["left"] = "Quick Join",
				["middle"] = "Chat Tweaks",
			},
			["LeftChatDataPanel"] = {
				["left"] = self.DataTextsByRole[self:GetDataText()],
				["right"] = "Mastery",
			},
			["RightMiniPanel"] = "Mounts",
			["LeftMiniPanel"] = "Time",
			["SLE_DataPanel_7"] = "Cecile Meter Overlay",
			["SLE_DataPanel_8"] = {
				["right"] = "Bags",
				["left"] = "S&L Target Range",
				["middle"] = "Talent Loadouts",
			},
			["SLE_DataPanel_2"] = {
				["right"] = "Improved System",
				["left"] = "Broker_TimeToExecute_kill",
				["middle"] = "LegionInvasionTimer",
			},
			["SLE_DataPanel_5"] = {
				["left"] = "DungeonHelper",
				["middle"] = "Archy",
				["right"] = "LDB-WoWToken",
			},
			["RightChatDataPanel"] = {
				["right"] = "S&L Currency",
				["left"] = "Haste",
				["middle"] = "Crit Chance",
			},
			["SLE_DataPanel_3"] = "ChaoticUI Version", 
		},
	}
	local bar7enabled = true;
	if (E.myclass == "DRUID" or E.myclass == "ROGUE") then
		bar7enabled = false;
	end

	E.db["actionbar"] = {
		["font"] = E.db.chaoticui.installer.font,
		["bar3"] = {
			["mouseover"] = true,
			["buttonsPerRow"] = 12,
		},
		["bar6"] = {
			["enabled"] = true,
			["mouseover"] = true,
			["buttons"] = 6,
		},
		["bar1"] = {
			["mouseover"] = true,
		},
		["bar2"] = {
			["enabled"] = true,
			["mouseover"] = true,
			["buttons"] = 6,
		},
		["bar7"] = {
			["buttonspacing"] = 2,
			["backdropSpacing"] = 2,
			["enabled"] = bar7enabled,
			["buttons"] = 6,
			["backdrop"] = false,
			["mouseover"] = true,
			["buttonsize"] = 32,
		},
		["bar8"] = {
			["buttonspacing"] = 2,
			["backdropSpacing"] = 2,
			["enabled"] = false,
			["buttons"] = 6,
			["backdrop"] = false,
			["mouseover"] = true,
			["buttonsize"] = 32,
		},
		["combatstate"] = {
			["bar3"] = {
				["enable"] = true,
			},
			["bar6"] = {
				["enable"] = true,
			},
			["bar2"] = {
				["enable"] = true,
			},
			["bar1"] = {
				["enable"] = true,
			},
			["bar5"] = {
				["enable"] = true,
			},
			["bar7"] = {
				["enable"] = true,
			},
			["bar8"] = {
				["enable"] = true,
			},
		},
		["barPet"] = {
			["mouseover"] = true,
		},
		["bar5"] = {
			["mouseover"] = true,
			["buttonsPerRow"] = 12,
		},
		["bar4"] = {
			["mouseover"] = true,
		},
		["stanceBar"] = {
			["mouseover"] = true,
			["point"] = "BOTTOM",
		},
	}

	E.db["epa"] = {
		["poh"] = {
			["enable"] = false,
		},
	}
	E.db["chat"] = {
		["panelBackdropNameLeft"] = CUI:GetClassTexture(),
		["panelBackdropNameRight"] = CUI:GetFactionTexture(),
		["font"] = E.db.chaoticui.installer.font,
		["tabFont"] = E.db.chaoticui.installer.font,
	}
	E.db["tooltip"] = {
		["font"] = E.db.chaoticui.installer.font,
		['healthBar'] = {
			["font"] = E.db.chaoticui.installer.font,
		}
	}

	E.private["general"] = {
		["glossTex"] = E.db.chaoticui.installer.texture,
		["normTex"] = E.db.chaoticui.installer.texture,
	}

    E.private["skins"]["blizzard"] = {
		["questChoice"] = true,
		["alertframes"] = true,
    }
    
    SetCVar("autoLootDefault", "1");
end

function CI:InstallBaseMovers()
    wipe(E.db.movers);

	-- Action Bars
	CUI:SaveMoverPosition("ElvAB_1", "BOTTOM", ElvUIParent, "BOTTOM", 0, 26);
	CUI:SaveMoverPosition("ElvAB_2", "BOTTOM", ElvUIParent, "BOTTOM", 307, 61);
	CUI:SaveMoverPosition("ElvAB_3", "BOTTOM", ElvUIParent, "BOTTOM", 307, 26);
	CUI:SaveMoverPosition("ElvAB_5", "BOTTOM", ElvUIParent, "BOTTOM", -307, 26);
	CUI:SaveMoverPosition("ElvAB_6", "BOTTOM", ElvUIParent, "BOTTOM", -307, 61);
	CUI:SaveMoverPosition("ElvAB_7", "BOTTOM", ElvUIParent, "BOTTOM", -307, 95);
	CUI:SaveMoverPosition("ShiftAB", "BOTTOM", ElvUIParent, "BOTTOM", -307, 132);
	CUI:SaveMoverPosition("TotemBarMover", "BOTTOMLEFT", ElvUIParent, "BOTTOMLEFT", 415, 0);
	CUI:SaveMoverPosition("BossButton", "BOTTOM", ElvUIParent, "BOTTOM", 0, 227);

	-- Unit Frames
	CUI:SaveMoverPosition("ElvUF_PlayerMover", "BOTTOM", ElvUIParent, "BOTTOM", -278, 110);
	CUI:SaveMoverPosition("ElvUF_TargetTargetMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 110);
	CUI:SaveMoverPosition("ElvUF_TargetMover", "BOTTOM", ElvUIParent, "BOTTOM", 278, 110);
	CUI:SaveMoverPosition("ElvUF_PetMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 150);
	CUI:SaveMoverPosition("ElvUF_BodyGuardMover", "TOP", ElvUIParent, "TOP", -331, -332);
	CUI:SaveMoverPosition("ElvUF_PlayerCastbarMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 42);
	CUI:SaveMoverPosition("BossHeaderMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 182);
	CUI:SaveMoverPosition("ArenaHeaderMover", "BOTTOMRIGHT", ElvUIParent, "BOTTOMRIGHT", -413, 375);
	CUI:SaveMoverPosition("BNETMover", "BOTTOMRIGHT", ElvUIParent, "BOTTOMRIGHT", -4, 226);

	CUI:SaveMoverPosition("LeftChatMover", "BOTTOMLEFT", ElvUIParent, "BOTTOMLEFT", 4, 0);
	CUI:SaveMoverPosition("GMMover", "TOPLEFT", ElvUIParent, "TOPLEFT", 405, -22);
	CUI:SaveMoverPosition("BuffsMover", "TOP", ElvUIParent, "TOP", -317, -303);
	CUI:SaveMoverPosition("DebuffsMover", "TOP", ElvUIParent, "TOP", -317, -399);
	CUI:SaveMoverPosition("BossButton", "BOTTOM", ElvUIParent, "BOTTOM", 0, 195);
	CUI:SaveMoverPosition("FlareMover", "TOP", ElvUIParent, "TOP", 0, -511);
	CUI:SaveMoverPosition("MicrobarMover", "TOPLEFT", ElvUIParent, "TOPLEFT", 0, -20);
	CUI:SaveMoverPosition("VehicleSeatMover", "TOPLEFT", ElvUIParent, "TOPLEFT", 138, -41);
	CUI:SaveMoverPosition("EnemyBattlePetMover", "TOPRIGHT", ElvUIParent, "TOPRIGHT", -415, -446);
	CUI:SaveMoverPosition("MinimapMover", "TOPRIGHT", ElvUIParent, "TOPRIGHT", 0, -20);
	CUI:SaveMoverPosition("SquareMinimapBar", "TOPRIGHT", ElvUIParent, "TOPRIGHT", 0, -219);
	CUI:SaveMoverPosition("FarmPortalAnchor", "TOPLEFT", ElvUIParent, "TOPLEFT", 41, -459);
   
	CUI:SaveMoverPosition("ExperienceBarMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 97);
	CUI:SaveMoverPosition("HonorBarMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 97);
	CUI:SaveMoverPosition("ReputationBarMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 110);
	CUI:SaveMoverPosition("AzeriteBarMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 123);

	CUI:SaveMoverPosition("RightChatMover", "BOTTOMRIGHT", ElvUIParent, "BOTTOMRIGHT", -4, 0);
	CUI:SaveMoverPosition("BattlePetMover", "TOPLEFT", ElvUIParent, "TOPLEFT", 415, -446);
	CUI:SaveMoverPosition("Dashboard", "TOPLEFT", ElvUIParent, "TOPLEFT", 0, -22);
	CUI:SaveMoverPosition("FarmToolAnchor", "TOPLEFT", ElvUIParent, "TOPLEFT", 41, -512);
	CUI:SaveMoverPosition("MinimapButtonAnchor", "TOPRIGHT", ElvUIParent, "TOPRIGHT", -4, -227);
	CUI:SaveMoverPosition("AltPowerBarMover", "TOP", ElvUIParent, "TOP", 0, -196);

	CUI:SaveMoverPosition("UIBFrameMover", "BOTTOMRIGHT", ElvUIParent, "BOTTOMRIGHT", -417, 0);
	CUI:SaveMoverPosition("RaidUtility_Mover", "TOP", ElvUIParent, "TOP", -158, -47);

	CUI:SaveMoverPosition("FarmSeedAnchor", "BOTTOMLEFT", ElvUIParent, "BOTTOMLEFT", 4, 423);
	CUI:SaveMoverPosition("PvPMover", "TOP", ElvUIParent, "TOP", 0, -93);

	CUI:SaveMoverPosition("AlertFrameMover", "TOP", ElvUIParent, "TOP", 0, -281);
    CUI:SaveMoverPosition("RaidMarkerBarAnchor", "BOTTOMRIGHT" ,ElvUIParent, "BOTTOMRIGHT" ,-650, 27);
	CUI:SaveMoverPosition("TalkingHeadFrameMover", "BOTTOM", ElvUIParent, "BOTTOM", 0, 263);
end

function CI:ChaoticUIDBSetup()
	E.db["chaoticui"]["autolog"] = {
		["enabled"] = true,
	}
    E.db["chaoticui"]["buttonStyle"] = {
		["texture"] = E.db.chaoticui.installer.texture,
	}
	E.db["chaoticui"]["chaoticchat"] = {
		["windows"] = {
			["font"] = E.db.chaoticui.installer.font,
		}
	}
	E.db["chaoticui"]["cooldownBar"] = {
		["autohide"] = true,
	}
	E.db["chaoticui"]["enhancedshadows"] = {
		["classcolor"] = true,
	}
	E.db["chaoticui"]["hiddenArtifactTracker"] = {
		["font"] = E.db.chaoticui.installer.font,
	}
	E.db["chaoticui"]["pxp"] = {
		["texture"] = E.db.chaoticui.installer.texture,
		["font"] = E.db.chaoticui.installer.font,
	}
	E.db["chaoticui"]["utilitybars"] = {
		["hideincombat"] = true,
		["baitBar"] = {
			["mouseover"] = true,
		},
		["equipmentManagerBar"] = {
			["mouseover"] = true,
		},
		["portalBar"] = {
			["enabled"] = true,
			["mouseover"] = true,
		},
		["professionBar"] = {
			["mouseover"] = true,
		},
		["raidPrepBar"] = {
			["enabled"] = true,
			["mouseover"] = true,
		},
		["specSwitchBar"] = {
			["enabled"] = true,
			["mouseover"] = true,
		},
		["toybar"] = {
			["mouseover"] = true,
			["buttonsPerRow"] = 6,
		}
	}
	E.db["chaoticui"]["raidCDs"] = {
		["solo"] = true,
		["texture"] = E.db.chaoticui.installer.texture,
		["font"] = E.db.chaoticui.installer.font,
	}
	E.db["chaoticui"]["vuf"] = { 
		["hideOOC"] = true,
		["copied"] = true,
		["hudcopied"] = true,
		["units"] = { ["pettarget"] = { ["enabled"] = false; } };
    }
   
	E.db["chaoticui"]["warlockdemons"] = {
		["font"] = E.db.chaoticui.installer.font,
		["texture"] = E.db.chaoticui.installer.texture,
	}
	
    for unit, tbl in pairs(P.chaoticui["vuf"].units) do
		for element, etbl in pairs(tbl) do
			if (etbl and type(etbl) == 'table' and etbl.value and etbl.value.tag) then
				 E.db.chaoticui.vuf.units[unit] = E.db.chaoticui.vuf.units[unit] or {};
				 E.db.chaoticui.vuf.units[unit][element] = E.db.chaoticui.vuf.units[unit][element] or {};
				 E.db.chaoticui.vuf.units[unit][element].value = E.db.chaoticui.vuf.units[unit][element].value or {};
				 E.db.chaoticui.vuf.units[unit][element].value.tag = ''..etbl.value.tag;
			end
		end
	end

    E.private.chaoticui.mounts.favAlt = 460;
end

function CI:InstallChaoticUIMovers()
    CUI:SaveMoverPosition("ChaoticUI_SpecSwitchBarMover", "BOTTOM", ElvUIParent, "BOTTOM", 300, 170);
    CUI:SaveMoverPosition("ChaoticUI_PartyXPHolderMover", "TOP", ElvUIParent, "TOP", 0, -167);
	CUI:SaveMoverPosition("ArtifactHiddenAppearanceTrackerMover", 'BOTTOM', ElvUIParent, 'BOTTOM', 0, 134);
	CUI:SaveMoverPosition("ChaoticChatDockMover", "TOP", ElvUIParent, "TOP", 194, -45);
    CUI:SaveMoverPosition("ChaoticUI_EquipmentSetsMover", "BOTTOMLEFT", ElvUIParent, "BOTTOMLEFT", 419, 21);
	CUI:SaveMoverPosition("CooldownBarMover", "BOTTOM", ElvUI_Bar1, "TOP", 0, 2);
    CUI:SaveMoverPosition("ChaoticUI_ToyBarMover", "BOTTOM", ElvUIParent, "BOTTOM", 307, 95);
	CUI:SaveMoverPosition("ChaoticUI_PortalBarMover", "TOPRIGHT", ElvUIParent, "TOPRIGHT", -180, -20);
	CUI:SaveMoverPosition("ChaoticUI_ProfessionBarMover", "TOPRIGHT", ChaoticUI_PortalBar, "BOTTOMRIGHT", 0, -4);
	CUI:SaveMoverPosition('ChaoticUF_Player AuraBar Mover', 'BOTTOM', ElvUIParent, 'BOTTOM', -310, 424);
	CUI:SaveMoverPosition('ChaoticUF_Target AuraBar Mover', 'BOTTOM', ElvUIParent, 'BOTTOM', 310, 424);
	CUI:SaveMoverPosition('ChaoticUF_Player Castbar Mover', 'BOTTOM', ElvUIParent, 'BOTTOM', 0, 371);
    CUI:SaveMoverPosition('ChaoticUF_Target Castbar Mover', 'BOTTOM', ElvUIParent, 'BOTTOM', 0, 346);
    CUI:SaveMoverPosition('AoE CCMover', 'TOPLEFT', ElvUIParent, 'TOPLEFT', 0, -20);

    if (E.myclass == "WARLOCK") then
    	CUI:SaveMoverPosition('WarlockDemonsMover', 'TOPLEFT', ElvUIParent, 'TOPLEFT', 413, -20);
    end

    local VUF = CUI:GetModule('VerticalUnitFrames');
    VUF:ResetFramePositions();
end

function CI:NonHealDBSetup()
    E.db["unitframe"]["units"]["raid40"] = {
		["visibility"] = "[@raid31,noexists] hide;show",
	}
	E.db["unitframe"]["units"]["raid"] = {
		["raidWideSorting"] = false,
		["numGroups"] = 6,
		["visibility"] = "[@raid6,noexists][@raid31,exists] hide;show",
	}
	E.db["unitframe"]["units"]["party"] = {
		["roleIcon"] = {
			["yOffset"] = -4,
			["attachTo"] = "Frame",
			["position"] = "TOP",
		},
    }
end

function CI:InstallNonHealMovers()
    CUI:SaveMoverPosition("ElvUF_RaidMover", "TOPLEFT", ElvUIParent, "BOTTOMLEFT", 4, 526);
	CUI:SaveMoverPosition("ElvUF_RaidpetMover", "TOPLEFT", ElvUIParent, "BOTTOMLEFT", 4, 752);
	CUI:SaveMoverPosition("ElvUF_Raid40Mover", "BOTTOMLEFT", ElvUIParent, "BOTTOMLEFT", 4, 548);
	CUI:SaveMoverPosition("ElvUF_PartyMover", "BOTTOMLEFT", ElvUIParent, "BOTTOMLEFT", 4, 247);
	CUI:SaveMoverPosition("ElvUF_TankMover", "TOPLEFT", ElvUIParent, "BOTTOMLEFT", 188, 591);
    CUI:SaveMoverPosition("ElvUF_AssistMover", "TOPLEFT", ElvUIParent, "BOTTOMLEFT", 188, 695);
end

function CI:HealerDBSetup()
    E.db["unitframe"]["units"]["party"] = {
		["horizontalSpacing"] = 9,
		["debuffs"] = {
			["sizeOverride"] = 16,
			["yOffset"] = -7,
			["anchorPoint"] = "TOPRIGHT",
			["xOffset"] = -4,
		},
		["health"] = {
			["frequentUpdates"] = true,
			["position"] = "BOTTOM",
			["text_format"] = "[healthcolor][health:deficit]",
		},
		["growthDirection"] = "LEFT_UP",
		["power"] = {
			["text_format"] = "",
		},
		["verticalSpacing"] = 9,
		["roleIcon"] = {
			["position"] = "BOTTOMRIGHT",
		},
		["GPSArrow"] = {
			["size"] = 40,
		},
		["healPrediction"] = true,
		["width"] = 80,
		["name"] = {
			["text_format"] = "[namecolor][name:short]",
			["position"] = "TOP",
		},
		["height"] = 45,
		["buffs"] = {
			["xOffset"] = 50,
			["yOffset"] = -6,
			["clickThrough"] = true,
			["useBlacklist"] = false,
			["noDuration"] = false,
			["playerOnly"] = false,
			["perrow"] = 1,
			["useFilter"] = "TurtleBuffs",
			["noConsolidated"] = false,
			["sizeOverride"] = 22,
			["enable"] = true,
		},
	}
	E.db["unitframe"]["units"]["raid40"] = {
		["growthDirection"] = "LEFT_UP",
		["healPrediction"] = true,
		["health"] = {
			["frequentUpdates"] = true,
		},
		["height"] = 30,
	}
	E.db["unitframe"]["units"]["raidpet"] = { ["enabled"] = true };
	E.db["unitframe"]["units"]["raid"] = {
		["horizontalSpacing"] = 9,
		["debuffs"] = {
			["sizeOverride"] = 16,
			["xOffset"] = -4,
			["yOffset"] = -7,
			["anchorPoint"] = "TOPRIGHT",
			["enable"] = true,
		},
		["growthDirection"] = "LEFT_UP",
		["verticalSpacing"] = 9,
		["rdebuffs"] = {
			["enable"] = false,
		},
		["raidWideSorting"] = false,
		["healPrediction"] = true,
		["health"] = {
			["frequentUpdates"] = true,
		},
		["height"] = 45,
		["buffs"] = {
			["xOffset"] = 50,
			["yOffset"] = -6,
			["clickThrough"] = true,
			["useBlacklist"] = false,
			["noDuration"] = false,
			["playerOnly"] = false,
			["perrow"] = 1,
			["useFilter"] = "TurtleBuffs",
			["noConsolidated"] = false,
			["sizeOverride"] = 22,
			["enable"] = true,
		},
    }
end

function CI:InstallHealMovers()
    CUI:SaveMoverPosition("ElvUF_Raid40Mover", "BOTTOMRIGHT", ElvUIParent, "BOTTOMLEFT", 550, 343);
	CUI:SaveMoverPosition("ElvUF_RaidMover", "BOTTOMRIGHT", ElvUIParent, "BOTTOMLEFT", 550, 343);
	CUI:SaveMoverPosition("ElvUF_PartyMover", "BOTTOMRIGHT", ElvUIParent, "BOTTOMLEFT", 550, 559);
	CUI:SaveMoverPosition("ElvUF_RaidpetMover", "TOPLEFT", ElvUIParent, "BOTTOMLEFT", 208, 341);
	CUI:SaveMoverPosition("ElvUF_TankMover", "TOPLEFT", ElvUIParent, "BOTTOMLEFT", 430, 341);
    CUI:SaveMoverPosition("ElvUF_AssistMover", "TOPLEFT", ElvUIParent, "BOTTOMLEFT", 430, 276);
end

function CI:AddProfile(db)
	db.profileKeys[self.profileKey] = self.profileKey;
end

function CI:SetProfile(db, tbl)
	db.profiles[self.profileKey] = tbl;
end

function CI:RegisterAddOnInstaller(addonName, func)
    if (not CI.AddOnSetupFunctions) then
        CI.AddOnSetupFunctions = {};
    end
    CI.AddOnSetupFunctions[addonName] = func;
end

function CI:RunAddOnInstallers()
	for addon, setupFunc in pairs(self.AddOnSetupFunctions) do
		if (not IsAddOnLoaded(addon)) then
			LoadAddOn(addon);
		end
		if (IsAddOnLoaded(addon)) then
			self[setupFunc](self);
        end
	end
end

function CI:CharacterSpecificSetup(realm, name)
    if (not CI.HasCharacterSpecificSetup) then
        return;
    end
    if (CI.HasCharacterSpecificSetup[realm]) then
        if (CI.HasCharacterSpecificSetup[realm][name]) then
            self[CI.HasCharacterSpecificSetup[realm][name]](self);
        end
    end
end

local features_supported = {
	["elvui"] = "ElvUI",
	["elvui_sle"] = "ElvUI_SLE",
	["addonskins"] = "AddOnSkins",
	["xct+"] = "XCT+",
	["xct+beta"] = "xCT_Beta",
	["details"] = "Details",
	["!kalielstracker"] = "!KalielsTracker",
	["inflight"] = "InFlight",
	['castbaroverlay'] = 'ElvUI_CastBarOverlay',
	['everysecondcounts'] = 'ElvUI_EverySecondCounts',
	['locplus'] = 'ElvUI_LocPlus',
	['visualauratimers'] = 'ElvUI_VisualAuraTimers',
	['combatstate'] = 'ElvUI_ActionbarCombatState',
	['extraactionbars'] = 'ElvUI_ExtraActionbars',
	['dtcolors'] = 'ElvUI_DTColors',
	['paragonrep'] = 'ParagonReputation',
	['ozcooldowns'] = 'OzCooldowns',
	['ifilger'] = 'iFilger',
	['projectazilroka'] = 'ProjectAzilroka',
	['enhancedpetbattleui'] = 'EnhancedPetBattleUI'
};

function CI:GetFeatureSet()
	local features = {};

	for feature, addon in pairs(features_supported) do
		if (IsAddOnLoaded(addon)) then
			tinsert(features, feature);
		end
	end

	return features;
end

function CI:AreFeaturesEqual(featureSetA, featureSetB)
	for i = 1, #featureSetA do
		if (not tContains(featureSetB, featureSetA[i])) then
			return false;
		end
	end

	return true;
end

CI.installVersion = { 6.00, 4 };

function CI:ShouldInstall()
	if (not E.private.chaoticui.installed) then
		return true;
	end
	local currentFeatureSet = self:GetFeatureSet();

	local prevFeatureSet = E.private.chaoticui.features or {};
	local hasNewInstall = type(E.private.chaoticui.installed) == "table" and E.private.chaoticui.installed.version ~= nil;
	if (not hasNewInstall) then
		return true;
	end

	if (E.private.chaoticui.installed.version < CI.installVersion[1] or E.private.chaoticui.installed.installerBuild < CI.installVersion[2]) then
		return true;
	end

	if (not self:AreFeaturesEqual(currentFeatureSet, prevFeatureSet)) then
		return true;
	end

	return false;
end

function CI:SaveInstallerVersion()
	E.private.chaoticui.installed = { ["version"] = CI.installVersion[1], ["installerBuild"] = CI.installVersion[2] };
end

function CI:GetCasterRole()
	if (E:CheckTalentTree(E.HealingClasses[E.myclass])) then
		return 'Healer';
	else
		return 'Caster DPS';
	end
end

function CI:GetDataText()
	if (E.role == "Tank") then
		return "tank";
	elseif (E.role == "Melee") then
		return "dpsMelee";
	elseif (E.role == "Caster") then
		if (self:GetCasterRole() == "Caster DPS") then
			return "dpsCaster";
		else
			return "healer";
		end
	end
end

function CI:Run()
    self:BaseDBSetup();
    self:ChaoticUIDBSetup();
    self:InstallBaseMovers();
    self:InstallChaoticUIMovers();
    if (E.role == "Caster" and self:GetCasterRole() == "Healer") then
        self:HealerDBSetup();
        self:InstallHealMovers();
    else
        self:NonHealDBSetup();
        self:InstallNonHealMovers();
    end
    self:RunAddOnInstallers();
    self:CharacterSpecificSetup(E.myname, E.myrealm);
    local CM = CUI:GetModule('Migration')
	CM:CheckMigrations();
	self:SaveInstallerVersion();
    E.private["chaoticui"]["features"] = self:GetFeatureSet();
end

CI.installTable = {
	["Name"] = "|cfff02020ChaoticUI|r",
	["Title"] = "|cfff02020ChaoticUI|r Installation",
	["tutorialImage"] = [[Interface\AddOns\ElvUI_ChaoticUI\media\textures\elvui_chaoticui_logo.tga]],
	["Pages"] = {
		[1] = function()
			PluginInstallFrame.SubTitle:SetText(format(L["Welcome to |cfff02020ChaoticUI|r version %s!"], CUI.version))
			PluginInstallFrame.Desc1:SetText(L["This will take you through a quick install process to setup ChaoticUI.\nIf you choose to not setup any options through this config, click Skip to finsh the installation."])
			PluginInstallFrame.Desc2:SetText("")

			PluginInstallFrame.Option1:Show()
			PluginInstallFrame.Option1:SetScript("OnClick", function()
				CI:SaveInstallerVersion();
				E.private["chaoticui"]["features"] = CI:GetFeatureSet();
				ReloadUI();
			end);
			PluginInstallFrame.Option1:SetText("Skip");	
		end,
		[2] = function()
			local VUF = CUI:GetModule("VerticalUnitFrames");

			PluginInstallFrame.SubTitle:SetText("Vertical Unit Frames");
			PluginInstallFrame.Desc1:SetText("Choose if you want to use Vertical Unit Frames only, ElvUI Unit Frames only, or both.");
			PluginInstallFrame.Desc2:SetText("");

			PluginInstallFrame.Option1:Show();
			PluginInstallFrame.Option1:SetText("VUF Only");
			PluginInstallFrame.Option1:SetScript("OnClick", function()
				E.db.chaoticui.vuf.enabled = true; E.db.chaoticui.vuf.hideElv = true; VUF:Enable();
			end);

			PluginInstallFrame.Option2:Show();
			PluginInstallFrame.Option2:SetText("ElvUI Only");
			PluginInstallFrame.Option2:SetScript("OnClick", function()
				E.db.chaoticui.vuf.enabled = false; VUF:Enable();
			end);

			PluginInstallFrame.Option3:Show();
			PluginInstallFrame.Option3:SetText("Both");
			PluginInstallFrame.Option3:SetScript("OnClick", function()
				E.db.chaoticui.vuf.enabled = true; E.db.chaoticui.vuf.hideElv = false; VUF:Enable();
			end);
		end,
		[3] = function()
			PluginInstallFrame.SubTitle:SetText("Use Authors Defaults");
			PluginInstallFrame.Desc1:SetText("Choose your role if you would like to setup ChaoticUI to match the authors defaults for your role.\nChoose finish if you would like to keep your current setup.");
			PluginInstallFrame.Desc2:SetText("");
			
			E:CheckRole();
			PluginInstallFrame.Option1:Show()
			PluginInstallFrame.Option1:SetScript("OnClick",function()
				CI:Run();
				ReloadUI();
			end);
			PluginInstallFrame.Option1:SetText(E.role == "Tank" and "Tank" or E.role == "Caster" and CI:GetCasterRole() or E.role == "Melee" and "Melee DPS")

			PluginInstallFrame.Option2:Show()
			PluginInstallFrame.Option2:SetScript("OnClick", function()
				CI:SaveInstallerVersion();
				E.private["chaoticui"]["features"] = CI:GetFeatureSet();
				local CM = CUI:GetModule('Migration')
				CM:CheckMigrations();
				ReloadUI();
			end);
			PluginInstallFrame.Option2:SetText("Finish");
		end
	},
}
function CI:Install()
	E:GetModule("PluginInstaller"):Queue(CI.installTable);
end

function CI:Initialize()
	self.profileKey = ("%s - %s"):format(E.myname, E.myrealm);
	if (self:ShouldInstall()) then	
		self:Install();
	end
end

CUI:RegisterModule(CI:GetName());