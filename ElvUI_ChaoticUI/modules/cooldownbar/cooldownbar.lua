local CUI, E, L, V, P, G = unpack(select(2, ...))
local CB = CUI:NewModule('CooldownBar','AceTimer-3.0', 'AceEvent-3.0');
local LSM = LibStub("LibSharedMedia-3.0");

local BS = CUI:GetModule("ButtonStyle");
local ES = CUI:GetModule("EnhancedShadows");

MAX_CB_VAL = math.pow(120, 0.3);

function CB:GetPosition(value)
	local val = math.pow(value, 0.3);
	if (not val or type(val) ~= 'number') then
		return 0;
	end
	local r = val / MAX_CB_VAL;
	return r > 1 and 1 or r;
end

function CB:CreateLabel(value)
	local fs = self.bar:CreateFontString(nil, "OVERLAY");
	fs:FontTemplate(LSM:Fetch("font", E.db.general.font), 12, "THINOUTLINE");
	local str = value;
	if (value % 60 == 0) then
		str = (value / 60) .. "m";
	end

	fs:SetText(str);

	local d = self.bar:GetHeight() / 2;
	local r = self.bar:GetWidth();
	local l = fs:GetWidth();

	local pos = self:GetPosition(value) * (r - d);

	if (pos + l > r) then
		pos = r - l;
	end

	fs:SetPoint("CENTER", self.bar, "LEFT", pos, 0);
	fs:Show();
end

function CB:CreateLabels()
	for _, value in pairs(self.values) do
		self:CreateLabel(value);
	end
end

function CB:CreateBar()
	local bar = CreateFrame("Frame", nil, E.UIParent);
	bar:SetWidth(ElvUI_Bar1:GetWidth());
	bar:SetHeight(ElvUI_Bar1Button1:GetHeight());
	bar:SetPoint("CENTER");
	E:CreateMover(bar, 'CooldownBarMover', 'Cooldown Bar', nil, nil, nil, 'ALL,ACTIONBARS');
	RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');
	bar:SetTemplate("Default");
	bar:SetAlpha(self.db.alpha);
	bar:CreateShadow();
	ES:RegisterShadow(bar.shadow);

	self.bar = bar;
end

function CB:CreateFlashFrame()
	local frame = CreateFrame("Frame", 'ChaoticUICooldownFlash', E.UIParent)
	frame:Size(self.db.cooldownFlash.size)
	frame:Point("CENTER", E.UIParent, "CENTER", 0, 140);

	frame:SetTemplate()
	local ES = CUI:GetModule("EnhancedShadows");
	frame:CreateShadow();
	ES:RegisterShadow(frame.shadow);
	frame:SetAlpha(0)

	frame.icon = frame:CreateTexture(nil, "OVERLAY")
	frame.icon:SetInside()
	frame.icon:SetTexCoord(unpack(E.TexCoords))

	E:CreateMover(frame, 'CooldownFlashMover', 'Cooldown Flash', nil, nil, nil, 'ALL,ACTIONBARS');

	return frame;
end

function CB:UpdateFlashFrame()
	self.flashFrame:Size(self.db.cooldownFlash.size);
end

function CB:CreateFrame(type, id)
	local frame = tremove(self.usedFrames);

	if (not frame) then
		frame = CreateFrame("Frame", nil, self.bar);
		frame:Size(32, 32);
		frame:SetTemplate("Default");
		
		local tex = frame:CreateTexture(nil, "ARTWORK");
		tex:SetInside(frame);

		frame.tex = tex;

		frame:SetScript("OnEnter", function()
			if ((frame.type =="spell" and frame.spellID) or (frame.type == "item" and frame.itemID)) then
				GameTooltip:SetOwner(frame, "ANCHOR_RIGHT");
				if (frame.type =="spell" and frame.spellID) then
					GameTooltip:SetSpellByID(frame.spellID);
				else
					GameTooltip:SetItemByID(frame.itemID);
				end
				GameTooltip:Show();
			end
		end);

		frame:SetScript("OnLeave", GameTooltip_Hide);

		frame:SetScript("OnMouseDown", function(_, button)
			if (button == "RightButton") then
				local message;
				if (frame.type == "item") then
					message = "|cffff0000Blacklisted|r the cooldown for |cffffff00item|r" .. GetItemInfo(frame.itemID);
					self.db.blacklist.items[frame.itemID] = true;
				else
					message = "|cffff0000Blacklisted|r the cooldown for |cffff00ffspell|r" .. GetSpellInfo(frame.spellID);
					self.db.blacklist.spells[frame.spellID] = true;
				end
			end
		end);
		frame.cooldown = CreateFrame("Cooldown", nil, frame, "CooldownFrameTemplate");
		frame.cooldown:SetAllPoints(frame);

		frame.cooldown:SetDrawBling(false)
		frame.cooldown.SetDrawBling = E.noop
		frame.cooldown:SetDrawEdge(false)
		frame.cooldown.SetDrawEdge = E.noop
		frame.cooldown:SetDrawSwipe(false)
		frame.cooldown.SetDrawSwipe = E.noop

		E:RegisterCooldown(frame.cooldown)

		BS:StyleButton(frame);
	end

	frame.type = type;
	frame[type.."ID"] = id;
	if (type == "spell") then
		frame.tex:SetTexture(select(3, GetSpellInfo(id)));
	else
		frame.tex:SetTexture(select(10, GetItemInfo(id)));
	end
	self.frameLevelSerial = self.frameLevelSerial + 5;
	frame.nativeFrameLevel = self.frameLevelSerial;
	frame:SetFrameLevel(frame.nativeFrameLevel);
	frame:SetAlpha(1);

	tinsert(self.liveFrames, frame);
	self:UpdateFrame(frame);
end

function CB:OnFrameUpdate(t)
	self.delta = self.delta + t;

	if (self.delta < 0.02) then
		return;
	end

	self.delta = self.delta - 0.02;

	self:Update();
end

function CB:AddToOverlapGroup(frame)
	tinsert(self.overlapGroups, frame);
end

function CB:FindFrameIndexInOverlapGroup(frame)
	for i = 1, #self.overlapGroups do
		if (self.overlapGroups[i] == frame) then
			return i;
		end
	end

	return -1;
end

function CB:RemoveFromOverlapGroup(frame)
	tremove(self.overlapGroups, self:FindFrameIndexInOverlapGroup(frame));
end

function CB:InOverlapGroup(frame)
	return tContains(self.overlapGroups, frame);
end

function CB:RotateOverlapGroups()
	local frame = tremove(self.overlapGroups);
	if (frame and frame:IsShown()) then
		self.frameLevelSerial = self.frameLevelSerial + 5;
		frame:SetFrameLevel(self.frameLevelSerial);
		tinsert(self.overlapGroups, 1, frame);
	end
end

function CB:CheckOverlap(current)
	local l, r = current:GetLeft(), current:GetRight()

	if not l or not r then
		return
	end

	local seenOverlap = false;

	for _, icon in ipairs(self.liveFrames) do
		if icon ~= current then
			local ir, il = icon:GetLeft(), icon:GetRight()

			if (ir and il) then
				if (ir >= l and ir <= r) or (il >= l and il <= r) then
					local overlap = math.min(math.abs(ir - l), math.abs(il - r))
					
					if overlap >= 0 then
						seenOverlap = true;
						if (not self:InOverlapGroup(current)) then
							self:AddToOverlapGroup(current);
						end
						if (not self:InOverlapGroup(icon)) then
							self:AddToOverlapGroup(icon);
						end
					end
				end
			end
		end
	end

	if (not seenOverlap and self:InOverlapGroup(current)) then
		self:RemoveFromOverlapGroup(current);
	end
end

function CB:UpdateSpells()
	for spellID, _ in pairs(self.cache) do
		if (self:SpellIsOnCooldown(spellID)) then
			local frame = self:FindFrame("spell", spellID);
			if (frame) then
				self:UpdateFrame(frame);
			else
				self:CreateFrame("spell", spellID);
			end
			self:Activate();
		end
	end
	self:Update();
end

function CB:UpdateItems()
	for i = 1, 18 do
		local start, duration, active = GetInventoryItemCooldown("player", i)

		if active == 1 and start > 0 and duration > 1.5 then
			local id = GetInventoryItemID("player",i)

			if id then
				local frame = self:FindFrame("item", id);
				if (frame) then
					self:UpdateFrame(frame);
				else
					self:CreateFrame("item", id);
				end
				self:Activate();
			end
		end
	end

	for i = 0, 4 do
		local slots = GetContainerNumSlots(i)

		for j = 1, slots do
			local start, duration, active = GetContainerItemCooldown(i,j)

			if active == 1 and start > 0 and duration > 1.5 then
				local id = GetContainerItemID(i,j)

				if id then
					local frame = self:FindFrame("item", id);
					if (frame) then
						self:UpdateFrame(frame);
					else
						self:CreateFrame("item", id);
					end
					self:Activate();
				end
			end
		end
	end
end

function CB:UpdateToys()
	for i = 1, C_ToyBox.GetNumToys() do
		local id = C_ToyBox.GetToyFromIndex(i);

		local start, duration, active = GetItemCooldown(id);

		if active == 1 and start > 0 and duration > 1.5 then
			local frame = self:FindFrame("item", id);
			if (frame) then
				self:UpdateFrame(frame);
			else
				self:CreateFrame("item", id);
			end
			self:Activate();
		end
	end
end

function CB:PlayFlashAnimation(frame)
	if (not self.db.cooldownFlash.enabled) then
		return;
	end

	local texture = self:GetTexture(frame);
	self.flashFrame.icon:SetTexture(texture);

	E:UIFrameFadeIn(self.flashFrame, 0.2, self.flashFrame:GetAlpha(), self.db.cooldownFlash.alpha)

	C_Timer.After(self.db.cooldownFlash.length, function()
		E:UIFrameFadeOut(self.flashFrame, 0.2, self.flashFrame:GetAlpha(), 0);
	end);
end

function CB:Update()
	local framesToRemove = {};

	for _, frame in pairs(self.liveFrames) do
		if (frame.type == "spell" and not self:SpellIsOnCooldown(frame.spellID)) then
			frame:Hide();
			tinsert(framesToRemove, frame);
		elseif (frame.type == "item" and not self:ItemIsOnCooldown(frame.itemID)) then
			frame:Hide();
			tinsert(framesToRemove, frame);
		else
			self:UpdateFrame(frame);
			self:Activate();
		end
	end

	local frameFading = false;
	for _, frame in pairs(framesToRemove) do
		tremove(self.liveFrames, self:FindIndexForFrame(frame));
		E:UIFrameFadeOut(frame, 0.2, frame:GetAlpha(), 0);
		frameFading = true;
		C_Timer.After(0.2, function() self:PlayFlashAnimation(frame); frame:Hide(); frame:SetAlpha(1) end);
		tinsert(self.usedFrames, frame);
	end

	if (#self.liveFrames == 0) then
		if (not frameFading) then
			self:Deactivate();
		else
			C_Timer.After(0.2, function() self:Deactivate() end);
		end
	end
end

function CB:UpdateFrame(frame)
	local cd, s, d = self:GetCooldown(frame);

	if (not cd) then
		return;
	end

	local w = self.bar:GetWidth() - (self.bar:GetHeight() / 2);

	local pos = self:GetPosition(cd) * w;

	if (cd > 0) then
		frame.cooldown:SetCooldown(s, d);
		frame.cooldown:Show();
	end

	frame.tex:SetTexture(self:GetTexture(frame))

	frame:ClearAllPoints();
	frame:SetPoint("CENTER", self.bar, "LEFT", pos, 0);
	frame:SetAlpha(1);
	frame:Show();

	self:CheckOverlap(frame);
end

function CB:FindFrame(type, id)
	for _, frame in pairs(self.liveFrames) do
		if (frame.type == type and frame[type.."ID"] == id) then
			return frame;
		end
	end

	return nil;
end

function CB:FindIndexForFrame(frame)
	for i, f in pairs(self.liveFrames) do
		if (f == frame) then
			return i;
		end
	end

	return 0;
end

function CB:Activate()
	if (self.db.autohide) then
		if (self.bar:IsVisible()) then
			E:UIFrameFadeIn(self.bar, 0.2, self.bar:GetAlpha(), self.db.alpha);
		end
	end
	self.bar:SetScript("OnUpdate", function(s, e) CB:OnFrameUpdate(e) end);
end

function CB:Deactivate()
	if (#self.liveFrames == 0) then
		self.bar:SetScript("OnUpdate", nil);

		if (self.db.autohide) then
			if (not (self.bar:IsMouseOver() or (UnitAffectingCombat("player") or UnitAffectingCombat("pet")))) then
				E:UIFrameFadeOut(self.bar, 0.2, self.bar:GetAlpha(), 0);
			end
		end
	end
end

function CB:SpellIsOnCooldown(spellID)
	if (not spellID or self.db.blacklist.spells[spellID]) then
		return false;
	end

	local start, duration = GetSpellCooldown(spellID);

	if (start ~= 0 and duration > 1.5) then
		return true;
	end

	return false;
end

function CB:ItemIsOnCooldown(itemID)
	if (self.db.blacklist.items[itemID]) then
		return false;
	end

	local start, duration = GetItemCooldown(itemID);

	if (start ~= 0 and duration > 1.5) then
		return true;
	end

	return false;
end

function CB:GetCooldown(frame)
	local start, duration;
	if (frame.type == "spell") then
		start, duration = GetSpellCooldown(frame.spellID);
	else
		start, duration = GetItemCooldown(frame.itemID);
	end

	if (start ~= 0 and duration > 1.5) then
		return (start + duration) - GetTime(), start, duration;
	end

	return 0;
end

function CB:GetTexture(frame)
	if (frame.type == "spell") then
		return select(3, GetSpellInfo(frame.spellID));
	else
		return select(10, GetItemInfo(frame.itemID));
	end
end

function CB:BAG_UPDATE_COOLDOWN()
	self:UpdateItems();
end

function CB:GET_ITEM_INFO_RECEIVED()
	self:UpdateItems();
end

function CB:UNIT_SPELLCAST_SUCCEEDED(_, ...)
	local unitID, _, spellID = ...;

	if (unitID ~= "player" and unitID ~= "pet") then
		return;
	end

	C_Timer.After(.2, function()
		local onCooldown = self:SpellIsOnCooldown(spellID);

		if (onCooldown) then
			self:CreateFrame("spell", spellID);
		end
	end)
end

function CB:SPELL_UPDATE_COOLDOWN()
	self:UpdateSpells();
end

function CB:SPELLS_CHANGED()
	self:UpdateCache();
end

function CB:PLAYER_REGEN_ENABLED()
	self:Deactivate();
end

function CB:PLAYER_REGEN_DISABLED()
	self:Activate();
end

function CB:PLAYER_ENTERING_WORLD()
	for _, frame in pairs(self.liveFrames) do
		frame:SetAlpha(1);
	end
	self:UnregisterEvent("PLAYER_ENTERING_WORLD");
end

function CB:ClearCache()
	wipe(self.cache);
end

function CB:UpdateCache()
	self:ClearCache();

	self:CacheSpells(BOOKTYPE_SPELL);
	self:CacheSpells(BOOKTYPE_PET);

	self:UpdateSpells();
end

function CB:CacheSpells(book)
	for i = 1, 1000 do
		local skillType, _, _, _, _, _, id = GetSpellBookItemInfo(i, book)
		if skillType and id and id > 0 then
			self.cache[id] = true
		end
	end
end

function CB:Enable(fromSettings)
	self:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED");
	self:RegisterEvent("SPELL_UPDATE_COOLDOWN");
	self:RegisterEvent("SPELLS_CHANGED");
	self:RegisterEvent("PLAYER_REGEN_ENABLED");
	self:RegisterEvent("PLAYER_REGEN_DISABLED");
	self:RegisterEvent("BAG_UPDATE_COOLDOWN");
	self:RegisterEvent("PLAYER_ENTERING_WORLD");
	self:RegisterEvent('GET_ITEM_INFO_RECEIVED');

	self.bar:SetScript("OnEnter", function() self:Activate() end);
	self.bar:SetScript("OnLeave", function() self:Deactivate() end);
	self.bar:Show();

	if (not fromSettings) then
		self.ticker = C_Timer.NewTicker(self.db.switchTime, function() self:RotateOverlapGroups() end);
	else
		self:UpdateCache();
		self:UpdateItems();
	end
end

function CB:Disable()
	self:UnregisterEvent("UNIT_SPELLCAST_SUCCEEDED");
	self:UnregisterEvent("SPELL_UPDATE_COOLDOWN");
	self:UnregisterEvent("SPELLS_CHANGED");
	self:UnregisterEvent("PLAYER_REGEN_ENABLED");
	self:UnregisterEvent("PLAYER_REGEN_DISABLED");
	self:UnregisterEvent("BAG_UPDATE_COOLDOWN");
	self:UnregisterEvent("PLAYER_ENTERING_WORLD");
	self:UnregisterEvent('GET_ITEM_INFO_RECEIVED');

	self.bar:SetScript("OnEnter", nil);
	self.bar:SetScript("OnLeave", nil);
	self.bar:Hide();

	self.ticker:Cancel();
end

function CB:UpdateSettings()
	if (self.db.enabled) then
		self:Enable(true);
	else
		self:Disable();
		return;
	end

	self.bar:SetAlpha(self.db.alpha);
	self.ticker:Cancel();
	self.ticker = C_Timer.NewTicker(self.db.switchTime, function() self:RotateOverlapGroups() end);
end

function CB:Initialize()
	self.delta = 0;
	self.frameLevelSerial = 0;

	CUI:RegisterDB(self, "cooldownBar")
	self.db.blacklist = self.db.blacklist or {};
	self.db.blacklist.spells = self.db.blacklist.spells or {};
	self.db.blacklist.items = self.db.blacklist.items or {};

	self.values = { 1, 10, 30, 60, 120 };

	self:CreateBar();
	self:CreateLabels();

	self.flashFrame = self:CreateFlashFrame();

	self.liveFrames = {};
	self.usedFrames = {};
	self.overlapGroups = {};

	self.activated = false;

	if (self.db.enabled) then
		self:Enable();
	end

	self.cache = {};

	self:UpdateCache();
	self:UpdateItems();

	hooksecurefunc('UseToy', function() if (self.db.enabled) then C_Timer.After(1.5, function() self:UpdateToys() end); end end);
	hooksecurefunc('UseToyByName', function() if (self.db.enabled) then C_Timer.After(1.5, function() self:UpdateToys() end);  end end);
end

CUI:RegisterModule(CB:GetName());
