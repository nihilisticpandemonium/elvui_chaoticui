local CUI, E, L, V, P, G = unpack(select(2, ...))
local WBN = CUI:NewModule('WatchBarNotifier','AceTimer-3.0', 'AceEvent-3.0');
local LSM = LibStub("LibSharedMedia-3.0");

local min, max = math.min, math.max

local cwhite, cyellow, corange, cazerite, cresume = "|cffffffff","|cffffff78","|cffff7831","|cffe6cc80","|r"
local fmt = string.format
local getfactioninfo = GetFactionInfo
local math_abs = math.abs
local math_ceil = math.ceil
local standingmax = 8
local standingmin = 1

function WBN:Initialize()
	self.values = {
		["xp"] = { last = UnitXP("player"), max = UnitXPMax("player") },
		["honor"] = { last = UnitHonor("player"), max = UnitHonorMax("player") },
		["faction"] = { },
	}

	self:RegisterEvent("UPDATE_FACTION");
	self:RegisterEvent("AZERITE_ITEM_EXPERIENCE_CHANGED");
	self:RegisterEvent("HONOR_XP_UPDATE");
	self:RegisterEvent("PLAYER_XP_UPDATE");

	self:ScanAzeriteXP();
	self:ScanFactions();
end

function WBN:ScanAzeriteXP()
    local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
	if (not azeriteItemLocation) then 
		return; 
	end
	
	local azeriteItem = Item:CreateFromItemLocation(azeriteItemLocation); 
	
	local xp, totalLevelXP = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);

	local powerLevel = C_AzeriteItem.GetPowerLevel(azeriteItemLocation);

	self.values.azerite = { last = xp, max = totalLevelXP, powerLevel = powerLevel }
end

function WBN:PLAYER_XP_UPDATE()
	if (not E.db.chaoticui.wbn.enabled) then return end
	if (tonumber(E.db.chaoticui.wbn.xpchatframe) == 0) then return end

	local chatframe = _G["ChatFrame" .. tonumber(E.db.chaoticui.wbn.xpchatframe)]
	local c = UnitXP("player")

	if (c < self.values.xp.last) then
		local change = math_abs(c + (self.values.xp.max - self.values.xp.last))
		self.values.xp.max = UnitXPMax("player")
		local remaining = self.values.xp.max - c
		local repetitions = math_ceil(remaining / change)
		local level = UnitLevel("player")
		local newvaluetext = fmt("%s%+d%s XP%s, %s%d%s more to %slevel %d%s (%s%d%s Repetitions).", 
		corange,change,cyellow,cresume,corange,remaining,cresume,
		cyellow,level + 1,cresume,corange,repetitions,cresume )

		chatframe:AddMessage(newvaluetext)
	else
		local change = math_abs(c - self.values.xp.last)
		local remaining = self.values.xp.max - c
		local repetitions = math_ceil(remaining / change)
		local level = UnitLevel("player")
		local newvaluetext = fmt("%s%+d%s XP%s, %s%d%s more to %slevel %d%s (%s%d%s Repetitions).", 
		corange,change,cyellow,cresume,corange,remaining,cresume,
		cyellow,level + 1,cresume,corange,repetitions,cresume )

		chatframe:AddMessage(newvaluetext)
	end

	self.values.xp.last = c
end

function WBN:AZERITE_ITEM_EXPERIENCE_CHANGED()
	if (not E.db.chaoticui.wbn.enabled) then return end
	if (tonumber(E.db.chaoticui.wbn.axpchatframe) == 0) then return end

	local chatframe = _G["ChatFrame" .. tonumber(E.db.chaoticui.wbn.axpchatframe)]
    local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
	if (not azeriteItemLocation) then 
		return; 
	end
	
	local azeriteItem = Item:CreateFromItemLocation(azeriteItemLocation); 
	
	local c, max = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);

	if (not self.values.azerite) then
		self:ScanAzeriteXP();
	end
		
    if (c < self.values.azerite.last) then
		local change = math_abs(c + (self.values.azerite.max - self.values.azerite.last))
		self.values.azerite.max = max
		local remaining = self.values.azerite.max - c
		local repetitions = math_ceil(remaining / change)
		local level = self.values.azerite.powerLevel
		local newvaluetext = fmt("%s%+.0f%s Azerite XP%s, %s%.0f%s more to %slevel %d%s (%s%d%s Repetitions).",
		corange,change,cazerite,cresume,corange,remaining,cresume,cyellow,level + 1,cresume,corange,repetitions,cresume )
		        
		chatframe:AddMessage(newvaluetext)
	else
		local change = math_abs(c - self.values.azerite.last)
		if (change == 0) then return end
		local remaining = self.values.azerite.max - c
		local repetitions = math_ceil(remaining / change)
		local level = self.values.azerite.powerLevel
		local newvaluetext = fmt("%s%+.0f%s Azerite XP%s, %s%.0f%s more to %slevel %d%s (%s%d%s Repetitions).",
		corange,change,cazerite,cresume,corange,remaining,cresume,cyellow,level + 1,cresume,corange,repetitions,cresume )

		chatframe:AddMessage(newvaluetext)
	end

	self:ScanAzeriteXP();
end

function WBN:HONOR_XP_UPDATE()
	if (not E.db.chaoticui.wbn.enabled) then return end
	if (tonumber(E.db.chaoticui.wbn.honorchatframe) == 0) then return end

	local chatframe = _G["ChatFrame" .. tonumber(E.db.chaoticui.wbn.honorchatframe)]
	local c = UnitHonor("player")

	if (c < self.values.honor.last) then
		local change = math_abs(c + (self.values.honor.max - self.values.honor.last))
		self.values.honor.max = UnitHonorMax("player")
		local remaining = self.values.honor.max - c
		local repetitions = math_ceil(remaining / change)
		local level = UnitHonorLevel("player")
		local newvaluetext = fmt("%s%+d%s Honor%s, %s%d%s more to %slevel %d%s (%s%d%s Repetitions).", 
		corange,change,cyellow,cresume,corange,remaining,cresume,
		cyellow,level + 1,cresume,corange,repetitions,cresume )

		chatframe:AddMessage(newvaluetext)
	else
		local change = math_abs(c - self.values.honor.last)
		local remaining = self.values.honor.max - c
		local repetitions = math_ceil(remaining / change)
		local level = UnitHonorLevel("player")
		local newvaluetext = fmt("%s%+d%s Honor%s, %s%d%s more to %slevel %d%s (%s%d%s Repetitions).", 
		corange,change,cyellow,cresume,corange,remaining,cresume,
		cyellow,level + 1,cresume,corange,repetitions,cresume )

		chatframe:AddMessage(newvaluetext)
	end

	self.values.honor.last = c
end

function WBN:ScanFactions()
	self.numFactions = GetNumFactions();
	for i = 1, self.numFactions do
		local name, _, standingID, _, _, barValue, _, _, isHeader, _, hasRep, _, _, factionID = getfactioninfo(i)
		if (name and not isHeader or hasRep) then
			local hasParagonReward = false;
			if (C_Reputation.IsFactionParagon(factionID)) then
				local currentValue, threshold, _, hasRewardPending = C_Reputation.GetFactionParagonInfo(factionID)
				barMin, barMax = 0, threshold
				barValue = currentValue % threshold
				if hasRewardPending then 
					barValue = barValue + threshold
				end
				hasParagonReward = hasRewardPending;
			end
			self.values.faction[name] = { Standing = C_Reputation.IsFactionParagon(factionID) and 999 or standingID, Value = barValue, HasParagonReward = hasParagonReward };
		end
	end
end

function WBN:UPDATE_FACTION()
	if (not E.db.chaoticui.wbn.enabled) then return end
	if (tonumber(E.db.chaoticui.wbn.repchatframe) == 0) then return end

	local chatframe = _G["ChatFrame" .. tonumber(E.db.chaoticui.wbn.repchatframe)]
	
	local tempfactions = GetNumFactions()
	if (tempfactions > self.numFactions) then
		self:ScanFactions()
		self.numFactions = tempfactions
	end
	for factionIndex = 1, GetNumFactions() do
		local name, _, standingID, barMin, barMax, barValue, _, _, isHeader, _, hasRep, _, _, factionID = getfactioninfo(factionIndex)
		local friendID, _, _, _, _, _, friendTextLevel = GetFriendshipReputation(factionID);
		local currentRank, maxRank = GetFriendshipReputationRanks(factionID);
		local isParagon = false;
		local hasParagonReward = false;
		if (factionID and C_Reputation.IsFactionParagon(factionID)) then
			local currentValue, threshold, _, hasRewardPending = C_Reputation.GetFactionParagonInfo(factionID)
			barMin, barMax = 0, threshold
			barValue = currentValue % threshold
			if hasRewardPending then 
				barValue = barValue + threshold
			end
			isParagon = true;
			hasParagonReward = hasRewardPending;
		end

		if (isParagon) then
			standingID = 999;
		end

		if (not isHeader or hasRep) and self.values.faction[name] then
			local diff = barValue - self.values.faction[name].Value
			local skipMessage = false;
			if diff ~= 0 then
				if standingID ~= self.values.faction[name].Standing or hasParagonReward ~= self.values.faction[name].HasParagonReward then
					local newfaction = friendID and friendTextLevel or _G["FACTION_STANDING_LABEL" .. standingID]
					
					local newstandingtext

					if (isParagon) then
						newstandingtext = "You have received a " .. cyellow .. "reward coffer" .. cresume .. " from " .. cyellow .. name .. cresume .. "!"
						skipMessage = true;
					else
						newstandingtext = "New standing with " .. cyellow .. name .. cresume ..
						" is " .. cyellow .. newfaction .. cresume .. "!"
					end
					chatframe:AddMessage(newstandingtext)
				end

				local remaining, nextstanding, plusminus
				if diff > 0 or isParagon then
					remaining = barMax - barValue
					if (friendID) then
						nextstanding = ("next rank (current rank: %s)"):format(friendTextLevel);
					else
						if standingID < standingmax then
							nextstanding = _G["FACTION_STANDING_LABEL" .. standingID + 1]
						elseif isParagon then
							nextstanding = "the next reward"
						else
							nextstanding = "End of " .. _G["FACTION_STANDING_LABEL" .. standingmax]
						end
					end
				else
					remaining = barValue - barMin
					if (friendID) then
						nextstanding = ("next rank (current rank: %s)"):format(friendTextLevel);
					else
						if standingID > standingmin then
							nextstanding = _G["FACTION_STANDING_LABEL" .. standingID - 1]
						else
							nextstanding = "Beginning of " .. _G["FACTION_STANDING_LABEL" .. standingmin]
						end
					end
				end

				local change = math_abs(barValue - self.values.faction[name].Value)
				local repetitions = math_ceil(remaining / change)

				local newvaluetext = fmt("%s%+d%s %s%s, %s%d%s more to %s%s%s (%s%d%s Repetitions).", 
				corange,change,cyellow,name,cresume,corange,remaining,cresume,
				cyellow,nextstanding,cresume,corange,repetitions,cresume )

				if (not skipMessage) then
					chatframe:AddMessage(newvaluetext)
				end
				
				self.values.faction[name].Value = barValue
				self.values.faction[name].Standing = isParagon and 999 or standingID
				self.values.faction[name].HasParagonReward = hasParagonReward
			end
		end
	end
end

CUI:RegisterModule(WBN:GetName())
