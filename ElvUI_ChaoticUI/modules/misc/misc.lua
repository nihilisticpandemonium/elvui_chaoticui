local CUI, E, L, V, P, G = unpack(select(2, ...));

local AFK = E:GetModule('AFK');
local LSM = LibStub("LibSharedMedia-3.0");
local AB = E:GetModule('ActionBars');

local KM = CUI:NewModule('ChaoticUIMisc');

local CP = CUI:NewModule('CardinalPoints');

local BRC = CUI:NewModule('BetterReputationColors');

function KM:HookAFK()
	AFK.AFKMode.bottom.logo:SetTexture("Interface\\AddOns\\ElvUI_ChaoticUI\\media\\textures\\elvui_chaoticui_logo.tga")
	--SS.Elv:SetPoint("CENTER", SS.Bottom, "TOP", -(T.GetScreenWidth()/10), 0)
	AFK.AFKMode.bottom.logo:SetPoint("CENTER", AFK.AFKMode.bottom, "TOP", -(GetScreenWidth()/10), 0);
end

function KM:UpdateQuestMapFrame()
	if (IsAddOnLoaded("QuestGuru") or IsAddOnLoaded("Classic Quest Log")) then
		WorldMapFrame.SidePanelToggle.OpenButton:Hide();
		WorldMapFrame.SidePanelToggle.OpenButton.Show = function() end;
		WorldMapFrameTitleText:SetText("World Map");
        WorldMapFrameTitleText.SetText = function() end;
	else
		local frame = CreateFrame("Frame", nil, QuestMapFrame);
		QuestMapFrame.QuestCountFrame = frame;

		frame:RegisterEvent("QUEST_LOG_UPDATE");
		frame:Size(240, 20);
		frame:Point("TOP", 0, 30);

		local text = frame:CreateFontString(nil, "OVERLAY");
		text:FontTemplate(LSM:Fetch("font", E.db.general.font), 12, "THINOUTLINE");
		text:SetAllPoints();

		frame.text = text;
		local str = "%d / 25 Quests";
		frame.text:SetFormattedText(str, select(2, GetNumQuestLogEntries()));

		frame:SetScript("OnEvent", function(self, event)
			local _, quests = GetNumQuestLogEntries();

			frame.text:SetFormattedText(str, quests);
		end);
	end
end

function KM:Initialize()
	self:HookAFK();
	self:UpdateQuestMapFrame()

	hooksecurefunc(AB, "UpdateButtonConfig", function(self, bar, buttonName)
		for i, button in pairs(bar.buttons) do
			button:SetAttribute("unit2", "player");
		end
	end);
	if (InFlight) then
		hooksecurefunc(InFlight, "StartTimer", function(self, slot)
			if (InFlightBar) then
				InFlightBar:ClearAllPoints();
				InFlightBar:SetPoint("TOP", 0, -262);
			end
		end);
	end
	PlayerTalentFrameSpecialization:HookScript("OnShow", function(self)
		local numSpecs = GetNumSpecializations(false, self.isPet);
		-- demon hunters have 2 specs, druids have 4
		if ( numSpecs == 1 ) then
			self.specButton1:SetPoint("TOPLEFT", 6, -161);
			self.specButton2:Hide()
			self.specButton3:Hide()
		elseif ( numSpecs == 2 ) then
			self.specButton1:SetPoint("TOPLEFT", 6, -131);
			self.specButton3:Hide();
		elseif ( numSpecs == 4 ) then
			self.specButton1:SetPoint("TOPLEFT", 6, -61);
			self.specButton4:Show();
		end
	end);
end

CUI:RegisterModule(KM:GetName());

function CP:CreateFrame()
	local frame = CreateFrame("FRAME","MMD_Frame",Minimap);
	frame:SetAllPoints();

	local n = frame:CreateFontString("$parentText","ARTWORK","GameFontNormal");
	local e = frame:CreateFontString("$parentText","ARTWORK","GameFontNormal");
	local s = frame:CreateFontString("$parentText","ARTWORK","GameFontNormal");
	local w = frame:CreateFontString("$parentText","ARTWORK","GameFontNormal");

	n:SetText("N")
	e:SetText("E")
	s:SetText("S")
	w:SetText("W")

	n:SetPoint("CENTER", frame, "TOP")
	e:SetPoint("CENTER", frame, "RIGHT")
	s:SetPoint("CENTER", frame, "BOTTOM")
	w:SetPoint("CENTER", frame, "LEFT")

	self.frame = frame;
end

function CP:Update()
	self.frame:SetShown(E.db.chaoticui.cardinalpoints.enabled);
end

function CP:Initialize()
	self:CreateFrame();

	self:Update();
end

CUI:RegisterModule(CP:GetName());

local strform = string.format
local floor = math.floor;
local ceil = math.ceil;

-- aura time colors for days, hours, minutes, seconds, fadetimer, hour with mins
E.TimeColors = { --Overwrite
	[0] = '|cffeeeeee',
	[1] = '|cffeeeeee',
	[2] = '|cffeeeeee',
	[3] = '|cffeeeeee',
	[4] = '|cfffe0000',
	[5] = '|cffeeeeee',
}

-- short and long aura time formats
E.TimeFormats = { --Overwrite
	[0] = { '%dd', '%dd' },
	[1] = { '%dh', '%dh' },
	[2] = { '%dm', '%dm' },
	[3] = { '%ds', '%d' },
	[4] = { '%.1fs', '%.1f' },
	[5] = { '%s', '%s'},
}

if (ElvUI_SLE) then
	local SLE = ElvUI_SLE[1]
	local AFK = E:GetModule("AFK")
	local S = SLE:GetModule("Screensaver")
	local SS

	local function Setup()
		if (not E.private.sle.module.screensaver) then return end
		SS = AFK.AFKMode
		SS.ClassCrest = SS.Top:CreateTexture(nil, "OVERLAY")
		SS.ClassCrest:Size(S.db.crest.size, S.db.crest.size)
		SS.ClassCrest:SetPoint("LEFT", SS.RaceCrest, "RIGHT", 10, 0)
		SS.ClassCrest:SetTexture(CUI:GetClassTexture())
		SS.ClassCrest:SetTexCoord(0.25, 0.75, 0, 1)
	end

	hooksecurefunc(S, "Setup", Setup)
end

function BRC:Initialize()
	self:UpdateFactionColors();
end

function BRC:UpdateFactionColors()
	E:CopyTable(FACTION_BAR_COLORS, E.db.chaoticui.betterreputationcolors);
end

CUI:RegisterModule(BRC:GetName());

-- Dressing room skin/additional functionality credits Simpy @ tukui.org
local S = E:GetModule('Skins')

local pairs, ipairs, tsort = pairs, ipairs, table.sort
local UnitSex = UnitSex
local CreateFrame = CreateFrame
local RaiseFrameLevel = RaiseFrameLevel
local IsAltKeyDown = IsAltKeyDown
local L_EasyMenu = L_EasyMenu
local UIDropDownMenu_Initialize = UIDropDownMenu_Initialize
local UIDropDownMenu_CreateInfo = UIDropDownMenu_CreateInfo
local UIDropDownMenu_AddButton = UIDropDownMenu_AddButton
local UIDropDownMenu_SetSelectedValue = UIDropDownMenu_SetSelectedValue
local MALE, FEMALE = MALE, FEMALE

-- GLOBALS: DressUpFrame, DressUpFrameResetButton, DressUpFrameCancelButton, DressUpFrameCloseButton, DressUpFrameOutfitDropDown
-- GLOBALS: WardrobeOutfitFrame, WardrobeOutfitEditFrame, DressUpModel, SideDressUpModelResetButton, SideDressUpModelCloseButton
-- GLOBALS: SideDressUpFrame, SideDressUpModel


local function LoadSkin()
	if E.private.skins.blizzard.enable ~= true or E.private.skins.blizzard.dressingroom ~= true then return end
	local pixelWidth, races, slots, raceExceptions = (E.PixelMode and 1 or 2),
	{
		["Human"]		= 01,
		["Orc"]			= 02,
		["Dwarf"]		= 03,
		["Night Elf"]	= 04,
		["Undead"]		= 05,
		["Tauren"]		= 06,
		["Gnome"]		= 07,
		["Troll"]		= 08,
		["Goblin"]		= 09,
		["Blood Elf"]	= 10,
		["Draenei"]		= 11,
		["Worgen"]		= 22,
		["Pandaren"]	= 24,
		["Nightborne"]	= 27,
		["Hightmountain Tauren"] = 28,
		["Void Elf"]	= 29,
		["Lightforged Draenei"] = 30,
		["Kul Tiran"] 	= 32,
		["Dark Iron Dwarf"] = 34,
		["Mag'har Orc"]	= 36,
		--UnitRace returns differently for the following races, so need to include exceptions
	},
	{
		[01] = "Head",
		[03] = "Shoulder",
		[04] = "Shirt",
		[05] = "Chest",
		[06] = "Waist",
		[07] = "Legs",
		[08] = "Feet",
		[09] = "Wrist",
		[10] = "Hands",
		[15] = "Back",
		[16] = "Main Hand",
		[17] = "Off Hand",
		[19] = "Tabard"
	},
	{
		["NightElf"]	= "Night Elf",
		["Scourge"]		= "Undead",
		["BloodElf"]	= "Blood Elf",
		["HighmountainTauren"] = "Highmountain Tauren",
		["VoidElf"]	= "Void Elf",
		["LightforgedDraenei"] = "Lightforged Draenei",
		["KulTiran"] 	= "Kul Tiran",
		["DarkIronDwarf"] = "Dark Iron Dwarf",
		["MagharOrc"]	= "Mag'har Orc",
	}

	local count, menuList, raceList, myGender, myRace = 0, {}, {}, (UnitSex("player") or 2)-2, races[E.myrace] or races[raceExceptions[E.myrace]]
	local menuFrame = CreateFrame("Frame", "CustomRaceMenu", E.UIParent, "UIDropDownMenuTemplate")
	local function UndressModel(model, button)
		if IsAltKeyDown() then model:SetUnit('target')
		elseif button == 'RightButton' then model:Undress()
	end end
	for i, x in pairs(slots) do count=count+1
		menuList[count] = {text=x, slot=i, notCheckable=true, func=function()
			DressUpModel:UndressSlot(i)
		end}
	end
	count=0 --reset
	for i, x in pairs(races) do count=count+1
		raceList[count] = {i=i, x=x}
	end
	tsort(menuList, function(a, b) return a.slot < b.slot end)
	tsort(raceList, function(a, b) return a.x < b.x end)

	DressUpFrame:StripTextures(true)
	DressUpFrame:CreateBackdrop("Transparent")
	DressUpFrame.backdrop:Point("TOPLEFT", 6, 0)
	DressUpFrame.backdrop:Point("BOTTOMRIGHT", -32, 70)

	S:HandleButton(DressUpFrameResetButton)
	S:HandleButton(DressUpFrameCancelButton)
	S:HandleButton(DressUpFrameOutfitDropDown.SaveButton)
	DressUpFrameOutfitDropDown.SaveButton:ClearAllPoints()
	DressUpFrameOutfitDropDown.SaveButton:SetPoint("RIGHT", DressUpFrameOutfitDropDown, 86, 4)
	S:HandleDropDownBox(DressUpFrameOutfitDropDown)
	DressUpFrameOutfitDropDown:SetSize(195, 34)
	
	S:HandleCloseButton(DressUpFrameCloseButton, DressUpFrame.backdrop)
	
	-- Wardrobe edit frame
	WardrobeOutfitFrame:StripTextures(true)
	WardrobeOutfitFrame:SetTemplate("Transparent")

	WardrobeOutfitEditFrame:StripTextures(true)
	WardrobeOutfitEditFrame:SetTemplate("Transparent")
	WardrobeOutfitEditFrame.EditBox:StripTextures()
	S:HandleEditBox(WardrobeOutfitEditFrame.EditBox)
	WardrobeOutfitEditFrame.EditBox.backdrop:Point("TOPLEFT", WardrobeOutfitEditFrame.EditBox, "TOPLEFT", -5, -5)
	WardrobeOutfitEditFrame.EditBox.backdrop:Point("BOTTOMRIGHT", WardrobeOutfitEditFrame.EditBox, "BOTTOMRIGHT", 0, 5)
	S:HandleButton(WardrobeOutfitEditFrame.AcceptButton)
	S:HandleButton(WardrobeOutfitEditFrame.CancelButton)
	S:HandleButton(WardrobeOutfitEditFrame.DeleteButton)

	DressUpFrameResetButton:Point("RIGHT", DressUpFrameCancelButton, "LEFT", -4, 0)
	DressUpFrameResetButton:HookScript('OnMouseDown', function(self, button)
		UndressModel(DressUpModel, button)
	end)
	DressUpFrameCancelButton:HookScript('OnMouseDown', function(self, button)
		if button == "RightButton" then
			L_EasyMenu(menuList, menuFrame, "cursor", 0, 0, "MENU", 2)
		end
	end)

	local dd1 = CreateFrame('Frame', 'DressUpFrameDropDown1', DressUpFrameResetButton, 'UIDropDownMenuTemplate')
	dd1:Point('RIGHT', DressUpFrameResetButton, 'LEFT', 5, -3)
	S:HandleDropDownBox(dd1, 100)
	local dd2 = CreateFrame('Frame', 'DressUpFrameDropDown2', dd1, 'UIDropDownMenuTemplate')
	dd2:Point('RIGHT', dd1, 'LEFT', 25, 0)
	S:HandleDropDownBox(dd2, 120)

	local function dd_OnClick(self, info)
		if self == dd1 then myGender = info.value
		else myRace = info.value end
		DressUpModel:SetCustomRace(myRace, myGender);
		DressUpModel:RefreshCamera();
		UIDropDownMenu_SetSelectedValue(self, info.value);
	end

	local function dd_Initialize(self)
		local info = UIDropDownMenu_CreateInfo();
		info.func = function(i) dd_OnClick(self, i) end;
		local selected = myRace;
		if self == dd1 then
			selected = myGender;
			for i, x in ipairs({MALE,FEMALE}) do
				info.text=x;info.value=i-1;
				UIDropDownMenu_AddButton(info);
			end
		else
			for _, x in ipairs(raceList) do
				info.text=x.i;info.value=x.x;
				UIDropDownMenu_AddButton(info);
			end
		end
		UIDropDownMenu_SetSelectedValue(self, selected);
	end

	UIDropDownMenu_Initialize(dd1, dd_Initialize)
	UIDropDownMenu_Initialize(dd2, dd_Initialize)

	DressUpFrameCloseButton:SetFrameLevel(DressUpFrameCloseButton:GetFrameLevel()+2)
	RaiseFrameLevel(DressUpFrameResetButton)
	RaiseFrameLevel(DressUpFrameCancelButton)

	DressUpModel:ClearAllPoints()
	DressUpModel:Point("BOTTOMRIGHT", DressUpFrame.backdrop, "BOTTOMRIGHT", -pixelWidth, pixelWidth)
	DressUpModel:Point("TOPLEFT", DressUpFrame.backdrop, "TOPLEFT", pixelWidth, -pixelWidth)

	S:HandleButton(SideDressUpModelResetButton)
	S:HandleCloseButton(SideDressUpModelCloseButton)
	RaiseFrameLevel(SideDressUpModelCloseButton)
	SideDressUpFrame:StripTextures()
	SideDressUpFrame:SetTemplate("Transparent")
	SideDressUpFrame:HookScript('OnShow', function(self)
		self:ClearAllPoints()
		self:Point("TOPLEFT", self.parentFrame, "TOPRIGHT", 1, 0)
	end)

	SideDressUpModel:Point("BOTTOMRIGHT", SideDressUpFrame, "BOTTOMRIGHT", -pixelWidth, pixelWidth)
	SideDressUpModel:Point("TOPLEFT", SideDressUpFrame, "TOPLEFT", pixelWidth, -pixelWidth)
	SideDressUpModelResetButton:Point("BOTTOMLEFT", SideDressUpFrame, "BOTTOMLEFT", 4, 4)
	SideDressUpModelResetButton:HookScript('OnMouseDown', function(self, button)
		UndressModel(SideDressUpModel, button)
	end)
end

S:AddCallback("DressingRoomChaoticUI", LoadSkin)
