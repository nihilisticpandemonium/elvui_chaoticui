local CUI, E, L, V, P, G = unpack(select(2, ...));

local DT = E:GetModule('DataTexts');

local TL = CUI:NewModule('TalentLoadouts');

local Frame		= CreateFrame("Frame")
local menu 		= {}

local function GatherLoadoutsForCurrentSpec()
    local specID = GetSpecializationInfo(GetSpecialization());

    local loadouts = {};
    local count = 0;
    if (TL.db[specID]) then
        for loadoutName, loadoutTalents in pairs(TL.db[specID]) do
            loadouts[loadoutName] = loadoutTalents;
            count = count + 1;
        end
    end

    return loadouts, count;
end

local function FilterChat(self, event, msg)
    if (msg:find("You have learned") or msg:find("You have unlearned")) then
        return true
    end
    return false;
end

local function CanApplyTalents()
	if (IsResting()) then
		return true;
	elseif ((UnitAura("player", GetSpellInfo(227563)) or UnitAura("player", GetSpellInfo(227565))) and UnitLevel("player") < 101) then
		return true;
	elseif (UnitAura("player", GetSpellInfo(227041)) or UnitAura("player", GetSpellInfo(226234))) then
		return true;
	end
	return false;
end

local cached_pvp_talents = nil;

local function CachePvpTalents()
	cached_pvp_talents = {};

	for slot = 1, 4 do
        local slotInfo = C_SpecializationInfo.GetPvpTalentSlotInfo(slot);
        local availableTalentIDs = slotInfo.availableTalentIDs;
   
        table.sort(availableTalentIDs, function(a, b)
            local unlockedA = select(7,GetPvpTalentInfoByID(a));
            local unlockedB = select(7,GetPvpTalentInfoByID(b));
        
            if (unlockedA ~= unlockedB) then
                return unlockedA;
            end
        
            if (not unlockedA) then
                local reqLevelA = C_SpecializationInfo.GetPvpTalentUnlockLevel(a);
                local reqLevelB = C_SpecializationInfo.GetPvpTalentUnlockLevel(b);
        
                if (reqLevelA ~= reqLevelB) then
                return reqLevelA < reqLevelB;
                end
            end
        
            return a < b;
        end);
        cached_pvp_talents[slot] = availableTalentIDs;
	end
	cached_pvp_talents.spec = GetSpecialization();
end

local function LoadoutClick(button, info, name)
    if (not CanApplyTalents()) then
        print("You cannot change your talents when you are not rested.");
        return;
    end

    local loadout = {};
    local str = "Talent loadout %s applied.";
	
    for i = 1, 7 do
        loadout[i] = GetTalentInfo(i, info[i], 1);
	end
	
	ChatFrame_AddMessageEventFilter("CHAT_MSG_SYSTEM", FilterChat)
	LearnTalents(unpack(loadout));
	
	if (C_PvP.IsWarModeDesired()) then	
		if (not cached_pvp_talents or cached_pvp_talents.spec ~= GetSpecialization()) then
			CachePvpTalents();
		end
		
		for i = 1, #info.pvp do
			LearnPvpTalent(cached_pvp_talents[i][info.pvp[i]], i);
		end
	end

    print(str:format(name));
    C_Timer.After(5, function() ChatFrame_RemoveMessageEventFilter("CHAT_MSG_SYSTEM", FilterChat) end);
end

local function CreateMenu(self, level)
	local loadouts, count = GatherLoadoutsForCurrentSpec();
	menu = wipe(menu)
	
	if count == 0 then print("No talent loadouts for the current specialization."); return end
	
	for name, loadout in CUI_PairsByKeys(loadouts) do
		menu.hasArrow = false
		menu.notCheckable = true
		menu.text = name;
		menu.func = LoadoutClick
		menu.arg1 = loadout
		menu.arg2 = name
		UIDropDownMenu_AddButton(menu)
	end
end

local function OnEnter(self)
	DT:SetupTooltip(self)
	DT.tooltip:AddLine("Saved Talent Loadouts");
	DT.tooltip:AddLine(" ")
	DT.tooltip:AddLine(L["Click to select a talent loadout."])
	DT.tooltip:Show()
end

function TL:Initialize()
	Frame.initialize = CreateMenu
	Frame.displayMode = "MENU"

    CUI:RegisterDB(self, "talentloadouts")

    local numSpecs = E.myclass == "DRUID" and 4 or E.myclass == "DEMONHUNTER" and 2 or 3;

    for i = 1, numSpecs do
        self.db[GetSpecializationInfo(i)] = self.db[GetSpecializationInfo(i)] or {};
    end
end

local function OnEvent(self, event)
	self.text:SetText("Talent Loadouts");
end

local function Click(self, button)
	ToggleDropDownMenu(1, nil, Frame, self, 0, 0)
end

CUI:RegisterModule(TL:GetName());
DT:RegisterDatatext("Talent Loadouts", {"PLAYER_ENTERING_WORLD"}, OnEvent, nil, Click, OnEnter)