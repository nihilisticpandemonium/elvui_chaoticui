local CUI, E, L, V, P, G = unpack(select(2, ...));

local LSM = LibStub("LibSharedMedia-3.0");
local ES = CUI:GetModule('EnhancedShadows');
local ADB = CUI:GetModule('AnimatedDataBars');
local HAT = CUI:NewModule('HiddenArtifactTracker', 'AceEvent-3.0');

function HAT:CreateHolder()
    local holder = CreateFrame('Frame', nil, E.UIParent);
    holder:SetSize(410, 10);
    holder:Show();
    holder:SetPoint('BOTTOM', E.UIParent, 'BOTTOM', 0, 180);
    E:CreateMover(holder, 'ArtifactHiddenAppearanceTrackerMover', 'Artifact Hidden Appearance Tracker',  nil, nil, nil, 'ALL,SOLO');
    E.FrameLocks[holder] = true;
    return holder
end

function HAT:CreateStatusBar(holder, info, ...)
    local _, _, _, completed, _, _, _, _, _, _, _, _, wasEarnedByMe, _ = GetAchievementInfo(info.achievementID);
    if (completed and wasEarnedByMe) then
        return;
    end

    local frame = CreateFrame("Frame", nil, holder);
    frame:SetTemplate('Default');
    frame:SetSize(self.db.width, self.db.height);
    frame:CreateShadow();
    ES:RegisterShadow(frame.shadow);

    local bar = CreateFrame('StatusBar', nil, frame);
    bar:SetStatusBarTexture(E.media.normTex);
    E:RegisterStatusBar(bar.statusBar);
    bar:SetInside();

    bar.info = info;
    frame:SetPoint(...);

    bar.textFrame = CreateFrame("Frame", nil, bar);
    bar.textFrame:SetAllPoints();
    bar.textFrame:SetFrameLevel(bar:GetFrameLevel() + 5);
    bar.textFrame:Show();
    bar.text = bar.textFrame:CreateFontString(nil, "THINOUTLINE") 				
	bar.text:FontTemplate(LSM:Fetch("font", self.db.font), self.db.fontSize, "THINOUTLINE")
	bar.text:SetPoint("CENTER", bar, "CENTER", 0, 0)
	bar.text:SetJustifyH("CENTER")
	bar.text:SetJustifyV("MIDDLE")

    bar:SetScript("OnEnter", function(self) HAT:Bar_OnEnter(self) end);
    bar:SetScript("OnLeave", function(self) HAT:Bar_OnLeave(self) end);

    ADB:CreateTicks(bar);

    return bar;
end

function HAT:Bar_OnEnter(bar)
    if (self.db.mouseoverText) then
        E:UIFrameFadeIn(bar.text, 0.2, 0, 1);
    end
    GameTooltip:ClearLines();
    GameTooltip:SetOwner(bar, 'ANCHOR_CURSOR', 0, -4);

    local info = bar.info;

    local count = 0;
    for i = 1, info.criteriaCount do
        local n = select(4, GetAchievementCriteriaInfo(info.achievementID, i));
        count = count + n;
    end

    GameTooltip:AddLine(info.label);
    GameTooltip:AddLine(" ");
    GameTooltip:AddDoubleLine("Count: ", count);
    GameTooltip:AddDoubleLine("Needed: ", info.needed);
    GameTooltip:AddDoubleLine("Remaining: ", info.needed - count);
    GameTooltip:Show();
end

function HAT:Bar_OnLeave(bar)
    GameTooltip:Hide();
    if (self.db.mouseoverText) then
        E:UIFrameFadeOut(bar.text, 0.2, 1, 0);
    end
end

function HAT:CheckAchievementProgress()
    local _, _, _, completed, _, _, _, _, _, _, _, _, wasEarnedByMe, _ = GetAchievementInfo(10460);
    if (not completed or not wasEarnedByMe) then
        return false;
    end
    return true;
end

function HAT:UpdateAll()
    local width = self.db.width;
    local height = self.db.height;

    for i, bar in ipairs(self.bars) do
        bar:SetSize(width, height);
        bar.text:FontTemplate(LSM:Fetch("font", self.db.font), self.db.fontSize, "THINOUTLINE")
        bar:SetStatusBarColor(unpack(bar.info.color));
    end
    self.holder:SetSize(width + 8, (height * #self.bars) + (2 * (#self.bars + 2)));
    self.holder:SetShown(self.db.enabled and self:CheckAchievementProgress());
end

function HAT:UpdateBars()
    if (not self:CheckAchievementProgress()) then
        for _, bar in ipairs(self.bars) do
            bar:Hide();
        end
        return;
    end

    local criteriaTracked = 0;
    local indexesToRemove = {};
    for i, bar in ipairs(self.bars) do
        local info = bar.info;
        local _, _, _, completed, _, _, _, _, _, _, _, _, wasEarnedByMe, _ = GetAchievementInfo(info.achievementID);
        if (completed and wasEarnedByMe) then
            tinsert(indexesToRemove, i);
        else
            bar:SetMinMaxValues(0, info.needed);
            local count = 0;
            for i = 1, info.criteriaCount do
                local n = select(4, GetAchievementCriteriaInfo(info.achievementID, i));
                count = count + n;
            end
            if (count > 0) then
                criteriaTracked = criteriaTracked + 1;
            end
            local textFormat = '%s: %d / %d';
            bar.text:SetText(textFormat:format(info.label, count, info.needed));
            bar:SetValue(count);
            bar:Show();
        end
    end
    local needsUpdate = false;
    for k, v in pairs(indexesToRemove) do
        tremove(self.bars, v);
        needsUpdate = true;
    end

    if (needsUpdate) then
        self:UpdateAll();
    end
   self.holder:SetShown(criteriaTracked > 0);
end

function HAT:Initialize()
    CUI:RegisterDB(self, "hiddenArtifactTracker")

    self.holder = self:CreateHolder();

    local dungeonInfo = {
        ['label'] = 'Dungeons',
        ['needed'] = 30,
        ['achievementID'] = 11152,
        ['criteriaCount'] = 15,
        ['color'] = {0, 1, 0, 1},
    }

    local point = {'TOP', self.holder, 'TOP', 0, 0};
    self.bars = {};
    local bar = self:CreateStatusBar(self.holder, dungeonInfo, unpack(point));
    if (bar) then
        tinsert(self.bars, bar);
        point = {'TOP', bar, 'BOTTOM', 0, -4};
    end

    local wqInfo = {
        ['label'] = 'World Quests',
        ['needed'] = 200,
        ['achievementID'] = 11153,
        ['criteriaCount'] = 1,
        ['color'] = {1, 1, 0, 1},
    };
    
    bar = self:CreateStatusBar(self.holder, wqInfo, unpack(point));
    if (bar) then
        tinsert(self.bars, bar);
        point = {'TOP', bar, 'BOTTOM', 0, -4};
    end

    local hkInfo = {
        ['label'] = 'Player Kills',
        ['needed'] = 1000,
        ['achievementID'] = 11154,
        ['criteriaCount'] = 1,
        ['color'] = {1, 0, 0, 1},
    }

    bar = self:CreateStatusBar(self.holder, hkInfo, unpack(point));
    if (bar) then
        tinsert(self.bars, bar);
    end

    self:UpdateAll();
    self:UpdateBars();
    self:RegisterEvent('CRITERIA_UPDATE', 'UpdateBars');
    self:RegisterEvent('ACHIEVEMENT_EARNED', 'UpdateBars');
    self:RegisterEvent('UNIT_INVENTORY_CHANGED', 'UpdateBars');
end

CUI:RegisterModule(HAT:GetName());


