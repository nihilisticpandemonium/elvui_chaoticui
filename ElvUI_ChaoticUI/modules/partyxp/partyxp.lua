local CUI, E, L, V, P, G = unpack(select(2, ...))
local PXP = CUI:NewModule('PartyXP','AceTimer-3.0', 'AceEvent-3.0');
local LSM = LibStub("LibSharedMedia-3.0");

local min, max = math.min, math.max

local watching = {}
local bars = {}
local id_table = {}
local frame

local num_bars_shown = 0

local threshold = 30
local xcurrent = 0

local function __HandleEvent(frame, event, ...)
	if not E.db.chaoticui.pxp.enabled then return end
	if event == "GROUP_ROSTER_UPDATE" or event == "PLAYER_XP_UPDATE" or event == "PLAYER_ENTERED_WORLD" then
		if event == "GROUP_ROSTER_UPDATE" then
			PXP:UpdatePartyMembers()
		end
		PXP:Message()
		PXP:Update()
	elseif event == "QUEST_LOG_UPDATE" then
		local currQXP = CUI:CalculateQuestLogXP();
		PXP:QXPMessage(currQXP)
	elseif event == "CHAT_MSG_ADDON" then
		local prefix, message, channel, sender = ...

		if (prefix ~= "PXP" and prefix ~= "PXPQXP") or not sender or sender == UnitName("player") then return end
		if sender:find("-") then sender = strsplit("-",sender) end
		
		if prefix == "PXP" then
			if message == "[DISABLED]" and watching[sender] then
				watching[sender].disabled = true
			else
				local level, current, max, rested = message:match("%[l:(%d+)c:(%d+)m:(%d+)r:(%d+)%]")
				level, current, max, rested = tonumber(level), tonumber(current), tonumber(max), tonumber(rested)

				if level == MAX_PLAYER_LEVEL_TABLE[GetExpansionLevel()] then return end
				
				if not watching[sender] then
					watching[sender] = {
						["level"] = level,
						["current"] = current,
						["max"] = max,
						["rested"] = rested,
					}
				else
					watching[sender].level = level
					watching[sender].current = current
					watching[sender].max = max
					watching[sender].rested = rested
				end
			end

			PXP:Update()
			if xcurrent == 0 then
				PXP:Message()
				xcurrent = 1
			else
				xcurrent = xcurrent + 1
				if xcurrent > threshold then
					xcurrent = 0
				end
			end
		else
			local qxp = message:match("%[qxp:(%d+)%]")
			if not watching[sender] then return end
			watching[sender].qxp = qxp
			PXP:Update()
		end
	end
end

function PXP:Enable()
	if not E.db.chaoticui.pxp.enabled then
		for i = 1,4 do
			PXP:HideBar(bars[i])
		end
		id_table = {}
		watching = {}
	end
end

function PXP:UpdateMedia()
	for i = 1, 4 do
		bars[i]:SetStatusBarTexture(LSM:Fetch("statusbar", E.db.chaoticui.pxp.texture))
		bars[i].text:FontTemplate(LSM:Fetch("font", E.db.chaoticui.pxp.font), E.db.chaoticui.pxp.fontsize, "THINOUTLINE")
		bars[i]:SetSize(E.db.chaoticui.pxp.width, E.db.chaoticui.pxp.height)
	end
end

function PXP:UpdateColorSetting()
	for _,pname in pairs(id_table) do
		local bar = watching[pname].bar
		if E.db.chaoticui.pxp.classColor then
			local _,class = UnitClass(pname)
			if class then
				local color = RAID_CLASS_COLORS[class]
				bar:SetStatusBarColor(color.r,color.g,color.b)
				bar.restedbar:SetStatusBarColor(color.r*0.8,color.g*0.8,color.b*0.8,0.2)
			end
		else
			bar:SetStatusBarColor(0, 0.4, 1)
			bar.restedbar:SetStatusBarColor(1, 0, 1, 0.2)
		end
	end
end

function PXP:UpdatePartyMembers()
	id_table = {}
	watching = {}
	num_bars_shown = 0
	for i = 1,4 do
		PXP:HideBar(bars[i])
	end
	current = 0
	PXP:Update()
end

function PXP:UpdateBar(id)
	local bar = bars[id]
	if not id_table[id] then return end
	local pname = id_table[id]

	local text = bar.text
	local rested_template = (E.db.chaoticui.pxp.detailedText and "%n Lvl %l XP: %c/%m (%p) (%r Rested)") or "%n Lvl %l (%r Rested)"
	local non_rested_template = (E.db.chaoticui.pxp.detailedText and "%n Lvl %l XP: %c/%m (%p)") or "%n Lvl %l"
	local level_up = (E.db.chaoticui.pxp.detailedText and " [Level Up!]" or " [LvlUp!]")
	local quest_template = (E.db.chaoticui.pxp.detailedText and " (%q Quest%u)") or " (%q Q%u)"
	local quest_rested_template = rested_template .. quest_template
	local quest_non_rested_template = non_rested_template .. quest_template
	local level = tonumber(watching[pname].level)
	local current = tonumber(watching[pname].current)
	local max = tonumber(watching[pname].max)
	local rested = tonumber(watching[pname].rested or 0)
	local quest = tonumber(watching[pname].qxp or 0)
	local template
	bar:SetMinMaxValues(0,max)
	bar:SetValue(current)

	if rested > 0 then
		if quest > 0 then
			template = quest_rested_template
		else
			template = rested_template
		end
	else
		if quest > 0 then
			template = quest_non_rested_template
		else
			template = non_rested_template
		end
	end

	local rbar = bar.restedbar
	rbar:SetMinMaxValues(0,max)
	rbar:SetValue(min(current + rested, max))
	
	local qbar = bar.questbar
	local cq = current + quest

    qbar:SetMinMaxValues(0, max);
    qbar:SetValue(min(max, current + quest));
    if (cq >= max) then
		qbar:SetStatusBarColor(0.2, 0.6, 0.2, 0.8);
	else
        qbar:SetStatusBarColor(1.0, 1.0, 0.2, 0.4);
	end
	qbar:SetShown(quest > 0);
	
	text:SetText(template:gsub("%%(%a)",
		function(arg)
			if arg == "n" then return pname
			elseif arg == "l" then return level
			elseif arg == "c" then return current
			elseif arg == "m" then return max
			elseif arg == "p" then return format("%.0f%%", (current / max * 100) + 0.5)
			elseif arg == "r" then return rested
			elseif arg == "q" then return quest
			elseif arg == "u" then return (cq == max) and level_up or ""
			end 
	end))
end

function PXP:Update() -- 1
	if not E.db.chaoticui.pxp.enabled then return end
	for i = 1,GetNumSubgroupMembers() do
		local pname = UnitName("party"..i)
		if watching[pname] then
			if watching[pname].level == MAX_PLAYER_LEVEL_TABLE[GetExpansionLevel()] and watching[pname].bar or watching[pname].disabled then
				local id = watching[pname].id
				watching[pname].bar:Hide()
				watching[pname] = nil
				if id < num_bars_shown then
					-- 4 people shown, remove 1, 2 shifts to 1, 3 shifts to 2, 4 shifts to 3
					for i = id+1, num_bars_shown do 
						local xname = id_table[i]
						id_table[i-1] = xname
						if not watching[xname] then break end
						PXP:HideBar(watching[xname].bar)
						watching[xname].bar = bars[i-1]
						watching[xname].id = i-1
						PXP:UpdateBar(i-1)
					end
					num_bars_shown = num_bars_shown - 1
					id_table[num_bars_shown+1] = nil
				end
			else
				if not watching[pname].bar then
					watching[pname].bar = bars[num_bars_shown+1]
					watching[pname].id = num_bars_shown+1
					id_table[watching[pname].id] = pname
					num_bars_shown = num_bars_shown + 1
					if E.db.chaoticui.pxp.classColor then
						local _,class = UnitClass(pname)
						if class then
							local color = RAID_CLASS_COLORS[class]
							watching[pname].bar:SetStatusBarColor(color.r,color.g,color.b)
							watching[pname].bar.restedbar:SetStatusBarColor(color.r*0.8,color.g*0.8,color.b*0.8,0.2)
						end
					else
						watching[pname].bar:SetStatusBarColor(0.8, 0.7, 0.8)
						watching[pname].bar.restedbar:SetStatusBarColor(1, 0, 1, 0.2)
					end
					PXP:ShowBar(watching[pname].bar)
				end
			end
			if watching[pname] then PXP:UpdateBar(watching[pname].id) end
		end
	end
end

function PXP:CreateBarGroup(p)
	local PartyXPBar = CreateFrame('StatusBar', nil, p)
	PartyXPBar:SetSize(p:GetSize())
	PartyXPBar:CreateBackdrop('Transparent')
	PartyXPBar:Point("TOP", p,"BOTTOM", 0, E.db.chaoticui.pxp.offset)
	PartyXPBar:SetStatusBarTexture(LSM:Fetch("statusbar", E.db.chaoticui.pxp.texture))
	PartyXPBar:SetFrameLevel(10)
	PartyXPBar:CreateShadow();
	if (CUI:GetModule('EnhancedShadows',true)) then
		CUI:GetModule('EnhancedShadows'):RegisterShadow(PartyXPBar.shadow);
	end

	local restedbar = CreateFrame('StatusBar', nil, PartyXPBar)
	restedbar:SetInside(PartyXPBar, 0, 0)
	restedbar:SetStatusBarTexture(LSM:Fetch("statusbar", E.db.chaoticui.pxp.texture))
	restedbar:SetFrameLevel(PartyXPBar:GetFrameLevel() - 2)
	restedbar:SetMinMaxValues(0, 1)
	restedbar:SetValue(0)

	PartyXPBar.restedbar = restedbar

	local questbar = CreateFrame('StatusBar', nil, PartyXPBar)
	questbar:SetPoint("TOPLEFT", PartyXPBar:GetStatusBarTexture("TOPRIGHT"));
	questbar:SetPoint("BOTTOMRIGHT", PartyXPBar, "BOTTOMRIGHT");
	questbar:SetFrameLevel(PartyXPBar:GetFrameLevel() - 1);
	questbar:SetStatusBarTexture(LSM:Fetch("statusbar", E.db.chaoticui.pxp.texture))
	questbar:SetMinMaxValues(0, 1)
	questbar:SetValue(0)
	
	PartyXPBar.questbar = questbar

	PartyXPBar.text = PartyXPBar:CreateFontString(nil, "THINOUTLINE") 				
	PartyXPBar.text:FontTemplate(LSM:Fetch("font", E.db.chaoticui.pxp.font), E.db.chaoticui.pxp.fontsize, "THINOUTLINE")
	PartyXPBar.text:SetPoint("CENTER", PartyXPBar, "CENTER", 0, 0)
	PartyXPBar.text:SetJustifyH("CENTER")
	PartyXPBar.text:SetJustifyV("MIDDLE")

	return PartyXPBar
end

function PXP:HideBar(bar)
	bar:Hide()
	bar.restedbar:Hide()
	bar.questbar:Hide()
end

function PXP:ShowBar(bar)
	bar:Show()
	bar.restedbar:Show()
	bar.questbar:Show()
end

function PXP:Enable()
	local frame = self.frame;
	if (self.db.enabled) then
		frame:RegisterEvent("CHAT_MSG_ADDON")
		frame:RegisterEvent("CHAT_MSG_CHANNEL")
		frame:RegisterEvent("GROUP_ROSTER_UPDATE")
		frame:RegisterEvent("PLAYER_XP_UPDATE")
		frame:RegisterEvent("PLAYER_ENTERING_WORLD")
		frame:RegisterEvent("QUEST_LOG_UPDATE");
		frame:SetScript("OnEvent", __HandleEvent);
	else
		frame:UnregisterEvent("CHAT_MSG_ADDON")
		frame:UnregisterEvent("CHAT_MSG_CHANNEL")
		frame:UnregisterEvent("GROUP_ROSTER_UPDATE")
		frame:UnregisterEvent("PLAYER_XP_UPDATE")
		frame:UnregisterEvent("PLAYER_ENTERING_WORLD")
		frame:UnregisterEvent("QUEST_LOG_UPDATE");
		frame:SetScript("OnEvent", nil);
	end
end

function PXP:Initialize()
	CUI:GenerateQuestXPArray();

	CUI:RegisterDB(self, "pxp");

 	frame = CreateFrame("Frame", "ChaoticUI_PartyXPHolder", E.UIParent)
 	frame:Size(self.db.width, self.db.height)
 	frame:Point("TOP", E.UIParent, "TOP", 0, -50)
 	E:CreateMover(frame, 'ChaoticUI_PartyXPHolderMover', 'ElvUI PartyXP Holder', nil, nil, nil, 'GENERAL,ALL')
	for i = 1,4 do
		p = i == 1 and ChaoticUI_PartyXPHolder or bars[i-1]
		bars[i] = PXP:CreateBarGroup(p)
		PXP:HideBar(bars[i])
	end
	self.frame = frame;
	self:Enable();
	C_ChatInfo.RegisterAddonMessagePrefix("PXP")
	C_ChatInfo.RegisterAddonMessagePrefix("PXPQXP")
end

function PXP:Message()
	local current, max = UnitXP("player"), UnitXPMax("player")
	local rested = GetXPExhaustion()
	local level = UnitLevel("player")

	if level == MAX_PLAYER_LEVEL_TABLE[GetExpansionLevel()] then return end

	local message
	if IsXPUserDisabled() then 
		message = ("[DISABLED]") 
	else
		message = ("[l:%dc:%dm:%dr:%d]"):format(level, current, max, rested)
	end

	C_ChatInfo.SendAddonMessage("PXP",message,"PARTY")
end

function PXP:QXPMessage(val)
	local level = UnitLevel("player")

	if level == MAX_PLAYER_LEVEL_TABLE[GetExpansionLevel()] or IsXPUserDisabled() then return end

	local message = ("[qxp:%d]"):format(val)

	C_ChatInfo.SendAddonMessage("PXPQXP",message,"PARTY")
end

CUI:RegisterModule(PXP:GetName())