local CUI, E, L, V, P, G = unpack(select(2, ...))

local ADB = CUI:NewModule("AnimatedDataBars")
local DB = E:GetModule("DataBars")
local LSM = LibStub("LibSharedMedia-3.0");

local CUI_GetLevelMap = {
	['Experience'] = function()
		return UnitLevel("player");
	end,
	['Honor'] = function()
		return UnitHonorLevel("player");
	end,
	['Azerite'] = function()
	    local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
	    if (not azeriteItemLocation) then 
		    return 0; 
	    end
	
	    return C_AzeriteItem.GetPowerLevel(azeriteItemLocation); 
    end,
	['Reputation'] = function()
		return 1;
	end,
}

ADB["TickBars"] = {};

function ADB:CreateTicks(bar)
	bar.ticks = {};
	for i = 1, 19 do
		local tick = CreateFrame("Frame", nil, bar);
		tick:SetFrameLevel(bar:GetFrameLevel()+3);
		tick:SetTemplate('Transparent');
		tick:SetPoint("LEFT", i * (bar:GetWidth()/20), 0);
		tick:EnableMouse(false);
		bar.ticks[i] = tick;
	end
	tinsert(ADB["TickBars"], bar);
	self:UpdateTicksForBar(bar);
end

function ADB:UpdateTicksForBar(bar)
	for i = 1, 19 do
		local tick = bar.ticks[i];
		tick:Size(self.db.ticks.width, bar:GetHeight());
		tick:SetAlpha(self.db.ticks.alpha);
		tick:SetShown(self.db.ticks.enabled);
	end
end

function ADB:UpdateTicks()
	for i, bar in ipairs(ADB["TickBars"]) do
		ADB:UpdateTicksForBar(bar);
	end
end

function ADB:CreateAnimatedBar(key)
	local bar = _G["ElvUI_"..key.."Bar"];
	bar:SetTemplate('Default')
	tremove(E.statusBars, CUI:InvertTable(E.statusBars)[bar.statusBar]);
	local color = {bar.statusBar:GetStatusBarColor()};
	local value = bar.statusBar:GetValue();
	local min, max = bar.statusBar:GetMinMaxValues();
	local level = CUI_GetLevelMap[key]();
	bar.statusBar:Kill();

	local db = DB.db[string.lower(key)];
	
	bar.animatedStatusBar = CreateFrame('StatusBar', nil, bar, 'AnimatedStatusBarTemplate');
	bar.animatedStatusBar:SetInside()
	bar.animatedStatusBar:SetStatusBarTexture(E.media.normTex)
	bar.animatedStatusBar:SetStatusBarColor(unpack(color));
	bar.animatedStatusBar:SetOrientation(db.orientation)
	bar.animatedStatusBar:SetAnimatedTextureColors(unpack(color));
	bar.animatedStatusBar:SetAnimatedValues(value, min, max, level);
	bar.animatedStatusBar:ProcessChangesInstantly();
	E:RegisterStatusBar(bar.animatedStatusBar)
	bar.textFrame = CreateFrame("Frame", nil, bar.animatedStatusBar);
	bar.textFrame:SetAllPoints();
	bar.textFrame:SetFrameLevel(bar:GetFrameLevel()+5);
	bar.textFrame:Show();
	bar.text = bar.textFrame:CreateFontString(nil, 'OVERLAY')
	bar.text:FontTemplate(LSM:Fetch("font", db.font), db.textSize, "THINOUTLINE");

	bar.text:Point('CENTER', 0, 0)
	hooksecurefunc(DB, "Update"..key, function()
		ADB["Set"..key.."Value"](ADB, bar);
	end);
	self:CreateTicks(bar);
end

function ADB:SetAzeriteValue(bar)
	local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
	if (not azeriteItemLocation) then 
		return; 
	end
	
	local azeriteItem = Item:CreateFromItemLocation(azeriteItemLocation); 
	
	local xp, xpForNextLevel = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);

	local currentLevel = C_AzeriteItem.GetPowerLevel(azeriteItemLocation);
    if (xp == self.xp and xpForNextLevel == self.xpForNextLevel) then
		return;
	end
	self.xp = xp;
	self.xpForNextLevel = xpForNextLevel;
	bar.animatedStatusBar:SetAnimatedValues(xp, 0, xpForNextLevel, currentLevel);
end

function ADB:SetExperienceValue(bar)
	local cur, max = DB:GetXP('player')
	bar.animatedStatusBar:SetAnimatedValues(cur, 0, max, UnitLevel('player'));
end

function ADB:SetHonorValue(bar)
	local current = UnitHonor("player");
	local max = UnitHonorMax("player");
	local level = UnitHonorLevel("player");

	bar.animatedStatusBar:SetAnimatedValues(current, 0, max, level);
end
	
function ADB:SetReputationValue(bar)
	local ID
	local isFriend, friendText, standingLabel
	local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()
	local isParagon = false;
	if (C_Reputation.IsFactionParagon(factionID)) then
		local currentValue, threshold, _, hasRewardPending = C_Reputation.GetFactionParagonInfo(factionID)
		min, max = 0, threshold
		value = currentValue % threshold
		if hasRewardPending then 
			value = value + threshold
		end
		isParagon = true;
	end
	local numFactions = GetNumFactions();

	local level;

	for i=1, numFactions do
		local factionName, _, standingID,_,_,_,_,_,_,_,_,_,_, factionID = GetFactionInfo(i);
		local friendID, friendRep, friendMaxRep, _, _, _, friendTextLevel = GetFriendshipReputation(factionID);
		if factionName == name then
			if friendID ~= nil then
				-- do something different for friendships
				local friendID, friendRep, friendMaxRep, friendName, friendText, friendTexture, friendTextLevel, friendThreshold, nextFriendThreshold = GetFriendshipReputation(factionID);
				level = GetFriendshipReputationRanks(factionID);
				if ( nextFriendThreshold ) then
					min, max, value = friendThreshold, nextFriendThreshold, friendRep;
				else
					-- max rank, make it look like a full bar
					min, max, value = 0, 1, 1;
					isCappedFriendship = true;
				end
				reaction = 5;
				isFriend = true
				friendText = friendTextLevel
				ID = friendID
			else
				ID = standingID
				level = reaction
			end
		end
	end
	local color = FACTION_BAR_COLORS[reaction] or FACTION_BAR_COLORS[1];
	if (isParagon and ParagonReputation) then
		color = ParagonReputationDB;
	end
	bar.animatedStatusBar:SetStatusBarColor(color.r, color.g, color.b);
	bar.animatedStatusBar:SetAnimatedTextureColors(color.r, color.g, color.b);
	bar.animatedStatusBar:SetAnimatedValues(value, min, max, level);
	if (ID ~= bar.lastSeenFactionID) then
		bar.lastSeenFactionID = ID;
		bar.animatedStatusBar:ProcessChangesInstantly();
	end
end

function ADB:ReputationBar_OnEnter()
	if DB.db.reputation.mouseover then
		E:UIFrameFadeIn(self, 0.4, self:GetAlpha(), 1)
	end
	GameTooltip:ClearLines()
	GameTooltip:SetOwner(self, 'ANCHOR_CURSOR', 0, -4)

	local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()

	local standing = _G['FACTION_STANDING_LABEL'..reaction]
	if (C_Reputation.IsFactionParagon(factionID)) then
		local currentValue, threshold, _, hasRewardPending = C_Reputation.GetFactionParagonInfo(factionID)
		min, max = 0, threshold
		value = currentValue % threshold
		if hasRewardPending then 
			value = value + threshold
		end
		standing = "Paragon"
	end

	local friendID, _, _, _, _, _, friendTextLevel = GetFriendshipReputation(factionID);
	if name then
		GameTooltip:AddLine(name)
		GameTooltip:AddLine(' ')

		GameTooltip:AddDoubleLine(STANDING..':', friendID and friendTextLevel or standing, 1, 1, 1)
		GameTooltip:AddDoubleLine(REPUTATION..':', format('%d / %d (%d%%)', value - min, max - min, (value - min) / ((max - min == 0) and max or (max - min)) * 100), 1, 1, 1)
	end
	GameTooltip:Show()
end

function ADB:ExperienceBar_OnEnter()
	CUI:GenerateQuestXPArray();
	local questXP = CUI:CalculateQuestLogXP();

	if (questXP and questXP > 0) then
		GameTooltip:AddDoubleLine(L["|cffffffffCompleted Quest XP|cff606060*|r|cffffffff:|r"], format(' %d ', questXP));
	
		local min, max = UnitXP('player'), UnitXPMax('player')
		local cq = questXP + min
		if (cq >= max) then
			GameTooltip:AddLine(L["|cff20ff20You have enough quest xp to level up!"]);
		end
		GameTooltip:AddLine(L["|cff606060*|r |cff808080 Completed Quest XP calculated using local zone quests only"]);

		GameTooltip:Show();
	end
end

function ADB:Initialize()
	CUI:RegisterDB(self, "animateddatabars");
	for i, barName in ipairs({"Experience", "Reputation", "Azerite", "Honor"}) do
		local bar = _G["ElvUI_"..barName.."Bar"];
		if (bar) then
			self:CreateAnimatedBar(barName);
			if (barName == "Experience") then
				bar:HookScript("OnEnter", ADB.ExperienceBar_OnEnter);
            elseif (barName == "Reputation") then
				bar:SetScript("OnEnter", ADB.ReputationBar_OnEnter);
			end
		end
	end
end

CUI:RegisterModule(ADB:GetName());
