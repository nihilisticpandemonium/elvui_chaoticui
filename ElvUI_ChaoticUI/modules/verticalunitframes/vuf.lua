local addon, ns = ...
ns.oUF = ElvUF
local oUF = ns.oUF
local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local VUF = CUI:NewModule('VerticalUnitFrames','AceTimer-3.0','AceEvent-3.0');
local UF = E:GetModule('UnitFrames');
local LSM = LibStub("LibSharedMedia-3.0");


E.VerticalUnitFrames = VUF

function VUF:CreateWarningFrame()
	local f=CreateFrame("ScrollingMessageFrame","ElvUIVerticalUnitFramesWarning",UIParent)
	f:SetFont(LSM:Fetch("font", (UF.db or P.unitframe).font),(UF.db or P.unitframe).fontSize*2,"THINOUTLINE")
	f:SetShadowColor(0,0,0,0)
	f:SetFading(true)
	f:SetFadeDuration(0.5)
	f:SetTimeVisible(0.6)
	f:SetMaxLines(10)
	f:SetSpacing(2)
	f:SetWidth(128)
	f:SetHeight(128)
	f:SetPoint("CENTER",0,E:Scale(-100))
	f:SetMovable(false)
	f:SetResizable(false)
	--f:SetInsertMode("TOP") -- Bugged currently
end

function VUF:CreateScreenFlash()
	local f = CreateFrame('Frame', "ElvUIVerticalUnitFramesScreenFlash", E.UIParent);
	f:SetToplevel(true)
	f:SetFrameStrata("FULLSCREEN_DIALOG")
	f:SetAllPoints(E.UIParent)
	f:EnableMouse(false);
	f.texture = f:CreateTexture(nil, "BACKGROUND")
	f.texture:SetTexture("Interface\\FullScreenTextures\\LowHealth")
	f.texture:SetAllPoints(E.UIParent)
	f.texture:SetBlendMode("ADD")
	f:SetAlpha(0)
end


function VUF:ActivateFrame(frame)
	if (UnitExists(frame.unit)) then
		E:UIFrameFadeIn(frame, 0.2, frame:GetAlpha(), self.db.alpha);
	end
end

function VUF:DeactivateFrame(frame)
	if (InCombatLockdown() or GetMouseFocus() == frame and frame:IsMouseOver(2, -2, -2, 2)) then
		return;
	end

	E:UIFrameFadeOut(frame, 0.2, frame:GetAlpha(), self.db.alphaOOC);
end

function VUF:UpdateHideSetting()
	if (self.db.hideOOC) then
		self:EnableHide();
	else
		self:DisableHide();
	end
end

function VUF:Hide(frame,event)
	if frame.unit == 'target' then return end
	if not UnitExists(frame.unit) then return end

	if (event == "PLAYER_REGEN_DISABLED") then
		self:ActivateFrame(frame);
	elseif (event == "PLAYER_REGEN_ENABLED" or event == "PLAYER_ENTERING_WORLD") then
		self:DeactivateFrame(frame);
	end
end

function VUF:DisableFrame(f)
	f:Hide()
	f:EnableMouse(false)
	f:SetAlpha(0)
end

function VUF:EnableFrame(f,a,m)
	if a == nil then a = 1 end
	if m == nil then m = true end
	f:Show()
	f:EnableMouse(m)
	f:SetAlpha(a)
end

local elv_units = { 'player', 'target', 'pet', 'pettarget', 'targettarget', 'focus', 'focustarget' }
local old_settings = {}

function VUF:UpdateElvUFSetting()
	local value
	if E.db.chaoticui.vuf.enabled then value = not E.db.chaoticui.vuf.hideElv else value = true end
	for _,unit in pairs(elv_units) do
		if not value and old_settings[unit] == nil then old_settings[unit] = E.db.unitframe.units[unit]['enable'] end
		E.db.unitframe.units[unit]['enable'] = (value and old_settings[unit] or (E.db.chaoticui.vuf.units[unit]['enabled'] and value)) or value; UF:CreateAndUpdateUF(unit)
	end
end

function VUF:EnableHide()
	self:RegisterEvent("PLAYER_REGEN_DISABLED")
	self:RegisterEvent("PLAYER_REGEN_ENABLED")
	self:RegisterEvent("PLAYER_ENTERING_WORLD")
	self:PLAYER_ENTERING_WORLD();
end

function VUF:DisableHide(frame)
	self:UnregisterEvent("PLAYER_REGEN_DISABLED")
	self:UnregisterEvent("PLAYER_REGEN_ENABLED")
	self:UnregisterEvent("PLAYER_ENTERING_WORLD")
	self:PLAYER_ENTERING_WORLD();
	for _,f in pairs(self.units) do
		f:SetAlpha(1);
	end
end

function VUF:AuraBarsSuck()
	if (UnitAffectingCombat("player")) then CUI:RegenWait(self.AuraBarsSuck, self); return end
	VUF.enableAuraBars = true
	VUF:UpdateAllFrames()
	VUF.enableAuraBars = false
end

function VUF:Enable()
	self:UpdateElvUFSetting()
	for _,f in pairs(self.units) do
		if not self.db.enabled then
			VUF:DisableFrame(f)
		else
			VUF:EnableFrame(f,self.db.alpha,self.db.enableMouse)
		end
	end
	
	if (self.db.hideOOC) then
		self:EnableHide();
	else
		self:DisableHide();
	end
	self:RegisterEvent("PLAYER_TARGET_CHANGED");
	VUF:ScheduleTimer('AuraBarsSuck',5)
end

function VUF:UpdateMouseSetting()
	for _,f in pairs(self.units) do
		if self.db.enableMouse or self.db.hideElv then
			f:EnableMouse(true)
		else
			f:EnableMouse(false)
		end
	end
end

function VUF:UpdateAll()
	if UnitAffectingCombat("player") then CUI:RegenWait(self.UpdateAll, self); return end

	self:UpdateAllFrames()
	self:UpdateMouseSetting()
	self:UpdateHideSetting()
	self:Enable()
end
function VUF:PLAYER_TARGET_CHANGED()
	local frame = self.units['target']

	frame:DisableElement('AuraBars');

	VUF:ScheduleTimer('AuraBarsSuck',0.25);

	self:UnregisterEvent("PLAYER_TARGET_CHANGED");
end

function VUF:PLAYER_REGEN_DISABLED()
	for _, frame in pairs(self.units) do
		if (frame.unit ~= "target") then
			VUF:Hide(frame, "PLAYER_REGEN_DISABLED");
		end
	end
end

function VUF:PLAYER_REGEN_ENABLED()
	for _, frame in pairs(self.units) do
		if (frame.unit ~= "target") then
			VUF:Hide(frame, "PLAYER_REGEN_ENABLED");
		end
	end
end

function VUF:PLAYER_ENTERING_WORLD()
	if (self.db.hideOOC) then
		if (InCombatLockdown()) then
			self:PLAYER_REGEN_DISABLED();
		else
			self:PLAYER_REGEN_ENABLED();
		end
	end
	self:UnregisterEvent("PLAYER_ENTERING_WORLD")
end

function VUF:UNIT_HEALTH_FREQUENT(event, unit)
	if (not self.db.hideOOC) then
		return;
	end

	if (not self.units[unit] or unit == "target") then
		return;
	end

	local f = self.units[unit]
	local healthSeen = UnitHealth(unit);
	if (healthSeen == UnitHealthMax(unit) or healthSeen == f.healthSeen) then
		if (not UnitAffectingCombat("player") and not UnitAffectingCombat("pet")) then
			VUF:Hide(f, "PLAYER_REGEN_ENABLED");
		end
		f.healthSeen = nil;
	else
		VUF:Hide(f, "PLAYER_REGEN_DISABLED");
		f.HealthSeen = healthSeen;
	end
end

function VUF:ResetFramePositions()
	E:ResetMovers('Focus Vertical Unit Frame');
	E:ResetMovers('Focus Target Vertical Unit Frame');
	E:ResetMovers('Pet Vertical Unit Frame');
	E:ResetMovers('Pet Target Vertical Unit Frame');
	E:ResetMovers('Player Vertical Unit Frame');
	E:ResetMovers('Target Vertical Unit Frame');
	E:ResetMovers('Target Target Vertical Unit Frame');
end

VUF.units = {};

function VUF:Initialize()
	CUI:RegisterDB(self, "vuf")
	
	self:CreateWarningFrame()
	self:CreateScreenFlash()

	oUF:RegisterStyle('ChaoticUI_VerticalUnitFrames',function(frame,unit)
		frame:SetFrameLevel(5)
		VUF:ConstructVerticalUnitFrame(frame,unit)
	end)

	oUF:SetActiveStyle('ChaoticUI_VerticalUnitFrames')

	local units = { 'player', 'target', 'pet', 'targettarget', 'pettarget', 'focus', 'focustarget' }

	for _,unit in pairs(units) do
		local stringTitle = E:StringTitle(unit)
		if stringTitle:find('target') then
			stringTitle = gsub(stringTitle, 'target', 'Target')
		end
		oUF:Spawn(unit, "ChaoticUF_"..stringTitle)
	end

	hooksecurefunc(E,"UpdateAll",function(self,ignoreInstall) VUF:UpdateAll() end)
	hooksecurefunc(UF,"Update_AllFrames",function(self) VUF:UpdateAllFrames() end)
	
	if UnitAffectingCombat("player") then CUI:RegenWait(self.UpdateAll, self) else self:UpdateAll() end

	self:RegisterEvent("UNIT_HEALTH_FREQUENT");

	self.version = GetAddOnMetadata(addon,'Version')
end

CUI:RegisterModule(VUF:GetName())
