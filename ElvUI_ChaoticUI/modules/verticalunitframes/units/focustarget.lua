local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local VUF = CUI:GetModule('VerticalUnitFrames');

function VUF:ConstructFocusTargetFrame(frame,unit)
	frame.unit = unit
	frame.Health = self:ConstructHealth(frame)

	frame.Name = self:ConstructName(frame)

	frame.Power = self:ConstructPower(frame)
	frame.RaidTargetIndicator = self:ConstructRaidIcon(frame)

	frame.HealPrediction = self:ConstructHealComm(frame)
	
	frame:SetAlpha(self.db.alpha)
	VUF:HookSetAlpha(frame);
	
	frame:Point("BOTTOMLEFT", ChaoticUF_Focus, "BOTTOMRIGHT", 110, 0)
	E:CreateMover(frame, frame:GetName()..'Mover', 'Focus Target Vertical Unit Frame', nil, nil, nil, 'ALL,SOLO')
end
