local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local VUF = CUI:GetModule('VerticalUnitFrames');
local UF = E:GetModule('UnitFrames');

function VUF:ConstructPlayerFrame(frame,unit)
	frame.unit = unit
	frame.Health = self:ConstructHealth(frame)

	frame.Power = self:ConstructPower(frame)

	frame.Castbar = self:ConstructCastbar(frame)
	frame.Castbar.LatencyTexture:Show()

	frame.Name = self:ConstructName(frame)
		
	frame.DummyAuraBars = self:ConstructAuraBarHeader(frame)
	frame:DisableElement('AuraBars') -- disable it until its configured

	if E.myclass == "WARLOCK" then
		frame.ClassPower = self:ConstructShardBar(frame)
		frame.ClassBar = frame.ClassPower;
	end

	if E.myclass == "PALADIN" then
		frame.ClassPower = self:ConstructHolyPower(frame)
		frame.ClassBar = frame.ClassPower;
	end

	if E.myclass == "DEATHKNIGHT" then
		frame.Runes = self:ConstructRunes(frame)
		frame.ClassBar = frame.Runes;
	end

	if E.myclass == "MONK" then
		frame.ClassPower = self:ConstructHarmony(frame)
		frame.Stagger = self:ConstructStagger(frame)
		frame.ClassBar = frame.ClassPower;
	end

	if E.myclass == "MAGE" then
		frame.ClassPower = self:ConstructArcaneBar(frame)
		frame.ClassBar = frame.ClassPower;
	end

	if E.myclass == "ROGUE" or E.myclass == "DRUID" then
		frame.ClassPower = self:ConstructComboPoints(frame)
		frame.ClassBar = frame.ClassPower;
	end
	
	if E.myclass == "PRIEST" or E.myclass == "SHAMAN" or E.myclass == "DRUID" then
		frame.AdditionalPower = self:ConstructAdditionalPower(frame)
	end

	frame:DisableElement('ClassBar');
	
	frame.Buffs = self:ConstructBuffs(frame)
	frame.Debuffs = self:ConstructDebuffs(frame)
	frame.RaidTargetIndicator = self:ConstructRaidIcon(frame)
	frame.Resting = self:ConstructRestingIndicator(frame)
	frame.Combat = self:ConstructCombatIndicator(frame)
	frame.PvPText = self:ConstructPvPIndicator(frame)
	frame.HealPrediction = self:ConstructHealComm(frame)
	frame.PowerPrediction = self:ConstructPowerPrediction(frame)
	frame.GCD = self:ConstructGCD(frame)
	frame.Portrait = self:ConstructPortrait(frame)
	frame.ResurrectIcon = self:ConstructResurrectIcon(frame)
	frame:SetAlpha(self.db.alpha)
	VUF:HookSetAlpha(frame)
	
	frame:Point("RIGHT", E.UIParent, "CENTER", -275, 0) --Set to default position 
	E:CreateMover(frame, frame:GetName()..'Mover', 'Player Vertical Unit Frame', nil, nil, nil, 'ALL,SOLO')
end

