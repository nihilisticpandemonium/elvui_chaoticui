local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local VUF = CUI:GetModule('VerticalUnitFrames');
local UF = E:GetModule('UnitFrames');
local LSM = LibStub("LibSharedMedia-3.0");

-- Health for all units
function VUF:ConstructHealth(frame)
	self:AddElement(frame,'health')

	-- Health Bar
	local health = self:ConstructStatusBar(frame,'health')
	health:SetFrameStrata("LOW")
	health:SetOrientation("VERTICAL")
	health:SetFrameLevel(frame:GetFrameLevel() + 5)
	health:Point("LEFT",frame,"LEFT")
	health.value = self:ConstructFontString(frame,'health',health)		
	health.PostUpdate = VUF.PostUpdateHealth
	health.frequentUpdates = true

	health.colorSmooth = nil
	health.colorHealth = nil
	health.colorClass = nil
	health.colorReaction = nil
	if UF.db['colors'].healthclass ~= true then
		if UF.db['colors'].colorhealthbyvalue == true then
			health.colorSmooth = true
		else
			health.colorHealth = true
		end	 
	else
		health.colorClass = true
		health.colorReaction = true
	end	

	return health
end

-- Power for units it is enabled on
function VUF:ConstructPower(frame)
	self:AddElement(frame,'power')
	
	local power = self:ConstructStatusBar(frame,'power')
	power:SetOrientation("VERTICAL")
	power:SetFrameLevel(frame:GetFrameLevel()+20)

	power.value = self:ConstructFontString(frame,'power',power)			   
	
	power.PreUpdate = VUF.PreUpdatePowerVerticalUnitFrame
	power.PostUpdate = VUF.PostUpdatePowerVerticalUnitFrame

	-- Update the Power bar Frequently
	power.frequentUpdates = true

	power.colorTapping = true   
	power.colorPower = true
	power.colorReaction = true
	power.colorDisconnected = true	  

	return power
end 

-- Power for units it is enabled on
function VUF:ConstructAdditionalPower(frame)
	self:AddElement(frame,'additionalpower')
	
	local power = self:ConstructStatusBar(frame,'additionalpower')
	power:SetOrientation("VERTICAL")
	power:SetFrameLevel(frame:GetFrameLevel()+20)

	power.value = self:ConstructFontString(frame,'additionalpower',power)			   

	-- Update the Power bar Frequently
	power.frequentUpdates = true

	power.colorTapping = true   
	power.colorPower = true
	power.colorReaction = true
	power.colorDisconnected = true	  

	return power
end


-- Castbar for units it is enabled on
-- For player/target castbar can be (and defaults) to horizontal mode.
-- For pet/targettarget/pettarget castbar is always vertical overlaid on the power bar.
-- Note in this version the castbar is no longer anchored to the power bar, so each
-- element can be disabled independently
function VUF:ConstructCastbar(frame)
	self:AddElement(frame,'hcastbar')
	self:AddElement(frame,'vcastbar')
	self:AddElement(frame,'castbar')
	local hcastbar = self:ConstructStatusBar(frame,'hcastbar')
	local vcastbar = self:ConstructStatusBar(frame,'vcastbar')

	vcastbar.CustomDelayText = VUF.CustomCastDelayText
	vcastbar.CustomTimeText = VUF.CustomTimeText
	vcastbar.PostCastStart = VUF.PostCastStart
	vcastbar.PostChannelStart = VUF.PostCastStart
	vcastbar.PostCastStop = UF.PostCastStop
	vcastbar.PostChannelStop = UF.PostCastStop
	vcastbar.PostChannelUpdate = VUF.PostChannelUpdate
	vcastbar.PostCastInterruptible = UF.PostCastInterruptible
	vcastbar.PostCastNotInterruptible = UF.PostCastNotInterruptible
	vcastbar.OnUpdate = VUF.CastbarUpdate

	vcastbar:SetOrientation("VERTICAL")
	vcastbar:SetFrameStrata(frame.Power:GetFrameStrata())
	vcastbar:SetFrameLevel(frame.Power:GetFrameLevel()+2)

	vcastbar.Time = self:ConstructFontString(frame,'vcastbar',vcastbar,'time')
	vcastbar.Time:Point("BOTTOM", vcastbar, "TOP", 0, 4)
	vcastbar.Time:SetTextColor(0.84, 0.75, 0.65)
	vcastbar.Time:SetJustifyH("RIGHT")
	
	vcastbar.Text = self:ConstructFontString(frame,'vcastbar',vcastbar,'text')
	vcastbar.Text:SetPoint("TOP", vcastbar, "BOTTOM", 0, -4)
	vcastbar.Text:SetTextColor(0.84, 0.75, 0.65)
	
	vcastbar.Spark = vcastbar:CreateTexture(nil, 'OVERLAY')
	vcastbar.Spark:Height(12)
	vcastbar.Spark:SetBlendMode('ADD')
	vcastbar.Spark:SetVertexColor(1, 1, 1)

	--Set to vcastbar.SafeZone
	vcastbar.LatencyTexture = self:ConfigureTexture(frame,'vcastbar',vcastbar,'latency')
	vcastbar.LatencyTexture:SetVertexColor(0.69, 0.31, 0.31, 0.75)   
	vcastbar.SafeZone = vcastbar.LatencyTexture
	
	local button = CreateFrame("Frame", nil, vcastbar)
	button:SetTemplate("Default")
	
	button:Point("BOTTOM", vcastbar, "BOTTOM", 0, 0)
	
	local icon = button:CreateTexture(nil, "ARTWORK")
	icon:Point("TOPLEFT", button, 2, -2)
	icon:Point("BOTTOMRIGHT", button, -2, 2)
	icon:SetTexCoord(0.08, 0.92, 0.08, .92)
	icon.bg = button
	
	--Set to castbar.Icon
	vcastbar.ButtonIcon = icon
	frame.VertCastbar = vcastbar

	hcastbar:SetOrientation("HORIZONTAL");
	hcastbar:SetFrameLevel(6)
	hcastbar:SetRotatesTexture(false)
	
	hcastbar.CustomTimeText = VUF.CustomCastTimeText
	hcastbar.CustomDelayText = VUF.CustomCastDelayText
	hcastbar.CustomDelayText = VUF.CustomCastDelayText
	hcastbar.CustomTimeText = VUF.CustomTimeText
	hcastbar.PostCastStart = VUF.PostCastStart
	hcastbar.PostChannelStart = VUF.PostCastStart
	hcastbar.PostCastStop = UF.PostCastStop
	hcastbar.PostChannelStop = UF.PostCastStop
	hcastbar.PostChannelUpdate = VUF.PostChannelUpdate
	hcastbar.PostCastInterruptible = UF.PostCastInterruptible
	hcastbar.PostCastNotInterruptible = UF.PostCastNotInterruptible

	hcastbar.Time = self:ConstructFontString(frame,'hcastbar',hcastbar,'time')
	hcastbar.Time:SetPoint("RIGHT", hcastbar, "RIGHT", -4, 0)
	hcastbar.Time:SetTextColor(0.84, 0.75, 0.65)
	hcastbar.Time:SetJustifyH("RIGHT")
	
	hcastbar.button = CreateFrame("Frame", nil, hcastbar)
	hcastbar.button:Size(26)
	hcastbar.button:SetTemplate("Default")
	hcastbar.button:CreateShadow("Default")

	hcastbar.Spark = hcastbar:CreateTexture(nil, 'OVERLAY')
	hcastbar.Spark:SetBlendMode('ADD')
	hcastbar.Spark:SetVertexColor(1, 1, 1)
	hcastbar.Spark:Width(12)

	hcastbar.Text = self:ConstructFontString(frame,'hcastbar',hcastbar,'text')
	hcastbar.Text:SetTextColor(0.84, 0.75, 0.65)
	hcastbar.Text:SetPoint("LEFT", hcastbar.button, "RIGHT", 4, 0)

	hcastbar.Icon = hcastbar.button:CreateTexture(nil, "ARTWORK")
	hcastbar.Icon:Point("TOPLEFT", hcastbar.button, 2, -2)
	hcastbar.Icon:Point("BOTTOMRIGHT", hcastbar.button, -2, 2)
	hcastbar.Icon:SetTexCoord(0.08, 0.92, 0.08, .92)

	hcastbar.button:SetPoint("LEFT")

	--Set to castbar.SafeZone
	hcastbar.LatencyTexture = self:ConfigureTexture(frame,'hcastbar',hcastbar,'latency')
	hcastbar.LatencyTexture:SetVertexColor(0.69, 0.31, 0.31, 0.75)   
	hcastbar.SafeZone = hcastbar.LatencyTexture
	frame.HorizCastbar = hcastbar

	if frame.unit == "player" then
		hcastbar:HookScript("OnShow", function(self) if E.db.chaoticui.vuf.hideOOC and not InCombatLockdown() then frame.casting = true; VUF:Hide(frame,"PLAYER_REGEN_DISABLED") end end)
		hcastbar:HookScript("OnHide", function(self) if E.db.chaoticui.vuf.hideOOC and not InCombatLockdown() then frame.casting = false; VUF:Hide(frame,"PLAYER_REGEN_ENABLED") end end)
		vcastbar:HookScript("OnShow", function(self) if E.db.chaoticui.vuf.hideOOC and not InCombatLockdown() then frame.casting = true; VUF:Hide(frame,"PLAYER_REGEN_DISABLED") end end)
		vcastbar:HookScript("OnHide", function(self) if E.db.chaoticui.vuf.hideOOC and not InCombatLockdown() then frame.casting = false; VUF:Hide(frame,"PLAYER_REGEN_ENABLED") end end)
	end
	
	if (frame.unit ~= 'player' and frame.unit ~= 'target') or not self.db.units[frame.unit].horizCastbar then
		return vcastbar
	else
		return hcastbar
	end
end

-- Name element
function VUF:ConstructName(frame)
	self:AddElement(frame,'name')
	local name = self:ConstructFontString(frame,'name')
	return name
end

function VUF:ConstructStagger(frame)
	self:AddElement(frame, 'stagger');

	local staggerBar = self:ConstructStatusBar(frame,'stagger', frame, 'staggerbar');
	staggerBar:SetFrameStrata("MEDIUM")
	staggerBar:SetTemplate("Default")
	staggerBar:SetFrameLevel(8)
	staggerBar:SetBackdropBorderColor(0,0,0,0)
	staggerBar.PostUpdateVisibility = VUF.PostUpdateStaggerBar
	
	return staggerBar;
end

function VUF:ConstructSubBars(frame,element,name,num)
	self:AddElement(frame,element)

	local bars = self:ConfigureFrame(frame,element,true)
	bars:SetFrameLevel(frame:GetFrameLevel() + 30)
	bars:SetTemplate("Default")
	bars:SetBackdropBorderColor(0,0,0,0)

	if (element == 'classbars') then
		bars.UpdateTexture = function() return end --We don't use textures but statusbars, so prevent errors
	end

	for i = 1, num do
		bars[i] = self:ConstructStatusBar(frame,element,frame,name..i)
		bars[i]:SetFrameStrata("MEDIUM")
		bars[i]:SetFrameLevel(frame:GetFrameLevel() + 35)
		bars[i]:SetHeight(math.floor(E.db.chaoticui.vuf.units[frame.unit].height / num));

		if i == 1 then
			bars[i]:SetPoint("BOTTOM", bars, "BOTTOM", 0, 0)
		else
			bars[i]:SetPoint("BOTTOM", bars[i-1], "TOP", 0, E:Scale(1))
		end
	 
		bars[i]:SetOrientation('VERTICAL')
	end

	if element == 'classbars' then
		bars.value = self:ConstructFontString(frame,element,frame)				
		bars.value:Hide()
	end

	return bars
end

-- Warlock spec bars
function VUF:ConstructShardBar(frame)
	local bars = self:ConstructSubBars(frame,'classbars','shardbar',5)

	bars._PostUpdate = VUF.PostUpdateSoulShards;

	return bars;
end

-- Construct holy power for paladins
function VUF:ConstructHolyPower(frame)
	local bars = self:ConstructSubBars(frame,'classbars','holypower',5)

	bars._PostUpdate = VUF.PostUpdateHolyPower;

	return bars;
end

-- Runes for death knights
function VUF:ConstructRunes(frame)
	local bars = self:ConstructSubBars(frame,'classbars','rune',6)

	bars.Override = VUF.UpdateRunes;
	bars.colorSpec = true;

	return bars;
end

-- Construct harmony bar for monks
function VUF:ConstructHarmony(frame)
   return self:ConstructSubBars(frame,'classbars','harmony',6)
end

-- Construct arcane bar for mages
function VUF:ConstructArcaneBar(frame)
	local bars = self:ConstructSubBars(frame,'classbars','arcanecharge',4) 
	
	bars._PostUpdate = VUF.PostUpdateArcaneChargeBar

	return bars
end

-- Combo points for rogues and druids
function VUF:ConstructComboPoints(frame)
	local bars = self:ConstructSubBars(frame,'classbars','combopoint',E.myclass == "ROGUE" and 10 or 5);
	
	bars._PostUpdate = VUF.PostUpdateComboPoints

	return bars
end

function VUF.ConstructAuraBars(self,unit)
	local bar = self.statusBar
	
	self:SetTemplate('Default')

	bar:SetInside(self)

	bar:SetStatusBarTexture(LSM:Fetch("statusbar", UF.db.statusbar))
	
	bar.spelltime:FontTemplate(LSM:Fetch("font", UF.db.font), UF.db.fontSize, "THINOUTLINE")
	bar.spellname:FontTemplate(LSM:Fetch("font", UF.db.font), UF.db.fontSize, "THINOUTLINE")

	bar.spellname:ClearAllPoints()
	bar.spellname:SetPoint('LEFT', bar, 'LEFT', 2, 0)
	bar.spellname:SetPoint('RIGHT', bar.spelltime, 'LEFT', -4, 0)
	
	bar.iconHolder:SetTemplate('Default')
	bar.icon:SetInside(bar.iconHolder)
	bar.icon:SetDrawLayer('OVERLAY')
	
	bar.bg = bar:CreateTexture(nil, 'BORDER')
	bar.bg:Hide()
	
	if (CUI:GetModule('EnhancedShadows', true)) then
		local ES = CUI:GetModule('EnhancedShadows');
		self:CreateShadow();
		ES:RegisterShadow(self.shadow);
	end

	bar.iconHolder:RegisterForClicks('RightButtonUp')
	bar.iconHolder:SetScript('OnClick', function(self)
		if not IsShiftKeyDown() then return; end
		local auraName = self:GetParent().aura.name
		
		if auraName then
			E:Print(format(L['The spell "%s" has been added to the Blacklist unitframe aura filter.'], auraName))
			E.global['unitframe']['aurafilters']['Blacklist']['spells'][auraName] = {
				['enable'] = true,
				['priority'] = 0,		   
			}
			UF:Update_AllFrames()
		end
	end)
end

function VUF:ConstructPlayerAuraBars()
	VUF.ConstructAuraBars(self,"player")
end

function VUF:ConstructTargetAuraBars()
	VUF.ConstructAuraBars(self,"target")
end

function VUF:ConstructAuraBarHeader(frame)
	self:AddElement(frame,'aurabars')
	local auraBar = self:ConfigureFrame(frame,'aurabars')

	if frame.unit == "player" then
		auraBar.PostCreateBar = VUF.ConstructPlayerAuraBars
	else
		auraBar.PostCreateBar = VUF.ConstructTargetAuraBars
	end
	local classColor = E.myclass == 'PRIEST' and E.PriestColors or RAID_CLASS_COLORS[E.myclass];
	auraBar.PostUpdate = UF.ColorizeAuraBars
	auraBar.gap = (E.PixelMode and -1 or 1)
	auraBar.spacing = (E.PixelMode and -1 or 1)
	auraBar.spark = true
	auraBar.PostUpdate = UF.ColorizeAuraBars
	auraBar.filter = UF.AuraBarFilter
	auraBar.down = true	
	if (frame.db) then
		frame.db.aurabar = UF.db['units'][frame.unit].aurabar
	end
	return auraBar
end

function VUF:ConstructRaidIcon(frame)
	self:AddElement(frame,'raidicon')
	local f = CreateFrame('Frame', nil, frame)
	f:SetFrameLevel(20)
	
	local tex = f:CreateTexture(nil, "OVERLAY")
	tex:SetTexture([[Interface\TargetingFrame\UI-RaidTargetingIcons]])

	return tex
end

function VUF:ConstructRestingIndicator(frame)
	self:AddElement(frame,'resting')
	local resting = frame:CreateTexture(nil, "OVERLAY")
	resting:Size(16)
	
	return resting
end

function VUF:ConstructCombatIndicator(frame)
	self:AddElement(frame,'combat')
	local combat = frame:CreateTexture(nil, "OVERLAY")
	combat:Size(13)
	combat:SetVertexColor(0.69, 0.31, 0.31)
	
	return combat
end

function VUF:ConstructResurrectIcon(frame)
	self:AddElement(frame,'resurrecticon')
	local tex = frame:CreateTexture(nil, "OVERLAY")
	tex:Point('CENTER', frame.Health, 'CENTER')
	tex:Size(30, 25)
	tex:SetDrawLayer('OVERLAY', 7)
	
	return tex
end

function VUF:ConstructPvPIndicator(frame)
	self:AddElement(frame,'pvp')
	local pvp = self:ConstructFontString(frame,'pvp')
	pvp:SetTextColor(0.69, 0.31, 0.31)
	
	return pvp
end

function VUF:ConstructHealComm(frame)
	self:AddElement(frame,'healcomm')
	local mhpb = self:ConstructStatusBar(frame,'healcomm',frame,'mybar',true)
	mhpb:SetStatusBarTexture(E["media"].blankTex)
	mhpb:SetFrameLevel(frame.Health:GetFrameLevel() - 2)
	mhpb:SetOrientation("VERTICAL")
	mhpb:Hide()
	
	local ohpb = self:ConstructStatusBar(frame,'healcomm',frame,'otherbar',true)
	ohpb:SetStatusBarTexture(E["media"].blankTex)
	ohpb:SetFrameLevel(mhpb:GetFrameLevel())
	ohpb:SetOrientation("VERTICAL")	
	ohpb:Hide()
	
	local absorbBar = self:ConstructStatusBar(frame,'healcomm',frame,'absorbbar',true)
	absorbBar:SetStatusBarTexture(E["media"].blankTex)
	absorbBar:SetFrameLevel(mhpb:GetFrameLevel())
	absorbBar:SetOrientation("VERTICAL")
	absorbBar:Hide()

	local healAbsorbBar = self:ConstructStatusBar(frame,'healcomm',frame,'healabsorbbar',true)
	healAbsorbBar:SetStatusBarTexture(E["media"].blankTex)
	healAbsorbBar:SetFrameLevel(mhpb:GetFrameLevel())
	healAbsorbBar:SetOrientation("VERTICAL")
	healAbsorbBar:Hide()

	if frame.Health then
		ohpb:SetParent(frame.Health)
		mhpb:SetParent(frame.Health)
		absorbBar:SetParent(frame.Health)
		healAbsorbBar:SetParent(frame.Health)
	end
	
	return {
		myBar = mhpb,
		otherBar = ohpb,
		absorbBar = absorbBar,
		healAbsorbBar = healAbsorbBar,
		maxOverflow = 1,
		PostUpdate = UF.UpdateHealComm
	}
end

function VUF:ConstructPowerPrediction(frame)
	self:AddElement(frame,'powerprediction')
	local mb = self:ConstructStatusBar(frame, "powerprediction", frame, "mainbar", true);
	mb:SetStatusBarTexture(E["media"].blankTex)
	mb:SetOrientation("VERTICAL")
	mb:SetReverseFill(true)
	mb:Hide();

	local ab = self:ConstructStatusBar(frame, "powerprediction", frame, "altbar", true)
	ab:SetStatusBarTexture(E["media"].blankTex)
	ab:SetOrientation("VERTICAL")
	ab:SetReverseFill(true)
	ab:Hide();

	if (frame.Power) then
		mb:SetFrameLevel(frame.Power:GetFrameLevel()-2)
		mb:SetPoint("LEFT")
		mb:SetPoint("RIGHT")
		mb:SetParent(frame.Power)
	end

	if (frame.AdditionalPower) then
		ab:SetFrameLevel(frame.AdditionalPower:GetFrameLevel()-2)
		ab:SetPoint("LEFT")
		ab:SetPoint("RIGHT")
		ab:SetParent(frame.AdditionalPower)
	end

	return {
		mainBar = mb,
		altBar = ab,
		PreUpdate = VUF.PreUpdatePowerPrediction
	}
end

function VUF:ConstructGCD(frame)
	self:AddElement(frame,'gcd')
	local GCD = self:ConstructStatusBar(frame,'gcd')
	GCD:SetStatusBarColor(.8,.8,.8,0)
	GCD:SetAlpha(1)
	GCD:SetOrientation('VERTICAL')
	GCD:SetFrameStrata(frame.Power:GetFrameStrata())
	GCD:SetFrameLevel(frame.Power:GetFrameLevel()+2)
	
	GCD.Spark = GCD:CreateTexture(frame:GetName().."_GCDSpark", "OVERLAY")
	GCD.Spark:SetTexture("Interface\\CastingBar\\UI-CastingBar-Spark")
	GCD.Spark:SetVertexColor(1,1,1)
	GCD.Spark:Height(12)
	GCD.Spark:Point('CENTER',GCD:GetStatusBarTexture(),'TOP')
	GCD.Spark:SetBlendMode("ADD")

	return GCD
end

function VUF:ConstructDebuffs(frame)
	self:AddElement(frame,'debuffs')
	local debuffs = self:ConfigureFrame(frame,'debuffs')

	debuffs.size = 26
	debuffs.num = 36

	debuffs.spacing = 2
	debuffs.initialAnchor = "TOPRIGHT"
	debuffs["growth-y"] = "UP"
	debuffs["growth-x"] = "LEFT"
	debuffs.PostCreateIcon = self.PostCreateAura
	debuffs.PostUpdateIcon = self.PostUpdateAura	   
	debuffs.CustomFilter = UF.AuraFilter
	debuffs.type = 'debuffs'

	-- an option to show only our debuffs on target
	--[[if unit == "target" then
		debuffs.onlyShowPlayer = C.unitframes.onlyselfdebuffs
	end]]
	return debuffs
end

function VUF:ConstructBuffs(frame)
	self:AddElement(frame,'buffs')
	local buffs = self:ConfigureFrame(frame,'buffs')
						
	buffs.size = 26
	buffs.num = 36
	buffs.numRow = 9
							
	buffs.spacing = 2
	buffs.initialAnchor = "TOPLEFT"
	buffs.PostCreateIcon = VUF.PostCreateAura
	buffs.PostUpdateIcon = VUF.PostUpdateAura
	buffs.CustomFilter = UF.AuraFilter
	buffs.type = 'buffs'

	return buffs
end 

local function CreatePortaitOverlay(frame,portrait)
	local pOverlay = CreateFrame("Frame", nil, frame.Health)
	pOverlay:SetBackdrop({ bgFile = E["media"].blankTex, })
	pOverlay:SetBackdropColor(.1, .1, .1, 1)
	pOverlay:SetPoint("BOTTOMLEFT", portrait)
	pOverlay:SetPoint("TOPRIGHT", portrait)
	portrait.overlay = pOverlay
end

function VUF:ConstructPortrait(frame)
	self:AddElement(frame,'portrait')
	local portrait = self:ConfigureFrame(frame,'portrait',nil,frame.Health)
	
	portrait = CreateFrame("PlayerModel", nil, frame)
	portrait:SetFrameStrata('LOW')
	
	portrait.PostUpdate = self.PortraitUpdate

	CreatePortaitOverlay(frame,portrait)

	return portrait
end

function VUF:ConstructPhaseIndicator(frame)
	self:AddElement(frame,'phaseindicator');
	local phaseIndicator = frame:CreateTexture(nil, 'ARTWORK', nil, 1);
	phaseIndicator:SetSize(30, 30);
	phaseIndicator:SetPoint('CENTER', frame.Health, 'CENTER');
	phaseIndicator:SetDrawLayer("OVERLAY", 7);

	phaseIndicator.PostUpdate = self.PhaseIndicatorUpdate

	return phaseIndicator
end