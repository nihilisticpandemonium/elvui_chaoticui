local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB

local LSM = LibStub("LibSharedMedia-3.0");

local FB = CUI:NewModule("FarmBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule('UtilityBars')
local B = E:GetModule('Bags');

local floor = math.floor

function FB:CreateBar()
	local bar = CUB:CreateBar("ChaoticUI_FarmBar", "farmBar", {'TOPLEFT', ChaoticUI_TrackerBar, 'BOTTOMLEFT', 0, -2}, 'Farm Bar')
	CUB:RegisterCreateButtonHook(bar, function(button) FB:CreateButtonHook(bar, button) end);
	CUB:RegisterUpdateButtonHook(bar, function(button, ...) FB:UpdateButtonHook(button, ...) end);
	
	return bar;
end

function FB:CreateButtonHook(bar, button)
	button.farmed = button:CreateFontString(nil, "OVERLAY");
	button.farmed:FontTemplate(LSM:Fetch("font", E.db.general.font), 10, "THINOUTLINE");
	button.farmed:SetWidth(E:Scale(bar.db.buttonsize) - 4);
	button.farmed:SetHeight(E:Scale(14));
	button.farmed:SetJustifyH("RIGHT");
	button.farmed:Point("TOPRIGHT", 0, 0);
	button.farmed:Show();
	button.target = button:CreateFontString(nil, "OVERLAY");
	button.target:FontTemplate(LSM:Fetch("font", E.db.general.font), 10, "THINOUTLINE");
	button.target:SetWidth(E:Scale(bar.db.buttonsize) - 4);
	button.target:SetHeight(E:Scale(14));
	button.target:SetJustifyH("RIGHT");
	button.target:Point("BOTTOMRIGHT", 0, 0);
	button.target:Show();

	button:RegisterForClicks("AnyDown");
	button:SetScript("OnClick", function(self, mouseButton)
		if (mouseButton == "RightButton") then
			local tbl = ElvDB["farmBar"][FB.myname][button.table];
			tremove(tbl, CUI:InvertTable(tbl)[button]);
			FB.sessionDB[button.table][button.data] = nil;
			FB:UpdateBar(bar);
		end
		self:SetChecked(false);
	end);
end

function FB:SetItemTooltip(itemID)
	local ret = GameTooltip:SetItemByID(itemID)

	GameTooltip:AddLine(" ");
	GameTooltip:AddLine(L["Session:"]);
	local gained = FB.sessionDB["items"][itemID]["gained"];
	local lost = FB.sessionDB["items"][itemID]["lost"];
	local change = gained - lost;

	GameTooltip:AddDoubleLine(L["Earned:"], gained, 1, 1, 1, 1, 1, 1)
	GameTooltip:AddDoubleLine(L["Spent:"], lost, 1, 1, 1, 1, 1, 1)
	if (change < 0) then
		GameTooltip:AddDoubleLine(L["Deficit:"], -change, 1, 0, 0, 1, 1, 1)
	elseif (change > 0) then
		GameTooltip:AddDoubleLine(L["Profit:"], change, 0, 1, 0, 1, 1, 1)
	end
	local count = ElvDB["farmBar"][FB.myname]["count"]["items"][itemID];
	local target = ElvDB["farmBar"][FB.myname]["target"]["items"][itemID];

	GameTooltip:AddDoubleLine(L["Total: "], count, 1, 1, 1, 1, 1, 1);
	
	local r, g, b;
	if (count < target) then
		r = 1;
		g = 0;
		b = 0;
	else
		r = 0;
		g = 1;
		b = 0;
	end

	GameTooltip:AddDoubleLine("Target: ", target, 1, 1, 1, r, g, b);
	GameTooltip:Show();

	return ret;
end

function FB:SetCurrencyTooltip(currencyID)
	local ret = GameTooltip:SetCurrencyByID(currencyID);

	CUB:AddAltoholicCurrencyInfo(currencyID);

	GameTooltip:AddLine(" ");
	GameTooltip:AddLine(L["Session:"]);
	local gained = FB.sessionDB["currency"][currencyID]["gained"];
	local lost = FB.sessionDB["currency"][currencyID]["lost"];
	local change = gained - lost;

	GameTooltip:AddDoubleLine(L["Earned:"], gained, 1, 1, 1, 1, 1, 1)
	GameTooltip:AddDoubleLine(L["Spent:"], lost, 1, 1, 1, 1, 1, 1)
	if (change < 0) then
		GameTooltip:AddDoubleLine(L["Deficit:"], -change, 1, 0, 0, 1, 1, 1)
	elseif (change > 0) then
		GameTooltip:AddDoubleLine(L["Profit:"], change, 0, 1, 0, 1, 1, 1)
	end
	local count = ElvDB["farmBar"][FB.myname]["count"]["currency"][currencyID];
	local target = ElvDB["farmBar"][FB.myname]["target"]["currency"][currencyID];

	GameTooltip:AddDoubleLine(L["Total: "], count, 1, 1, 1, 1, 1, 1);
	
	local r, g, b;
	if (count < target) then
		r = 1;
		g = 0;
		b = 0;
	else
		r = 0;
		g = 1;
		b = 0;
	end

	GameTooltip:AddDoubleLine("Target: ", target, 1, 1, 1, r, g, b);
	GameTooltip:Show();

	return ret;
end

function FB:UpdateItemButton(button)
	local v = button.data
	if (not self.sessionDB["items"][v]) then
		self:AddWatchStartValue(true, v);
	end
	button.table = "items";
	local count = GetItemCount(v, true);
	FB:UpdateAndNotify(true, v, count);
	button.farmed:SetText(count);
	local target = ElvDB["farmBar"][FB.myname]["target"]["items"][v];
	button.target:SetText(target);
	if (count < target) then
		button.target:SetTextColor(1.0, 0.2, 0.2);
	else
		button.target:SetTextColor(0.2, 1.0, 0.2);
	end

	button.SetTooltip = function(self) return FB:SetItemTooltip(v) end;
end

function FB:UpdateCurrencyButton(button)
	local v = button.data
	if (not self.sessionDB["currency"][v]) then
		self:AddWatchStartValue(false, v);
	end
	button.table = "currency";
	local _, amount, texture = GetCurrencyInfo(v);
	FB:UpdateAndNotify(false, v, amount);
	button.farmed:SetText(amount);
	local target = ElvDB["farmBar"][FB.myname]["target"]["currency"][v];
	button.target:SetText(target);
	if (amount < target) then
		button.target:SetTextColor(1.0, 0.2, 0.2);
	else
		button.target:SetTextColor(0.2, 1.0, 0.2);
	end

	button.SetTooltip = function(self) return FB:SetCurrencyTooltip(v) end;
end

function FB:UpdateButtonHook(button, type, index)
	button.index = index;
	button.count:Hide();
	if (type == "item") then
		FB:UpdateItemButton(button);
	else
		FB:UpdateCurrencyButton(button);
	end
end

function FB:UpdateBar(bar)
	local items = ElvDB["farmBar"][FB.myname]["items"];
	local currency = ElvDB["farmBar"][FB.myname]["currency"];

	CUB:CreateButtons(bar, #items + #currency);

	for i = 1, #bar.buttons do
		local button = bar.buttons[i];
		button.data = nil;
	end
	
	for i = 1, #items do
		local button = bar.buttons[i];
		
		CUB:UpdateButtonAsItem(bar, button, items[i], "item", i);	
	end

	for i = #items + 1, #items + #currency do
		local button = bar.buttons[i];
		
		local v = currency[i - #items];
		local _, amount, texture = GetCurrencyInfo(v);
		button.data = v;
		CUB:UpdateButtonAsCustom(bar, button, texture, "currency", i - #items);
	end

	CUB:UpdateBar(self, bar, "ELVUIBAR21BINDBUTTON");
end

function FB:AddWatchStartValue(isItem, id)
	local table = isItem and "items" or "currency";

	self.sessionDB[table][id] = {};
	self.sessionDB[table][id]["gained"] = 0;
	self.sessionDB[table][id]["lost"] = 0;
end

function FB:AddWatch(item, id, target)
	local table = item and "items" or "currency";

	local notificationItem = L["Added item watch for %s"];
	local notificationCurrency = L["Added currency watch for %s"];

	if (not tContains(ElvDB["farmBar"][FB.myname][table], id)) then
		ElvDB["farmBar"][FB.myname]["count"][table][id] = item and GetItemCount(id, true) or select(2, GetCurrencyInfo(id))
		tinsert(ElvDB["farmBar"][FB.myname][table], id);
		if (E.db.chaoticui.utilitybars.farmBar.notify) then
			local string = item and notificationItem or notificationCurrency;
			if (not item or GetItemInfo(id)) then
				UIErrorsFrame:AddMessage(string:format(item and select(2, GetItemInfo(id)) or GetCurrencyLink(id)));
			end
		end
	end
	ElvDB["farmBar"][FB.myname]["target"][table][id] = target;
	FB:UpdateBar(FB.bar);
end

function FB:UpdateAndNotify(item, id, count)
	local table = item and "items" or "currency";
	local oldCount = ElvDB["farmBar"][FB.myname]["count"][table][id] or 0;
	local target = ElvDB["farmBar"][FB.myname]["target"][table][id] or 0;

	local earned = "You have |cff00ff00earned|r %d %s (|cff00ffffcurrently|r %d, |cff0000ffTarget|r %d)"
	local repstr = "%d |cffff00ffRepetitions|r";
	local change = count - oldCount;
	local link = item and select(2, GetItemInfo(id)) or GetCurrencyLink(id,0)
	local notify = E.db.chaoticui.utilitybars.farmBar.notify;
	if (change and link and change > 0) then
		self.sessionDB[table][id]["gained"] = self.sessionDB[table][id]["gained"] + change;
		if (notify) then
			if (count < target) then
				local reps = math.ceil(((target - count)/change));
				UIErrorsFrame:AddMessage(repstr:format(reps));
			end
			UIErrorsFrame:AddMessage(earned:format(change, link, count, target));
		end
	end
	ElvDB["farmBar"][FB.myname]["count"][table][id] = count;
end

function FB:FixDataTable()
	local fixed = false;
	if (ElvDB["farmBar"]["items"]) then
		local oldTable = CopyTable(ElvDB["farmBar"]);
		wipe(ElvDB["farmBar"])
		ElvDB["farmBar"] = {};
		ElvDB["farmBar"][FB.myname] = CopyTable(oldTable);
		fixed = true;
	end
	if (ElvDB["farmBar"][FB.myname]) then
		if (ElvDB["farmBar"][FB.myname]["count"]["currency"] == ElvDB["farmBar"][FB.myname]["count"]["items"]) then
			ElvDB["farmBar"][FB.myname]["count"]["items"] = {};
			ElvDB["farmBar"][FB.myname]["count"]["currency"] = {};
			for i = 1, #ElvDB["farmBar"][FB.myname]["items"] do
				local itemId = ElvDB["farmBar"][FB.myname]["items"][i];
				local count = GetItemCount(itemId, true);
				FB:UpdateAndNotify(true, itemId, count);
			end
			for i = 1, #ElvDB["farmBar"][FB.myname]["currency"] do
				local currencyId = ElvDB["farmBar"][FB.myname]["currency"][i];
				local count = select(2, GetCurrencyInfo(currencyId));
				FB:UpdateAndNotify(false, currencyId, count);
			end
			fixed = true;
		end
	end

	return fixed;
end

function FB:GetID(ID, item)
	if (not ID) then
		return nil;
	end
	if (item and strfind(ID, "item:")) then
		return tonumber(strmatch(ID, "\124\124Hitem:(%d+)"));
	elseif ((not item) and strfind(ID, "currency:")) then
		return tonumber(strmatch(ID, "\124\124Hcurrency:(%d+)"));
	else
		return tonumber(ID);
	end
end

local function AddFarmWatch(msg)
	msg = msg:gsub("\124", "\124\124");
	local type, id, target = strmatch(msg, "(.+) (%d+) (%d+)");

	local hasType = type == "item" or type == "currency";
	local hasID = FB:GetID(id, type == "item") ~= nil;
	local hasTarget = tonumber(target) ~= nil;

	if (not hasType or not hasID or not hasTarget) then
		id, target = strmatch(msg, "(\124\124H.*\124\124r) (%d+)");
		if (id and target) then
			type = strmatch(id, "\124\124H(item:)") and "item" or strmatch(id, "\124\124H(currency:)") and "currency" or nil;
			if (type) then
				id = FB:GetID(id, type == "item");
			end
		end
		if (not id or not type or not target) then
			print("Usage:  /fbadd (item|currency) id target");
			return;
		end
	end

	id = FB:GetID(id, type == "item");
	target = tonumber(target);
	FB:AddWatch(type == "item", id, target);
end

function FB:Initialize()
	CUB:InjectScripts(self);
	
	--[[if (not E.global.chaoticui.farmmigrated) then
		CUI:MigrateFarmDataToTracker();
		E.global.chaoticui.farmmigrated = true;
	end]]

	FB.sessionDB = {};
	FB.sessionDB["items"] = {};
	FB.sessionDB["currency"] = {};

	FB.myname = ("%s-%s"):format(E.myname,E.myrealm);
	ElvDB = ElvDB or {};
	ElvDB["farmBar"] = ElvDB["farmBar"] or {};
	if (not self:FixDataTable()) then
		ElvDB["farmBar"][FB.myname] = ElvDB["farmBar"][FB.myname] or {};
		ElvDB["farmBar"][FB.myname]["items"] = ElvDB["farmBar"][FB.myname]["items"] or {};
		ElvDB["farmBar"][FB.myname]["currency"] = ElvDB["farmBar"][FB.myname]["currency"] or {};
		ElvDB["farmBar"][FB.myname]["count"] = ElvDB["farmBar"][FB.myname]["count"] or {};
		ElvDB["farmBar"][FB.myname]["count"]["items"] = ElvDB["farmBar"][FB.myname]["count"]["items"] or {};
		ElvDB["farmBar"][FB.myname]["count"]["currency"] = ElvDB["farmBar"][FB.myname]["count"]["currency"] or {};
		ElvDB["farmBar"][FB.myname]["target"] = ElvDB["farmBar"][FB.myname]["target"] or {};
		ElvDB["farmBar"][FB.myname]["target"]["items"] = ElvDB["farmBar"][FB.myname]["target"]["items"] or {};
		ElvDB["farmBar"][FB.myname]["target"]["currency"] = ElvDB["farmBar"][FB.myname]["target"]["currency"] or {};
	end

	local frame = CreateFrame("Frame", "ChaoticUI_FarmBarController");
	frame:RegisterEvent('BAG_UPDATE');
	frame:RegisterEvent('CHAT_MSG_MONEY');
	frame:RegisterEvent('CHAT_MSG_CURRENCY');
	frame:RegisterEvent('CHAT_MSG_COMBAT_HONOR_GAIN');
	frame:RegisterEvent('CURRENCY_DISPLAY_UPDATE');
	CUB:RegisterEventHandler(self, frame);

	local bar = self:CreateBar();
	self.bar = bar;
	self.hooks = {};

	self:UpdateBar(bar);

	SLASH_FBADD1 = '/fbadd';
	SlashCmdList.FBADD = AddFarmWatch;
end

CUB:RegisterUtilityBar(FB);
