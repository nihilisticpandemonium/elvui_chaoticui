local CUI, E, L, V, P, G = unpack(select(2, ...))


local SSB = CUI:NewModule('SpecSwitchBar', 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0");
local CUB = CUI:GetModule('UtilityBars')
local B = E:GetModule('Bags');
local PT = LibStub("LibPeriodicTable-3.1");

function SSB:CreateBar()
	local bar = CUB:CreateBar("ChaoticUI_SpecSwitchBar", "specSwitchBar", {'CENTER',E.UIParent,'CENTER',0,0}, 'SpecSwitchBar')
	CUB:RegisterCreateButtonHook(bar, function(button) SSB:CreateButtonHook(button) end);
	CUB:RegisterUpdateButtonHook(bar, function(button) SSB:UpdateButtonHook(button) end);
	bar:Size(32, 32);
	
	CUB:CreateButtons(bar, GetNumSpecializations() + 1);

	return bar;
end

function SSB:CreateButtonHook(button)
	button:RegisterForClicks("AnyDown");
    button.SetTooltip = function(self)
        local _, name, description, texture = GetSpecializationInfo(self.data);
        local icon = format('|T%s:14:14:0:0:64:64:4:60:4:60|t', texture)
        GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
        GameTooltip:SetText(format("%s %s", icon, name), 1, 1, 1);
        GameTooltip:AddLine(description, nil, nil, nil, true);
        if (GetSpecialization() ~= self.data) then
            GameTooltip:AddLine(" ");
            GameTooltip:AddLine("Click to change specialization");
        end
		if (GetLootSpecialization() ~= GetSpecializationInfo(self.data)) then
			local lootSpec = GetLootSpecialization();
			local str = "Right-click to set the loot specialization to %s.|nCurrently %s.";
			local lootStr;
			if (lootSpec == 0) then
				lootStr = ("Default (%s)"):format(select(2, GetSpecializationInfo(GetSpecialization())));
			else
				lootStr = select(2, GetSpecializationInfoByID(lootSpec));
			end
			GameTooltip:AddLine(str:format(name, lootStr));
			GameTooltip:AddLine()
		end
        GameTooltip:Show();
        return false;
    end
    button:SetScript("OnClick", function(self, button)
		if (button == "LeftButton" and self.data ~= GetSpecialization()) then
            SetSpecialization(self.data);
        end
		if (button == "RightButton" and self.data ~= GetLootSpecialization()) then
			SetLootSpecialization(GetSpecializationInfo(self.data));
		end
    end);
end

function SSB:UpdateButtonHook(button)
    if (button.data == 0) then
        button.SetTooltip = function(self)
            local lootSpec = GetLootSpecialization();
            local str = "Set the loot specialization to Default.|nCurrently %s.";
            local lootStr;
            if (lootSpec == 0) then
                lootStr = ("Default (%s)"):format(select(2, GetSpecializationInfo(GetSpecialization())));
            else
                lootStr = select(2, GetSpecializationInfoByID(lootSpec));
            end
            GameTooltip:SetOwner(self, "ANCHOR_RIGHT");
            GameTooltip:SetText(str:format(lootStr));
		    GameTooltip:Show();
		    return false;
	    end;
        button:SetScript('OnClick', function(self)
            SetLootSpecialization(0);
        end);
		if (GetLootSpecialization() == 0) then
			button:SetBackdropBorderColor(1, 1, 0);
		else
			button:SetTemplate();
		end
    else
		if (GetSpecialization() == button.data) then
			button:SetBackdropBorderColor(0, 1, 0);
		elseif (GetLootSpecialization() == GetSpecializationInfo(button.data)) then
			button:SetBackdropBorderColor(1, 1, 0);
		else
			button:SetTemplate();
		end
	end
end

function SSB:UpdateBar(bar)
	if (GetNumSpecializations() == 0) then
		C_Timer.After(1, function() SSB:UpdateBar(bar) end);
		return
	end
    for i = 1, GetNumSpecializations() + 1 do
        local button = bar.buttons[i];
        if (i <= GetNumSpecializations()) then
            button.data = i;
            CUB:UpdateButtonAsCustom(bar, button, select(4, GetSpecializationInfo(i)));
        else
            button.data = 0;
            CUB:UpdateButtonAsCustom(bar, button, "Interface\\Icons\\inv_misc_lockbox_1");
        end
    end

    CUB:UpdateBar(self, bar, "ELVUIBAR29BINDBUTTON");
end

function SSB:SecondaryButton_OnEnter(button)
	local bar = button:GetParent():GetParent()
	local pb = button:GetParent();
	pb.hover:Show();
	E:UIFrameFadeIn(bar, 0.2, bar:GetAlpha(), E.db.chaoticui.utilitybars.specSwitchBar.alpha)
end

function SSB:SecondaryButton_OnLeave(button)
	local bar = button:GetParent():GetParent()
	E:UIFrameFadeOut(bar, 0.2, bar:GetAlpha(), 0)
end

function SSB:Initialize()
	CUB:InjectScripts(self);

	local frame = CreateFrame("Frame", "ChaoticUI_SpecSwitchBarController");
	frame:RegisterEvent('PLAYER_ENTERING_WORLD')
	frame:RegisterEvent('CHARACTER_POINTS_CHANGED')
	frame:RegisterEvent('PLAYER_TALENT_UPDATE')
    frame:RegisterEvent('ACTIVE_TALENT_GROUP_CHANGED')
    frame:RegisterEvent('PLAYER_LOOT_SPEC_UPDATED')
	frame:RegisterUnitEvent('UNIT_INVENTORY_CHANGED', 'player')
	CUB:RegisterEventHandler(self, frame);

	local bar = self:CreateBar();

	self.bar = bar;
	self.hooks = {};
	self.sbhooks = {};

	self:UpdateBar(bar);
end

CUB:RegisterUtilityBar(SSB);
