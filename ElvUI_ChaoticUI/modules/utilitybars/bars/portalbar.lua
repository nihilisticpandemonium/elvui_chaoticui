local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local LSM = LibStub("LibSharedMedia-3.0");

local PRB = CUI:NewModule("PortalBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule('UtilityBars')
local PT = LibStub("LibPeriodicTable-3.1");

local floor = math.floor

function PRB:CreateBar()
	return CUB:CreateBar("ChaoticUI_PortalBar", "portalBar", {'BOTTOMRIGHT', ChaoticUI_ProfessionBar, 'TOPRIGHT', 0, 2}, 'Portal Bar')
end

function PRB:CreateButtons(bar)
	for i = 1, #bar.buttons do
		local button = bar.buttons[i];
		button.data = nil;
	end

	local j = 1;
	
	local function addItemButton(itemID, row)
		local count = GetItemCount(itemID);

		if (count > 0) then
			local button = bar.buttons[j];
			if (not button) then
				button = CUB:CreateButton(bar);
				bar.buttons[j] = button;
			end

			CUB:UpdateButtonAsItem(bar, button, itemID);
			button.row = row;

			j = j + 1;
		end
	end

	local function addSpellButton(spellID, row)
		if (IsSpellKnown(spellID)) then
			local name = GetSpellInfo(spellID);
			local button = bar.buttons[j];
			if (not button) then
				button = CUB:CreateButton(bar);
				bar.buttons[j] = button;
			end

			CUB:UpdateButtonAsSpell(bar, button, spellID);
			button.row = row;

			j = j + 1;
		end
	end

	local function addButton(id, row)
		if (id < 0) then
			addSpellButton(math.abs(id), row);
		else
			addItemButton(id, row);
		end
	end

	local checkAndAdd;

	checkAndAdd = function(k, v, row)
		if (type(v) == "table") then
			for _k, _v in pairs(v) do
				if (type(_k) == "number") then
					checkAndAdd(_k, _v, row);
				end
			end
		else
			addButton(k, row);
		end
	end

	for row, key in pairs(bar.keys) do
		local db = PT:GetSetTable(key);
        for k, v in pairs(db) do
			if (type(k) == "number") then
				checkAndAdd(k, v, row);
			end
		end
	end
end

function PRB:UpdateBarKeys(bar)
	bar.keys = { "ChaoticUI.TeleportSpells.Teleports", "ChaoticUI.TeleportSpells.Portals", "Misc.Hearth" };

	if (bar.db.challengeModePandaria) then
		tinsert(bar.keys, 3, "ChaoticUI.TeleportSpells.ChallengeMode.Pandaria");
	end

	if (bar.db.challengeModeDraenor) then
		tinsert(bar.keys, 4, "ChaoticUI.TeleportSpells.ChallengeMode.Draenor");
	end
end

function PRB:UpdateBar(bar)
	PRB:UpdateBarKeys(bar);
	PRB:CreateButtons(bar);

	CUB:UpdateBarMultRow(self, bar, "ELVUIBAR24BINDBUTTON");
end

function PRB:AddTeleportSpells()
	PT:AddData("ChaoticUI.TeleportSpells", "Rev: 1",
	{
		["ChaoticUI.TeleportSpells.Teleports"] = "-556,-53140,-120145,-3565,-32271,-3562,-3567,-33690,-35715,-32272,-49358,-176248,-3561,-49359,-3566,-88342,-88344,-3563,-132621,-132627,-176242,-18960,-50977,-126892,-193753,-193759, -224768, -281403, -281404",
		["ChaoticUI.TeleportSpells.Portals"] = "-53142, -120146, -11419, -32266, -11416, -11417, -33691, -35717, -32267, -49361, -176246, -10059, -49360, -11420, -88345, -88346, -11418, -132620, -132626, -176244, -224871, -281400, -281402",
		["ChaoticUI.TeleportSpells.ChallengeMode.Pandaria"] = "-131228, -131204, -131222, -131225, -131206, -131205, -131229, -131231, -131232",
		["ChaoticUI.TeleportSpells.ChallengeMode.Draenor"] = "-169764, -169762, -169766, -169763, -169769, -169765, -169767, -169769",
	});
end

function PRB:Initialize()
	self:AddTeleportSpells();
	CUB:InjectScripts(self);

	

	local frame = CreateFrame("Frame", "ChaoticUI_PortalBarController");
	frame:RegisterEvent('SPELLS_CHANGED');
	frame:RegisterEvent('BAG_UPDATE');
	frame:RegisterEvent('GET_ITEM_INFO_RECEIVED');
    frame:RegisterEvent('SPELL_UPDATE_ICON');
	CUB:RegisterEventHandler(self, frame);

	local bar = self:CreateBar();
	self.bar = bar;
	self.hooks = {};

	self:UpdateBar(bar);
end

CUB:RegisterUtilityBar(PRB);
