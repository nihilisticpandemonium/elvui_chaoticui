local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local BB = CUI:NewModule("BaitBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule("UtilityBars")
local B = E:GetModule('Bags');
local FL = LibStub("LibFishing-1.0-SLE");

local PT = LibStub("LibPeriodicTable-3.1");

local floor = math.floor

function BB:AddBaits()
	PT:AddData("ChaoticUI.Baits", "Rev: 1",
	{
		["ChaoticUI.Baits.Draenor"] = "110293, 110291, 110289, 110290, 110292, 110274, 110294, 128229",
		["ChaoticUI.Baits.BrokenIsles"] = "133701, 133702, 133703, 133704, 133705, 133706, 133707, 133708, 133709, 133710, 133711, 133712, 133713, 133714, 133715, 133716, 133717, 133719, 133720, 133721, 133722, 133723, 133724, 133795, 139175",
	});
end

function BB:CreateBar()
	local bar = CUB:CreateBar("ChaoticUI_BaitBar", "baitBar", {'CENTER', E.UIParent, 'CENTER', 0, -280}, 'Bait Bar')
	CUB:RegisterUpdateButtonHook(bar, function(button, ...) BB:UpdateButtonHook(button, ...) end);
	
	return bar
end

function BB:UpdateButtonHook(button, ...)
	button.texture:SetDesaturated(GetItemCount(button.data) == 0);
end

local DRAENOR_MAP_ID = 572
local BROKEN_ISLES_MAP_ID = 619

function BB:IsInDraenor()
	return E.MapInfo.continentMapID == DRAENOR_MAP_ID
end

function BB:IsInBrokenIsles()
	return E.MapInfo.continentMapID == BROKEN_ISLES_MAP_ID
end

function BB:UpdateBar(bar)
	local inBaitZone = false;
	local key;
	if (self:IsInDraenor()) then
		key = "ChaoticUI.Baits.Draenor";
		inBaitZone = true;
	elseif (self:IsInBrokenIsles()) then
		key = "ChaoticUI.Baits.BrokenIsles";
		inBaitZone = true;
	end

	local j = 1;

	local function addButton(itemID)
		local count = GetItemCount(itemID);

		if (count > 0) then
			local button = bar.buttons[j];
			if (not button) then
				button = CUB:CreateButton(bar);
				bar.buttons[j] = button;
			end

			CUB:UpdateButtonAsItem(bar, button, itemID);
			
			j = j + 1;
		end
	end

	local checkAndAdd;

	checkAndAdd = function(k, v)
		if (type(v) == "table") then
			for _k, _v in pairs(v) do
				if (type(_k) == "number") then
					checkAndAdd(_k, _v);
				end
			end
		else
			addButton(k);
		end
	end

	if(inBaitZone) then
		local db = PT:GetSetTable(key);
		for k, v in pairs(db) do
			if (type(k) == "number") then
				checkAndAdd(k, v);
			end
		end
	end

	for x = j, #bar.buttons do
		bar.buttons[x].data = nil;
	end

	CUB:UpdateBar(self, bar, "ELVUIBAR23BINDBUTTON");
	if (not FL:IsFishingPole() or not inBaitZone) then
		RegisterStateDriver(bar, 'visibility', 'hide');
	end
end

function BB:Initialize()
	self:AddBaits();
	local frame = CreateFrame("Frame", "ChaoticUI_BaitBarController");
	frame:RegisterEvent('BAG_UPDATE');
	frame:RegisterEvent('PLAYER_EQUIPMENT_CHANGED');
	frame:RegisterEvent('GET_ITEM_INFO_RECEIVED');
	frame:RegisterEvent("PLAYER_ENTERING_WORLD");
	CUB:RegisterEventHandler(self, frame);

	CUB:InjectScripts(self);
	
	local bar = self:CreateBar();
	self.bar = bar;
	self:UpdateBar(bar);
end

CUB:RegisterUtilityBar(BB);
