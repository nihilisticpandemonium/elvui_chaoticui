local CUI, E, L, V, P, G = unpack(select(2, ...));

local EM = CUI:NewModule("EquipmentManagerBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule('UtilityBars')
local LSM = LibStub("LibSharedMedia-3.0");

local GetNumEquipmentSets, GetEquipmentSetInfo, GetEquipmentSetInfoByName = GetNumEquipmentSets, GetEquipmentSetInfo, GetEquipmentSetInfoByName

function EM:CreateBar()
	local bar = CUB:CreateBar("ChaoticUI_EquipmentManagerBar", "equipmentManagerBar", {'BOTTOMLEFT', LeftChatPanel, 'BOTTOMRIGHT', 4, 24}, 'Equipment Manager Bar')
	CUB:RegisterCreateButtonHook(bar, function(button) EM:CreateButtonHook(button) end)
	CUB:RegisterUpdateButtonHook(bar, function(button, ...) EM:UpdateButtonHook(bar, button) end);
	bar:Size(36, 160);

	return bar;
end

function EM:CreateEditButton(button)
	local editButton = CreateFrame('Button', nil, button);

	editButton:SetFrameLevel(button:GetFrameLevel() + 2);
	editButton:SetSize(14, 14)
	editButton:SetPoint('BOTTOMRIGHT', button, 'BOTTOMRIGHT', 0, 0)
	editButton.Icon = editButton:CreateTexture(nil, 'ARTWORK')
	editButton.Icon:SetTexture("Interface\\WorldMap\\GEAR_64GREY")
	editButton.Icon:SetAllPoints()
	editButton.Icon:SetAlpha(.5)

	editButton:SetScript('OnEnter', function(self)
		local setName = self:GetParent().data
		if not setName then return end
		self.Icon:SetAlpha(1)
		GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
		GameTooltip:SetText(EQUIPMENT_SET_EDIT)
	end)
	editButton:SetScript('OnLeave', function(self)
		local setName = self:GetParent().data
		if not setName then return end
		self.Icon:SetAlpha(.5)
		GameTooltip_Hide()
	end)
	editButton:SetScript('OnClick', function(self)
		local setName = self:GetParent().data
		if not setName then return end
 		ToggleCharacter("PaperDollFrame");
 		local isShown = PaperDollEquipmentManagerPane:IsVisible();
		PaperDollEquipmentManagerPane:Show();
		PaperDollEquipmentManagerPane:SetShown(isShown);
		PaperDollEquipmentManagerPane.selectedSetName = setName
		PaperDollFrame_ClearIgnoredSlots()
		PaperDollFrame_IgnoreSlotsForSet(setName)
		PaperDollEquipmentManagerPane_Update()
		GearManagerDialogPopup:Hide()
		StaticPopup_Hide("CONFIRM_SAVE_EQUIPMENT_SET")
		StaticPopup_Hide("CONFIRM_OVERWRITE_EQUIPMENT_SET")
		GearManagerDialogPopup:Show()
		GearManagerDialogPopup.isEdit = true
		GearManagerDialogPopup.origName = setName;
		RecalculateGearManagerDialogPopup(setName, self:GetParent().texture:GetTexture());
	end)

	editButton:Show();

	return editButton;
end

function EM:CreateSaveButton(button)
	local saveButton = CreateFrame('Button', nil, button);

	saveButton:SetFrameLevel(button:GetFrameLevel() + 2);
	saveButton:SetSize(14, 14)
	saveButton:SetPoint('BOTTOMLEFT', button, 'BOTTOMLEFT', 0, 0)
	saveButton.Icon = saveButton:CreateTexture(nil, 'ARTWORK')
	saveButton.Icon:SetAllPoints()
	saveButton:SetScript('OnEnter', function(self)
		local setName = self:GetParent().data
		if not setName then return end
		GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
		GameTooltip:SetText(SAVE_CHANGES)
	end)
	saveButton:SetScript('OnLeave', GameTooltip_Hide)
	saveButton:SetScript('OnClick', function(self)
		local setName = self:GetParent().data
		if not setName then return end
		local setID = C_EquipmentSet.GetEquipmentSetID(setName);
		local Dialog = StaticPopup_Show("CONFIRM_SAVE_EQUIPMENT_SET", setName)
		Dialog.data = setID
	end)

	saveButton:Show();

	return saveButton;
end

function EM:CreateButtonHook(button)
	button.missingOverlay = button:CreateTexture(nil, 'OVERLAY');
	button.missingOverlay:SetInside();
	button.missingOverlay:SetColorTexture(1, .2, .2, .4);

	button.setnametext = button:CreateFontString(nil, "OVERLAY");
	button.setnametext:FontTemplate(LSM:Fetch("font", E.db.general.font), 10, "THINOUTLINE");
	button.setnametext:SetWidth(E:Scale(self.bar.db.buttonsize) - 4);
	button.setnametext:SetHeight(E:Scale(14));
	button.setnametext:SetJustifyH("CENTER");
	button.setnametext:Point("TOP", 0, 0);

	button.SetTooltip = function(self)
		if not self.data then return nil end
		local ret = GameTooltip:SetEquipmentSet(self.data)
		GameTooltip:Show();
		return ret;
	end;
	button:SetScript('OnClick', function(self)
		if not self.data then return end
		local equipmentSetID = C_EquipmentSet.GetEquipmentSetID(self.data);
		if not select(4, C_EquipmentSet.GetEquipmentSetInfo(equipmentSetID)) then
			C_EquipmentSet.UseEquipmentSet(equipmentSetID)
		end
	end)

	button.editButton = self:CreateEditButton(button);
	button.saveButton = self:CreateSaveButton(button);
end

function EM:UpdateButtonHook(bar, button)
	local equipmentSetIDs = C_EquipmentSet.GetEquipmentSetIDs();
	local setName, setIcon, _, isEquipped, numItems, numEquipped, _, numMissing = C_EquipmentSet.GetEquipmentSetInfo(equipmentSetIDs[CUI:InvertTable(bar.buttons)[button]]);
	button.data = setName;
	button.setnametext:SetText(setName);
	button.texture:SetTexture(setIcon);
	button.missingOverlay:SetShown(numMissing > 0);

	if (numEquipped < numItems) then
		button.saveButton.Icon:SetTexture("Interface\\RaidFrame\\ReadyCheck-NotReady")
		button.saveButton:Enable()
		button.saveButton:EnableMouse(true)
	else
		button.saveButton.Icon:SetTexture("Interface\\RaidFrame\\ReadyCheck-Ready")
		button.saveButton:Disable()
		button.saveButton:EnableMouse(false)
	end
end

function EM:UpdateBar(bar)
	CUB:CreateButtons(bar, C_EquipmentSet.GetNumEquipmentSets());

	local equipmentSetIDs = C_EquipmentSet.GetEquipmentSetIDs();
	for i, v in ipairs(bar.buttons) do
		local setName, setIcon, _, isEquipped, numItems, numEquipped, _, numMissing = C_EquipmentSet.GetEquipmentSetInfo(equipmentSetIDs[i]);
		CUB:UpdateButtonAsCustom(bar, v, setIcon);
	end

	CUB:UpdateVertBar(self, bar, "ELVUIBAR20BINDBUTTON");
	for i, button in ipairs(bar.buttons) do
		if (bar.db.mouseover) then
			if not (self.sbhooks[button]) then
				self:HookScript(button.editButton, 'OnEnter', 'SecondaryButton_OnEnter');
				self:HookScript(button.editButton, 'OnLeave', 'SecondaryButton_OnLeave');
				self:HookScript(button.saveButton, 'OnEnter', 'SecondaryButton_OnEnter');
				self:HookScript(button.saveButton, 'OnLeave', 'SecondaryButton_OnLeave');
				self.sbhooks[button] = true;
			end
		else
			if (self.sbhooks[button]) then
				self:Unhook(button.editButton, 'OnEnter');	
				self:Unhook(button.editButton, 'OnLeave');
				self:Unhook(button.saveButton, 'OnEnter');	
				self:Unhook(button.saveButton, 'OnLeave');
				self.sbhooks[button] = nil;
			end
		end
	end
end

function EM:SecondaryButton_OnEnter(button)
	local bar = button:GetParent():GetParent()
	local pb = button:GetParent();
	pb.hover:Show();
	E:UIFrameFadeIn(bar, 0.2, bar:GetAlpha(), E.db.chaoticui.utilitybars.equipmentManagerBar.alpha)
end

function EM:SecondaryButton_OnLeave(button)
	local bar = button:GetParent():GetParent()
	E:UIFrameFadeOut(bar, 0.2, bar:GetAlpha(), 0)
end

function EM:Initialize()
	CUB:InjectScripts(self);

	local frame = CreateFrame("Frame", "ChaoticUI_EquipMgrBarController");
	frame:RegisterEvent('PLAYER_ENTERING_WORLD')
	frame:RegisterEvent('PLAYER_EQUIPMENT_CHANGED')
	frame:RegisterEvent('EQUIPMENT_SETS_CHANGED')
	frame:RegisterUnitEvent('UNIT_INVENTORY_CHANGED', 'player')
	CUB:RegisterEventHandler(self, frame);

	local bar = self:CreateBar();
	self.bar = bar;
	self.hooks = {};
	self.sbhooks = {};

	self:UpdateBar(bar);
end

CUB:RegisterUtilityBar(EM);
