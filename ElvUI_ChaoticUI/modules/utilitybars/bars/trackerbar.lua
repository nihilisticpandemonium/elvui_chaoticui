local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB

local LSM = LibStub("LibSharedMedia-3.0");

local TB = CUI:NewModule("TrackerBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule('UtilityBars')
local B = E:GetModule('Bags');

local floor = math.floor

function TB:CreateBar()
	local bar = CUB:CreateBar("ChaoticUI_TrackerBar", "trackerbar", {'TOPLEFT', ElvUIParent, 'TOPLEFT', 0, -20}, 'Tracker Bar')
	CUB:RegisterCreateButtonHook(bar, function(button) TB:CreateButtonHook(bar, button) end);
	CUB:RegisterUpdateButtonHook(bar, function(button, ...) TB:UpdateButtonHook(button, ...) end);

	return bar;
end

function TB:CreateButtonHook(bar, button)
	button.farmed = button:CreateFontString(nil, "OVERLAY");
	button.farmed:FontTemplate(LSM:Fetch("font", E.db.general.font), 10, "THINOUTLINE");
	button.farmed:SetWidth(E:Scale(bar.db.buttonsize) - 4);
	button.farmed:SetHeight(E:Scale(14));
	button.farmed:SetJustifyH("RIGHT");
	button.farmed:Point("BOTTOMRIGHT", 0, 0);
	button.farmed:Show();

	button:RegisterForClicks("AnyDown");
	button:SetScript("OnClick", function(self, mouseButton)
		if (mouseButton == "RightButton") then
			tremove(ElvDB["trackerbar"][TB.myname][button.table], button.index);
			TB:UpdateBar(bar);
		end
		self:SetChecked(false);
	end);
end

function TB:SetItemTooltip(itemID)
	local ret = GameTooltip:SetItemByID(itemID)

	if (not TB.sessionDB["items"][currencyID]) then
		return ret;
	end
	GameTooltip:AddLine(" ");
	GameTooltip:AddLine(L["Session:"]);
	local gained = TB.sessionDB["items"][itemID]["gained"];
	local lost = TB.sessionDB["items"][itemID]["lost"];
	local change = gained - lost;

	GameTooltip:AddDoubleLine(L["Earned:"], gained, 1, 1, 1, 1, 1, 1)
	GameTooltip:AddDoubleLine(L["Spent:"], lost, 1, 1, 1, 1, 1, 1)
	if (change < 0) then
		GameTooltip:AddDoubleLine(L["Deficit:"], -change, 1, 0, 0, 1, 1, 1)
	elseif (change > 0) then
		GameTooltip:AddDoubleLine(L["Profit:"], change, 0, 1, 0, 1, 1, 1)
	end
	local count = ElvDB["trackerbar"][TB.myname]["count"]["items"][itemID];

	GameTooltip:AddDoubleLine(L["Total: "], count, 1, 1, 1, 1, 1, 1);

	GameTooltip:Show();
	
	return ret;
end

function TB:SetCurrencyTooltip(currencyID)
	local ret = GameTooltip:SetCurrencyByID(currencyID);

	CUB:AddAltoholicCurrencyInfo(currencyID);

	if (not TB.sessionDB["currency"][currencyID]) then
		return ret;
	end
	GameTooltip:AddLine(" ");
	GameTooltip:AddLine(L["Session:"]);
	local gained = TB.sessionDB["currency"][currencyID]["gained"];
	local lost = TB.sessionDB["currency"][currencyID]["lost"];
	local change = gained - lost;

	GameTooltip:AddDoubleLine(L["Earned:"], gained, 1, 1, 1, 1, 1, 1)
	GameTooltip:AddDoubleLine(L["Spent:"], lost, 1, 1, 1, 1, 1, 1)
	if (change < 0) then
		GameTooltip:AddDoubleLine(L["Deficit:"], -change, 1, 0, 0, 1, 1, 1)
	elseif (change > 0) then
		GameTooltip:AddDoubleLine(L["Profit:"], change, 0, 1, 0, 1, 1, 1)
	end
	local count = ElvDB["trackerbar"][TB.myname]["count"]["currency"][currencyID];

	GameTooltip:AddDoubleLine(L["Total: "], count, 1, 1, 1, 1, 1, 1);
	
	GameTooltip:Show();

	return ret;
end

function TB:UpdateItemButton(button)
	local v = button.data
	if (not self.sessionDB["items"][v]) then
		self:AddWatchStartValue(true, v);
	end

	local count = GetItemCount(v, true);
	TB:UpdateAndNotify(true, v, count);
	button.farmed:SetText(count);
	button.texture:SetDesaturated(count == 0);
	button.table = "items";

	button.SetTooltip = function(self) return TB:SetItemTooltip(v) end;
end

function TB:UpdateCurrencyButton(button)
	local v = button.data
	if (not self.sessionDB["currency"][v]) then
		self:AddWatchStartValue(false, v);
	end
	local _, amount, texture = GetCurrencyInfo(v);
	TB:UpdateAndNotify(false, v, amount);
	button.farmed:SetText(amount);
	button.table = "currency";

	button.SetTooltip = function(self) return TB:SetCurrencyTooltip(v) end;
end

function TB:UpdateButtonHook(button, type, index)
	button.index = index;
	button.count:Hide();
	if (type == "item") then
		TB:UpdateItemButton(button);
	else
		TB:UpdateCurrencyButton(button);
	end
end

function TB:UpdateBar(bar)
	local items = ElvDB["trackerbar"][TB.myname]["items"];
	local currency = ElvDB["trackerbar"][TB.myname]["currency"];

	CUB:CreateButtons(bar, #items + #currency);

	for i = 1, #bar.buttons do
		local button = bar.buttons[i];
		button.data = nil;
	end
	
	local function f(key, id)
		for i, v in ipairs(ElvDB["trackerbar"][TB.myname][key]) do
			if (v == id) then
				return i;
			end
		end
	end

	table.sort(items, function(a, b) return a > b end);
	for i = 1, #items do
		local button = bar.buttons[i];
		
		CUB:UpdateButtonAsItem(bar, button, items[i], "item", f("items", items[i]));	
	end

	-- Holy crap why are there strings for the currency ids??
	local fixMePls = {};
	for i, v in pairs(ElvDB["trackerbar"][TB.myname]["currency"]) do
		if type(v) ~= "number" then
			tinsert(fixMePls, i);
		end
	end

	for i, v in ipairs(fixMePls) do
		ElvDB["trackerbar"][TB.myname]["currency"][v] = tonumber(ElvDB["trackerbar"][TB.myname]["currency"][v]);
	end
	
	table.sort(currency, function(a, b) return a > b end);
	for i = #items + 1, #items + #currency do
		local button = bar.buttons[i];
		
		local v = currency[i - #items];
		local _, amount, texture = GetCurrencyInfo(v);
		button.data = v;
		CUB:UpdateButtonAsCustom(bar, button, texture, "currency", f("currency", currency[i - #items]));
	end

	CUB:UpdateBar(self, bar, "ELVUIBAR22BINDBUTTON");
end

function TB:AddWatchStartValue(isItem, id)
	if (not id) then return end
	local table = isItem and "items" or "currency";

	self.sessionDB[table][id] = {};
	self.sessionDB[table][id]["gained"] = 0;
	self.sessionDB[table][id]["lost"] = 0;
end

function TB:AddWatch(item, id)
	local table = item and "items" or "currency";

	local notificationItem = L["Added item watch for %s"];
	local notificationCurrency = L["Added currency watch for %s"];

	if (not tContains(ElvDB["trackerbar"][TB.myname][table], id)) then
		ElvDB["trackerbar"][TB.myname]["count"][table][id] = item and GetItemCount(id, true) or select(2, GetCurrencyInfo(id))
		tinsert(ElvDB["trackerbar"][TB.myname][table], id);
		if (E.db.chaoticui.utilitybars.trackerbar.notify) then
			local string = item and notificationItem or notificationCurrency;
			UIErrorsFrame:AddMessage(string:format(item and select(2, GetItemInfo(id)) or GetCurrencyLink(id)));
		end
	end

	TB:UpdateBar(TB.bar);
end

function TB:HookElvUIBags()
	if (not B.BagFrames) then return end
	for _, bagFrame in pairs(B.BagFrames) do
		for _, bagID in pairs(bagFrame.BagIDs) do
			if (not self.hookedBags[bagID]) then
				for slotID = 1, GetContainerNumSlots(bagID) do
					local button = bagFrame.Bags[bagID][slotID];
					button:RegisterForClicks("AnyUp");
					button:HookScript("OnDoubleClick", function(self, mouseButton, down)
						if (mouseButton == "LeftButton") then
							TB:AddWatch(true, GetContainerItemID(bagID, slotID));
						end
						ClearCursor();
					end)
				end
				self.hookedBags[bagID] = true;
			end
		end
	end

	if (ElvUIReagentBankFrameItem1 and not self.hookedBags[REAGENTBANK_CONTAINER]) then
		for slotID = 1, 98 do
			local button = _G["ElvUIReagentBankFrameItem"..slotID];
			button:RegisterForClicks("AnyUp");
			button:HookScript("OnDoubleClick", function(self, mouseButton, down)
				if (mouseButton == "LeftButton") then
					TB:AddWatch(true, GetContainerItemID(REAGENTBANK_CONTAINER, slotID));
				end
				ClearCursorItem();
			end)
		end
		self.hookedBags[REAGENTBANK_CONTAINER] = true;
	end
end

function TB:HookCurrencyButtons()
	if (not TokenFrameContainer.buttons) then
		return false;
	end

	for i = 1, #TokenFrameContainer.buttons do
		local button = TokenFrameContainer.buttons[i];
		if (not button.hooked) then
			button:RegisterForClicks("AnyUp");
			button:HookScript("OnDoubleClick", function(self, mouseButton, down)
				if (mouseButton == "LeftButton") then
					local offset = HybridScrollFrame_GetOffset(TokenFrameContainer);
					local _, isHeader = GetCurrencyListInfo(i + offset);
					if (not isHeader) then
						local link = GetCurrencyListLink(i + offset);
						local id = tonumber(string.match(link, "currency:(%d+)"));
						TB:AddWatch(false, id);
					end
				end
				TokenFramePopup:Hide();
			end)
			button.hooked = true;
		end
	end

	return #TokenFrameContainer.buttons
end

function TB:UpdateAndNotify(item, id, count)
	if (not id) then
		return;
	end
	local table = item and "items" or "currency";
	local oldCount = ElvDB["trackerbar"][TB.myname]["count"][table][id] or 0;

	local earned = L["You have |cff00ff00earned|r %d %s (|cff00ffffcurrently|r %d)"]
	local lost = L["You have |cffff0000lost|r %d %s (|cff00ffffcurrently|r %d)"]
	local change = count - oldCount;
	local link = item and select(2, GetItemInfo(id)) or GetCurrencyLink(id, 0)
	local notify = E.db.chaoticui.utilitybars.trackerbar.notify;
	if (change and link and change > 0) then
		self.sessionDB[table][id]["gained"] = self.sessionDB[table][id]["gained"] + change;
		if (notify) then
			UIErrorsFrame:AddMessage(earned:format(change, link, count));
		end
	elseif (change and link and change < 0) then
		self.sessionDB[table][id]["lost"] = self.sessionDB[table][id]["lost"] + -change;
		if (notify) then
			UIErrorsFrame:AddMessage(lost:format(-change, link, count));
		end
	end
	ElvDB["trackerbar"][TB.myname]["count"][table][id] = count;
end

function TB:FixDataTable()
	local fixed = false;
	return fixed;
end

local function GetNumberOfTokenFrameButtons()
	if (not TokenFrameContainer.buttons) then
		return 0;
	end

	return #TokenFrameContainer.buttons;
end

function TB:Initialize()
	CUB:InjectScripts(self);
	
	--[[if (not E.global.chaoticui.farmmigrated) then
		CUI:MigrateFarmDataToTracker();
		E.global.chaoticui.farmmigrated = true;
	end]]

	TB.sessionDB = {};
	TB.sessionDB["items"] = {};
	TB.sessionDB["currency"] = {};

	TB.myname = ("%s-%s"):format(E.myname,E.myrealm);
	ElvDB = ElvDB or {};
	if (not self:FixDataTable()) then
		ElvDB["trackerbar"] = ElvDB["trackerbar"] or {};
		ElvDB["trackerbar"][TB.myname] = ElvDB["trackerbar"][TB.myname] or {};
		ElvDB["trackerbar"][TB.myname]["items"] = ElvDB["trackerbar"][TB.myname]["items"] or {};
		ElvDB["trackerbar"][TB.myname]["currency"] = ElvDB["trackerbar"][TB.myname]["currency"] or {};
		ElvDB["trackerbar"][TB.myname]["count"] = ElvDB["trackerbar"][TB.myname]["count"] or {};
		ElvDB["trackerbar"][TB.myname]["count"]["items"] = ElvDB["trackerbar"][TB.myname]["count"]["items"] or {};
		ElvDB["trackerbar"][TB.myname]["count"]["currency"] = ElvDB["trackerbar"][TB.myname]["count"]["currency"] or {};
	end

	local frame = CreateFrame("Frame", "ChaoticUI_TrackerBarController");
	frame:RegisterEvent('BAG_UPDATE');
	frame:RegisterEvent('CHAT_MSG_MONEY');
	frame:RegisterEvent('CHAT_MSG_CURRENCY');
	frame:RegisterEvent('CHAT_MSG_COMBAT_HONOR_GAIN');
	frame:RegisterEvent('CURRENCY_DISPLAY_UPDATE');
	CUB:RegisterEventHandler(self, frame);

	local bar = self:CreateBar();

	self.bar = bar;
	self.hooks = {};

	self:UpdateBar(bar);

	self.hookedBags = {};
	self:HookElvUIBags();
	hooksecurefunc(B, "Layout", function()
		self:HookElvUIBags()
	end);

	hooksecurefunc(_G, "TokenFrame_Update", function()
		if (GetNumberOfTokenFrameButtons() ~= self.hookedCurrency) then
			self.hookedCurrency = self:HookCurrencyButtons();
		end
	end);
end


function TB:CopyFrom(name, realm)
	if (not realm) then
		realm = E.myrealm;
	end

	local theirName = ("%s-%s"):format(name, realm);
	if (not ElvDB["trackerbar"][theirName]) then
		return;
	end

	ElvDB["trackerbar"][TB.myname] = CopyTable(ElvDB["trackerbar"][theirName]);
	ElvDB["farmBar"][TB.myname] = CopyTable(ElvDB["farmBar"][theirName]);
	TB:UpdateBar(TB.bar);
end

CUB:RegisterUtilityBar(TB);
