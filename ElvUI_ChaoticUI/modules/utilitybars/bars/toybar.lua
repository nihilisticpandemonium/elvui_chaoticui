local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local LSM = LibStub("LibSharedMedia-3.0");

local TOYB = CUI:NewModule("ToyBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule('UtilityBars')

local floor = math.floor

function TOYB:CreateBar()
	local bar = CUB:CreateBar("ChaoticUI_ToyBar", "toybar", {'BOTTOMRIGHT', RightChatPanel, 'TOPRIGHT', 0, 45}, 'Toy Bar')

	return bar;
end

function TOYB:GetToys()
	local toys = {};

    if (not C_ToyBox.HasFavorites()) then
        return toys;
    end

    for i = 1, C_ToyBox.GetNumToys() do
        local itemID = C_ToyBox.GetToyFromIndex(i);

        if (C_ToyBox.GetIsFavorite(itemID)) then
            tinsert(toys, itemID);
        end
    end

	return toys;
end

function TOYB:UpdateBar(bar)
	local toys = self:GetToys();

	CUB:CreateButtons(bar, #toys);

	for i, toy in ipairs(toys) do
		local button = bar.buttons[i];
		CUB:UpdateButtonAsToy(bar, button, toy);
	end

	CUB:UpdateBar(self, bar, "ELVUIBAR30BINDBUTTON");
end

function TOYB:Initialize()
	local frame = CreateFrame("Frame", "ChaoticUI_ToyBarController");
	frame:RegisterEvent('TOYS_UPDATED');
	CUB:RegisterEventHandler(self, frame);

	CUB:InjectScripts(self);
	
	local bar = self:CreateBar();
	self.bar = bar;
	self.hooks = {};

	self:UpdateBar(bar);
end

CUB:RegisterUtilityBar(TOYB);
