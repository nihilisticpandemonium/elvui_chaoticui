local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local LSM = LibStub("LibSharedMedia-3.0");

local PB = CUI:NewModule("ProfessionBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule('UtilityBars')

local floor = math.floor

function PB:CreateBar()
	local bar = CUB:CreateBar("ChaoticUI_ProfessionBar", "professionBar", {'BOTTOMRIGHT', RightChatPanel, 'TOPRIGHT', 0, 2}, 'Profession Bar')
	
	CUB:RegisterCreateButtonHook(bar, function(button) PB:CreateButtonHook(button) end);
	CUB:RegisterUpdateButtonHook(bar, function(button, ...) PB:UpdateButtonHook(button, ...) end);

	return bar;
end

-- Positive numbers here mean use this skill id for this profession rather than the profession itself
-- Negative numbers mean this skill id is used for the second skill for this profession
PB.SkillFixMap = {
	[186] = 2656,
	[182] = 2366,
	[185] = -818,
	[333] = -13262,
	[755] = -31252,
	[773] = -51005,
	[794] = -80451,
};

PB.ClassProfMap = {
	["DEATHKNIGHT"] = 53428
};

function PB:CreateButtonHook(button)
	button.ranktext = button:CreateFontString(nil, "OVERLAY");
	button.ranktext:FontTemplate(LSM:Fetch("font", E.db.general.font), 10, "THINOUTLINE");
	button.ranktext:SetWidth(E:Scale(self.bar.db.buttonsize) - 4);
	button.ranktext:SetHeight(E:Scale(14));
	button.ranktext:SetJustifyH("RIGHT");
	button.ranktext:Point("BOTTOMRIGHT", 0, 0);
end

function PB:UpdateButtonHook(button, prof, ranks)
	button.texture:SetTexture(GetSpellTexture(prof));
	button.ranktext:SetText(ranks[prof] or "");
end

function PB:GetProfessions()
	local professions = {};
	local ranks = {};

	for k, v in pairs({GetProfessions()}) do
		local name, _, rank, maxRank, _, _, skillID = GetProfessionInfo(v);

		if (name) then
			if (self.SkillFixMap[skillID]) then
				local realSkill = self.SkillFixMap[skillID];
				if (realSkill > 0) then
					name = GetSpellInfo(realSkill);
					tinsert(professions, name);
					ranks[name] = rank;
				else
					tinsert(professions, name);
					ranks[name] = rank;
					local secondSkillName = GetSpellInfo(-(self.SkillFixMap[skillID]));
					if (secondSkillName) then
						tinsert(professions, secondSkillName);
					end
				end
			else
				tinsert(professions, name);
				ranks[name] = rank;
			end
		end
	end

	if (self.ClassProfMap[E.myclass]) then
		local name = GetSpellInfo(self.ClassProfMap[E.myclass]);
		tinsert(professions, name);
	end

	return professions, ranks;
end

function PB:UpdateBar(bar)
	local professions, ranks = self:GetProfessions();

	CUB:CreateButtons(bar, #professions);

	for i, prof in ipairs(professions) do
		local button = bar.buttons[i];
		local _, _, _, _, _, _, spellID = GetSpellInfo(prof);
		CUB:UpdateButtonAsSpell(bar, button, spellID, prof, ranks);
	end

	CUB:UpdateBar(self, bar, "ELVUIBAR25BINDBUTTON");
end

function PB:Initialize()
	local frame = CreateFrame("Frame", "ChaoticUI_ProfBarController");
	frame:RegisterEvent('SPELLS_CHANGED');
	frame:RegisterEvent('TRADE_SKILL_LIST_UPDATE');
	CUB:RegisterEventHandler(self, frame);

	CUB:InjectScripts(self);
	
	local bar = self:CreateBar();
	self.bar = bar;
	self.hooks = {};

	self:UpdateBar(bar);
end

CUB:RegisterUtilityBar(PB)
