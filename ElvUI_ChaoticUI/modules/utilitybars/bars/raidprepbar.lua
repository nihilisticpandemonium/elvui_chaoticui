local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local addon, ns = ...

local RPB = CUI:NewModule("RaidPrepBar", 'AceHook-3.0', "AceTimer-3.0", "AceEvent-3.0")
local CUB = CUI:GetModule('UtilityBars')
local B = E:GetModule('Bags');
local PT = LibStub("LibPeriodicTable-3.1");

local floor = math.floor

function RPB:CreateBar()
	return CUB:CreateBar("ChaoticUI_RaidPrepBar", "raidPrepBar", {'TOPRIGHT', ChaoticUI_TrackerBar, 'BOTTOMRIGHT', 0, -2}, 'Raid Prep Bar')
end

function RPB:CreateButtons(bar)
	local keys = {
		{
			key = "ChaoticUI.RaidPrep.LegionBuffFood",
		 	req = function() return UnitLevel("player") > 100 end,
		},
		{
			key = "Muffin.Food.Buff",
		  	req = function() return UnitLevel("player") == 100 end,
		},
		{
			key = "ChaoticUI.RaidPrep.AugmentRunes",
		  	req = function() return UnitLevel("player") < 110 end,
		},
		{
			key = "ChaoticUI.RaidPrep.LegionFlasks",
		 	req = function() return UnitLevel("player") >= 100 end,
		},
		{
			key = "ChaoticUI.RaidPrep.LegionAugmentRunes",
			req = function() return UnitLevel("Player") >= 110 end,
		},
		{
			key = "ChaoticUI.RaidPrep.DemonHunter",
		},
		{
			key = "Muffin.Flasks",
		  	req = function() return UnitLevel("player") == 100 end,
		},
	};

	local j = 1;

	local function addButton(itemID)
		local count = GetItemCount(itemID);

		if (count > 0) then
			local button = bar.buttons[j];
			if (not button) then
				button = CUB:CreateButton(bar);
				bar.buttons[j] = button;
			end

			CUB:UpdateButtonAsItem(bar, button, itemID);
			
			j = j + 1;
		end
	end

	local checkAndAdd;

	checkAndAdd = function(k, v)
		if (type(v) == "table") then
			for _k, _v in pairs(v) do
				if (type(_k) == "number") then
					checkAndAdd(_k, _v);
				end
			end
		else
			addButton(k);
		end
	end

	for _, info in pairs(keys) do
		if (not info.req or info.req()) then
			local db = PT:GetSetTable(info.key);
			for k, v in pairs(db) do
				if (type(k) == "number") then
					checkAndAdd(k, v);
				end
			end
		end
	end

	for x = j, #bar.buttons do
		bar.buttons[x].data = nil;
	end
end

function RPB:UpdateBar(bar)
	RPB:CreateButtons(bar);
	CUB:UpdateBar(self, bar, "ELVUIBAR26BINDBUTTON");
end

function RPB:AddAugmentRunes()
	PT:AddData("ChaoticUI.RaidPrep", "Rev: 1",
	{
		["ChaoticUI.RaidPrep.AugmentRunes"] = "118630, 118631, 118632, 128482",
		["ChaoticUI.RaidPrep.LegionAugmentRunes"] = "140587,153023",
		["ChaoticUI.RaidPrep.LegionFlasks"] = "127847,127848,127849,127850,127858,147707",
		["ChaoticUI.RaidPrep.LegionBuffFood"] = "133565,133566,133567,133568,133569,133570,133571,133572,133573,133574,133576,133577,133578,133579,133681",
		["ChaoticUI.RaidPrep.DemonHunter"] = "129192,129210"
	});
end

function RPB:Initialize()
	self:AddAugmentRunes();
	CUB:InjectScripts(self);

	local frame = CreateFrame("Frame", "ChaoticUI_RaidPrepBarController");
	frame:RegisterEvent('BAG_UPDATE');
	frame:RegisterEvent('PLAYER_ENTERING_WORLD');
	frame:RegisterEvent('GET_ITEM_INFO_RECEIVED');
	CUB:RegisterEventHandler(self, frame);
	
	local bar = self:CreateBar();
	self.bar = bar;
	self.hooks = {};

	self:UpdateBar(bar);
end

CUB:RegisterUtilityBar(RPB);
