local CUI, E, L, V, P, G = unpack(select(2, ...));

local AB = E:GetModule('ActionBars');

local CUB = CUI:NewModule('UtilityBars', 'AceEvent-3.0');
local LAB = LibStub("LibActionButton-1.0-ElvUI")
local LSM = LibStub("LibSharedMedia-3.0");
local Masque = LibStub("Masque", true)
local MasqueGroup = Masque and Masque:Group("ElvUI", "ActionBars")

local CreateFrame, GetItemCount, GetItemInfo, GetMouseFocus, GetSpellInfo, InCombatLockdown, RegisterStateDriver, SetModifiedClick, select, unpack, tinsert, ipairs, pairs, floor, format, max = CreateFrame, GetItemCount, GetItemInfo, GetMouseFocus, GetSpellInfo, InCombatLockdown, RegisterStateDriver, SetModifiedClick, select, unpack, tinsert, ipairs, pairs, floor, format, max

-- GLOBALS: LeftChatPanel

function CUB:ActivateBar(bar)
	if (bar:IsVisible()) then
		E:UIFrameFadeIn(bar, 0.2, bar:GetAlpha(), bar.db.alpha);
	end
end

function CUB:DeactivateBar(bar)
	E:UIFrameFadeOut(bar, 0.2, bar:GetAlpha(), 0);
end

local watcher = 0
local function onUpdate(self, elapsed)
	if watcher > 0.1 then
		if not self:IsMouseOver() then
			self:SetScript("OnUpdate", nil)
			CUB:DeactivateBar(self);
		end
		watcher = 0
	else
		watcher = watcher + elapsed
	end
end

function CUB:InjectScripts(tbl)
	tbl["Button_OnEnter"] = function(self, button)
		local bar = button:GetParent();
		bar:SetScript("OnUpdate", onUpdate)
		CUB:ActivateBar(bar);
	end
end

CUB["RegisteredBars"] = {};
function CUB:RegisterUtilityBar(tbl)
	CUB["RegisteredBars"][tbl:GetName()] = tbl;
	CUI:RegisterModule(tbl:GetName());
end

local chaoticui_ab_id = 770;

CUB["CreatedBars"] = {};

function CUB:PLAYER_REGEN_DISABLED()
	if (E.db.chaoticui.utilitybars.hideincombat) then
		for bar, _ in pairs(CUB["CreatedBars"]) do
 		   	RegisterStateDriver(bar, 'visibility', 'hide');
		end
	end
end

function CUB:PLAYER_REGEN_ENABLED()
	for _, tbl in pairs(CUB["RegisteredBars"]) do
    	tbl:UpdateBar(tbl.bar);
	end
end

function CUB:CreateBar(name, db, point, moverName)
	local bar = CreateFrame('Frame', name, E.UIParent, "SecureHandlerStateTemplate");

	if (type(db) == 'string') then
		CUI:RegisterDB(bar, "utilitybars."..db);
	else
    	bar.db = db;
	end
	bar.id = chaoticui_ab_id;

	if (not AB.db["bar"..chaoticui_ab_id]) then
		AB.db["bar"..chaoticui_ab_id] = { showGrid = false };
	end

	local ES = CUI:GetModule('EnhancedShadows');
	bar:SetFrameLevel(1);
	bar:SetTemplate('Transparent');
	bar:CreateShadow();
	ES:RegisterShadow(bar.shadow);
	bar:SetFrameStrata('MEDIUM');
	bar.buttons = {};

	RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');

	bar:Size(320, 36);
	if (point) then
		bar:Point(unpack(point));
	else
		bar:Point('BOTTOMLEFT', LeftChatPanel, 'TOPRIGHT', 0, 15);
	end
	E:CreateMover(bar, name..'Mover', moverName or name, nil, nil, nil, 'ALL,ACTIONBARS');

	CUB["CreatedBars"][bar] = true;
	return bar;
end

function CUB:RegisterCreateButtonHook(bar, func)
	if (not bar.createButtonHooks) then
		bar.createButtonHooks = {};
	end
	tinsert(bar.createButtonHooks, func);
end

function CUB:RegisterUpdateButtonHook(bar, func)
	if (not bar.updateButtonHooks) then
		bar.updateButtonHooks = {};
	end
	tinsert(bar.updateButtonHooks, func);
end

local function ExecuteHooks(tbl, ...)
	if (not tbl) then return end;

	for i, f in ipairs(tbl) do
		f(...);
	end
end

function CUB:CreateButton(bar)
	local button = LAB:CreateButton(#bar.buttons + 1, format(bar:GetName().."Button%d", #bar.buttons + 1), bar, nil);
	button:SetFrameLevel(bar:GetFrameLevel() + 2);
	button:SetTemplate();
	button.cooldown = CreateFrame("Cooldown", nil, button, "CooldownFrameTemplate");
	button.cooldown:SetAllPoints(button);
	button.texture = button:CreateTexture(nil, 'ARTWORK');
	button.texture:SetInside();
	button.texture:SetTexCoord(unpack(E.TexCoords));
	button.count = button:CreateFontString(nil, "OVERLAY");
	button.count:FontTemplate(LSM:Fetch("font", E.db.general.font), 10, "THINOUTLINE");
	button.count:SetWidth(E:Scale(bar.db.buttonsize) - 4);
	button.count:SetHeight(E:Scale(14));
	button.count:SetJustifyH("RIGHT");
	button.count:Point("BOTTOMRIGHT", 0, 0);
	AB:StyleButton(button, nil, MasqueGroup and E.private.actionbar.masque.actionbars and true or nil, true);
	button:SetCheckedTexture("")
	RegisterStateDriver(button, 'visibility', '[petbattle] hide; show')

	if MasqueGroup and E.private.actionbar.masque.actionbars then
		button:AddToMasque(MasqueGroup)
	end
	ExecuteHooks(bar.createButtonHooks, button);

	if (not AB.handledbuttons[button]) then
		E:RegisterCooldown(button.cooldown)

		AB.handledbuttons[button] = true;
	end
			
	return button;
end

function CUB:CreateButtons(bar, num)
	for i = 1, num do
		local button = bar.buttons[i];
		if (not button) then
			button = CUB:CreateButton(bar);
			bar.buttons[i] = button;
		end

		local size = E:Scale(bar.db.buttonsize);

		button:Size(size);
	end
end

local function GenericButtonUpdate(bar, button, ...)
	local size = E:Scale(bar.db.buttonsize);
	button:Size(size);

	ExecuteHooks(bar.updateButtonHooks, button, ...);
end

function CUB:UpdateButtonAsItem(bar, button, id, ...)
	button.data = id;
	local args = { ... };
    Item:CreateFromItemID(id):ContinueOnItemLoad(function()
    	local itemName, _, quality, _, _, _, _, _, _, texture = GetItemInfo(id);
		local count = GetItemCount(id);
		button:SetAttribute('item', itemName);
		button.count:SetText(count);
		button.count:SetShown(count > 1);
		button.texture:SetTexture(texture);
		button.texture:SetDesaturated(count == 0);
		button:SetState(0, 'item', id);
		
		GenericButtonUpdate(bar, button, unpack(args));
	end);
end

function CUB:UpdateButtonAsSpell(bar, button, id, ...)
	button.data = id;
	local args = { ... };
	Spell:CreateFromSpellID(id):ContinueOnSpellLoad(function()
		local name = GetSpellInfo(id);
		button:SetAttribute('type', 'spell');
		button:SetAttribute("spell", name);
		button:SetState(0, 'spell', id);
	    local spellTexture = GetSpellTexture(id);
		button.texture:SetTexture(spellTexture);
		button.count:Hide();
		button:SetBackdropBorderColor(0, 0, 0, 1)

		GenericButtonUpdate(bar, button, unpack(args));
	end)
end

function CUB:UpdateButtonAsToy(bar, button, id, ...)
	button.data = id;
	local name = select(2, C_ToyBox.GetToyInfo(id));
	button:SetAttribute('type', 'toy');
	button:SetAttribute('toy', name);
	button:SetState(0, 'toy', id);
	button.texture:SetTexture(select(3, C_ToyBox.GetToyInfo(id)));
	button.count:Hide();
	button:SetBackdropBorderColor(0, 0, 0, 1)

	GenericButtonUpdate(bar, button, ...);
end

function CUB:UpdateButtonAsCustom(bar, button, texture, ...)
	local state = {
		func = function(button)
			return
		end,
		texture = texture,
		tooltip = nil, 
	}

	button.texture:SetTexture(texture);
	button.count:Hide();
	button:SetState(0, "custom", state);
	button:SetBackdropBorderColor(0, 0, 0, 1)
	GenericButtonUpdate(bar, button, ...);
end

function CUB:AddAltoholicCurrencyInfo(id)
	if not id then return end
	if not DataStore or not Altoholic then return end

	local colors = Altoholic.Colors;
	local currency = GetCurrencyInfo(id)
	if not currency then return end

	GameTooltip:AddLine(" ",1,1,1);

	local total = 0
	for _, character in pairs(DataStore:GetCharacters()) do
		local _, _, count = DataStore:GetCurrencyInfoByName(character, currency)
		if count and count > 0 then
			GameTooltip:AddDoubleLine(DataStore:GetColoredCharacterName(character),  colors.teal .. count);
			total = total + count
		end
	end

	if total > 0 then
		GameTooltip:AddLine(" ",1,1,1);
	end
	GameTooltip:AddLine(format("%s: %s", colors.gold..L["Total owned"], colors.teal..total ) ,1,1,1);
	GameTooltip:Show()
end

function CUB:UpdateBar(tbl, bar, bindButtons)
	if (not bar.db.enabled) then
		RegisterStateDriver(bar, 'visibility', 'hide');
		return;
	else
		RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');
	end

	bar.mouseover = bar.db.mouseover;

	local spacing, mult = bar.db.spacing or 0, 1;
	local size = bar.db.buttonsize;
	local buttonsPerRow = bar.db.buttonsPerRow or 12;

	local shownButtons, anchorX, anchorY = 0, 0, 0
	for i = 1, #bar.buttons do
		local button = bar.buttons[i];
		if (button.data) then
			RegisterStateDriver(button, 'visibility', '[petbattle] hide; show')
			anchorX = anchorX + 1;
			shownButtons = shownButtons + 1;
			local xOffset, yOffset;
			if ((shownButtons - 1) % buttonsPerRow) == 0 then
				anchorX = 1;
				anchorY = anchorY + 1;
			end

			xOffset = spacing + ((size + spacing) * (anchorX - 1));
			yOffset = -(spacing + ((size + spacing) * (anchorY - 1)));

			button:ClearAllPoints();
			button:SetPoint('TOPLEFT', bar, 'TOPLEFT', xOffset, yOffset);
			if bar.db.mouseover == true then
				if not bar:IsMouseOver() then
					bar:SetAlpha(0);
				end
				
				if not tbl.hooks[button] then
					tbl:HookScript(button, 'OnEnter', 'Button_OnEnter');
				end
			else
				bar:SetAlpha(bar.db.alpha);
				
				if tbl.hooks[button] then
					tbl:Unhook(button, 'OnEnter');	
				end
			end
		else
			RegisterStateDriver(button, 'visibility', 'hide')
		end
	end
	local numRows;
	if (shownButtons <= buttonsPerRow) then
		buttonsPerRow = shownButtons;
		numRows = 1;
	else
		numRows = floor(shownButtons / buttonsPerRow) + (shownButtons % buttonsPerRow == 0 and 0 or 1);
	end

	local barWidth = spacing + ((size * (buttonsPerRow * mult)) + ((spacing * (buttonsPerRow - 1) * mult) + (spacing * mult)))
	local barHeight = size * numRows + spacing*numRows+spacing;
	bar:Size(barWidth, barHeight);
	if (shownButtons == 0) then
		RegisterStateDriver(bar, 'visibility', 'hide');
	else
		RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');
	end

	for i = shownButtons + 1, #bar.buttons do
		RegisterStateDriver(bar.buttons[i], 'visibility', 'hide')
	end

	AB:UpdateButtonConfig(bar, bindButtons);
	if MasqueGroup and E.private.actionbar.masque.actionbars then MasqueGroup:ReSkin() end
end

function CUB:UpdateBarMultRow(tbl, bar, bindButtons)
	if (not bar.db.enabled) then
		RegisterStateDriver(bar, 'visibility', 'hide');
		return;
	else
		RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');
	end

	bar.mouseover = bar.db.mouseover;

	local spacing, mult = bar.db.spacing, 1;
	local size = bar.db.buttonsize;
	local buttonsPerRow = bar.db.buttonsPerRow;

	local shownButtons, anchorX, anchorY = {}, {}, 0;

	local totalShown = 0;

	local seenButton = false;
	for i = 1, #bar.buttons do
		local button = bar.buttons[i];
		if (button.data) then
			local row = button.row;
			RegisterStateDriver(button, 'visibility', '[petbattle] hide; show')
			if (not anchorX[row]) then
				anchorX[row] = 0;
				shownButtons[row] = 0;
			end
			anchorX[row] = anchorX[row] + 1;
			shownButtons[row] = shownButtons[row] + 1;
			totalShown = totalShown + 1;

			seenButton = true;
			local xOffset, yOffset;
			
			anchorY = 1;
			for i = row - 1, 1, -1 do
				if (shownButtons[i] and shownButtons[i] > 0) then
					anchorY = anchorY + 1;
				end
			end

			xOffset = spacing + ((size + spacing) * (anchorX[row] - 1));
			yOffset = -(spacing + ((size + spacing) * (anchorY - 1)));

			button:ClearAllPoints();
			button:SetPoint('TOPLEFT', bar, 'TOPLEFT', xOffset, yOffset);
			if bar.db.mouseover == true then
				if not bar:IsMouseOver() then
					bar:SetAlpha(0);
				end
				
				if not tbl.hooks[button] then
					tbl:HookScript(button, 'OnEnter', 'Button_OnEnter');
				end
			else
				bar:SetAlpha(bar.db.alpha);
				
				if tbl.hooks[button] then
					tbl:Unhook(button, 'OnEnter');	
				end
			end
		else
			RegisterStateDriver(button, 'visibility', 'hide')
		end
	end
	local numRows = 0
	local buttonsPerRow = 0;
	for i = 1, #bar.keys do
		if (shownButtons[i] and shownButtons[i] > 0) then
			buttonsPerRow = max(shownButtons[i], buttonsPerRow);
			numRows = numRows + 1;
		end
	end

	local barWidth = spacing + ((size * (buttonsPerRow * mult)) + ((spacing * (buttonsPerRow - 1) * mult) + (spacing * mult)))
	local barHeight = max((spacing * ((numRows * 2) - 1)), spacing) + (size * numRows)
	bar:Size(barWidth, barHeight);
	if (not seenButton) then
		RegisterStateDriver(bar, 'visibility', 'hide');
	else
		RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');
	end

	for i = totalShown + 1, #bar.buttons do
		RegisterStateDriver(bar.buttons[i], 'visibility', 'hide')
	end

	AB:UpdateButtonConfig(bar, bindButtons);
	if MasqueGroup and E.private.actionbar.masque.actionbars then MasqueGroup:ReSkin() end
end

function CUB:UpdateVertBar(tbl, bar, bindButtons)
	if (not bar.db.enabled) then
		RegisterStateDriver(bar, 'visibility', 'hide');
		return;
	else
		RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');
	end

	bar.mouseover = bar.db.mouseover;

	local spacing, mult = bar.db.spacing or 0, 1;
	local size = bar.db.buttonsize;
	local buttonsPerRow = bar.db.buttonsPerRow or 12;

	local shownButtons, anchorX, anchorY = 0, 0, 0
	for i = 1, #bar.buttons do
		local button = bar.buttons[i];
		if (button.data) then
			RegisterStateDriver(button, 'visibility', '[petbattle] hide; show')
			anchorY = anchorY + 1;
			shownButtons = shownButtons + 1;
			local xOffset, yOffset;
			if ((shownButtons - 1) % buttonsPerRow) == 0 then
				anchorY = 1;
				anchorX = anchorX + 1;
			end

			xOffset = spacing + ((size + spacing) * (anchorX - 1));
			yOffset = spacing + ((size + spacing) * (anchorY - 1));

			button:ClearAllPoints();
			button:SetPoint('BOTTOMLEFT', bar, 'BOTTOMLEFT', xOffset, yOffset);
			if bar.db.mouseover == true then
				if not bar:IsMouseOver() then
					bar:SetAlpha(0);
				end
				
				if not tbl.hooks[button] then
					tbl:HookScript(button, 'OnEnter', 'Button_OnEnter');
				end
			else
				bar:SetAlpha(bar.db.alpha);

				if tbl.hooks[button] then
					tbl:Unhook(button, 'OnEnter');	
				end
			end
		else
			RegisterStateDriver(button, 'visibility', 'hide')
		end
	end
	local numRows;
	if (shownButtons <= buttonsPerRow) then
		buttonsPerRow = shownButtons;
		numRows = 1;
	else
		numRows = floor(shownButtons / buttonsPerRow) + (shownButtons % buttonsPerRow == 0 and 0 or 1);
	end

	local barWidth = max((spacing * ((numRows * 2) - 1)), spacing) + (size * numRows)
	local barHeight = spacing + ((size * (buttonsPerRow * mult)) + ((spacing * (buttonsPerRow - 1) * mult) + (spacing * mult)))
	bar:Size(barWidth, barHeight);
	if (shownButtons == 0) then
		RegisterStateDriver(bar, 'visibility', 'hide');
	else
		RegisterStateDriver(bar, 'visibility', '[petbattle] hide; show');
	end

	for i = shownButtons + 1, #bar.buttons do
		RegisterStateDriver(bar.buttons[i], 'visibility', 'hide')
	end
	
	AB:UpdateButtonConfig(bar, bindButtons);
	if MasqueGroup and E.private.actionbar.masque.actionbars then MasqueGroup:ReSkin() end
end

function CUB:HandleEvent(mod, frame, event)
	if (not mod.bar) then
		return;
	end

	if UnitAffectingCombat("player") or InCombatLockdown() then
		frame:RegisterEvent("PLAYER_REGEN_ENABLED");
		return;
	end

	if (event == "PLAYER_REGEN_ENABLED") then
		frame:UnregisterEvent(event);
	end

	mod:UpdateBar(mod.bar);
end

function CUB:RegisterEventHandler(mod, frame)
	frame:SetScript("OnEvent", function(_, event) CUB:HandleEvent(mod, frame, event) end);
end

function CUB:Initialize()
	self:RegisterEvent("PLAYER_REGEN_DISABLED");
    self:RegisterEvent("PLAYER_REGEN_ENABLED");
end

CUI:RegisterModule(CUB:GetName());
