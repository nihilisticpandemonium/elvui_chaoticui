if not IsAddOnLoaded("AddOnSkins") then return end;

local CUI, E, L, V, P, G = unpack(select(2, ...));

local AS = unpack(AddOnSkins);
local SASX = CUI:NewModule('ChaoticUIAddOnSkinExtension');

function SASX:HideBloodShieldTracker()
	local Bars = {
		BloodShieldTracker_AMSBar,
		BloodShieldTracker_BloodChargeBar,
		BloodShieldTracker_EstimateBar,
		BloodShieldTracker_HealthBar,
		BloodShieldTracker_IllumBar,
		BloodShieldTracker_PurgatoryBar,
		BloodShieldTracker_PWSBar,
		BloodShieldTracker_ShieldBar,
		BloodShieldTracker_TotalAbsorbsBar,
	}

	for _, bar in pairs(Bars) do
		AS:RegisterForPetBattleHide(bar)
	end
end

local BOEICONTEMPLATE = "BarrelsOEasyTarget%d";
local BOERADIOTEMPLATE = "Radio%d";

function SASX:SkinBarrelsOEasy()
	BOEIconFrame:SetTemplate('Default')
	AS:SkinCloseButton(_G["BarrelsOEasyClose"])
	for i = 1, 8 do
		local icon = _G[BOEICONTEMPLATE:format(i)];
		local button = _G[BOERADIOTEMPLATE:format(i)];
		local texture = icon:GetTexture();
		local texcoord = {icon:GetTexCoord()};
		AS:SkinCheckBox(button);
		icon:SetTexture(texture)
		icon:SetTexCoord(unpack(texcoord));
	end
end

function SASX:SkinKalielsTracker()
	AS:CreateShadow(_G["!KalielsTrackerActiveButton"]);
end

function SASX:Initialize()
	AS:RegisterSkin('BloodShieldTracker', SASX.HideBloodShieldTracker);
	AS:RegisterSkin('BarrelsOEasy', SASX.SkinBarrelsOEasy);
	AS:RegisterSkin('!KalielsTracker', SASX.SkinKalielsTracker);
end

CUI:RegisterModule(SASX:GetName());