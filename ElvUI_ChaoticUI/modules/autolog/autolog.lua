--
-- Created by IntelliJ IDEA.
-- User: mtindal
-- Date: 8/29/2017
-- Time: 10:50 PM
-- To change this template use File | Settings | File Templates.
--

local CUI, E, L, V, P, G = unpack(select(2, ...))

local AL = CUI:NewModule('AutoLog', 'AceEvent-3.0')

-- Garrisons
local ignoredInstanceMapIDs = {
	1152,
	1330,
	1153,
	1154,
	1158,
	1331,
	1159,
	1160
}

-- Order Halls
local ignoredMapIDs = {
	1068,
	1052,
	1040,
	1035,
	1077,
	1057,
	1044,
	1072
}

function AL:DisableCombatLogging()
	print("|cffff2020Combat logging off.|r");
	LoggingCombat(false);
end

function AL:EnableCombatLogging()
	print("|cff20ff20Combat logging on.|r");
	LoggingCombat(true);
end

function AL:CheckInstance(instanceType)
	if (instanceType == "none") then return end

	if self.db[instanceType] then
		return true;
	end
end

function AL:PLAYER_ENTERING_WORLD()
	local inInstance, instanceType = IsInInstance();
	if (not inInstance) then
		if (LoggingCombat()) then
			self:DisableCombatLogging();
		end
		return;
	end

	local instanceMapID = select(8,GetInstanceInfo());
	local currentMapAreaID = GetCurrentMapAreaID();

	if ((tContains(ignoredInstanceMapIDs, instanceMapID) or tContains(ignoredMapIDs, currentMapAreaID))) then
		if LoggingCombat() then
			self:DisableCombatLogging();
		end
		return;
	end

	if (AL:CheckInstance(instanceType)) then
		self:EnableCombatLogging();
	end
end

function AL:PLAYER_LOGOUT()
	self:DisableCombatLogging();
end

function AL:UpdateEnabled()
	if (not self.db.enabled) then
		self:UnregisterEvent('PLAYER_ENTERING_WORLD');
		self:UnregisterEvent('PLAYER_LOGOUT');
		if (LoggingCombat()) then
			self:DisableCombatLogging();
		end
	else
		self:RegisterEvent('PLAYER_ENTERING_WORLD');
		self:RegisterEvent('PLAYER_LOGOUT');
		self:PLAYER_ENTERING_WORLD();
	end
end

function AL:Initialize()
	CUI:RegisterDB(self, "autolog")

	self:RegisterEvent('PLAYER_ENTERING_WORLD');
	self:RegisterEvent('PLAYER_LOGOUT');

	self:UpdateEnabled();
end

CUI:RegisterModule(AL:GetName());