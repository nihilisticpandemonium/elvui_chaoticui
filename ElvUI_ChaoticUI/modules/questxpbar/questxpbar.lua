local CUI, E, L, V, P, G = unpack(select(2, ...))

local QXP = CUI:NewModule("QuestXPBar", "AceEvent-3.0")
local DB = E:GetModule("DataBars")

function QXP:CreateQuestXPBar()
    local bar = ElvUI_ExperienceBar.animatedStatusBar or ElvUI_ExperienceBar.statusBar;

    local questXPBar = CreateFrame("StatusBar", "ChaoticUI_QuestXPBar", bar);
    questXPBar:SetPoint("TOPLEFT", bar:GetStatusBarTexture(), "TOPRIGHT");
    questXPBar:SetPoint("BOTTOMRIGHT", bar, "BOTTOMRIGHT");
    questXPBar:SetStatusBarTexture(E.media.normTex);
    E:RegisterStatusBar(questXPBar);
    return questXPBar;
end

local QXP_CURRENT_QUEST_XP_MESSAGE = "|cffffff00Quest XP|r|cffffffff: %d";
local QXP_LEVEL_UP_MESSAGE = "|cffffff00Quest XP|r|cffffffff: %d (|r|cff00ff00Level Up!|r|cffffffff)|r";

function QXP:UpdateQuestXPBar()
    local currQXP = CUI:CalculateQuestLogXP();
        
    local min, max = UnitXP("player"), UnitXPMax("player")
    local cq = min + currQXP;

    self.questXPBar:SetMinMaxValues(0, max - min);
    self.questXPBar:SetValue(math.min(currQXP, max - min));
    if (cq >= max) then
		self.questXPBar:SetStatusBarColor(0.2, 0.6, 0.2, 0.8);
	else
        self.questXPBar:SetStatusBarColor(1.0, 1.0, 0.2, 0.4);
	end
    self.questXPBar:SetShown(currQXP > 0);
end

function QXP:UpdateEnabledState()
    local enabled = self.db.enabled;
    self.questXPBar:SetShown(enabled);
    if (enabled) then
       self:UpdateQuestXPBar();
    end
end

function QXP:OnEvent()
    local level = UnitLevel("player")

	if level == MAX_PLAYER_LEVEL_TABLE[GetExpansionLevel()] or IsXPUserDisabled() then
        self:UnregisterEvent("QUEST_LOG_UPDATE");
        self:UnregisterEvent("PLAYER_XP_UPDATE");
        return;
    end
    local currQXP = CUI:CalculateQuestLogXP();
        
    local min, max = UnitXP("player"), UnitXPMax("player")
    local cq = math.min(currQXP + min, max);

    if (not self.currQXP or self.currQXP ~= currQXP) then
        C_Timer.After(3, function()
            UIErrorsFrame:Clear();
            UIErrorsFrame:AddMessage((cq >= max and QXP_LEVEL_UP_MESSAGE or QXP_CURRENT_QUEST_XP_MESSAGE):format(currQXP));
        end);
    end

    local function CheckBarUpdate(min, max, qxp)
        if (not self.currXP or not self.currQXP) then
            return true;
        end

        return self.currXP[1] ~= min or self.currXP[2] ~= max or self.currQXP ~= qxp;
    end

    if (CheckBarUpdate(min, max, currQXP)) then
        self:UpdateQuestXPBar();
    end

    self.currXP = { min, max };
    self.currQXP = currQXP;
end

function QXP:Initialize()
    CUI:RegisterDB(self, "questXPBar")
    self.questXPBar = self:CreateQuestXPBar();
    self:RegisterEvent("QUEST_LOG_UPDATE", "OnEvent");
    self:RegisterEvent("PLAYER_XP_UPDATE", "OnEvent");
    self:UpdateEnabledState();
end

CUI:RegisterModule(QXP:GetName());