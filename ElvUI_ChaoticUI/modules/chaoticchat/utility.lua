local CUI, E, L, V, P, G = unpack(select(2, ...))
local CC = CUI:GetModule('ChaoticChat')
local LW = LibStub("LibWho-2.0");

local Backdrop = {
	bgFile = E["media"].blankTex, 
	edgeFile = E["media"].blankTex, 
	tile = false,
	tileSize = 0,
	edgeSize = 1,
}

function CC:URLChatFrame_OnHyperlinkShow(self, link)
	CH.clickedframe = self
	if (link):sub(1, 3) == "url" then
		local ChatFrameEditBox = ChatEdit_ChooseBoxForSend()
		local currentLink = (link):sub(5)
		if (not ChatFrameEditBox:IsShown()) then
			ChatEdit_ActivateChat(ChatFrameEditBox)
		end
		ChatFrameEditBox:Insert(currentLink)
		ChatFrameEditBox:HighlightText()
		return;
	end
end

function CC:SetStyle(f, panel, border)
	f:SetBackdrop(Backdrop)

	if panel then
		local color = self.db.general.backdropcolor
		f:SetBackdropColor(color.r, color.g, color.b, self.db.general.alpha)
	else
		f:SetBackdropColor(0, 0, 0, 1)
	end
	
	if border then
		local color = self.db.general.bordercolor
		f:SetBackdropBorderColor(color.r, color.g, color.b, self.db.general.alpha)
	else
		f:SetBackdropBorderColor(0, 0, 0)
	end
end

function CC:CreateDock()
	local dock = CreateFrame("Frame", "ChaoticChatDock", E.UIParent)
	local bcolor = self.db.general.backdropcolor
	local rcolor = self.db.general.bordercolor

	dock:Size(160, 8);
	dock:SetPoint("LEFT", E.UIParent, "LEFT", 0, 0)
	dock:CreateBackdrop('Transparent')
	dock.backdrop:SetBackdropColor(bcolor.r, bcolor.g, bcolor.b, self.db.general.alpha)
	dock.backdrop:SetBackdropBorderColor(rcolor.r, rcolor.g, rcolor.b, self.db.general.alpha)
	dock:SetAlpha(0);
	dock:EnableMouse(false);

	E:CreateMover(dock, dock:GetName()..'Mover', 'ChaoticChat Dock', nil, nil, nil, 'ALL,SOLO');

	self.dock = dock;
end

function CC:CheckDelayed()
	for sender, _ in pairs(self.delayed) do
		if (self.userCache[sender]) then
			for _, args in ipairs(self.delayed[sender]) do
				self:AddIncoming(args.event, args.msg, sender, args.guid);
			end
		end
	end
end

function CC:FixSameRealm(string)
	if (string:find('%-')) then
		local name, realm = string.split('-',string);
		if (realm == CC.myrealm) then
			return name;
		end
	end
	return string;
end


function CC:SetInfoString(event, sender, guid)
	sender = CC:FixSameRealm(sender);
	local chat = self.chats[sender]

	local infoString
	local tabName
	local hasInfo = true;

	if (event == "CHAT_MSG_BN_WHISPER" or event == "CHAT_MSG_BN_WHISPER_INFORM") then
		chatType = "BN_WHISPER"
		chatColor = ChatTypeInfo[chatType]
		local id = BNet_GetBNetIDAccount(sender)
		local presenceID, presenceName, battleTag, isBattleTagPresence, toonName, toonID = BNGetFriendInfo(BNGetFriendIndex(id))
		local _, toonName, client, realmName, realmID, faction, race, class, guild, zoneName, level, gameText = BNGetGameAccountInfo(toonID or 0)

		if (client == "WoW") then
			local token = self.maleClasses[class]
			if (not token) then token = self.femaleClasses[class] end
			local color = GetQuestDifficultyColor(level)
			local levelColor = E:RGBToHex(color.r, color.g, color.b);
			local classColor = RAID_CLASS_COLORS[token]
			classColor = E:RGBToHex(classColor.r, classColor.g, classColor.b);
			tabName = classColor .. presenceName .. "|r";
			infoString = classColor .. presenceName .. " " .. toonName .. "|r (" .. levelColor .. level .. "|r  " .. race .. " "  .. classColor .. class .. "|r) "
			self.senderInfo[sender] = { classColor = classColor, toonName = toonName };
		elseif (client) then
			local fixedClient
			if (client == "WTCG") then
				fixedClient = "Hearthstone"
			elseif (client == "D3") then
				fixedClient = "Diablo 3"
			elseif (client == "S2") then
				fixedClient = "StarCraft 2"
			elseif (client == "Hero") then
				fixedClient = "Heroes of the Storm"
			elseif (client == "App") then
				fixedClient = "Battle.net Desktop App"
			elseif (client == "S1") then
				fixedClient = "StarCraft: Remasted"
			elseif (client == "DST2") then
				fixedClient = "Destiny 2"
			elseif (client == "VIPR") then
				fixedClient = "Call of Duty 4"
			elseif (client == "BSAp") then
				fixedClient = "Mobile"
			else
				fixedClient = ("Unknown (%s)"):format(client);
			end

			tabName = presenceName;
			infoString = presenceName .. " (" .. fixedClient .. ")"
			self.senderInfo[sender] = nil;
		else
			tabName = "Unknown";
			infoString = "Unknown";
		end
		chat.hex = E:RGBToHex(chatColor.r, chatColor.g, chatColor.b)
	elseif (event == "CHAT_MSG_WHISPER" or event == "CHAT_MSG_WHISPER_INFORM") then
		chatType = "WHISPER"
		chatColor = ChatTypeInfo[chatType]
		local realm, class, level, race, name;
		local onMyRealm = true;
		local inParty = false;
		self.senderInfo[sender] = nil;
		
		if (guid) then
			local _
			class, _, race, _, _, name, realm = GetPlayerInfoByGUID(guid);
			
			onMyRealm = realm == "";
			inParty = UnitInParty(name);

			if (not onMyRealm and not inParty) then
				level = UnitLevel(sender);
				if (not level or level == 0) then
					level = "??";
				end		
			
				self.userCache[sender] = {}
				self.userCache[sender].level = level;
				self.userCache[sender].race = race;
				self.userCache[sender].class = class;
				self.userCache[sender].name = name;
			end
		end
		if (onMyRealm or inParty) then
			realm = onMyRealm and CC.myrealm or realm;
		
			class = UnitClass(sender)
			level = UnitLevel(sender)
			race = UnitRace(sender)
			name = UnitName(sender)
			
			if (class and level and level > 0 and race and name and not self.userCache[sender]) then
				self.userCache[sender] = {}
				self.userCache[sender].level = level;
				self.userCache[sender].race = race;
				self.userCache[sender].class = class;
				self.userCache[sender].name = name;
			end

			if (not self.userCache[sender] or self.userCache[sender].level == "UNKNOWN") then
				local results = LW:UserInfo(sender, {
					["callback"] = function(results)
							if (results) then
								self.userCache[sender] = {}
								self.userCache[sender].level = results.Level;
								self.userCache[sender].class = results.Class;
								self.userCache[sender].race = results.Race;
								self.userCache[sender].name = results.Name;
								self:CheckDelayed();
							else
								self.userCache[sender] = {}
								self.userCache[sender].level = UNKNOWN;
								self.userCache[sender].class = UNKNOWN;
								self.userCache[sender].race = UNKNOWN;
								self.userCache[sender].name = sender;
								self:CheckDelayed();
							end
						end
					});
				if (results) then
					self.userCache[sender] = {}
					self.userCache[sender].level = results.Level;
					self.userCache[sender].class = results.Class;
					self.userCache[sender].race = results.Race;
					self.userCache[sender].name = results.Name;
				elseif (not self.userCache[sender]) then
					hasInfo = false;
					chat.hex = E:RGBToHex(1.0, 1.0, 1.0);
					tabName = sender;
					infoString = sender;
				end
			end
		end

		if (hasInfo) then
			level = self.userCache[sender].level;
			class = self.userCache[sender].class;
			race = self.userCache[sender].race;
			name = self.userCache[sender].name;
			
			local classColor;

			if (level ~= UKNNOWN) then
				local color = level ~= "??" and GetQuestDifficultyColor(level) or GetQuestDifficultyColor(1)
				local levelColor = E:RGBToHex(color.r, color.g, color.b);

				local token = self.maleClasses[class]
				if (not token) then token = self.femaleClasses[class] end
				classColor = RAID_CLASS_COLORS[token]
				classColor = E:RGBToHex(classColor.r, classColor.g, classColor.b);

				if (realm ~= CC.myrealm) then
					infoString = classColor .. name .. "|r (" .. levelColor .. level .. "|r " .. race .. " " .. classColor .. class .. "|r) " .. realm
				else
					infoString = classColor .. name .. "|r (" .. levelColor .. level .. "|r  " .. race .. " " .. classColor .. class .. "|r)"
				end
				tabName = classColor .. name .. "|r";
			else
				classColor = E:RGBToHex(1.0, 1.0, 1.0);
				tabName = name;
				infoString = name;
			end

			self.senderInfo[sender] = { classColor = classColor, toonName = name };
			chat.hex = classColor
		end
	end
	
	chat.Name:SetText(infoString)
	chat.DockedName:SetText(infoString)

	CC:UpdateTab(chat, tabName);

	return true;
end

function CC:AddIncoming(event, msg, sender, guid) -- Add messages to the text
	sender = CC:FixSameRealm(sender);
	local chat = self.chats[sender]
	if (not chat) then return end;
	local text = chat.Text;
	local editbox = chat.EditBox;
	if (not text) or (strfind(msg, "<DBM>")) or (strfind(msg, "<Deadly Boss Mods>")) or (strfind(msg, "<VEM>")) or (strfind(msg, "<Voice Encounter Mods>")) or (strfind(msg, "<BigWigs>")) then return end -- Blocking dumb DBM messages
	local color, str

	if event == "CHAT_MSG_BN_WHISPER" or event == "CHAT_MSG_BN_WHISPER_INFORM" then
		color = ChatTypeInfo["BN_WHISPER"]
	elseif event == "CHAT_MSG_WHISPER" or event == "CHAT_MSG_WHISPER_INFORM" then
		color = ChatTypeInfo["WHISPER"]
	end

	local res = CC:SetInfoString(event, sender, guid)
	if (res == true) then
		if (self.delayed[sender]) then
			local remove;
			for i, v in ipairs(self.delayed[sender]) do
				if(v.msg == msg) then
					remove = i;
					break;
				end
			end
			tremove(self.delayed[sender], remove);
		end
	elseif (res == false) then
		if not (self.delayed[sender]) then self.delayed[sender] = {} end
		tinsert(self.delayed[sender], { ["event"] = event, ["msg"] = msg, ["guid"] = guid });
		return
	else
		return
	end
	
	local timestamp = ""
	if CC.db.windows.timestamp then
		timestamp = CC:GetFormattedTime(false, true)
	end
	
	if (not chat.hex) then
		chat.hex = "|cffffffff";
	end
	
	if event == "CHAT_MSG_WHISPER_INFORM" then -- Whisper
		if (self.senderInfo[sender]) then
			str = "|cffff2020@|r" ..self.senderInfo[sender].classColor .. "|Hplayer:"..sender.."|h"..chat.hex..sender.."|r|h: "..msg
		else
			str = "|cffff2020@|r".." |Hplayer:"..sender.."|h"..chat.hex..sender.."|r|h:"..msg
		end
	elseif event == "CHAT_MSG_WHISPER" then
		if (self.senderInfo[sender]) then
			str = "|Hplayer:"..sender.."|h"..self.senderInfo[sender].classColor..sender.."|r|h" .. chat.hex..": "..msg
		else
			str = "|Hplayer:"..sender.."|h"..chat.hex..sender.."|r|h"..": "..msg
		end
		chat.LastMessage:SetText("Last message recieved at " .. CC:GetFormattedTime(true))
	elseif event == "CHAT_MSG_BN_WHISPER_INFORM" then -- BNet
		if (self.senderInfo[sender]) then
			str = "|cffff2020@|r"..self.senderInfo[sender].classColor..self.senderInfo[sender].toonName.."|r"..chat.hex.." ("..sender.."): " .. msg
		else
			str = "|cffff2020@|r"..sender..": "..msg
		end
	else
		if (self.senderInfo[sender]) then
			str = self.senderInfo[sender].classColor..self.senderInfo[sender].toonName.."|r"..chat.hex.." ("..sender.."): " .. msg
		else
			str = sender..": "..msg
		end
		chat.LastMessage:SetText("Last message recieved at " .. CC:GetFormattedTime(true))
	end
	
	if chat.minimized then
		chat.Flash:SetScript("OnUpdate", function()
			self:StartFlash(chat.DockedName, 0.6)
		end)
	elseif not chat.active then
		self.tabs[chat].Flash:SetScript("OnUpdate", function()
			self:StartFlash(self.tabs[chat].text, 0.6);
		end);
	end
	
	text:AddMessage(timestamp .. str, color.r, color.g, color.b, nil, false)
end

function CC:AddStatus(event, toast, author) -- Add messages to the text
	if not self.chats[author] then return end
	local text = self.chats[author].Text
	if not text then return end

	local status, hr, min, timestamp
	
	if CC.db.windows.timestamp then
		hr, min = GetGameTime()
		timestamp = format("[%d:%d] ", hr, min)
	else
		timestamp = ""
	end
	
	if toast == "FRIEND_ONLINE" then
		status = format(ERR_FRIEND_ONLINE_SS, author, author)
	else
		status = format(ERR_FRIEND_OFFLINE_S, "["..author.."]")
	end
	
	text:AddMessage(timestamp..status, ChatTypeInfo["BN_WHISPER"].r, ChatTypeInfo["BN_WHISPER"].g, ChatTypeInfo["BN_WHISPER"].b, nil, false)
end

function CC:CopyChat(frame)
	if not CopyChatFrame:IsShown() then
		local lines = {};
		for i = 1, frame:GetNumMessages() do
			local t = frame:GetMessageInfo(i);
			if (t) then
				tinsert(lines, t);
			end
		end
		local text = table.concat(lines, "\n");

		CopyChatFrameEditBox:SetText(text)
		CopyChatFrame:Show();
	else
		CopyChatFrame:Hide()
	end
end

local date = date;

function CC:GetFormattedTime(tag, timestamp)
	local hour, minutes;
	if (self.db.windows.localtime) then
		hour, minutes = tonumber(date("%H")), tonumber(date("%M"))
	else
		hour, minutes = GetGameTime()
	end

	local _tag = "AM"

	if (minutes < 10) then
		minutes = "0" .. tostring(minutes)
	end
	
	if (hour > 12) then
		_tag = "PM"
	
		if (self.db.windows.timeformat == "12Hour") then
			hour = hour - 12
		end
	end
	
	if tag then
		if (self.db.windows.timeformat == "12Hour") then
			return format("%s:%s %s", hour, minutes, _tag)
		else
			return format("%s:%s ", hour, minutes)
		end
	elseif timestamp then
		return format("[%s:%s] ", hour, minutes)
	end
end

function CC:StartFlash(obj, duration)
	if not obj.anim then
		obj.anim = obj:CreateAnimationGroup("Flash")
		
		obj.anim.fadein = obj.anim:CreateAnimation("ALPHA", "FadeIn")
		obj.anim.fadein:SetFromAlpha(0);
		obj.anim.fadein:SetToAlpha(1);
		obj.anim.fadein:SetOrder(2)

		obj.anim.fadeout = obj.anim:CreateAnimation("ALPHA", "FadeOut")
		obj.anim.fadeout:SetFromAlpha(1);
		obj.anim.fadeout:SetToAlpha(0);
		obj.anim.fadeout:SetOrder(1)
	end

	obj.anim.fadein:SetDuration(duration)
	obj.anim.fadeout:SetDuration(duration)
	obj.anim:Play()
end

function CC:StopFlash(obj)
	if obj.anim then
		obj.anim:Stop()
	end
end

local date = date;

function CC:GetFormattedTime(tag, timestamp)
       local hour, minutes;
       if (self.db.windows.localtime) then
               hour, minutes = tonumber(date("%H")), tonumber(date("%M"))
       else
               hour, minutes = GetGameTime()
       end

       local _tag = "AM"

       if (minutes < 10) then
               minutes = "0" .. tostring(minutes)
       end

       if (hour > 12) then
               _tag = "PM"

               if (self.db.windows.timeformat == "12Hour") then
                       hour = hour - 12
               end
       end

       if tag then
               if (self.db.windows.timeformat == "12Hour") then
                       return format("%s:%s %s", hour, minutes, _tag)
               else
                       return format("%s:%s ", hour, minutes)
               end
       elseif timestamp then
               return format("[%s:%s] ", hour, minutes)
       end
end
