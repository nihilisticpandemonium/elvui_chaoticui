local CUI, E, L, V, P, G = unpack(select(2, ...))
local CC = CUI:NewModule('ChaoticChat', 'AceEvent-3.0')
local CH = E:GetModule('Chat');
local LSM = LibStub("LibSharedMedia-3.0")

function CC:Initialize()
	if (not E.db.chaoticui.chaoticchat.general.enabled) then return end
	self.frame = CreateFrame("Frame", "ChaoticChat", E.UIParent);
	self.chats = {}

	self.maleClasses = {}
	self.femaleClasses = {}
	self.userCache = {}
	self.delayed = {}
	self.mobileStatus = {}

	local tMale = {}
	local tFemale = {}
	FillLocalizedClassList(tMale, false)
	FillLocalizedClassList(tFemale, true)

	for k, v in pairs(tMale) do
		self.maleClasses[v] = k;
	end

	for k, v in pairs(tFemale) do
		self.femaleClasses[v] = k;
	end

	CUI:RegisterDB(self, "chaoticchat")

	self:SetUpHooks()
	self:CreateDock();
	self:InitializeChatSystem();

	CC:RegisterEvent("CHAT_MSG_BN_WHISPER")
	CC:RegisterEvent("CHAT_MSG_BN_WHISPER_INFORM")
	CC:RegisterEvent("CHAT_MSG_WHISPER")
	CC:RegisterEvent("CHAT_MSG_WHISPER_INFORM")
	CC:RegisterEvent("CHAT_MSG_BN_INLINE_TOAST_ALERT")
	CC:RegisterEvent("GUILD_ROSTER_UPDATE")

	SetCVar("chatStyle", "im")
	SetCVar("whisperMode", "popout");
	
	for i = 1, 4 do -- Make sure whispers don't exist in chatframe 1-4
		local Frame = _G[format("ChatFrame%s", i)]
		ChatFrame_RemoveMessageGroup(Frame, "WHISPER")
		ChatFrame_RemoveMessageGroup(Frame, "BN_WHISPER")
	end
	
	self.myrealm = E.myrealm:gsub(' ','')

	GuildRoster();
end

CUI:RegisterModule(CC:GetName())