local CUI, E, L, V, P, G = unpack(select(2, ...)); --Inport: Engine, Locales, ProfileDB, GlobalDB
local addon, ns = ...
ns.oUF = ElvUF
local oUF = ns.oUF
local LSM = LibStub("LibSharedMedia-3.0");
local CT = LibStub("LibChaoticUITags-1.0");
local CWB = CUI:NewModule("CustomWatchBar", "AceTimer-3.0", "AceEvent-3.0")

local DB = E:GetModule('DataBars')

local floor = math.floor

--Replacement of text formats on 'player'frames.
local styles = {
	['CURRENT'] = '%s',
	['CURRENT_MAX'] = '%s - %s',
	['CURRENT_PERCENT'] =  '%s ( %s%% )',
	['CURRENT_MAX_PERCENT'] = '%s - %s ( %s%% )',
	['CURRENT_RESTED'] = '%s | R: %s',
	['CURRENT_MAX_RESTED'] = '%s - %s | R: %s',
	['CURRENT_PERCENT_RESTED'] =  '%s ( %s%% ) | R: %s',
	['CURRENT_MAX_PERCENT_RESTED'] = '%s - %s ( %s%% ) | R: %s',
	['TONEXT'] = '%s',
	['BUBBLES'] = '%s',
	['PERCENT'] = '%s%%',
	['RESTED'] = '%s'
}

function CWB:GetFormattedText(style, min, max, rested)
	if (not styles[style] or not min) then
		return;
	end

	if  max == 0 then max = 1 end

	local useStyle = styles[style]

	local percentValue
	if max ~= nil then
		percentValue = floor(min / max * 100)
	end

	if style == 'TONEXT' then
		local deficit = max - min
		if deficit <= 0 then
			return ''
		else
			return string.format(useStyle, deficit)
		end
	elseif style == 'CURRENT' or ((style == 'CURRENT_MAX' or style == 'CURRENT_MAX_PERCENT' or style == 'CURRENT_PERCENT') and min == max) then
			return string.format(styles['CURRENT'], min)
	elseif style == 'CURRENT_MAX' then
			return string.format(useStyle, min, max)
	elseif style == 'CURRENT_PERCENT' then
			return string.format(useStyle, min, percentValue)
	elseif style == 'CURRENT_MAX_PERCENT' then
			return string.format(useStyle, min, max, percentValue)
	elseif style == 'CURRENT_RESTED' or ((style == 'CURRENT_MAX_RESTED' or style == 'CURRENT_MAX_PERCENT_RESTED' or style == 'CURRENT_PERCENT_RESTED') and min == max) then
			return string.format(styles['CURRENT_RESTED'], min, rested)
	elseif style == 'CURRENT_MAX_RESTED' then
			return string.format(useStyle, min, max, rested)
	elseif style == 'CURRENT_PERCENT_RESTED' then
			return string.format(useStyle, min, percentValue, rested)
	elseif style == 'CURRENT_MAX_PERCENT_RESTED' then
			return string.format(useStyle, min, max, percentValue, rested)
	elseif style == "BUBBLES" then
		local bubbles = floor(20 * (max - min) / max)
		return string.format(useStyle, bubbles)
	elseif style == "RESTED" then
		if not rested then rested = 0 end
		return string.format(useStyle,rested)
	elseif style == "PERCENT" then
		return string.format(useStyle, percentValue);
	end
end

function CWB:RegisterXPTags()
	CT:RegisterTag('xp:current', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('CURRENT', min, max)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:max', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('CURRENT', max, max);
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:percent', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('PERCENT', min, max);
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:tonext', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('TONEXT', min, max)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:current-percent', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('CURRENT_PERCENT', min, max)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:current-max', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('CURRENT_MAX', min, max)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:current-max-percent', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('CURRENT_MAX_PERCENT', min, max)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:rested', function()
		local min, max = UnitXP('player'), UnitXPMax('player')
		local rested = GetXPExhaustion()

		return CWB:GetFormattedText('RESTED',min,max,rested)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:current-rested', function()
		local min, max = UnitXP('player'), UnitXPMax('player')
		local rested = GetXPExhaustion()

		if rested and rested > 0 then
			return CWB:GetFormattedText('CURRENT_RESTED', min, max, rested)
		else 
			return CWB:GetFormattedText('CURRENT', min, max)
		end
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:current-percent-rested', function()
		local min, max = UnitXP('player'), UnitXPMax('player')
		local rested = GetXPExhaustion()
		
		if rested and rested > 0 then
			return CWB:GetFormattedText('CURRENT_PERCENT_RESTED', min, max, rested)
		else 
			return CWB:GetFormattedText('CURRENT_PERCENT', min, max)
		end
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:current-max-rested', function()
		local min, max = UnitXP('player'), UnitXPMax('player')
		local rested = GetXPExhaustion()
		
		if rested and rested > 0 then
			return CWB:GetFormattedText('CURRENT_MAX_RESTED', min, max, rested)
		else 
			return CWB:GetFormattedText('CURRENT_MAX', min, max)
		end
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:current-max-percent-rested', function()
		local min, max = UnitXP('player'), UnitXPMax('player')
		local rested = GetXPExhaustion()
		
		if rested and rested > 0 then
			return CWB:GetFormattedText('CURRENT_MAX_PERCENT_RESTED', min, max, rested)
		else 
			return CWB:GetFormattedText('CURRENT_MAX_PERCENT', min, max)
		end
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:bars', function()
		local min, max = UnitXP('player'), UnitXPMax('player')

		return CWB:GetFormattedText('BUBBLES', min, max)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:level', function()
		local level = UnitLevel('player')

		return CWB:GetFormattedText('CURRENT', level)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:nextlevel', function()
		local level = UnitLevel('player')

		return CWB:GetFormattedText('CURRENT', level + 1)
	end, 'PLAYER_XP_UPDATE');

	CT:RegisterTag('xp:quest', function()
		CWB.currQXP = CUI:CalculateQuestLogXP();
		return CWB:GetFormattedText('CURRENT', CWB.currQXP or 0);
	end, 'QUEST_ITEM_UPDATE UNIT_QUEST_LOG_CHANGED QUEST_LOG_UPDATE PLAYER_XP_UPDATE UNIT_PORTRAIT_UPDATE PLAYER_ENTERING_WORLD');

	CT:RegisterTag('xp:levelup?', function()
		CWB.currQXP = CUI:CalculateQuestLogXP();

		local min, max = UnitXP('player'), UnitXPMax('player')
		local cq = CWB.currQXP + min
		if (cq >= max) then
			return '[Level Up!]';
		else
			return '';
		end
	end, 'QUEST_ITEM_UPDATE UNIT_QUEST_LOG_CHANGED QUEST_LOG_UPDATE PLAYER_XP_UPDATE UNIT_PORTRAIT_UPDATE PLAYER_ENTERING_WORLD');

	CT:RegisterTag('xp:quest-percent', function()
		CWB.currQXP = CUI:CalculateQuestLogXP();
		local max = UnitXPMax('player');
		return CWB:GetFormattedText('PERCENT', CWB.currQXP or 0, max);
	end, 'QUEST_ITEM_UPDATE UNIT_QUEST_LOG_CHANGED QUEST_LOG_UPDATE PLAYER_XP_UPDATE UNIT_PORTRAIT_UPDATE PLAYER_ENTERING_WORLD');
end

function CWB:RegisterRepTags()
	local function GetParagonInfo(factionID)
		if (C_Reputation.IsFactionParagon(factionID)) then
			local currentValue, threshold, _, hasRewardPending = C_Reputation.GetFactionParagonInfo(factionID)
			local min, max = 0, threshold
			value = currentValue % threshold
			if hasRewardPending then 
				value = value + threshold
			end
			return true, min, max, value;
		end

		return false;
	end

	CT:RegisterTag('rep:name', function()
		local name, reaction, min, max, value = GetWatchedFactionInfo()

		if not name then return '' end

		return name
	end, 'UPDATE_FACTION');

	CT:RegisterTag('rep:standing', function()
		local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()
		local friendID, _, _, _, _, _, friendTextLevel = GetFriendshipReputation(factionID);
		if not name then return '' end

		if (not friendID and GetParagonInfo(factionID)) then
			return "Paragon"
		end

		return friendID and friendTextLevel or _G['FACTION_STANDING_LABEL'..reaction]
	end, 'UPDATE_FACTION');

	CT:RegisterTag('rep:current', function()
		local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()
		local numFactions = GetNumFactions();

		if not name then return '' end

		local isParagon, pmin, pmax, pvalue = GetParagonInfo(factionID);
		if (isParagon) then
			min, max, value = pmin, pmax, pvalue;
		end

		return CWB:GetFormattedText('CURRENT', value - min, max - min)
	end, 'UPDATE_FACTION');

	CT:RegisterTag('rep:tonext', function()
		local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()
		local numFactions = GetNumFactions();

		if not name then return '' end

		local isParagon, pmin, pmax, pvalue = GetParagonInfo(factionID);
		if (isParagon) then
			min, max, value = pmin, pmax, pvalue;
		end

		return CWB:GetFormattedText('TONEXT', value - min, max - min)
	end, 'UPDATE_FACTION');

	CT:RegisterTag('rep:current-percent', function()
		local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()
		local numFactions = GetNumFactions();
		
		if not name then return '' end

		local isParagon, pmin, pmax, pvalue = GetParagonInfo(factionID);
		if (isParagon) then
			min, max, value = pmin, pmax, pvalue;
		end

		return CWB:GetFormattedText('CURRENT_PERCENT', value - min, max - min)
	end, 'UPDATE_FACTION');

	CT:RegisterTag('rep:current-max', function()
		local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()
		local numFactions = GetNumFactions();
		
		if not name then return '' end

		local isParagon, pmin, pmax, pvalue = GetParagonInfo(factionID);
		if (isParagon) then
			min, max, value = pmin, pmax, pvalue;
		end

		return CWB:GetFormattedText('CURRENT_MAX', value - min, max - min)
	end, 'UPDATE_FACTION');

	CT:RegisterTag('rep:current-max-percent', function()
		local name, reaction, min, max, value, factionID = GetWatchedFactionInfo()
		local numFactions = GetNumFactions();

		if not name then return '' end

		local isParagon, pmin, pmax, pvalue = GetParagonInfo(factionID);
		if (isParagon) then
			min, max, value = pmin, pmax, pvalue;
		end

		return CWB:GetFormattedText('CURRENT_MAX_PERCENT', value - min, max - min)
	end, 'UPDATE_FACTION');
end

function CWB:RegisterAzeriteTags()
	CT:RegisterTag('azerite:name', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local azeriteItem = Item:CreateFromItemLocation(azeriteItemLocation); 
	
		return azeriteItem:GetItemName();
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:current', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local xp, xpForNextPoint = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);
	
		return CWB:GetFormattedText('CURRENT', xp, xpForNextPoint)
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:max', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local xp, xpForNextPoint = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);
		return CWB:GetFormattedText('CURRENT', xpForNextPoint, xpForNextPoint);
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:percent', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local xp, xpForNextPoint = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);
		return CWB:GetFormattedText('PERCENT', xp, xpForNextPoint);
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:tonext', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local xp, xpForNextPoint = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);
		return CWB:GetFormattedText('TONEXT', xp, xpForNextPoint)
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:current-percent', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local xp, xpForNextPoint = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);
		return CWB:GetFormattedText('CURRENT_PERCENT', xp, xpForNextPoint)
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:current-max', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local xp, xpForNextPoint = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);
		return CWB:GetFormattedText('CURRENT_MAX', xp, xpForNextPoint)
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:current-max-percent', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local xp, xpForNextPoint = C_AzeriteItem.GetAzeriteItemXPInfo(azeriteItemLocation);
		return CWB:GetFormattedText('CURRENT_MAX_PERCENT', xp, xpForNextPoint)
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');

	CT:RegisterTag('azerite:level', function()
		local azeriteItemLocation = C_AzeriteItem.FindActiveAzeriteItem(); 
	
		if (not azeriteItemLocation) then 
			return; 
		end
	
		local currentLevel = C_AzeriteItem.GetPowerLevel(azeriteItemLocation);

		return CWB:GetFormattedText('CURRENT', currentLevel)
	end, 'AZERITE_ITEM_EXPERIENCE_CHANGED UNIT_INVENTORY_CHANGED');
end

function CWB:RegisterHonorTags()
	CT:RegisterTag('honor:current', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('CURRENT', min, max)
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:max', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('CURRENT', max, max);
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:percent', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('PERCENT', min, max);
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:tonext', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('TONEXT', min, max)
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:current-percent', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('CURRENT_PERCENT', min, max)
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:current-max', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('CURRENT_MAX', min, max)
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:current-max-percent', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('CURRENT_MAX_PERCENT', min, max)
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:bars', function()
		local min, max = UnitHonor('player'), UnitHonorMax('player')

		return CWB:GetFormattedText('BUBBLES', min, max)
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:level', function()
		local level = UnitHonorLevel('player')

		return CWB:GetFormattedText('CURRENT', level)
	end, 'HONOR_XP_UPDATE');

	CT:RegisterTag('honor:nextlevel', function()
		local level = UnitHonorLevel('player')

		return CWB:GetFormattedText('CURRENT', level + 1)
	end, 'HONOR_XP_UPDATE');
end

function CWB:RegisterDataBar(key, frame)
	CT:RegisterFontString(key, frame.text)
end

function CWB:UpdateTag(frameKey)
	local tagstr
	if frameKey == 'xp' then
		tagstr = E.db.databars.experience.tag
	elseif frameKey == 'rep' then
		tagstr = E.db.databars.reputation.tag
	elseif frameKey == 'azerite' then
		tagstr = E.db.databars.azerite.tag
	elseif frameKey == 'honor' then
		tagstr = E.db.databars.honor.tag
	end
	CT:Tag(frameKey, tagstr)
end

function CWB:Initialize()
	E.db.databars.experience.textFormat = "NONE"
	E.db.databars.reputation.textFormat = "NONE"
	E.db.databars.azerite.textFormat = "NONE"
	E.db.databars.honor.textFormat = "NONE"

	CWB.currQXP = CUI:CalculateQuestLogXP();

	CT:RegisterTag('name', function() return UnitName('player') end);

	self:RegisterXPTags();
	self:RegisterRepTags();
	self:RegisterAzeriteTags();
	self:RegisterHonorTags();

	self:RegisterDataBar('xp', ElvUI_ExperienceBar);
	self:RegisterDataBar('rep', ElvUI_ReputationBar);
	self:RegisterDataBar('azerite', ElvUI_AzeriteBar);
	self:RegisterDataBar('honor', ElvUI_HonorBar);

	self:UpdateTag('xp')
	self:UpdateTag('rep')
	self:UpdateTag('azerite')
	self:UpdateTag('honor')
end

CUI:RegisterModule(CWB:GetName());