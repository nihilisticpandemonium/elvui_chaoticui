local CUI, E, L, V, P, G = unpack(select(2, ...))
local BS = CUI:NewModule('ButtonStyle');
local AB = E:GetModule('ActionBars');
local LSM = LibStub("LibSharedMedia-3.0");


function BS:StyleButton(button, noBackdrop, useMasque)
	if (useMasque) then
		return;
	end

	if E.db.chaoticui.buttonStyle.enabled then
		if not button.overlayGloss then
			local overlay = button:CreateTexture();
			overlay:SetBlendMode("BLEND");
			overlay:SetInside();
			overlay:SetDrawLayer("OVERLAY");
			button.overlayGloss = overlay;
		end

		button.overlayGloss:SetTexture(LSM:Fetch('statusbar',E.db.chaoticui.buttonStyle.texture));
		button.overlayGloss:SetAlpha(E.db.chaoticui.buttonStyle.alpha);
		button.overlayGloss:SetVertexColor(0.2, 0.2, 0.2);
		button.overlayGloss:Show();

		if (E.db.chaoticui.buttonStyle.invertedShadows) then
			button:CreateInvertedShadow();
			button.shadow:Show();
		end
	else
		if (button.overlayGloss) then
			button.overlayGloss:Hide();
		end
	end
	BS.styledButtons[button] = true;
end

function BS:UpdateButtons()
	for button, _ in pairs(BS.styledButtons) do
		BS:StyleButton(button);
	end
end

BS.styledButtons = {};
function BS:Initialize()
	hooksecurefunc(AB, 'StyleButton', BS.StyleButton);

	AB:UpdateButtonSettings();
end

CUI:RegisterModule(BS:GetName());