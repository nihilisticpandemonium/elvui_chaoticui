local CUI, E, L, V, P, G = unpack(select(2, ...))
local HDT = CUI:NewModule('HyperDT')
local DT = E:GetModule('DataTexts')


E.HyperDT = HDT

local datatexts = {}
local datatextMap = {}
local menu = {}
local chosenDT = nil
local menuFrame = CreateFrame("Frame", "HyperDTMenuFrame", E.UIParent)
HyperDTMenuFrame.displayMode = "MENU"
HyperDTMenuFrame.initialize = function(self, level) HDT.CreateMenu(self, level); end

function HDT:EnableHyperModeForDataText(dt)
	if not dt.overlay then return end
	dt.oldClick = dt:GetScript('OnClick')
	dt.oldEnter = dt:GetScript('OnEnter')
	dt.oldLeave = dt:GetScript('OnLeave')
	dt:SetScript('OnEnter',nil)
	dt:SetScript('OnLeave',nil)
	dt:SetScript('OnClick',function(self,button)
		if button == "RightButton" then
			chosenDT = dt
			ToggleDropDownMenu(1, nil, menuFrame, self, 0, -2);
		end
	end)
end

function HDT:DisableHyperModeForDataText(dt)
	if not dt.overlay then return end
	dt:SetScript('OnClick',dt.oldClick)
	dt:SetScript('OnEnter',dt.oldEnter)
	dt:SetScript('OnLeave',dt.oldLeave)
	dt.oldClick = nil
	dt.oldEnter = nil
	dt.oldLeave = nil
end

function HDT:EnableHyperMode()
	for datatext, _ in pairs(datatexts) do
		self:EnableHyperModeForDataText(datatext)
		datatext.overlay:Show()
	end
end

function HDT:DisableHyperMode()
	for datatext, _ in pairs(datatexts) do
		self:DisableHyperModeForDataText(datatext)
		datatext.overlay:Hide()
	end
end

local startChar = {
	["AB"] = {
	},
	["CD"] = {
	},
	["EF"] = {
	},
	["GH"] = {
	},
	["IJ"] = {
	},
	["KL"] = {
	},
	["MN"] = {
	},
	["OP"] = {
	},
	["QR"] = {
	},
	["ST"] = {
	},
	["UV"] = {
	},
	["WX"] = {
	},
	["YZ"] = {
	}
}

local menu = {};

function HDT:CreateMenu(level)
	menu = wipe(menu)
	
	local numDTs = 0;
	local nameMap = {};
	for name, _ in pairs(DT.RegisteredDataTexts) do
		numDTs = numDTs + 1;
		nameMap[numDTs] = name;
	end

	sort(nameMap, function(a, b) return a < b end);
	
	if (numDTs > 0) then
		numDTs = numDTs + 1;
		nameMap[numDTs] = NONE;
	end

	if numDTs == 0 then
		return
	elseif numDTs <= 20 then
		for i = 1, numDTs do
			local name = nameMap[i];
			local active = chosenDT.menuGetFunc() == name;
			menu.hasArrow = false
			menu.notCheckable = true
			menu.text = name
			menu.colorCode = active and hexColor or "|cffffffff"
			menu.func = function() chosenDT.menuSetFunc(name) end;
			UIDropDownMenu_AddButton(menu)
		end
	else
		level = level or 1
		nameMap[numDTs] = nil;
		numDTs = numDTs - 1;

		if level == 1 then
			for key, value in CUI_PairsByKeys(startChar) do
				menu.text = key
				menu.notCheckable = true
				menu.hasArrow = true
				menu.value = {["Level1_Key"] = key}
				UIDropDownMenu_AddButton(menu, level)
			end
			local active = chosenDT.menuGetFunc() == NONE;
			menu.hasArrow = false
			menu.notCheckable = true
			menu.text = NONE
			menu.colorCode = active == 1 and hexColor or "|cffffffff"
			menu.func = function() chosenDT.menuSetFunc(NONE) end;
			UIDropDownMenu_AddButton(menu)
		elseif level == 2 then
			local Level1_Key = UIDROPDOWNMENU_MENU_VALUE["Level1_Key"]
			for i = 1, numDTs do
				local name = nameMap[i];
				local active = chosenDT.menuGetFunc() == name;
				local firstChar = name:sub(1, 1):upper()
				menu.text = name
				menu.colorCode = active and hexColor or "|cffffffff"
				menu.func = function() chosenDT.menuSetFunc(name) end;
				menu.hasArrow = false
				menu.notCheckable = true
				
				if firstChar >= "A" and firstChar <= "B" and Level1_Key == "AB" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "C" and firstChar <= "D" and Level1_Key == "CD" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "E" and firstChar <= "F" and Level1_Key == "EF" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "G" and firstChar <= "H" and Level1_Key == "GH" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "I" and firstChar <= "J" and Level1_Key == "IJ" then
					UIDropDownMenu_AddButton(menu, level)
				end
			
				if firstChar >= "K" and firstChar <= "L" and Level1_Key == "KL" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "M" and firstChar <= "N" and Level1_Key == "MN" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "O" and firstChar <= "P" and Level1_Key == "OP" then
					UIDropDownMenu_AddButton(menu, level)
				end
			
				if firstChar >= "Q" and firstChar <= "R" and Level1_Key == "QR" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "S" and firstChar <= "T" and Level1_Key == "ST" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "U" and firstChar <= "V" and Level1_Key == "UV" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "W" and firstChar <= "X" and Level1_Key == "WX" then
					UIDropDownMenu_AddButton(menu, level)
				end
				
				if firstChar >= "Y" and firstChar <= "Z" and Level1_Key == "YZ" then
					UIDropDownMenu_AddButton(menu, level)
				end
			end
		end
	end
end

function HDT:Initialize()
	if (IsAddOnLoaded("ElvUI_SLE") and not ElvUI_SLE[1].initialized) then
		return;
	end

	local ES = CUI:GetModule("EnhancedShadows");

	menuFrame:SetTemplate("Transparent", true)
	
	for panelName, panel in pairs(DT.RegisteredPanels) do
		--Restore Panels
		for i=1, panel.numPoints do
			local pointIndex = DT.PointLocation[i]
			panel.dataPanels[pointIndex].overlay = panel.dataPanels[pointIndex]:CreateTexture(nil, 'OVERLAY')
			panel.dataPanels[pointIndex].overlay:SetColorTexture(0,1,0,.3)
			panel.dataPanels[pointIndex].overlay:SetAllPoints()
			panel.dataPanels[pointIndex].overlay:Hide()

			panel.dataPanels[pointIndex].menuSetFunc = function(value)
				if string.find(datatextMap[panel.dataPanels[pointIndex]],"::") then
					local option,pointIndex = string.match(datatextMap[panel.dataPanels[pointIndex]],"([%w_]+)::([%w_]+)")
					DT.db.panels[option][pointIndex] = value
				else
					local option = datatextMap[panel.dataPanels[pointIndex]]
					DT.db.panels[option] = value
				end
				DT:LoadDataTexts(); 
				self:EnableHyperMode() 
			end
			panel.dataPanels[pointIndex].menuGetFunc = function(value)
				if string.find(datatextMap[panel.dataPanels[pointIndex]],"::") then
					local option,pointIndex = string.match(datatextMap[panel.dataPanels[pointIndex]],"([%w_]+)::([%w_]+)")
					return DT.db.panels[option][pointIndex]
				else
					local option = datatextMap[panel.dataPanels[pointIndex]]
					return DT.db.panels[option]
				end
			end
			for option, value in pairs(DT.db.panels) do
				if value and type(value) == 'table' then
					if option == panelName and DT.db.panels[option][pointIndex] then
						datatextMap[panel.dataPanels[pointIndex]] = option.."::"..pointIndex
					end
				elseif value and type(value) == 'string' then
					if option == panelName then
						datatextMap[panel.dataPanels[pointIndex]] = option
					end
				end
			end
			panel.dataPanels[pointIndex]:CreateShadow();
			ES:RegisterShadow(panel.dataPanels[pointIndex].shadow);
			
			datatexts[panel.dataPanels[pointIndex]] = true
		end
	end

	ES:UpdateShadows();
	
	SLASH_HYPERDT1 = '/hdt';
	local hdt_enabled = false
	function SlashCmdList.HYPERDT(msg, editbox)
		if hdt_enabled then
			hdt_enabled = false
			HDT:DisableHyperMode()
		else
			hdt_enabled = true
			HDT:EnableHyperMode()
		end
	end
end

CUI:RegisterModule(HDT:GetName())		