local CUI, E, L, V, P, G = unpack(select(2, ...));
--Version datatext. Only in Russian for now.
local DT = E:GetModule('DataTexts')

local SLE = ElvUI_SLE and ElvUI_SLE[1] or nil

local displayString = '';
local lastPanel;
local self = lastPanel
local join = string.join
local AddLine = AddLine
local AddDoubleLine =AddDoubleLine
local Eversion = E.version
local format = format

local function OnEvent(self, event, ...)
	if (SLE) then
		self.text:SetFormattedText(displayString, CUI.version, Eversion, SLE.version);
	else
		self.text:SetFormattedText(displayString, CUI.version, Eversion);
	end
end

local ACD;
local function Click()
	E:ToggleConfig();

	ACD = ACD or LibStub("AceConfigDialog-3.0-ElvUI", true);
	if (not ACD) then
		if (not IsAddOnLoaded("ElvUI_Config")) then
			LoadAddOn("ElvUI_Config");
		end
		ACD = LibStub("AceConfigDialog-3.0-ElvUI", true);
	end
	if (ACD) then
		ACD:SelectGroup("ElvUI", "ChaoticUI")
	end
end

local function OnEnter(self)
	DT:SetupTooltip(self)

	DT.tooltip:AddDoubleLine("ElvUI "..GAME_VERSION_LABEL..format(": |cff99ff33%s|r", Eversion))
	DT.tooltip:AddLine(" ");

	if (SLE) then
		DT.tooltip:AddLine(L["SLE_AUTHOR_INFO"]..". "..GAME_VERSION_LABEL..format(": |cff99ff33%s|r", SLE.version))
		DT.tooltip:AddLine(" ")
		DT.tooltip:AddLine(L['SLE_CONTACTS'])
		DT.tooltip:AddLine(" ");
	end

	DT.tooltip:AddLine(L["CHAOTICUI_AUTHOR_INFO"]..". "..GAME_VERSION_LABEL..format(": |cff99ff33%s|r", CUI.version))
	DT.tooltip:AddLine(" ")
	DT.tooltip:AddLine(L['CHAOTICUI_CONTACTS'])
	
	DT.tooltip:Show()
end

local function ValueColorUpdate(hex, r, g, b)
	local name = "|cffff2020ChaoticUI|r"..(": |cff99ff33%s|r"):format(CUI.version).." (|cfffe7b2cElvUI|r"..format(": |cff99ff33%s|r",E.version);
	if (IsAddOnLoaded("ElvUI_SLE")) then
		name = name .. ", |cff9482c9Shadow & Light|r"..format(": |cff99ff33%s|r",ElvUI_SLE[1].version);
	end
	name = name .. ")";
	displayString = name;

	if lastPanel ~= nil then
		OnEvent(lastPanel)
	end
end
E['valueColorUpdateFuncs'][ValueColorUpdate] = true

DT:RegisterDatatext("ChaoticUI Version", {'PLAYER_ENTERING_WORLD'}, OnEvent, Update, Click, OnEnter)