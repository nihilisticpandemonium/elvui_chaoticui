-------------------------------------------------------------------------------
-- ElvUI Companions Datatext By Lockslap
-------------------------------------------------------------------------------
local CUI, E, L, V, P, G = unpack(select(2, ...))
local DT = E:GetModule("DataTexts")
local F = CreateFrame("frame")

local wipe = table.wipe
local tinsert = table.insert
local sort = table.sort
local format = string.format
local join = string.join

local menu = {}

local displayString = ""
local lastPanel
local hexColor = "|cff00ff96"

local db = {}

function GetCurrentMount()
	local numMounts = C_MountJournal.GetNumDisplayedMounts();
	if numMounts == 0 then return false, false end
	for i = 1, numMounts do
		local name, spellID, icon, isActive, isUsable, sourceType, isFavorite, isFactionSpecific, faction, shouldHideOnChar, isCollected, mountID = C_MountJournal.GetDisplayedMountInfo(i);
		if active then
			return i, name
		end
	end
	return false, false
end

local function UpdateDisplay(self, ...)	
	local curMount, name = GetCurrentMount()
	if curMount and name then
		self.text:SetFormattedText(displayString, name)
		db.id = curMount
		db.text = name
	else
		self.text:SetText(("%s"):format(L["Mounts"]))
	end
end

local function ModifiedClick(button, id)
	local specID, specname = GetSpecializationInfo(GetSpecialization());
	if (not db.specFavs[specID]) then
		db.specFavs[specID] = {}
	end
	local name, _, _, _, _ = C_MountJournal.GetMountInfoByID(id)
	if not IsShiftKeyDown() and not IsControlKeyDown() and not IsAltKeyDown() then
		C_MountJournal.SummonByID(id)
	elseif IsShiftKeyDown() and not IsControlKeyDown() and not IsAltKeyDown() then
		db.specFavs[specID].favFlyer = id
		DEFAULT_CHAT_FRAME:AddMessage((L["%sMounts:|r %s added as flying favorite. (%s)"]):format(hexColor, name, specname), 1, 1, 1)
	elseif not IsShiftKeyDown() and IsControlKeyDown() and not IsAltKeyDown() then
		db.specFavs[specID].favGround = id
		DEFAULT_CHAT_FRAME:AddMessage((L["%sMounts:|r %s added as ground favorite. (%s)"]):format(hexColor, name, specname), 1, 1, 1)
	elseif not IsShiftKeyDown() and not IsControlKeyDown() and IsAltKeyDown() then
		db.favAlt = id
		DEFAULT_CHAT_FRAME:AddMessage((L["%sMounts:|r %s added as alternate favorite."]):format(hexColor, name), 1, 1, 1)
	else
		MountJournal_Pickup(id)
	end
end

local function ClearFavorites(self)
	db.specFavs = {};
	db.favAlt = nil;
	DEFAULT_CHAT_FRAME:AddMessage((L["%sMounts:|r Favorites Cleared"]):format(hexColor))
end

local waterStriders = {
	449, -- Azure Water Strider
	488, -- Crimson Water Strider
}

local specialMounts = CopyTable(waterStriders);
tinsert(specialMounts, 522) -- Sky Golem;

function SummonFavoriteMount()
	local specID = GetSpecializationInfo(GetSpecialization());

	if (IsFlyableArea()) and db.specFavs[specID] and db.specFavs[specID].favFlyer then
		C_MountJournal.SummonByID(db.specFavs[specID].favFlyer);
	elseif db.specFavs[specID] and db.specFavs[specID].favGround then
		C_MountJournal.SummonByID(db.specFavs[specID].favGround);
	end
end

function SummonRandomStrider()
	local valid = {};
	for i, id in ipairs(waterStriders) do
		if (select(11, C_MountJournal.GetMountInfoByID(id))) then
			tinsert(valid, id);
		end
	end

	if (#valid == 0) then
		return
	elseif (#valid == 1) then
		C_MountJournal.SummonByID(valid[1])
	else
		C_MountJournal.SummonByID(valid[math.random(1, #valid)]);
	end
end

local function SummonSkyGolem()
	if (select(11, C_MountJournal.GetMountInfoByID(522))) then
		C_MountJournal.SummonByID(522);
	end
end

local function AddSpecialMounts(self, level)
	for i, id in ipairs(specialMounts) do
		local name, _, icon, active, _, _, _, _, _, _, isCollected, _ = C_MountJournal.GetMountInfoByID(id);
		if (isCollected) then
			menu.text = name
			menu.icon = icon
			menu.colorCode = active == 1 and hexColor or "|cffffffff"
			menu.func = ModifiedClick
			menu.arg1 =  id
			menu.hasArrow = false
			menu.notCheckable = true
			UIDropDownMenu_AddButton(menu, level)
		end
	end
end

local function AddFavorites(self, level)
	local specID = GetSpecializationInfo(GetSpecialization());
	if (not db.specFavs[specID]) then
		return;
	end

	if db.specFavs[specID].favFlyer ~= nil then
		local name, _, icon, active, _ = C_MountJournal.GetMountInfoByID(db.specFavs[specID].favFlyer)
		menu.text = ("Flying: %s"):format(name)
		menu.icon = icon
		menu.colorCode = active == 1 and hexColor or "|cffffffff"
		menu.func = ModifiedClick
		menu.arg1 =  db.specFavs[specID].favFlyer
		menu.hasArrow = false
		menu.notCheckable = true
		UIDropDownMenu_AddButton(menu, level)
	end

	-- favorite two
	if db.specFavs[specID].favGround ~= nil then
		local name, _, icon, active, _ = C_MountJournal.GetMountInfoByID(db.specFavs[specID].favGround)
		menu.text = ("Ground: %s"):format(name)
		menu.icon = icon
		menu.colorCode = active == 1 and hexColor or "|cffffffff"
		menu.func = ModifiedClick
		menu.arg1 = db.specFavs[specID].favGround
		menu.hasArrow = false
		menu.notCheckable = true
		UIDropDownMenu_AddButton(menu, level)
	end

	-- favorite three
	if db.favAlt ~= nil then
		local name, _, icon, active, _ = C_MountJournal.GetMountInfoByID(db.favAlt)
		menu.text = ("Alternate: %s"):format(name)
		menu.icon = icon
		menu.colorCode = active == 1 and hexColor or "|cffffffff"
		menu.func = ModifiedClick
		menu.arg1 = db.favAlt
		menu.hasArrow = false
		menu.notCheckable = true
		UIDropDownMenu_AddButton(menu, level)
	end

	AddSpecialMounts(self, level)
end

local function CreateMenu(self, level)
	local numMounts = C_MountJournal.GetNumMounts();
	menu = wipe(menu)

	if numMounts == 0 then
		return
	elseif numMounts <= 20 then
		for i = 1, numMounts do
			local name, _, icon, active, _, _, _, _, _, _, isCollected, id = C_MountJournal.GetDisplayedMountInfo(i)
			if (isCollected) then
				menu.hasArrow = false
				menu.notCheckable = true
				menu.text = name
				menu.icon = icon
				menu.colorCode = active == 1 and hexColor or "|cffffffff"
				menu.func = ModifiedClick
				menu.arg1 = id
				UIDropDownMenu_AddButton(menu)
			end
		end
		AddFavorites(self, level);
	else
		local function CollectMountsByFirstChar(firstChar)
			local mounts = {};
			for i = 1, numMounts do
				local name, _, icon, active, _, _, _, _, _, _, isCollected = C_MountJournal.GetDisplayedMountInfo(i);
				if name and isCollected and name:sub(1, 1):upper() == firstChar then
					tinsert(mounts, i);
				end
			end
			return mounts;
		end

		local depthByKey = {};
		local countByKey = {};
		local key = "A";
		local alphabet ='ABCDEFGHIJKLMNOPQRSTUVWXYZA';
		repeat
			local count = #(CollectMountsByFirstChar(key));
			depthByKey[key] = (count / 16) + 1;
			countByKey[key] = count;
			key = alphabet:match(key..'(.)');
		until key == 'A';

		level = level or 1
		
		if level == 1 then
			local key = "A";
			local alphabet ='ABCDEFGHIJKLMNOPQRSTUVWXYZA';
			repeat
				if (countByKey[key] > 0) then
					for i = 1, depthByKey[key] do
						menu.text = key
						menu.notCheckable = true
						menu.hasArrow = true
						menu.value = {["Level1_Key"] = key, ["Level1_Depth"] = i};
						UIDropDownMenu_AddButton(menu, level)
					end
				end
				key = alphabet:match(key..'(.)')
			until key == 'A';
			AddFavorites(self, level);
		elseif level == 2 then
			local Level1_Key = UIDROPDOWNMENU_MENU_VALUE["Level1_Key"]
			local Level1_Depth = UIDROPDOWNMENU_MENU_VALUE["Level1_Depth"];
			local mounts = CollectMountsByFirstChar(Level1_Key);
			local depthMod = 1 + ((Level1_Depth - 1) * 16);
			for k = depthMod, depthMod + 15 do
				if (mounts[k]) then
					local name, _, icon, active, _, _, _, _, _, _, isCollected, id = C_MountJournal.GetDisplayedMountInfo(mounts[k]);
					if (name) then
						if (isCollected) then
							menu.text = name
							menu.icon = icon
							menu.colorCode = active == 1 and hexColor or "|cffffffff"
							menu.func = ModifiedClick
							menu.arg1 = id
							menu.hasArrow = false
							menu.notCheckable = true

							UIDropDownMenu_AddButton(menu, level)
						end
					end
				else
					break;
				end
			end
		end
	end
end

local interval = 1
local function OnUpdate(self, elapsed)
	--if not self.lastUpdate then self.lastUpdate = 0 end
	self.lastUpdate = self.lastUpdate and self.lastUpdate + elapsed or 0
	if self.lastUpdate >= interval then
		UpdateDisplay(self)
		self.lastUpdate = 0
	end
end

local function OnClick(self, button)
	DT.tooltip:Hide()
	if IsAltKeyDown() and IsShiftKeyDown() then
		SummonRandomStrider();
	elseif IsAltKeyDown() and IsControlKeyDown() then
		SummonSkyGolem();
	elseif IsAltKeyDown() then
		if (db.favAlt) then
			C_MountJournal.SummonByID(db.favAlt);
		end
	elseif button == "RightButton" then
		ToggleDropDownMenu(1, nil, F, self, 0, 0)
	elseif button == "LeftButton" then
		if IsShiftKeyDown() then
			SummonFavoriteMount();
		else
			ToggleCollectionsJournal(1);
		end
	end
end

local function OnEnter(self)
	DT:SetupTooltip(self)
	DT.tooltip:AddLine((L["%sElvUI|r ChaoticUI - Mounts Datatext"]):format(hexColor), 1, 1, 1)
	DT.tooltip:AddLine(("     %s"):format(L["<Left Click> to open Pet Journal."]))
	DT.tooltip:AddLine(("     %s"):format(L["<Right Click> to open mount list."]))
	DT.tooltip:AddLine(("     %s"):format(L["<Shift + Left Click> to summon your favorite flying or ground mount based on your ability in the current zone."]))
	DT.tooltip:AddLine(("     %s"):format(L["<Alt + Click> to summon your favorite alternate mount."]))
	DT.tooltip:AddLine(" ")
	DT.tooltip:AddLine(("     %s"):format(L["<Left Click> a mount to summon it."]))
	DT.tooltip:AddLine(("     %s"):format(L["<Right Click> a mount to pick it up."]))
	DT.tooltip:AddLine(("     %s"):format(L["<Shift + Click> a mount to set as your favorite flying mount."]))
	DT.tooltip:AddLine(("     %s"):format(L["<Ctrl + Click> a mount to set as your favorite ground mount."]))
	DT.tooltip:AddLine(("     %s"):format(L["<Alt + Click> a mount to set as your favorite alternate mount."]))
	if C_MountJournal.GetNumMounts() == 0 then
		DT.tooltip:AddLine("|cffff0000You have no mounts!|r")
	else
		DT.tooltip:AddLine(("|cff00ff00You have %d mounts.|r"):format(C_MountJournal.GetNumMounts()))
	end
	DT.tooltip:Show()
end

local function MigrateMounts()
	if (not db.favAlt) then
		if (GetNumSpecializations() == 0) then
			C_Timer.After(1, function() MigrateMounts() end)
			return
		end
		for i = 1, GetNumSpecializations() do
			local specID = GetSpecializationInfo(i);
			db.specFavs[specID] = {};
			db.specFavs[specID].favFlyer = db.favOne;
			db.specFavs[specID].favGround = db.favTwo;
		end
		db.favAlt = db.favThree;
		db.favOne = nil;
		db.favTwo = nil;
		db.favThree = nil;
	end
end

local function ValueColorUpdate(hex, r, g, b)
	displayString = join("", hex, "%s|r")
	hexColor = hex
	if lastPanel ~= nil then OnEvent(lastPanel) end
end
E["valueColorUpdateFuncs"][ValueColorUpdate] = true

SLASH_MCF1 = "/mcf";
SlashCmdList.MCF = ClearFavorites;

F:RegisterEvent("PLAYER_ENTERING_WORLD")
F:SetScript("OnEvent", function(self, event, ...)
	db = E.private.chaoticui.mounts
	if (not db.favAlt) then
		MigrateMounts();
	end
	self.initialize = CreateMenu
	self.displayMode = "MENU"
	self:UnregisterEvent("PLAYER_ENTERING_WORLD")
end)

DT:RegisterDatatext(L["Mounts"], {"PLAYER_ENTERING_WORLD", "COMPANION_UPDATE", "PET_JOURNAL_LIST_UPDATE"}, UpdateDisplay, OnUpdate, OnClick, OnEnter)
