local CUI, E, L, V, P, G = unpack(select(2, ...));
local RCD = CUI:NewModule('RaidCDs', 'AceEvent-3.0');
local CandyBar = LibStub('LibCandyBar-3.0');
local LSM = LibStub("LibSharedMedia-3.0");
local GI = LibStub("LibGroupInSpecT-1.1");

function RCD:GetModifiedCooldown(node)
    if (type(node.cd) == "number") then
        return node.cd;
    end

    local guid = node.guid;
    local unitInfo = self.cached_players[guid].unitInfo;
    local cd = node.cd.base;
    for mkey, mtbl in pairs(node.cd) do
        if (mkey ~= "base") then
            local mtype = RCD.modifierTypes.Talent;
            if mkey == "pvp_talent" then
                mtype = RCD.modifierTypes.PvPTalent;
            elseif mkey == "legendary" then
                mtype = RCD.modifierTypes.Legendary;
            end
            local modifier, isPercent = self:EvaluateModifier(unitInfo, mtbl, mtype);
            if (isPercent) then
                cd = cd * (1 - (modifier / 100));
            else
                cd = cd - modifier;
            end
        end
    end
    return cd;
end

function RCD:BuildList()
    wipe(self.cd_list);
    for category, _ in pairs(self.categories) do
        self.cd_list[category] = {};
        for guid, _ in pairs(self.cached_players) do
            local knownSpells = self:GetSpellsForCategory(category, guid);
            for spell_id, tbl in pairs(knownSpells) do
                if (not self.cd_list[category][spell_id]) then
                    self.cd_list[category][spell_id] = {};
                end
                self.cd_list[category][spell_id][guid] = self:GetModifiedCooldown(tbl);
            end
        end
    end
end

local barCache = {};

function RCD:UpdateCDs()
    if (not self.initialized) then return end

    for category, _ in pairs(self.categories) do
        local bars = self.bars[category];
        local changed;
        repeat
            changed = false;
            for i, bar in ipairs(bars) do
                if not self.cached_players[bar.info.guid] or not self.cd_list[category][bar.info.id] or not self.cd_list[category][bar.info.id][bar.info.guid] then
                    bar:cdyStop();
                    tremove(bars, i);
                    barCache[category][bar.info.id][bar.info.guid] = nil;
                    if (bar.cdyStop) then
                        bar.Stop = bar.cdyStop;
                        bar.cdyStop = nil;
                    end
                    changed = true;
                    break;
                end
            end
        until not changed;
    end

    self:BuildList();
    self:GenerateReverseMappings();

    for category, _ in pairs(self.categories) do 
        local cd_list = self.cd_list[category];
        for spell_id, guid_tbl in pairs(cd_list) do
            for guid, _ in pairs(guid_tbl) do
                self:CreateBar(spell_id, guid);
            end
        end
    end
    
    self:PositionAllBars();
end

function RCD:GenerateReverseMappings()
    wipe(self.reverse_mappings);
    for category, spells in pairs(self.categories) do
        for spell_id, _ in pairs(spells) do
            self.reverse_mappings[spell_id] = category;
        end
    end
end

function RCD:CreateBar(spell_id, guid)
    local category = self.reverse_mappings[spell_id];

    if (barCache[category] and barCache[category][spell_id] and barCache[category][spell_id][guid]) then
        return barCache[category][spell_id][guid];
    end
    local bar = CandyBar:New(LSM:Fetch('statusbar', self.db.texture), self.db.width, self.db.height);
    
    bar:SetParent(self:GetHolder(category));
    local class = self.cached_players[guid].unitInfo.class;
    local classColor = class == "PRIEST" and E.PriestColors or RAID_CLASS_COLORS[class];
    bar:SetColor(classColor.r, classColor.g, classColor.b);
    bar.candyBarDuration:FontTemplate(LSM:Fetch('font', self.db.font), self.db.fontSize, "THINOUTLINE");
    bar.candyBarLabel:FontTemplate(LSM:Fetch('font', self.db.font), self.db.fontSize, "THINOUTLINE");
    bar:SetLabel(UnitName(self.cached_players[guid].unit));
    local duration = self.cd_list[category][spell_id][guid];
    bar:SetDuration(duration);
    local icon = GetSpellTexture(spell_id);
    bar:SetIcon(icon);
    bar.info = { id = spell_id, guid = guid, duration = duration, raidBattleRes = self.categories[category][spell_id].raid_battle_res };
    bar:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED");
    bar:SetScript("OnEvent", function(self, event, ...)
        local unit, _, spell_id = ...;
        if (UnitGUID(unit) == self.info.guid and spell_id == self.info.id) then
            if (IsInRaid() and self.info.raidBattleRes) then
                local currentCharges, maxCharges, cooldownStart, cooldownDuration = GetSpellCharges(spell_id);
                if (currentCharges == 0) then
                    self:SetDuration(cooldownDuration);
                    self:Start();
                    self.loopProtect = false;
                    return;
                else
                    self.candyBarDuration:SetText(("%d RDY"):format(currentCharges));
                end
            end
            self:Start();
            self.loopProtect = nil;
        end
    end);
    if (not bar.cdyStop) then
        bar.cdyStop = bar.Stop;
    end
    bar.Stop = function(self)
        self.updater:Stop();
        self.candyBarDuration:SetText("READY");
        if (not self.loopProtect) then
            self.loopProtect = true;
            self.remaining = self.info.duration;
            self:Start();
            self:Stop();
        end
    end
    bar:Start();
    bar.loopProtect = true;
    bar:Stop();
    bar.loopProtect = false;
    bar.candyBarDuration:SetText("READY");

    bar:SetScript("OnEnter", function(self)
        GameTooltip:SetOwner(self, "ANCHOR_RIGHT");
        GameTooltip:SetSpellByID(spell_id);
        GameTooltip:Show();
    end);

    barCache[category] = barCache[category] or {};
    barCache[category][spell_id] = barCache[category][spell_id] or {};
    barCache[category][spell_id][guid] = bar;
    tinsert(self.bars[category], bar);
    return bar; 
end

function RCD:CheckRaidBattleRes()
    if (not IsInRaid()) then
        return;
    end
    for _, bar in ipairs(self.bars["battleRes"]) do
        if bar.info.raidBattleRes then
            local currentCharges = GetSpellCharges(bar.info.id);
            bar.candyBarDuration:SetText(("%d RDY"):format(currentCharges));
        end
    end
end

function RCD:UpdateMedia()
    for category, _ in pairs(self.categories) do
        for _, bar in ipairs(self.bars[category]) do
            bar:SetTexture(self.db.texture);
            bar.candyBarLabel:FontTemplate(LSM:Fetch('font', self.db.font), self.db.fontSize, "THINOUTLINE");
            bar.candyBarDuration:FontTemplate(LSM:Fetch('font', self.db.font), self.db.fontSize, "THINOUTLINE");
        end
    end
end

function RCD:GetHolder(category)
    return self.holders[category];
end

function RCD:CreateHolder(category, defaultPoint)
    local holder = CreateFrame("Frame", "ChaoticUIRaidCDs_"..category, E.UIParent);
    holder:SetSize(self.db.width, self.db.height);
    holder:SetTemplate();

    local fs = holder:CreateFontString(nil, "OVERLAY");
    fs:FontTemplate(LSM:Fetch('font', self.db.font), self.db.fontSize, "THINOUTLINE");
    fs:SetTextColor(1, 1, 1);
    fs:SetText(self.category_labels[category]);
    fs:Point("CENTER");

    holder:SetPoint(unpack(defaultPoint));

    local container = CreateFrame("Frame", holder:GetName().."Container", holder);
    container:SetPoint("TOPLEFT", holder, "TOPLEFT");
    container:SetPoint("BOTTOMRIGHT", holder, "BOTTOMRIGHT");
    holder.Container = container;

    E:CreateMover(holder, self.category_labels[category]..'Mover', self.category_labels[category], nil, nil, nil, 'ALL,RAID');

    self.holders[category] = holder;
end

function RCD:PositionAllBars()
    for category, _ in pairs(self.categories) do
        self:PositionBars(category);
    end
end

function RCD:PositionBars(category)
    if (not self.bars[category] or #self.bars[category] == 0) then
        self.holders[category]:SetAlpha(0);
        return
    end

    self.holders[category]:SetAlpha(1);
    local function SortBars(barA, barB)
        if barA.info.spell_id ~= barB.info.spell_id then
            return barA.info.spell_id < barB.info.spell_id;
        end

        local server_idA, player_idA = select(2,strsplit("-", barA.info.guid));
        local server_idB, player_idB = select(2,strsplit("-", barB.info.guid));
        if (player_idA ~= player_idB) then
            return player_idA < player_idB;
        end

        return server_idA < server_idB;
    end

    table.sort(self.bars[category], SortBars);

    for _, bar in ipairs(self.bars[category]) do
        bar:ClearAllPoints();
    end

    for i, bar in ipairs(self.bars[category]) do
        if i == 1 then
            bar:SetPoint("TOP", self:GetHolder(category), "BOTTOM", 0, -2);
        else
            bar:SetPoint("TOP", self.bars[category][i - 1], "BOTTOM", 0, -2);
        end
    end

    self.holders[category].Container:SetPoint("BOTTOMRIGHT", self.bars[category][#self.bars[category]], "BOTTOMRIGHT");
end

function RCD:Hide()
    for category, holder in pairs(self.holders) do
        holder:Hide();
    end
end

function RCD:Show()
    if (self.db.onlyInCombat and not UnitAffectingCombat("player") and not UnitAffectingCombat("pet")) then
        self:Hide();
        return;
    end
    for _, holder in pairs(self.holders) do
        holder:Show();
    end
    self:UpdateCDs();
end

function RCD:CheckCombatState()
    local inCombat = UnitAffectingCombat("player") or UnitAffectingCombat("pet");
    local show = (not self.db.onlyInCombat or inCombat) and self:ShouldShow();
    if (show) then
        self:Show();
    else
        self:Hide();
    end
end

function RCD:SetShown(show)
    if (show) then
        self:Show();
    else
        self:Hide();
    end
end

function RCD:PLAYER_REGEN_ENABLED()
    if (self.db.onlyInCombat) then
        self:Hide();
    end

   if (IsInRaid()) then
       for i = 1, GetNumGroupMembers() do
            if (not self.cached_players[UnitGUID("raid"..i)]) then
                GI:Rescan();
                break;
            end
        end
   elseif (IsInGroup) then
       for i = 2, 5 do
            if (not UnitExists("party"..i)) then
                break;
            end
            if (not self.cached_players[UnitGUID("party"..i)]) then
                GI:Rescan();
                break;
            end
        end
   end
end

function RCD:PLAYER_REGEN_DISABLED()
    if (self.db.onlyInCombat and self:ShouldShow()) then
        self:Show();
    end
    self:CheckRaidBattleRes();
end

function RCD:ShouldShow()
    local inRaid = IsInRaid();
    local inParty = IsInGroup() and not IsInRaid();

    if (not inRaid and not inParty) then
        return self.db.solo
    end

    if (not inRaid) then
        return self.db.inParty
    end

    return self.db.inRaid
end

function RCD:GROUP_ROSTER_UPDATE()
    GI:Rescan();
    self:UpdateCDs();
    self:SetShown(self:ShouldShow());
end

function RCD:PLAYER_ENTERING_WORLD()
    GI:UpdateCommScope();
end

function RCD:UpdateEnableState()
    if (not self.db.enabled) then
        self:Hide();
        self:UnregisterAllEvents();
    else
        self:RegisterEvent("GROUP_ROSTER_UPDATE");
        self:RegisterEvent("PLAYER_REGEN_DISABLED");
        self:RegisterEvent("PLAYER_REGEN_ENABLED");
        self:RegisterEvent("PLAYER_ENTERING_WORLD");
        self:RegisterEvent("UNIT_PET", "UpdateCDs");
        self:GROUP_ROSTER_UPDATE();
    end
end

RCD.holders = {};
RCD.cached_players = {};
RCD.bars = {};
RCD.cd_list = {};
RCD.reverse_mappings = {};


function RCD:Initialize()
    CUI:RegisterDB(self, "raidCDs");

    local defaultPoint = {
        ["aoeCC"] = { "TOPLEFT", E.UIParent, "TOPLEFT", 0, -100 },
        ["externals"] = { "TOP", "ChaoticUIRaidCDs_aoeCCContainer", "BOTTOM", 0, -20 },
        ["raidCDs"] = { "TOP", "ChaoticUIRaidCDs_externalsContainer", "BOTTOM", 0, -20 },
        ["utilityCDs"] = { "TOP", "ChaoticUIRaidCDs_raidCDsContainer", "BOTTOM", 0, -20 },
        ["immunities"] = { "TOPLEFT", "ChaoticUIRaidCDs_aoeCCContainer", "TOPRIGHT", 60, 0 },
        ["interrupts"] = { "TOP", "ChaoticUIRaidCDs_immunitiesContainer", "BOTTOM", 0, -20 },
        ["battleRes"] = { "TOP", "ChaoticUIRaidCDs_interruptsContainer", "BOTTOM", 0, -20 },
    }

    local function CH(category)
        self:CreateHolder(category, defaultPoint[category]);
        self.bars[category] = {};
    end

    CH("aoeCC");
    CH("externals");
    CH("raidCDs");
    CH("utilityCDs");
    CH("immunities");
    CH("interrupts");
    CH("battleRes");

    GI:Rescan();
    
    self.initialized = true;
    
    self:UpdateEnableState();
end

CUI:RegisterModule(RCD:GetName());