local CUI, E, L, V, P, G = unpack(select(2, ...));
local RCD = CUI:GetModule("RaidCDs");

RCD.categories = {};

RCD.category_labels = {
    ["aoeCC"] = "AoE CC",
    ["externals"] = "Externals",
    ["raidCDs"] = "Raid CDs",
    ["utilityCDs"] = "Utility CDs",
    ["immunities"] = "Immunities",
    ["interrupts"] = "Interrupts",
    ["battleRes"] = "Battle Res",
}

RCD.categories.aoeCC = {
    [20549] = { -- War Stomp
        race = "TAUREN",
        cd = 90,
    },
    [255654] = { -- Bull Rush
        race = "HIGHMOUNTAINTAUREN",
        cd = 120,
    },
    [108199] = { -- Gorefiend's Grasp
        class = "DEATHKNIGHT",
        spec = 1,
        cd = { base = 120,
               talent = {
                   id = 206970,
                   modifier = 30,
               }
            },
    },
    [179057] = { -- Chaos Nova
        class = "DEMONHUNTER",
        spec = 1,
        cd = { base = 60,
               talent = {
                   id = 206477,
                   modifier = 20,
               }
        },
    },
    [202138] = { -- Sigil of Chains
        class = "DEMONHUNTER",
        spec = 2,
        is_talent = true,
        cd = { base = 90,
               pvp_talent = {
                   id = 211489,
                   modifierPct = 25,
               },
             }    
    },
    [102793] = { -- Ursol's Vortex
        class = "DRUID",
        spec = 4,
        cd = 60,
    },
    [132469] = { -- Typhoon
        class = "DRUID",
        cd = 30,
        is_talent = true,
    },
    [109248] = { -- Binding Shot
        class = "HUNTER",
        cd = 45,
        is_talent = true,
    },
    [119381] = { -- Leg Sweep
        class = "MONK",
        cd = { base = 60,
               talent = {
                   id = 264348,
                   modifier = 10,
               },
            },
    },
    [116844] = { -- Ring of Peace
        class = "MONK",
        cd = 45,
        is_talent = true,
    },
    [205369] = { -- Mind Bomb
        class = "PRIEST",
        spec = 3,
        cd = 30,
        is_talent = true,    
    },
    [204263] = { -- Shining Force
        class = "PRIEST",
        spec = { 1, 2 },
        cd = 45,
        is_talent = true,
    },
    [192058] = { -- Capacitor Totem
        class = "SHAMAN",
        cd = 60,    
    },
    [51490] = { -- Thunderstorm
        class = "SHAMAN",
        spec = 2,
        cd = { base = 45,
               pvp_talent = {
                   id = 204403,
                   modifier = 15,
               }
        }
    },
    [30283] = { -- Shadowfury
        class = "WARLOCK",
        cd = { base = 60,
                talent = {
                    id = 264874,
                    modifier = 15
                }
        }
    },
    [46968] = { -- Shockwave
        class = "WARRIOR",
        spec = 3,
        cd = 40,
    },
}

RCD.categories.externals = {
    [116849] = { -- Life Cocoon
        class = "MONK",
        spec = 2,
        cd = { base = 120,
               pvp_talent = {
                   id = 202424,
                   modifier = 30,
               }
            }
    }, 
    [6940] = { -- Blessing of Sacrifice
        class = "PALADIN",
        spec = { 1, 2 },
        cd = 120,
    },
    [1022] = { -- Blessing of Protection
        class = "PALADIN",
        cd = { base = 300,
            pvp_talent = {
                id = 216853,
                spec = 2,
                modifierPct = 33,
            },
        }
    },
    [204018] = { -- Blessing of Spellwarding
        class = "PALADIN",
        spec = 2,
        is_talent = true,
        cd = { base = 300,
            pvp_talent = {
                id = 216853,
                spec = 2,
                modifierPct = 33,
            },
        }
    },
    [633] = { -- Lay on Hands
        class = "PALADIN",
        cd = { base = 600,
               legendary = {
                   id = 137059,
                   slot = INVSLOT_HAND,
                   modifierPct = 70,
               },
               talent = {
                   id = 114154,
                   modifierPct = 30,
               }
             },
    },
    [47788] = { -- Guardian Spirit
        class = "PRIEST",
        spec = 2,
        cd = { base = 180,
               pvp_talent = {
                   id = 196602,
                   modifier = 60,
               }
        }
    },
    [33206] = { -- Pain Suppression
        class = 'PRIEST',
        spec = 1,
        cd = { base = 180,
                pvp_talent = {
                    id = 236771,
                    modifier = 120,
                }
            }
    },
}

RCD.categories.raidCDs = {
    [196718] = { -- Darkness
        class = "DEMONHUNTER",
        spec = 2,
        cd = 180,
    },
    [740] = { -- Tranquility
        class = "DRUID",
        spec = 4,
        cd = { 
            base = 180,
            talent = {
                id = 197073,
                modifier = 60,
            }
        }
    },
    [197721] = { -- Flourish
        class = "DRUID",
        spec = 4,
        cd = 90,
        is_talent = true,
    },
    [115310] = { -- Revival
        class = "MONK",
        spec = 2,
        cd = 180,
    },
    [204150] = { -- Aegis of Light
        class = "PALADIN",
        spec = 2,
        cd = 180,
        is_talent = true,
    },
    [31821] = { -- Aura Mastery
        class = "PALADIN",
        spec = 1,
        cd = 180,
    },
    [31884] = { -- Avenging Wrath
        class = "PALADIN",
        cd = 120,
    },
    [64843] = { -- Divine Hymn
        class = "PRIEST",
        spec = 2,
        cd = 180,
    },
    [200183] = { -- Apotheosis
        class = "PRIEST",
        spec = 2,
        cd = 120,
        is_talent = true,
    },
    [265202] = { -- Holy Word: Salvation
        class = "PRIEST",
        spec = 2,
        cd = 720,
    },
    [62618] = { -- Power Word: Barrier
        class = "PRIEST",
        spec = 1,
        cd = {
            base = 180,
            pvp_talent = {
                id = 197590,
                modifier = 60,
            },
        },
    },
    [47536] = { -- Rapture
        class = "PRIEST",
        spec = 1,
        cd = 90,
    },
    [15286] = { -- Vampiric Embrace
        class = "PRIEST",
        spec = 3,
        cd = 120,
    },
    [108281] = { -- Ancestral Guidance
        class = "SHAMAN",
        spec = 1,
        cd = 120,
        is_talent = true,
    },
    [108280] = { -- Healing Tide Totem
        class = "SHAMAN",
        spec = 3,
        cd = 180,
    },
    [98008] = { -- Spirit Link Totem
        class = "SHAMAN",
        spec = 3,
        cd = 180,
    },
    [114052] = { -- Ascendance
        class = "SHAMAN",
        spec = 3,
        cd = 180,
        is_talent = true,
    },
    [207399] = { -- Anscestral Protection Totem
        class = "SHAMAN",
        spec = 3,
        cd = 300,
        is_talent = true,
    },
    [97462] = { -- Rallying Cry
        class = "WARRIOR",
        cd = {
            base = 180,
            pvp_talent = {
                id = 235941,
                modifier = 120,
            },
        },
    },
}

RCD.categories.utilityCDs = {
    [29166] = { -- Innervate
        class = "DRUID",
        spec = { 1, 4 },
        cd = 180,
    },
    [205636] = { -- Force of Nature
        class = "DRUID",
        spec = 1,
        cd = 60,
    },
    [106898] = { -- Stampeding Roar
        class = "DRUID",
        spec = { 2, 3 },
        cd = {
            base = 120,
            pvp_talent = {
                id = 236148,
                modifier = 60,
            },
        },
    },
    [34477] = { -- Misdirect
        class = "HUNTER",
        cd = 30,
    },
    [1022] = { -- Blessing of Protection
        class = "PALADIN",
        cd = 300,
    },
    [73325] = { -- Leap of Faith
        class = "PRIEST",
        cd = {
            base = 90,
            pvp_talent = {
                id = 196611,
                modifier = 45,
            },
        },
    },
    [64901] = { -- Symbol of Hope
        class = "PRIEST",
        spec = 2,
        cd = 300,
    },
    [57934] = { -- Tricks of the Trade
        class = "ROGUE",
        cd = 30,
    },
    [114018] = { -- Shroud of Concealment
        class = "ROGUE",
        cd = 360,
    },
    [192077] = { -- Wind Rush Totem
        class = "SHAMAN",
        cd = 120,
        is_talent = true,
    },
}

RCD.categories.immunities = {
    [196555] = { -- Netherwalk
        class = "DEMONHUNTER",
        spec = 1,
        cd = 120,
        is_talent = true,
    },
    [186265] = { -- Aspect of the Turtle
        class = "HUNTER",
        cd = {
            base = 180,
            talent = {
                id = 266921,
                modifierPct = 20,
            },
        },
    },
    [45438] = { -- Ice Block
        class = "MAGE",
        cd = 240,
    },
    [642] = { -- Divine Shield
        class = "PALADIN",
        cd = {
            base = 300,
            talent = {
                id = 114154,
                modifierPct = 30,
            },
        },
    },
    [31224] = { -- Cloak of Shadows
        class = "ROGUE",
        cd = 120,
    },
}

RCD.categories.interrupts = {
    [47528] = { -- Mind Freeze
        class = "DEATHKNIGHT",
        cd = 15,
    },
    [183752] = { -- Disrupt
        class = "DEMONHUNTER",
        cd = 15,
    },
    [202137] = { -- Sigil of Silence
        class = "DEMONHUNTER",
        spec = 2,
        cd = {
            base = 60,
            talent = {
                id = 209281,
                modifierPct = 20,
            },
            pvp_talent = {
                id = 211489,
                modifierPct = 25,
            },
        },
    },
    [78675] = { -- Solar Beam
        class = "DRUID",
        spec = 1,
        cd = {
            base = 60,
            pvp_talent = {
                id = 200928,
                modifier = 20,
            },
        },
    },
    [106839] = { -- Skull Bash
        class = "DRUID",
        spec = { 2, 3 },
        cd = 15,
    },
    [147362] = { -- Counter Shot
        class = "HUNTER",
        spec = { 1, 2 },
        cd = 24,
    },
    [187707] = { -- Muzzle
        class = "HUNTER",
        spec = 3,
        cd = 15,
    },
    [2139] = { -- Counterspell
        class = "MAGE",
        cd = 24,
    },
    [116705] = { -- Spear Hand Strike
        class = "MONK",
        spec = { 1, 3 },
        cd = 15,
    },
    [96231] = { -- Rebuke
        class = "PALADIN",
        spec = { 2, 3 },
        cd = 15,
    },
    [15487] = { -- Silence
        class = "PRIEST",
        spec = 3,
        cd = {
            base = 45,
            talent = {
                id = 263716,
                modifier = 15,
            },
        },
    },
    [1766] = { -- Kick
        class = "ROGUE",
        cd = 15,
    },
    [57994] = { -- Wind Shear
        class = "SHAMAN",
        cd = 12,
    },
    [19647] = { -- Spell Lock
        class = "WARLOCK",
        cd = 24,
        required_pet = 417, 
    },
    [6552] = { -- Pummel
        class = "WARRIOR",
        cd = 15,
    },
}

RCD.categories.battleRes = {
    [61999] = { -- Raise Ally
        class = "DEATHKNIGHT",
        cd = 600,
        raid_battle_res = true,
    },
    [20484] = { -- Rebirth
        class = "DRUID",
        cd = 600,
        raid_battle_res = true,
    },
    [20608] = { -- Reincarnation
        class = "SHAMAN",
        cd = 1800,
    },
    [20707] = { -- Soulstone
        class = "WARLOCK",
        cd = 600,
        raid_battle_res = true,
    },
}

function RCD:GetSpellsForCategory(category, guid)
    if (not self.cached_players[guid]) then return {} end
    local unitInfo = self.cached_players[guid].unitInfo;
    local spells = {};
    for k, tbl in pairs(self.categories[category]) do
        local failed = false;
        if (tbl.class) then
            if (tbl.class ~= unitInfo.class) then
                failed = true;
            end
            if (tbl.class == "WARLOCK" and tbl.required_pet) then
                failed = true;
                if (UnitExists(self.cached_players[guid].unit.."pet")) then
                    local petGUID = UnitGUID(self.cached_players[guid].unit.."pet");
                    local petID = select(6, strsplit("-", petGUID));
                    failed = tbl.required_pet ~= tonumber(petID);
                end
            end
        end
        if (not failed and tbl.race) then
            if (tbl.race ~= unitInfo.race) then
                failed = true;
            end
        end
        if (not failed and tbl.spec) then
            if (type(tbl.spec) == "table") then
                failed = not tContains(tbl.spec, unitInfo.spec_index);
            else
                failed = tbl.spec ~= unitInfo.spec_index;
            end
        end
        if (not failed and tbl.is_talent) then
            failed = unitInfo.talents[k] ~= nil;
        end
        if (not failed and tbl.is_pvp_talent) then
            failed = true;
            for _, talentID in pairs(unitInfo.pvpTalents) do
                if (talentID == k) then
                    failed = false;
                    break;
                end
            end
        end
        if (not failed) then
            spells[k] = CopyTable(tbl);
            spells[k].guid = guid;
        end
    end
    return spells;
end