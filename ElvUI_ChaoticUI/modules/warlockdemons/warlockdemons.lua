if (not IsAddOnLoaded("zPets")) then return end

local CUI, E, L, V, P, G = unpack(select(2, ...));

local WD = CUI:NewModule('WarlockDemons', 'AceEvent-3.0')
local CandyBar = LibStub('LibCandyBar-3.0');
local LSM = LibStub('LibSharedMedia-3.0');
local CB = CUI:GetModule('CooldownBar');

local zPets = zPets;

function WD:CreateHeader()
	local header = CreateFrame("Frame", nil, E.UIParent);
	header:Size(self.db.width, self.db.height);
	header:SetTemplate();

	local fs = header:CreateFontString(nil, "ARTWORK");
	fs:FontTemplate(LSM:Fetch("font", self.db.font), self.db.fontSize, "THINOUTLINE");
	fs:SetAllPoints();

	fs:SetText(L["Demon Tracking"]);
	header.fs = fs;

	header:Point('TOPLEFT', E.UIParent, 'TOPLEFT', 0, -80);
	E:CreateMover(header, 'WarlockDemonsMover', L['Demon Tracker'], nil, nil, nil, 'ALL,SOLO');

	return header;
end

function WD:CreateBar(icon, duration, guid)
	local bar = CandyBar:New(LSM:Fetch('statusbar', self.db.texture), self.db.width, self.db.height);
	bar:SetParent(self.header);
	bar:SetIcon(icon);
	bar:SetDuration(duration + 15);
	bar.remaining = duration;
	bar.max_duration = duration + 15; -- Demonic Tyrant
	bar:SetColor(self.db.color.r, self.db.color.g, self.db.color.b, self.db.alpha);
    bar.candyBarDuration:FontTemplate(LSM:Fetch('font', self.db.font), self.db.fontSize, "THINOUTLINE");
    bar.candyBarLabel:FontTemplate(LSM:Fetch('font', self.db.font), self.db.fontSize, "THINOUTLINE");
	bar.updater:SetScript('OnStop', function() WD:RemoveBar(bar); WD:UpdateBars() end)
	bar.guid = guid;
	return bar;
end

function WD:RemoveBar(bar)
	local toRemove;
	for k, b in pairs(self.activeBars) do
		if b.petGUID == bar.petGUID then
			toRemove = k;
			break;
		end
	end
	tremove(self.activeBars, toRemove);
end

function WD:UpdateBars(isDemonicTyrant)
	local growingDown = self.db.grow == 'DOWN';
	local point, relativePoint, yOffset;
	if (growingDown) then
		point = "TOPLEFT";
		relativePoint = "BOTTOMLEFT";
		yOffset = -self.db.spacing;
	else
		point = "BOTTOMLEFT";
		relativePoint = "TOPLEFT";
		yOffset = self.db.spacing;
	end

	if isDemonicTyrant then
		for _, b in ipairs(self.activeBars) do
			if zPets.GetPetName(b.petGUID) ~= "Demonic Tyrant" then
				b:SetDuration(b.remaining + 15);
			end
		end
	end

	table.sort(self.activeBars, function(a, b) if (zPets.GetPetName(a.petGUID) == zPets.GetPetName(b.petGUID)) then return a.remaining < b.remaining else return self.demons[zPets.GetPetName(a.petGUID)].priority < self.demons[zPets.GetPetName(b.petGUID)].priority end end);
	local total = 0;
	for i, b in ipairs(self.activeBars) do
		b:ClearAllPoints();
		if (i == 1) then
			b:Point(point, self.header, relativePoint, 0, yOffset);
		else
			b:Point(point, self.activeBars[i-1], relativePoint, 0, yOffset);
		end
		if (not b.running) then
			b:Start();
		end
	end

	if (#self.activeBars > 0) then
		self.header.fs:SetFormattedText(L["Total Demons: %d"], #self.activeBars);
	else
		self.header.fs:SetText(L["Demon Tracking"]);
	end
end

function WD:CheckEnabled()
	if (E.myclass == "WARLOCK" and GetSpecialization() == 2 and self.db.enabled) then
		self:RegisterEvent('PLAYER_REGEN_DISABLED');
		self:RegisterEvent('PLAYER_REGEN_ENABLED');
		self.enabled = true;
	else
		self:UnregisterEvent('PLAYER_REGEN_DISABLED');
		self:UnregisterEvent('PLAYER_REGEN_ENABLED');
		self.header:Hide();
		self.enabled = false;
	end
end

function WD:UpdateAll()
	self:CheckEnabled();
	if (self.enabled) then
		self.header:Size(self.db.width, self.db.height);
	    self.header.fs:FontTemplate(LSM:Fetch("font", self.db.font), self.db.fontSize, "THINOUTLINE");

		self.header.totalLabel:FontTemplate(LSM:Fetch("font", self.db.font), self.db.fontSize, "THINOUTLINE");
	end
end

function WD:ACTIVE_TALENT_GROUP_CHANGED()
    self:CheckEnabled();
end

function WD:PLAYER_REGEN_ENABLED()
	self.header:Hide();
	self.header.fs:SetText("Demon Count");
end

function WD:PLAYER_ENTERING_WORLD()
	if (not InCombatLockdown()) then
		self.header:Hide();
	end
end

function WD:PLAYER_REGEN_DISABLED()
	self.header:Show();
	self:UpdateBars();
end

function WD:Initialize()
	if E.myclass ~= "WARLOCK" then return end
	CUI:RegisterDB(self, "warlockdemons")
	self.activeBars = {};

	self.demons = {
		["Wild Imp"] = { icon = GetSpellTexture(205145), priority = 4, optionOrder = 2 },
        ["Demonic Tyrant"] = { icon = GetSpellTexture(265187), priority = 1, optionOrder = 1 },
		["Dreadstalker"] = { icon = GetSpellTexture(104316), priority = 5, optionOrder = 3 },
		["Felguard"] = { icon = GetSpellTexture(111898), priority = 6, optionOrder = 11 },
		["Bilescourge"] = {icon=GetSpellTexture(267992), priority = 9, optionOrder = 14 },
		["Vilefiend"] = {icon=GetSpellTexture(264119), priority = 10, optionOrder = 13 },
		["Prince Malchezaar"] = {icon=GetSpellTexture(267986), priority=2, optionOrder = 4},
		["Illidari Satyr"] = {icon=GetSpellTexture(267987), priority=7, optionOrder=15},
		["Vicious Hellhound"] = {icon=GetSpellTexture(267988), priority=8, optionOrder=16},
		["Eyes of Gul'dan"] = {icon=GetSpellTexture(267989), priority=11, optionOrder=17},
		["Void Terror"] = {icon=GetSpellTexture(267991),priority=12, optionOrder=18},
		["Bilescourge"] = {icon=GetSpellTexture(267992),priority=13, optionOrder=19},
		["Shivarra"] = {icon=GetSpellTexture(267994),priority=14, optionOrder=20},
		["Wrathguard"] = {icon=GetSpellTexture(267995),priority=15, optionOrder=21},
		["Darkhound"] = {icon=GetSpellTexture(267996),priority=16, optionOrder=22},
		["Ur'zul"] = {icon=GetSpellTexture(268001),priority=17, optionOrder=23},
	}
	self.header = self:CreateHeader();

	zPets.RegisterPetEvent("OnSpawn", function(petGUID, petTable) WD:OnSpawn(petGUID, petTable) end);
	zPets.RegisterPetEvent("OnDespawn", function(petGUID) WD:OnDespawn(petGUID) end);
	
	self:RegisterEvent('ACTIVE_TALENT_GROUP_CHANGED');
	self:RegisterEvent('PLAYER_ENTERING_WORLD');
	self:CheckEnabled();
end

function WD:OnSpawn(petGUID, petTable)
	local petName = zPets.GetPetName(petGUID);
	if (petName == UnitName("pet")) then
		return;
	end

	if not (self.demons[petName]) then
		print("Unknown demon ",petName);
		return
	end
	local demon_info = self.demons[zPets.GetPetName(petGUID)];

	local bar = self:CreateBar(demon_info.icon, select(2,zPets.GetPetDurationInfo(petGUID)));
	
	bar:SetLabel(petName);
	bar.petGUID = petGUID
	table.insert(self.activeBars, bar);
	self:UpdateBars(zPets.GetPetName(petGUID) == "Demonic Tyrant");
end

function WD:OnDespawn(petGUID)
	for i, b in ipairs(self.activeBars) do
		if b.petGUID == petGUID then
			b:Stop();
			self:RemoveBar(b);
		end
	end
	self:UpdateBars();
end

CUI:RegisterModule(WD:GetName());
