local CUI, E, L, V, P, G = unpack(select(2, ...));

local ENA = CUI:NewModule('EnhancedNameplateAuras', 'AceEvent-3.0');
local NP = E:GetModule('NamePlates');

function ENA:SetAura(aura, index, name, icon, count, duration, expirationTime, spellID)
	if aura and icon and spellID then
		local spell = E.global['nameplate']['spellList'][spellID]
		-- Icon
		aura.icon:SetTexture(icon)

		-- Size
		local width = 20
		local height = 20

		if spell and spell['width'] then
			width = spell['width']
		elseif E.global['nameplate']['spellListDefault']['width'] then
			width = E.global['nameplate']['spellListDefault']['width']
		end

		if spell and spell['height'] then
			height = spell['height']
		elseif E.global['nameplate']['spellListDefault']['height'] then
			height = E.global['nameplate']['spellListDefault']['height']
		end

		if width > height then
			local aspect = height / width
			aura.icon:SetTexCoord(0.07, 0.93, (0.5 - (aspect/2))+0.07, (0.5 + (aspect/2))-0.07)
		elseif height > width then
			local aspect = width / height
			aura.icon:SetTexCoord((0.5 - (aspect/2))+0.07, (0.5 + (aspect/2))-0.07, 0.07, 0.93)
		else
			aura.icon:SetTexCoord(0.07, 0.93, 0.07, 0.93)
		end

		aura:SetWidth(width)
		aura:SetHeight(height)

		-- Stacks
		local stackSize = 7

		if spell and spell['stackSize'] then
			stackSize = spell['stackSize']
		elseif E.global['nameplate']['spellListDefault']['stackSize'] then
			stackSize = E.global['nameplate']['spellListDefault']['stackSize']
		end

		aura.count:FontTemplate(nil, stackSize, 'OUTLINE')
		if count > 1 then
			aura.count:SetText(count)
		else
			aura.count:SetText("")
		end

		-- Text
		local textSize = 9

		if spell and spell['text'] then
			textSize = spell['textSize']
		elseif E.global['nameplate']['spellListDefault']['text'] then
			textSize = E.global['nameplate']['spellListDefault']['text']
		end

		if aura.cooldown.timer then -- check if the aura has a timer
			aura.cooldown.timer.text:FontTemplate(nil, textSize, 'OUTLINE')
			aura.cooldown.timer.text:Point('TOPLEFT', 1, 1) -- Maybe adjust the position, but looks cool.
		end

		ENA:SortAuras(aura:GetParent());
	end
end

function ENA:SortAuras(auras)
	local function sortAuras(iconA, iconB)
		if (iconA:IsShown() ~= iconB:IsShown()) then
			return iconA:IsShown();
		end

		local aWidth = iconA:GetWidth();
		local aHeight = iconA:GetHeight();

		local bWidth = iconB:GetWidth();
		local bHeight = iconB:GetHeight();

		local aCalc = (aWidth + aHeight) * (aWidth / aHeight);
		local bCalc = (bWidth + bHeight) * (bWidth / bHeight);

		return aCalc > bCalc;
	end
	table.sort(auras.icons, sortAuras);
	ENA:RepositionAuras(auras);
end

function ENA:UpdateAuraIcons(auras)

	local maxAuras = auras.db.numAuras
	local numCurrentAuras = #auras.icons

	if (not auras.auraCache) then
		auras.auraCache = {};
	end

	local width = 20
	local height = 20

	if E.global['nameplate']['spellListDefault']['width'] then
		width = E.global['nameplate']['spellListDefault']['width']
	end

	if E.global['nameplate']['spellListDefault']['height'] then
		height = E.global['nameplate']['spellListDefault']['height']
	end

	if numCurrentAuras > maxAuras then
		for i = auras.db.numAuras, #auras.icons do
			tinsert(auras.auraCache, auras.icons[i])
			auras.icons[i]:Hide()
			auras.icons[i] = nil
		end 
	end

	if (maxAuras > numCurrentAuras) then
		for i=1, maxAuras do
			auras.icons[i] = tremove(auras.auraCache)
			if (not auras.icons[i]) then
				auras.icons[i] =  NP:CreateAuraIcon(auras)
				auras.icons[i]:SetParent(auras)
				auras.icons[i]:Hide()
			end
			auras.icons[i]:ClearAllPoints()
			auras.icons[i]:SetHeight(height)
			auras.icons[i]:SetWidth(width);
		end
	end

	ENA:RepositionAuras(auras);
end

function ENA:ConstructElement_Auras(frame, maxAuras, size)
	local auras = CreateFrame("FRAME", nil, frame)

	auras:SetHeight(18) -- this really doesn't matter
	auras.side = side
	auras.icons = {}

	return auras
end

function ENA:RepositionAuras(auras)
	for i = 1, #auras.icons do
		if(auras.side == "LEFT") then
			if(i == 1) then
				auras.icons[i]:SetPoint("BOTTOMLEFT", auras, "BOTTOMLEFT")
			else
				auras.icons[i]:SetPoint("BOTTOMLEFT", auras.icons[i-1], "BOTTOMRIGHT", E.Border + E.Spacing*3, 0)
			end
		else
			if(i == 1) then
				auras.icons[i]:SetPoint("BOTTOMRIGHT", auras, "BOTTOMRIGHT")
			else
				auras.icons[i]:SetPoint("BOTTOMRIGHT", auras.icons[i-1], "BOTTOMLEFT", -(E.Border + E.Spacing*3), 0)
			end
		end
	end
end

function ENA:UpdateAuraSet(auras)
	self:UpdateHeight(auras);
	self:SortAuras(auras);
end

function ENA:UpdateHeight(auras)
	local height = 20;
	for i, icon in ipairs(auras.icons) do
		height = math.max(height, icon:GetHeight());
	end
	auras:SetHeight(height);
end

function ENA:UpdateElement_Auras(frame)
	ENA:UpdateAuraSet(frame.Debuffs);
	ENA:UpdateAuraSet(frame.Buffs);
end

function ENA:UpdateSpellList()
	local filters = E.global['nameplate']['spellList'];

	for key, value in pairs(filters) do
		if (not tonumber(key) or value.version ~= 2) then
			local spellID = select(7, GetSpellInfo(key));
			if (spellID) then
				filters[spellID] = value;
				filters[spellID].version = 2;
			end
			filters[key] = nil;
		end
	end
end

function ENA:PLAYER_ENTERING_WORLD()
	self:UpdateSpellList();
	self:UnregisterEvent('PLAYER_ENTERING_WORLD');
end

function ENA:Initialize()
	hooksecurefunc(NP, "SetAura", ENA.SetAura);
	hooksecurefunc(NP, "UpdateElement_Auras", ENA.UpdateElement_Auras);
	NP.UpdateAuraIcons = ENA.UpdateAuraIcons;
	NP.ConstructElement_Auras = ENA.ConstructElement_Auras;
	self:RegisterEvent('PLAYER_ENTERING_WORLD');
end

CUI:RegisterModule(ENA:GetName());