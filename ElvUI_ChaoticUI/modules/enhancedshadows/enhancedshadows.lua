local CUI, E, L, V, P, G = unpack(select(2, ...))
local ES = CUI:NewModule('EnhancedShadows', 'AceEvent-3.0')

local LSM = LibStub('LibSharedMedia-3.0')
local A = E:GetModule('Auras')
local EAB = E:GetModule('ExtraActionBars', true);
local NP = E:GetModule('NamePlates');
local ClassColor = RAID_CLASS_COLORS[E.myclass]
local Border, LastSize
local S = E:GetModule('Skins');

ES.shadows = {}

if (not IsAddOnLoaded("Blizzard_TalentUI")) then
	LoadAddOn("Blizzard_TalentUI");
end

-- Auras
A.UpdateAuraShadows = A.UpdateAura
function A:UpdateAura(button, index)
	self:UpdateAuraShadows(button, index)

	button:CreateShadow()
	ES:RegisterShadow(button.shadow)
end

function ES:ElvUIShadows()
	local Frames = {
		ElvUI_BottomPanel,
		ElvUI_TopPanel,
		ElvUI_ExperienceBar,
		ElvUI_ReputationBar,
		ElvUI_AzeriteBar,
		ElvUI_HonorBar,
		ElvUI_ConsolidatedBuffs,
		ElvUI_ContainerFrame,
		ElvUI_BankFrame,
		PlayerTalentFrame,
		CharacterFrame,
		WorldMapFrame,
		FriendsFrame,
		PVEFrame,
		QuestGuru,
		TradeFrame,
		AchievementFrame,
		ZoneAbilityFrame.SpellButton,

		LeftMiniPanel,
		RightMiniPanel,
		GameTooltip,
		--LeftChatDataPanel,
		--LeftChatToggleButton,
		--RightChatDataPanel,
		--RightChatToggleButton,
		ElvConfigToggle,
	}

	local BackdropFrames = {
		ElvUIBags,
		ElvUI_BarPet,
		ElvUI_StanceBar,
		ElvUI_TotemBar,
		LeftChatPanel,
		RightChatPanel,
		Minimap,
		FarmModeMap,
		GameTooltipStatusBar,
	}

	for _, frame in pairs(Frames) do
		if frame then
			frame:CreateShadow()
			ES:RegisterShadow(frame.shadow)
		end
	end
	for _, frame in pairs(BackdropFrames) do
		if frame.backdrop then
			frame.backdrop:CreateShadow()
			ES:RegisterShadow(frame.backdrop.shadow)
		end
	end
	for i = 1, 10 do
		if _G["ElvUI_Bar"..i] then
			if (_G["ElvUI_Bar"..i].backdrop) then
				_G["ElvUI_Bar"..i].backdrop:CreateShadow()
				ES:RegisterShadow(_G["ElvUI_Bar"..i].backdrop.shadow)
				for k = 1, 12 do
					_G["ElvUI_Bar"..i.."Button"..k].backdrop:CreateShadow()
					ES:RegisterShadow(_G["ElvUI_Bar"..i.."Button"..k].backdrop.shadow)
				end
				_G["ElvUI_Bar"..i].backdrop:HookScript('OnShow', function(self) for k = 1, 12 do _G[self:GetParent():GetName().."Button"..k].backdrop.shadow:Hide() end end)
				_G["ElvUI_Bar"..i].backdrop:HookScript('OnHide', function(self) for k = 1, 12 do _G[self:GetParent():GetName().."Button"..k].backdrop.shadow:Show() end end)
				if _G["ElvUI_Bar"..i].backdrop:IsShown() then
					_G["ElvUI_Bar"..i].backdrop:Hide()
					_G["ElvUI_Bar"..i].backdrop:Show()
				else
					_G["ElvUI_Bar"..i].backdrop:Show()
					_G["ElvUI_Bar"..i].backdrop:Hide()
				end
			end
		end
	end

	-- Stance Bar buttons
	for i = 1, NUM_STANCE_SLOTS do
		local stanceBtn = {_G["ElvUI_StanceBarButton"..i]}
		for _, button in pairs(stanceBtn) do
			if button then
				button:CreateShadow()
				ES:RegisterShadow(button.shadow)
			end
		end
	end
	_G["ElvUI_StanceBar"].backdrop:HookScript('OnShow', function(self) for i = 1, NUM_STANCE_SLOTS do _G[self:GetParent():GetName().."Button"..i].shadow:Hide() end end)
	_G["ElvUI_StanceBar"].backdrop:HookScript('OnHide', function(self) for i = 1, NUM_STANCE_SLOTS do _G[self:GetParent():GetName().."Button"..i].shadow:Show() end end)
	if _G["ElvUI_StanceBar"].backdrop:IsShown() then
		_G["ElvUI_StanceBar"].backdrop:Hide()
		_G["ElvUI_StanceBar"].backdrop:Show()
	else
		_G["ElvUI_StanceBar"].backdrop:Show()
		_G["ElvUI_StanceBar"].backdrop:Hide()
	end

	-- Unitframes (toDo Player ClassBars, Target ComboBar)
	local unitframes = {"Player", "Target", "TargetTarget", "Pet", "PetTarget", "Focus", "FocusTarget"}

	do
		for _, frame in pairs(unitframes) do 
			local self = _G["ElvUF_"..frame]
			local unit = string.lower(frame)
			local health = self.Health
			local power = self.Power
			local castbar = self.Castbar
			local portrait = self.Portrait

			health:CreateShadow()
			ES:RegisterShadow(health.shadow)
			if power then
				power:CreateShadow()
				ES:RegisterShadow(power.shadow)
			end
			if (unit == "player" or unit == "target" or unit == "focus") then
				if castbar then
					castbar:CreateShadow()
					castbar.ButtonIcon.bg:CreateShadow()
					ES:RegisterShadow(castbar.shadow)
					ES:RegisterShadow(castbar.ButtonIcon.bg.shadow)
				end
			end
			if (unit == "player" or unit == "target") then
				if portrait then
					portrait:CreateShadow()
					ES:RegisterShadow(portrait.shadow)
				end
			end
		end
	end

	if (LeftChatToggleButton) then
		LeftChatToggleButton:SetFrameStrata('BACKGROUND')
		LeftChatToggleButton:SetFrameLevel(LeftChatDataPanel:GetFrameLevel() - 1)
		RightChatToggleButton:SetFrameStrata('BACKGROUND')
		RightChatToggleButton:SetFrameLevel(RightChatDataPanel:GetFrameLevel() - 1)
	end

	if (IsAddOnLoaded("ElvUI_LocPlus")) then
		LeftCoordDtPanel:CreateShadow();
		RightCoordDtPanel:CreateShadow();
		LocationPlusPanel:CreateShadow();
		XCoordsPanel:CreateShadow();
		YCoordsPanel:CreateShadow();

		ES:RegisterShadow(LeftCoordDtPanel.shadow);
		ES:RegisterShadow(RightCoordDtPanel.shadow);
		ES:RegisterShadow(LocationPlusPanel.shadow);
		ES:RegisterShadow(XCoordsPanel.shadow);
		ES:RegisterShadow(YCoordsPanel.shadow);
	end
end

function ES:SkinAlerts()
	if (not E.private.skins.blizzard.alertframes) then return end
	local function AddShadow(f)
		f.backdrop:CreateShadow();
		ES:RegisterShadow(f.backdrop.shadow);
	end

	local function H(s)
		local function S()
			return function(f) AddShadow(f) end
		end

		hooksecurefunc(s, "setUpFunction", S());
	end

	local systems = {
		AchievementAlertSystem,
		CriteriaAlertSystem,

		-- Encounters
		DungeonCompletionAlertSystem,
		GuildChallengeAlertSystem,
		InvasionAlertSystem,
		ScenarioAlertSystem,
		WorldQuestCompleteAlertSystem,

		-- Garrisons
		GarrisonFollowerAlertSystem,
		-- GarrisonShipFollowerAlertSystem,
		-- GarrisonTalentAlertSystem,

		-- Loot
		LegendaryItemAlertSystem,
		LootAlertSystem,
		LootUpgradeAlertSystem,
		MoneyWonAlertSystem,
		StorePurchaseAlertSystem,
		-- Professions
		DigsiteCompleteAlertSystem,
		NewRecipeLearnedAlertSystem,
	}

	for _, system in pairs(systems) do
		if system then
			H(system)
		end
	end

	local alerts = {
		ScenarioLegionInvasionAlertFrame,
		BonusRollMoneyWonFrame,
		BonusRollLootWonFrame,
		GarrisonBuildingAlertFrame,
		GarrisonMissionAlertFrame,
		GarrisonShipMissionAlertFrame,
		GarrisonRandomMissionAlertFrame,
		WorldQuestCompleteAlertFrame,
		GarrisonFollowerAlertFrame,
		LegendaryItemAlertFrame,
	}
	
	for _, alert in pairs(alerts) do
		if (alert) then
			AddShadow(alert);
		end
	end
end

function ES:UpdateShadows()
	if (ES.updateTimerFrame) then
		local enabled = E.db.chaoticui.enhancedshadows.enabled;
		if (enabled ~= ES.updateTimerFrame.enabled) then
			ES.updateTimerFrame:SetScript("OnUpdate", function(self, elapsed) ES:TimerUpdate(self, elapsed) end);
		else
			ES.updateTimerFrame:SetScript("OnUpdate", nil);
		end

		ES.updateTimerFrame.enabled = enabled;
	end

	for frame, _ in pairs(ES.shadows) do
		ES:UpdateShadow(frame)
	end
end

function ES:RegisterShadow(shadow)
	if shadow.isRegistered then return end
	ES.shadows[shadow] = true
	shadow.isRegistered = true
end

function ES:UpdateShadow(shadow)
	if (shadow:GetParent():IsProtected() and InCombatLockdown() or UnitAffectingCombat("player") or UnitAffectingCombat("pet")) then
		CUI:RegenWait(ES.UpdateShadow, ES, shadow);
		return;
	end

	local ShadowColor = E.db.chaoticui.enhancedshadows.shadowcolor
	local r, g, b = ShadowColor['r'], ShadowColor['g'], ShadowColor['b']
	if E.db.chaoticui.enhancedshadows.classcolor then r, g, b = ClassColor['r'], ClassColor['g'], ClassColor['b'] end

	local size = E.db.chaoticui.enhancedshadows.size
	if size ~= LastSize then
		shadow:SetOutside(nil, size, size)
		shadow:SetBackdrop({
			edgeFile = Border, edgeSize = E:Scale(size > 3 and size or 3),
			insets = {left = E:Scale(5), right = E:Scale(5), top = E:Scale(5), bottom = E:Scale(5)},
		})
		LastSize = size
	end
	shadow:SetBackdropColor(r, g, b, 0)
	shadow:SetBackdropBorderColor(r, g, b, 0.9)
	shadow:SetShown(E.db.chaoticui.enhancedshadows.enabled);
end

function ES:TimerUpdate(frame, elapsed)
	local timeToUpdate = .5;
	if ((frame.time or 0) + elapsed > timeToUpdate) then
		ES:UpdateShadows();
		frame.time = 0;
	else
		frame.time = (frame.time or 0) + elapsed;
	end
end

function ES:Initialize()
	Border = LSM:Fetch('border', 'ElvUI GlowBorder')
	if (EAB) then
		hooksecurefunc(EAB, "CreateBars", function(self)
			ES:ElvUIShadows()
		end);
	end
	hooksecurefunc(NP, "StyleFrame", function(self, frame, useMainFrame)
		if (not frame.shadow) then
			frame:CreateShadow();
		end
		ES:RegisterShadow(frame.shadow);
	end);
	if (InFlight) then
		hooksecurefunc(InFlight, "StartTimer", function(self, slot)
			if (InFlightBar and not InFlightBar.shadow) then
				InFlightBar:CreateShadow();
				ES:RegisterShadow(InFlightBar.shadow)
			end
		end);
	end
	
	ES.updateTimerFrame = CreateFrame("Frame");

	ES:SkinAlerts();
	ES:UpdateShadows();
end

_G.EnhancedShadows = ES;

CUI:RegisterModule(ES:GetName())
