## Interface: 80000
## Title: |cff1784d1ElvUI |r|cfff02020ChaoticUI|r
## Author: Whiro
## Version: 6.05
## Notes: An external edit for ElvUI and ElvUI Shadow & Light
## RequiredDeps: ElvUI, ElvUI_SLE
## OptionalDeps: EnhancedPetBattleUI, ElvUI_BagFilter, ParagonReputation
## OptionalDeps: AddOnSkins, xCT+, xCT_Beta, Details, !KalielsTracker
## OptionalDeps: InFlight, ElvUIDev, ElvUI_CastBarOverlay, ElvUI_DTColors
## OptionalDeps: ElvUI_EverySecondCounts, ElvUI_LocPlus, ElvUI_VisualAuraTimers
## OptionalDeps: OzCooldowns, iFilger, ProjectAzilroka, Classic Quest Log
## OptionalDeps: zPets, AddOnSkins_PopupDisabler
## X-oUF: ElvUF

## X-Tukui-ProjectID: 4
## X-Tukui-ProjectFolders: ElvUI_ChaoticUI_Config, ElvUI_ChaoticUI

locales\load_locales.xml
core\load_core.xml
installer\load_installer.xml
libs\load_libs.xml
modules\load_modules.xml
