local E, L, V, P, G = unpack(ElvUI); --Inport: Engine, Locales, PrivateDB, ProfileDB, GlobalDB, Localize Underscore
local S = E:GetModule('Skins');

local max = max
local format = format
local floor = floor
local ipairs = ipairs

local GetTime = GetTime
local UnitExists = UnitExists
local CreateFrame = CreateFrame
local SetDesaturation = SetDesaturation
local UnitDetailedThreatSituation = UnitDetailedThreatSituation
local hooksecurefunc = hooksecurefunc

-- GLOBALS: Skada, LibStub, LeftChatToggleButton, GameTooltip, GameTooltip_SetDefaultAnchor

local color = {
	[0] = P.nameplates.threat.goodColor,				--not tanking at all
	[1] = P.nameplates.threat.goodTransition,			--not tanking but threat higher than tank
	[2] = P.nameplates.threat.badTransition,			--insecurely tanking
	[3] = P.nameplates.threat.badColor					--securely tanking
}

local targets = {
	'mouseover',
	'mouseovertarget',
	'target',
	'targettarget'
}

local tankColor = {
	[0]=3,
	[1]=2,
	[2]=1,
	[3]=0
}

local function prefix(x)
	if x >= 1e12 then	  
        return format('%it', x/1e12)
	elseif x >= 1e9 then  
        return format('%ig', x/1e9)
	elseif x >= 1e6 then  
        return format('%im', x/1e6)
	elseif x >= 1e3 then  
        return format('%ik', x/1e3)
	else				  
        return format('%i',  x/1e0)
    end 
end

local getTPS --localize this to get meter current time
local calledOnce = false

local function LoadSkin()
	local t = E.Threat
	local d = E.DataTexts
	t:UnregisterAllEvents()

	local oldOnEnter = LeftChatToggleButton:GetScript('OnEnter')
	LeftChatToggleButton:RegisterForClicks('AnyUp')
	LeftChatToggleButton:SetScript('OnEnter', function(...)
		oldOnEnter(...)
		GameTooltip:AddDoubleLine(L["Right Click:"], 'Toggle Threat Bar', 1, 1, 1)
		GameTooltip:Show()
	end)

	local oldOnClick = LeftChatToggleButton:GetScript('OnClick')
	LeftChatToggleButton:SetScript('OnClick', function(self, btn)
		if btn == 'RightButton' and t then
			if t.bar:IsShown() then t.bar:Hide()
			else t.bar:Show() end
			return
		end
		oldOnClick(self, btn)
	end)

	function t:Update()
		--if not t.bar:IsShown() then return end
		local h, _, s, p, v, c = E.media.hexvaluecolor
		for i, x in ipairs(targets) do
			if not s and UnitExists(x) then
				_, s, p, _, v = UnitDetailedThreatSituation('player', x)
			end
		end

		s = (E.role == 'Tank' and tankColor[s]) or s or 0
		p = p and p > 0 and (((E.role == 'Tank') and format('%i', 100-p)) or format('%i', p)) or 0
		c = E:RGBToHex(color[s].r, color[s].g, color[s].b)
		t.bar:SetStatusBarColor(color[s].r, color[s].g, color[s].b)
		t.bar.backdrop:SetBackdropColor(color[s].r*0.25, color[s].g*0.25, color[s].b*0.25)
		t.bar.tps.text:SetFormattedText('%s%s%s%s%s%i%s', 'TPS ', h, getTPS(v), '|r (', c, p, '%|r)')
		t.bar:SetValue(p)
	end

	function t:Show()
		if not t.bar:IsShown() then
			t.bar:Show()
		end
	end

	function t:Hide()
		if t.bar:IsShown() then
			t.bar:Hide()
		end
	end

	function t:ToggleEnable()
		if t.db.enable then
			t:RegisterEvent('PLAYER_REGEN_ENABLED', 'Hide')
			t:RegisterEvent('PLAYER_REGEN_DISABLED', 'Show')
			t:RegisterEvent('PLAYER_TARGET_CHANGED', 'Update')
			t:RegisterEvent('UNIT_THREAT_LIST_UPDATE', 'Update')
			t:RegisterEvent('UPDATE_MOUSEOVER_UNIT', 'Update')
		else
			t.bar:Hide()
			t:UnregisterAllEvents()
		end
	end

	local oldUpdatePosition = t.UpdatePosition
	function t:UpdatePosition(...)
		oldUpdatePosition(self, ...)
		local width = ((t.bar:GetWidth() / 3) - 4)
		local height = (t.bar:GetHeight() - 4)

		t.bar.hps:SetSize(width, height)
		t.bar.dps:SetSize(width, height)
		t.bar.tps:SetSize(width, height)

		t.bar.hps.text:FontTemplate(E.LSM:Fetch("font", d.db.font), t.db.textSize, 'OUTLINE')
		t.bar.dps.text:FontTemplate(E.LSM:Fetch("font", d.db.font), t.db.textSize, 'OUTLINE')
		t.bar.tps.text:FontTemplate(E.LSM:Fetch("font", d.db.font), t.db.textSize, 'OUTLINE')
	end

	t.bar.dps = CreateFrame('Button', 'Threat_DPS', t.bar)
	t.bar.dps:Point('RIGHT', t.bar, 'RIGHT', -4, 0)
	t.bar.dps.text = t.bar.dps:CreateFontString(nil, 'OVERLAY')
	t.bar.dps.text:SetJustifyH('CENTER')
	t.bar.dps.text:SetJustifyV('middle')
	t.bar.dps.text:SetAllPoints()
	t.bar.dps:EnableMouse(false)

	t.bar.hps = CreateFrame('Button', 'Threat_HPS', t.bar)
	t.bar.hps:Point('CENTER', t.bar, 'CENTER')
	t.bar.hps.text = t.bar.hps:CreateFontString(nil, 'OVERLAY')
	t.bar.hps.text:SetJustifyH('CENTER')
	t.bar.hps.text:SetJustifyV('middle')
	t.bar.hps.text:SetAllPoints()
	t.bar.hps:EnableMouse(false)

	t.bar.tps = CreateFrame('Button', 'Threat_TPS', t.bar)
	t.bar.tps:Point('LEFT', t.bar, 'LEFT', 4, 0)
	t.bar.tps.text = t.bar.tps:CreateFontString(nil, 'OVERLAY')
	t.bar.tps.text:SetJustifyH('CENTER')
	t.bar.tps.text:SetJustifyV('middle')
	t.bar.tps.text:SetAllPoints()
	t.bar.tps:EnableMouse(false)

	t:UpdatePosition()
	t:ToggleEnable()
end

local function HookTinyDPS()
	if calledOnce then return end
	calledOnce = true

	local z = CreateFrame('Frame', 'tdpsBorder', tdpsFrame)
	z:SetTemplate('Transparent', nil, true)
	z:SetBackdropColor(0, 0, 0, 0)
	z:SetInside(nil, 1, 1)
	E.FrameLocks['tdpsFrame'] = true

	local sec = 2
	hooksecurefunc('tdpsOnUpdate', function(self, elapsed)
		sec = sec + elapsed
		if not (sec > tdps.speed) or not tdpsInCombat then return end
		local x, p = E.media.hexvaluecolor, tdpsPlayer[E.myguid]
		local a, b, c, d, e, f, g, h
		if p then
			a, b, c = p.pet, p.fight[tdpsF].t, tdpsFight[tdpsF]
			d, e = p.fight[tdpsF]['d'], p.fight[tdpsF]['h']
			for i=1, #a do
				f = tdpsPet[ a[i] ].fight[tdpsF]
				d, e = d+f['d'], e+f['h']
				if f.t > b then b = f.t end
			end
			g, h = max(0, d/c['d']*100), max(0, e/c['h']*100)
			E.Threat.bar.hps.text:SetFormattedText('%s%s%i%s%s%i%s', 'HPS: ', x, max(0, e/b), '|r (', x, h, '%|r)')
			E.Threat.bar.dps.text:SetFormattedText('%s%s%i%s%s%i%s', 'DPS: ', x, max(0, d/b), '|r (', x, g, '%|r)')
		end
		sec = 0
	end)
	--[[FarmModeMap:HookScript('OnShow', function()
		if tdpsFrame:IsVisible() then
			tdps.hidePvP, tdps.hideSolo, tdps.hideIC, tdps.hideOOC = true, true, true, true
			tdpsFrame:Hide()
	end end)
	FarmModeMap:HookScript('OnHide', function()
		if not tdpsFrame:IsVisible() then
			tdps.hidePvP, tdps.hideSolo, tdps.hideIC, tdps.hideOOC = nil, nil, nil, nil
			tdpsFrame:Show()
			tdpsRefresh()
	end end)]]
end

local function HookSkada()
	if calledOnce then return end
	calledOnce = true

	local Skada = Skada
	local SkadaDisplayBar = Skada.displays['bar']
	local SkadaL = LibStub('AceLocale-3.0'):GetLocale('Skada', false)
	hooksecurefunc(Skada, 'SetTooltipPosition', function(self, tt, frame)
		GameTooltip:ClearAllPoints()
		GameTooltip_SetDefaultAnchor(tt, frame)
	end)

	hooksecurefunc(SkadaDisplayBar, 'AddDisplayOptions', function(self, win, options)
		options.baroptions.args.barspacing = nil
		options.titleoptions.args.texture = nil
		options.titleoptions.args.bordertexture = nil
		options.titleoptions.args.thickness = nil
		options.titleoptions.args.margin = nil
		options.titleoptions.args.color = nil
		options.windowoptions = nil
	end)

	--[[local lib = LibStub:GetLibrary("SpecializedLibBars-1.0");
	local barPrototype = lib.barPrototype;
	hooksecurefunc(barPrototype, 'UpdateOrientationLayout', function(self)
		local o, t = self.orientation, self.texture;
		t:ClearAllPoints()
		t:Point("TOPLEFT", self, "TOPLEFT", 0, -5)
		t:Point("BOTTOMLEFT", self, "BOTTOMLEFT")

		t = self.timerLabel
		t:ClearAllPoints()
		t:Point("RIGHT", self, "RIGHT", -6, 5)

		t = self.label
		t:ClearAllPoints()
		t:Point("LEFT", self, "LEFT", 6, 5)
		t:Point("RIGHT", self.timerLabel, "LEFT", 0, 0)

		--t = self.icon
		--t:ClearAllPoints();
		--t:Point("RIGHT", self, "LEFT", 0, 5)
	end)]] --half bar skin idea for BuG

	hooksecurefunc(SkadaDisplayBar, 'ApplySettings', function(self, win)
		local p, x = win.db, win.bargroup
		x:SetFrameLevel(5)
		x:SetBackdrop(nil)
		if p.enabletitle then
			x.button:SetTemplate('Transparent', true)
			if not E.PixelMode then
				x.button:ClearAllPoints()
				x.button:SetPoint('BOTTOMLEFT', x, 'TOPLEFT', 0, 3)
			end
		end
		if not x.backdrop then
			x:CreateBackdrop('Transparent', true)
			--x.backdrop:Point('BOTTOMRIGHT', x, 'BOTTOMRIGHT', 1, 0)
		end
		if not x.desaturated then
			x.desaturated = true
			for i, b in ipairs(x.buttons) do
				SetDesaturation(b:GetHighlightTexture(), true)
				SetDesaturation(b:GetNormalTexture(), true)
			end
		end
	end)

	hooksecurefunc(Skada, "OpenReportWindow", function(self)
		local f = self.ReportWindow and self.ReportWindow.frame
		if f and not f.skinned then
			f:StripTextures()
			f:SetTemplate("Transparent")
			S:HandleCloseButton(f:GetChildren())
			f.skinned = true
		end
	end)

	hooksecurefunc(SkadaDisplayBar, 'Update', function(self, win)
		local cur, dps, hps, dpc, hpc = Skada.current, 0, 0, 0, 0
		if cur then
			local my = Skada:find_player(cur, E.myguid)
			if my then
				local tt = Skada:PlayerActiveTime(cur, my)
				dps, dpc = max(0,my.damage/max(1,tt)), max(0,my.damage/cur.damage*100)
				hps, hpc = max(0,my.healing/max(1,tt)), max(0,my.healing/cur.healing*100)
			end
			E.Threat.bar.hps.text:SetFormattedText('%s%s%s%s%s%i%s', 'HPS: ', E.media.hexvaluecolor, prefix(hps), '|r (', E.media.hexvaluecolor, hpc, '%|r)')
			E.Threat.bar.dps.text:SetFormattedText('%s%s%s%s%s%i%s', 'DPS: ', E.media.hexvaluecolor, prefix(dps), '|r (', E.media.hexvaluecolor, dpc, '%|r)')
		end
	end)

	getTPS = function(x)
		local c = Skada.current
		if not x or not c then return 0 end
		return prefix(x/max(1,GetTime()-c.starttime))
	end

	local function getRaidHPS(x)
		local tt = x.time>0 and x.time
		local et = x.endtime or GetTime()
		return x.healing/max(1,tt or (et-x.starttime))
	end

	local HPS = Skada:GetModule(SkadaL["Healing"])
	function HPS:AddToTooltip(x, tt)
		GameTooltip:AddDoubleLine(SkadaL["HPS"], Skada:FormatNumber(getRaidHPS(x)), 1,1,1)
	end

	LoadSkin()
end

local function HookDetails()
	if calledOnce then return end
	calledOnce = true

	--[[local startTime = combat:GetStartTime()
	if not startTime or startTime == 0 then
		return --break when we have no data
	end

	local endTime = combat:GetEndTime()
	if endTime and (GetTime() + (Details.update_speed*10)) > endTime then
		return --break when we stop the data
	end]] -- optional delay instead of Details:IsInCombat()

	--[[if not instancia or type (instancia) == "boolean" then --> the first parameter was not an instance or ALL
		forcar = instancia
		instancia = self
	end
	if (instancia == -1) then
		--ALL
	elseif not instancia.ativa then
		--not active
		return
	end]] -- idk more delay shit

	local Details = Details
	local ToKFunctions = Details.ToKFunctions
	local afterTimes = 3 -- Times to check after combat ends
	local afterCount = 0 -- Keep track of updates after combat
	local effectiveTime = false -- Active / Effective Timing

	local function getGroupTotal(combat, atributo)
		local total, container = 0, combat:GetContainer(atributo)
		if not container then return total end --safe af
		for i, actor in container:ListActors() do
			if actor:IsGroupPlayer() then
				total = total + actor.total_without_pet
			elseif actor:IsPetOrGuardian() and actor.owner:IsGroupPlayer() then
				total = total + actor.total
			end
		end
		return floor(total)
	end

	hooksecurefunc(Details, "AtualizaGumpPrincipal", function(self, instancia, forcar)
		if not Details:IsInCombat() then
			if afterCount == afterTimes then
				return
			else
				afterCount = afterCount + 1
			end
		else
			afterCount = 0
		end

		local combat = Details:GetCombat()
		if combat then
			local dps, hps, dpc, hpc, combatTime = 0, 0, 0, 0
			--local prefix = ToKFunctions[Details.minimap.text_format]
			local damageActor = combat:GetActor(DETAILS_ATTRIBUTE_DAMAGE, Details.playername)
			local healingActor = combat:GetActor(DETAILS_ATTRIBUTE_HEAL, Details.playername)
			if effectiveTime then combatTime = combat:GetCombatTime() end

			if damageActor then
				--local allDamage = combat:GetTotal(DETAILS_ATTRIBUTE_DAMAGE)
				--local allDamageGroup = combat:GetTotal(DETAILS_ATTRIBUTE_DAMAGE, nil, true)
				local myDamage, damageTime = floor(damageActor.total), (effectiveTime and combatTime) or damageActor:Tempo()
				local simpyDamage = getGroupTotal(combat, DETAILS_ATTRIBUTE_DAMAGE)
				dps, dpc = myDamage/max(0.1,damageTime), myDamage/max(0.1,simpyDamage)*100
				--if damageTime and damageTime > 0 then dps = (myDamage/damageTime) end
				--if simpyDamage > 0 then dpc = (myDamage/simpyDamage)*100 end
				E.Threat.bar.dps.text:SetFormattedText('%s%s%s%s%s%i%s', 'DPS ', E.media.hexvaluecolor, prefix(dps), '|r (', E.media.hexvaluecolor, dpc, '%|r)')
			end

			if healingActor then
				--local allHealing = combat:GetTotal(DETAILS_ATTRIBUTE_HEAL)
				--local allHealingGroup = combat:GetTotal(DETAILS_ATTRIBUTE_HEAL, nil, true)
				local myHealing, healingTime = floor(healingActor.total), (effectiveTime and combatTime) or healingActor:Tempo()
				local simpyHealing = getGroupTotal(combat, DETAILS_ATTRIBUTE_HEAL)
				hps, hpc = myHealing/max(0.1,healingTime), myHealing/max(0.1,simpyHealing)*100
				--if healingTime and healingTime > 0 then dps = (myHealing/healingTime) end
				--if simpyHealing > 0 then dpc = (myHealing/simpyHealing)*100 end
				E.Threat.bar.hps.text:SetFormattedText('%s%s%s%s%s%i%s', 'HPS ', E.media.hexvaluecolor, prefix(hps), '|r (', E.media.hexvaluecolor, hpc, '%|r)')
			end
		end
	end)

	--[[hooksecurefunc(Details.atributo_damage, "AtualizaBarra", function(self, instancia, barras_container, qual_barra, lugar, total, sub_atributo, forcar, keyName, combat_time, percentage_type, use_animations, bars_show_data, bars_brackets, bars_separator)
		if self.nome == Details.playername then
			local porcentagem = format("%.1f", self[keyName] / total * 100)
			print("damage", porcentagem)
		end
	end)
	hooksecurefunc(Details.atributo_heal, "AtualizaBarra", function(self, instancia, barras_container, qual_barra, lugar, total, sub_atributo, forcar, keyName, combat_time, percentage_type, use_animations, bars_show_data, bars_brackets, bars_separator)
		if self.nome == Details.playername then
			local porcentagem = format("%.1f", self[keyName] / total * 100)
			print("heal", porcentagem)
		end
	end)]]

	getTPS = function(x)
		local combat = Details:GetCurrentCombat()
		local startTime = combat and combat:GetStartTime()
		if not x or not startTime then return 0 end
		return prefix(x/max(1,GetTime()-startTime))
	end

	LoadSkin()
end

S:AddCallbackForAddon('Details', 'Details', HookDetails)
S:AddCallbackForAddon('Skada', 'Skada', HookSkada)