![ElvUI_NenaUI logo](http://i.imgur.com/KemfwjC.png)

NenaUI is an external edit for ElvUI that provides a multitude of additional features.
  
NenaUI is developed to run on top of ElvUI Shadow & Light and requires it to be installed and enabled.

Major Highlights:
* Automatically Filling Utility Bars:
  * Portals/Teleports
  * Professions
  * Raid food/flasks
* Double click items in your bags or currencies to track them as you farm them.
* Nenachat:  A fully ElvUI integrated instant messaging environment.
* PartyXP:  A never-publicly-released before extension to elvui that will allow you to see your party members experience bars as long as they are also running partyxp.
* Tracker bars for hidden artifact skin achievements.
* A popup button to use artifact power tokens as you get them, configurable to only show for a specific spec.
* A logarithmic cooldown bar inspired by SexyCooldown directly integrated into ElvUI.
* The return of the Vertical Unit Frames and EnhancedNameplateAuras of yesteryear.
* For Demonology Warlocks:  A demon counter showing you your currently active demons, which ones are empowered or not, and how much damage your Thal'kiels Consumption will deal at any moment.

The following addons/plugins are directly integrated into this edit and should not be run separately:
* EnhancedPetBattleUI

[Screenshots](http://imgur.com/a/pYX6B)

The installer is my UI setup, so skip it if you dont want my specific setup.

Report any issues [here](https://git.tukui.org/Infinitron/ElvUI_NenaUI/issues).